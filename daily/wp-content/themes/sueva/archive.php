<?php 

	get_header();
	
	if ( (wip_setting('wip_category_layout') == "span4") || (wip_setting('wip_category_layout') == "span6") ) { 

		get_template_part('archive','masonry');

	} else {
		
		get_template_part('archive','blog');

	}
	
	get_footer(); 

?>