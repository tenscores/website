<?php

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

$wip_new_metaboxes = new wip_metaboxes ('page', array (

array( "name" => "Navigation",  
       "type" => "navigation",  
       "item" => array( "setting" => __( "Setting","wip") , "seo" => __( "SEO","wip") ),   
       "start" => "<ul>", 
       "end" => "</ul>"),  

array( "type" => "begintab",
	   "tab" => "setting",
	   "element" =>

		array( "name" => __( "Setting","wip"),
			   "type" => "title",
			  ),

		array( "name" => __( "Template","wip"),
			   "desc" => __( "Select a template for this page","wip"),
			   "id" => "wip_template",
			   "type" => "select",
			   "options" => array(
				   "full" => __( "Full Width","wip"),
				   "left-sidebar" =>  __( "Left Sidebar","wip"),
				   "right-sidebar" => __( "Right Sidebar","wip"),
			  ),
			  ),
			  
		array( "name" => __( "Sidebar","wip"),
			   "desc" => __( "Select side sidebar","wip"),
			   "id" => "wip_sidebar",
			   "type" => "select",
			   "options" => wip_sidebar_list('side'),
			),

),

array( "type" => "endtab"),

array( "type" => "begintab",
	   "tab" => "seo",
	   "element" =>
	  
		array( "name" => __( "Seo Options","wip"),
			   "type" => "title",
			  ),
		
		array( "name" => __( "Metatag Title","wip"),
			   "desc" => __( "Insert the metatag title of page, use a maximum of 60 chars","wip"),
			   "id" => "wip_seo_title",
			   "type" => "text",
			  ),
		
		array( "name" => __( "Metatag Description","wip"),
			   "desc" => __( "Insert the metatag description of page, use a maximum of 160 chars","wip"),
			   "id" => "wip_seo_description",
			   "type" => "textarea",
			  ),
		
		array( "name" => __( "Keywords","wip"),
			   "desc" => __( "Insert the keywords separated by comma","wip"),
			   "id" => "wip_seo_keywords",
			   "type" => "textarea",
			  ),

),

array( "type" => "endtab"),

array( "type" => "endtab")
)

);


?>