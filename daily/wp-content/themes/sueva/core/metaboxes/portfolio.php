<?php

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

$wip_new_metaboxes = new wip_metaboxes ('portfolio', array (

array( "name" => "Navigation",  
       "type" => "navigation",  
       "item" => array( "setting" => __( "Setting","wip") ),   
       "start" => "<ul>", 
       "end" => "</ul>"),  

array( "type" => "begintab",
	   "tab" => "setting",
	   "element" =>

		array( "name" => __( "Setting","wip"),
			   "type" => "title",
			  ),

		array( "name" => __( "Url","wip"),
			   "desc" => __( "Work's url.","wip"),
			   "id" => "wip_folio_url",
			   "type" => "text",
			  ),

		array( "name" => __( "Skills","wip"),
			   "desc" => __( "Insert the skills","wip"),
			   "id" => "wip_folio_skills",
			   "type" => "text",
			  ),

),

array( "type" => "endtab"),

array( "type" => "endtab")
)

);


?>