<?php

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

/*-----------------------------------------------------------------------------------*/
/* Analytics code */
/*-----------------------------------------------------------------------------------*/ 

function analytics() {

	if(wip_setting('wip_analytics_code'))
	echo stripslashes ( wip_setting('wip_analytics_code'));
}

add_action('wp_footer', 'analytics');


/*-----------------------------------------------------------------------------------*/
/* Socials */
/*-----------------------------------------------------------------------------------*/ 

function socials() {
	
	$socials = array ("facebook","twitter","flickr","google","linkedin","myspace","pinterest","tumblr","xing","youtube","vimeo","skype","email");
	
	foreach ( $socials as $social ) 
	
	{
		if (wip_setting('wip_footer_'.$social.'_button')): 
		if ($social == "email") $email = "mailto:"; else $email = "";
		if ($social == "skype") $skype = "skype:"; else $skype = "";
            echo '<a href="'.$email.$skype.wip_setting('wip_footer_'.$social.'_button').'" target="_blank" title="'.$social.'" class="social '.$social.'"> '.$social.'  </a> ';
		endif;
	}
	
	if (wip_setting('wip_footer_rss_button') == "on"): 
    	echo '<a href="'; bloginfo('rss2_url'); echo '" title="Rss" class="social rss"> Rss  </a> ';
	endif; 
}

add_action( 'wip_socials', 'socials', 10, 2 );


?>