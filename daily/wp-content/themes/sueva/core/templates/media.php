<?php

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

function wip_thumbnail($id) {
	if ( has_post_thumbnail() ) :
		echo '<div class="pin-container">';
			the_post_thumbnail($id);
		echo '</div>';
	endif; 
}

function wip_video() {
	
	global $post ;
	
	echo '<div class="pin-container video-thumb">';
		echo '<iframe src="'. wip_postmeta('wip_video_type') . wip_postmeta( 'wip_video_id' ) . '"></iframe>';
	echo '</div>';
	
}

?>