<?php 

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

/*-----------------------------------------------------------------------------------*/
/* Styles,scripts and fonts */
/*-----------------------------------------------------------------------------------*/ 

function wip_header() { 
	
	$fonts = array ( wip_setting("wip_logo_font"),wip_setting("wip_menu_font"),wip_setting("wip_logo_description_font"),wip_setting("wip_titles_font"),wip_setting("wip_content_font"));
	
	$fonts = array_unique($fonts); 
	
	echo "<link href='http://fonts.googleapis.com/css?family=Droid+Sans|Allura|Oswald";
	
	foreach ( $fonts as $font ) 
	
	{
		if ($font) 
		
		{ 
			echo "|".str_replace(" ","+",$font); 
		}

	}
	
	echo "&subset=latin,latin-ext' rel='stylesheet' type='text/css'>";
		
	wip_enqueue_style('/css');
	
	wp_enqueue_script( "jquery-ui-core", array('jquery'));
	wp_enqueue_script( "jquery-ui-tabs", array('jquery'));
	
	wip_enqueue_script('/js');

	if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
	
	wp_enqueue_script('jquery'); 
	wp_head(); 

}

add_action( 'wip_head', 'wip_header', 10, 2 );

/*-----------------------------------------------------------------------------------*/
/* Metatags */
/*-----------------------------------------------------------------------------------*/ 

function wip_meta() {
	
	global $post;
	
	echo "<title>";
	
		if (!wip_postmeta('wip_seo_title')):
			wp_title( '|', true, 'right' );
			echo get_bloginfo('name')." - ";
			echo get_bloginfo('description');
		else:
			echo wip_postmeta('wip_seo_title');
		endif;
	
	echo "</title>";
	
	if (wip_postmeta('wip_seo_description')):
		
		echo '<meta name="description" content="' . wip_postmeta('wip_seo_description') . '"/>';
		
		endif;

	if (wip_postmeta('wip_seo_keywords')):
		
		echo '<meta name="keywords" content="' . wip_postmeta('wip_seo_keywords') . '"/>';
	
	endif;
	
}

add_action( 'wip_metatags','wip_meta', 10, 2 );

?>