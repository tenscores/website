<?php 

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

function wip_post_info_function() { ?>

    <div class="line"> 
    
        <div class="entry-info">
       
            <span> <i class="fa fa-clock-o"></i>
				<?php echo get_the_date(); ?>
            </span>
            
            <?php if (wip_setting('wip_view_comments') == "on" ): ?>
            
                <span> <i class="fa fa-comments-o"></i>
                    <?php echo comments_number( '<a href="'.get_permalink($post->ID).'#respond">'.__( "0 comments","wip").'</a>', '<a href="'.get_permalink($post->ID).'#comments">1 '.__( "comment","wip").'</a>', '<a href="'.get_permalink($post->ID).'#comments">% '.__( "comments","wip").'</a>' ); ?>
                </span>
            
            <?php endif; ?>
            
            <?php echo wip_posticon(); ?>
            
            <span> <i class="fa fa-tags"></i> 
				<?php the_category(', '); ?> 
            </span>

        </div>

    </div>

<?php

} 

add_action( 'wip_post_info', 'wip_post_info_function', 10, 2 );

?>