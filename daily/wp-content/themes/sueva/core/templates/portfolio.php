<?php

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

function portfolio() {
	
	get_template_part( 'core/post-formats/portfolio' );

}

add_action( 'wip_portfolio','portfolio', 10, 2 );

?>