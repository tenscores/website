<?php

function fourcolumns($atts,  $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'icon' => '',
		'span' => 'true'
	), $atts));

	if ($icon) { $icon = '<i class="fa '.$icon.'"></i>'; } 

	if ($title) { $title = '<h3 class="title">'.$icon.$title.'</h3>'; }
	
	$content = "<div class='span3'>" . $title .  $content . "</div>" ;
	
	return do_shortcode($content);
}

add_shortcode('four_columns','fourcolumns');



?>
