<?php

function highlightcode($atts,  $content = null) {
	extract(shortcode_atts(array(
		'background' => '',
		'color' => '',

	), $atts));

	$content = '<span style="background:' . $background . '; color:'. $color .'">' . $content . '</span>' ;
	
	return $content;
}

add_shortcode('highlight','highlightcode');

?>
