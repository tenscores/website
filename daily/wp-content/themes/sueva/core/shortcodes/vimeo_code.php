<?php

function vimeo_code ($atts,  $content = null) {
	
	extract(shortcode_atts(array(
		'id_video' => '',
		'autoplay' => 'false',
	), $atts));
	
	$html =  '<div class="video-container"><div class="video-thumb"><iframe src="http://player.vimeo.com/video/' . $id_video . '?wmode=opaque&amp;autoplay=' . $autoplay . '"></iframe></div></div>';

	return $html;

}

add_shortcode('vimeo','vimeo_code');

?>
