<?php

function tabs_code ($atts,  $content = null) {
	
	extract(shortcode_atts(array(
		'title' => '',
		'id' => '',
	), $atts));
	
    $html = '<div id="'.$id.'"><p>'.$content.'</p></div>';
	
	return $html;

}

add_shortcode('tabs','tabs_code');

?>
