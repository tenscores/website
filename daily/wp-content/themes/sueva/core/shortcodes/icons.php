<?php

function icon_function($atts,  $content = null) {
	extract(shortcode_atts(array(
		'name' => '',
		'size' => '',
	), $atts));

	if ($size) { $iconsize = $size; } 

	$icon = '<i class="fa '.$name.' '.$iconsize.'"></i>'; 
	
	return do_shortcode($icon.$content);
}

add_shortcode('icon','icon_function');

?>
