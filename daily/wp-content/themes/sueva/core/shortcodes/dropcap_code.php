<?php

function dropcap_code ($atts,  $content = null) {
	
	$html =  '<p class="dropcap">' . $content . '<div class="clear"></div></p>';
	
	return $html;

}

add_shortcode('dropcap','dropcap_code');

?>
