<?php

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */


$skins = array( 

"Orange" => array(

    "wip_skins" => "Orange", 
	
    "wip_text_font_color" => "#616161", 
    "wip_copyright_font_color" => "#ffffff", 
    "wip_link_color" => "#ff6644", 
    "wip_link_color_hover" => "#d14a2b", 
    "wip_border_color" => "#ff6644", 

	"wip_body_background" => "/images/background/patterns/pattern12.jpg",
	"wip_body_background_repeat" => "repeat",
	"wip_body_background_color" => "#f3f3f3",
	
	"wip_footer_background" => "/images/background/patterns/pattern11.jpg",
	"wip_footer_background_repeat" => "repeat",
	"wip_footer_background_color" => "#f3f3f3",

	"home-default" => "default",

),

"Cyan" => array(

    "wip_skins" => "Cyan", 
	
    "wip_text_font_color" => "#616161", 
    "wip_copyright_font_color" => "#ffffff", 
    "wip_link_color" => "#48c2ae", 
    "wip_link_color_hover" => "#3aa694", 
    "wip_border_color" => "#48c2ae", 

	"wip_body_background" => "/images/background/patterns/pattern12.jpg",
	"wip_body_background_repeat" => "repeat",
	"wip_body_background_color" => "#f3f3f3",
	
	"wip_footer_background" => "/images/background/patterns/pattern11.jpg",
	"wip_footer_background_repeat" => "repeat",
	"wip_footer_background_color" => "#f3f3f3",

	"home-default" => "default",

),

"Blue" => array(

    "wip_skins" => "Blue", 
	
    "wip_text_font_color" => "#616161", 
    "wip_copyright_font_color" => "#ffffff", 
    "wip_link_color" => "#0090ff", 
    "wip_link_color_hover" => "#0074cc", 
    "wip_border_color" => "#0090ff", 

	"wip_body_background" => "/images/background/patterns/pattern12.jpg",
	"wip_body_background_repeat" => "repeat",
	"wip_body_background_color" => "#f3f3f3",
	
	"wip_footer_background" => "/images/background/patterns/pattern11.jpg",
	"wip_footer_background_repeat" => "repeat",
	"wip_footer_background_color" => "#f3f3f3",

	"home-default" => "default",

),

"Red" => array(

    "wip_skins" => "Red", 
	
    "wip_text_font_color" => "#616161", 
    "wip_copyright_font_color" => "#ffffff", 
    "wip_link_color" => "#b93333", 
    "wip_link_color_hover" => "#872424", 
    "wip_border_color" => "#b93333", 

	"wip_body_background" => "/images/background/patterns/pattern12.jpg",
	"wip_body_background_repeat" => "repeat",
	"wip_body_background_color" => "#f3f3f3",
	
	"wip_footer_background" => "/images/background/patterns/pattern11.jpg",
	"wip_footer_background_repeat" => "repeat",
	"wip_footer_background_color" => "#f3f3f3",

	"home-default" => "default",

),

"Pink" => array(

    "wip_skins" => "Pink", 
	
    "wip_text_font_color" => "#616161", 
    "wip_copyright_font_color" => "#ffffff", 
    "wip_link_color" => "#c71c77", 
    "wip_link_color_hover" => "#941559", 
    "wip_border_color" => "#c71c77", 

	"wip_body_background" => "/images/background/patterns/pattern12.jpg",
	"wip_body_background_repeat" => "repeat",
	"wip_body_background_color" => "#f3f3f3",
	
	"wip_footer_background" => "/images/background/patterns/pattern11.jpg",
	"wip_footer_background_repeat" => "repeat",
	"wip_footer_background_color" => "#f3f3f3",

	"home-default" => "default",

),

"Yellow" => array(

    "wip_skins" => "Yellow", 
	
    "wip_text_font_color" => "#616161", 
    "wip_copyright_font_color" => "#ffffff", 
    "wip_link_color" => "#f0b70c", 
    "wip_link_color_hover" => "#bd9009", 
    "wip_border_color" => "#f0b70c", 

	"wip_body_background" => "/images/background/patterns/pattern12.jpg",
	"wip_body_background_repeat" => "repeat",
	"wip_body_background_color" => "#f3f3f3",
	
	"wip_footer_background" => "/images/background/patterns/pattern11.jpg",
	"wip_footer_background_repeat" => "repeat",
	"wip_footer_background_color" => "#f3f3f3",

	"home-default" => "default",

),

"Green" => array(

    "wip_skins" => "Green", 
	
    "wip_text_font_color" => "#616161", 
    "wip_copyright_font_color" => "#ffffff", 
    "wip_link_color" => "#84ad37", 
    "wip_link_color_hover" => "#5d7a27", 
    "wip_border_color" => "#84ad37", 

	"wip_body_background" => "/images/background/patterns/pattern12.jpg",
	"wip_body_background_repeat" => "repeat",
	"wip_body_background_color" => "#f3f3f3",
	
	"wip_footer_background" => "/images/background/patterns/pattern11.jpg",
	"wip_footer_background_repeat" => "repeat",
	"wip_footer_background_color" => "#f3f3f3",

	"home-default" => "default",

),

"Pink" => array(

    "wip_skins" => "Pink", 
	
    "wip_text_font_color" => "#616161", 
    "wip_copyright_font_color" => "#ffffff", 
    "wip_link_color" => "#c71c77", 
    "wip_link_color_hover" => "#941559", 
    "wip_border_color" => "#c71c77", 

	"wip_body_background" => "/images/background/patterns/pattern12.jpg",
	"wip_body_background_repeat" => "repeat",
	"wip_body_background_color" => "#f3f3f3",
	
	"wip_footer_background" => "/images/background/patterns/pattern11.jpg",
	"wip_footer_background_repeat" => "repeat",
	"wip_footer_background_color" => "#f3f3f3",

	"home-default" => "default",

),

);
	$current = $skins[$_REQUEST["wip_skins"]];
	
	$message_action = 'Options saved successfully.';

?>