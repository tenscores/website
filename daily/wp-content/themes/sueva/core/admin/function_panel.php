<?php

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

function add_menu() {
	global $themename, $adminmenuname, $optionfile;
	add_theme_page("Theme Options", "Theme Options", 'administrator',  'themeoption', 'themeoption');
	add_theme_page("Get Features", "Get Features", 'administrator',  'getfeatures', 'getfeatures');
}

add_action('admin_menu', 'add_menu'); 

function getfeatures() { ?>
		
                <h2> <?php echo wip_theme_data('Name')." ". __( 'Avaiable Features','wip'); ?> </h2>
                
                <p style=" font-weight:bold; width:800px"> <?php _e( 'Now Sueva is 100% compatible with the GNU license, these are some features that you can include in Sueva theme, not present in the package because with them, is not possible make Sueva 100% compatible with the GNU license.','wip'); ?> </p>
                
                <p> <?php _e( 'You can download the features from <a href="http://wpinprogress.com/get-features/" target="_blank">www.wpinprogress.com/get-features/</a>, with the username and password that you have received after the payment.','wip'); ?> </p>
                
                <h3> <?php _e( '1) Patterns.','wip'); ?> </h3>

                <a href="http://subtlepatterns.com/" target="_blank"> <img src="<?php echo get_template_directory_uri(); ?>/core/admin/include/images/subpatterns.jpg" /> </a>
      
                <p style=" font-weight:bold; width:800px"> <?php _e( 'You can integrate these patterns in this way:.','wip'); ?> 
                
                <ol>
                	<li><?php _e( 'Download your pattern.','wip'); ?></li>
                    <li><?php _e( 'Extract the archive.','wip'); ?></li>
                    <li><a href="themes.php?page=themeoption&tab=Backgrounds"><?php _e( 'Import the pattern from option panel, like custom background.','wip'); ?></a></li>
                </ol>
                
                <h3> <?php _e( '2) Masonry layout with Isotope jQuery plugin.','wip'); ?> </h3>
                
                <ol>
                	<li><?php _e( 'Download and extract the plugin, from <a href="http://wpinprogress.com/get-features/" target="_blank">www.wpinprogress.com/get-features/</a>','wip'); ?></li>
                    <li><?php _e( 'Extract the archive <strong>"Sueva_package.zip"</strong>, after install and activate the plugin called <strong>"wip-sueva-isotope.zip"</strong>, from Plugins => Add new => Upload ','wip'); ?></li>
                    <li><?php _e( 'Isotope plugin is released with MIT license, for non-commercial, personal, or open source projects and applications.','wip'); ?></li>
                </ol>

<?php }

function themeoption() {
		
		$shortname = "wip";
		require_once dirname(__FILE__) . '/option/options.php';   

}

function add_script() {
	
	 global $wp_version;
     wp_enqueue_style( "thickbox" );
     add_thickbox();

	 $file_dir = get_template_directory_uri()."/core/admin/include";

	 wp_enqueue_style ( 'wip_panel', $file_dir.'/css/wip_panel.css' ); 
	 wp_enqueue_style ( 'wip_on_off', $file_dir.'/css/wip_on_off.css' );
	 wp_enqueue_style ( 'wip_shortcodes', $file_dir.'/css/wip_shortcodes.css' );

	 wp_enqueue_script( 'wip_panel', $file_dir.'/js/wip_panel.js',array('jquery','media-upload','thickbox'),'1.0',TRUE ); 
	 wp_enqueue_script( 'wip_on_off', $file_dir.'/js/wip_on_off.js','3.5', '', TRUE); 
     wp_enqueue_script( "mColorPicker", $file_dir."/mColorPicker.php", '', '1.0', TRUE ); 

	 wp_enqueue_script( "jquery-ui-core", array('jquery'));
	 wp_enqueue_script( "jquery-ui-tabs", array('jquery'));
	 wp_enqueue_script( "jquery-ui-sortable", array('jquery'));

}

add_action('admin_init', 'add_script');

function wip_shortcodes_button() {
	
	global $wp_version;
	
	if ( $wp_version >= '3.9.0' ) {

		if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
			return;
		}
	
		if ( 'true' == get_user_option( 'rich_editing' ) ) {
			add_filter( 'mce_external_plugins', 'wip_shortcodes_add_button' );
			add_filter( 'mce_buttons', 'wip_shortcodes_register_button' );
		}
	
	}
}

add_action('admin_head', 'wip_shortcodes_button');

function wip_shortcodes_add_button( $plugin_array ) {
	
	$file_dir = get_template_directory_uri()."/core/admin/include";

	$plugin_array['add_shortcodes_button'] = get_template_directory_uri()."/core/admin/include/js/wip_shortcodes.js";

	return $plugin_array;
}


function wip_shortcodes_register_button( $buttons ) {
	
	array_push( $buttons, 'add_shortcodes_button' );
	
	return $buttons;
}

function save_option ( $panel ) {
	
	global $message_action;
	
	$wip_setting = get_option( wip_themename() );
	
	if ( $wip_setting != false ) {
			$wip_setting = maybe_unserialize( $wip_setting );
		} 
						
	else {
			$wip_setting = array();
		}      
		
	if ( "Add Sidebar" == wip_request('action') ) {
	
		require_once dirname(__FILE__) . '/sidebars.php';

	} 
	
	else
	
	if ( "Save" == wip_request('action') )

	{
				

		foreach ($panel as $element ) 
		{
			
			if ( ( isset($element['tab']) )  && ( $element['tab'] == $_GET['tab'] ) ){
					
				foreach ($element as $value )
		
					{
		
						if ( $_REQUEST['element-opened'] == "Skins" ) {
								
								require_once dirname(__FILE__) . '/option/skins.php';
								update_option( wip_themename(), array_merge( $wip_setting  ,$current) );
								break;
							} 
						
						else if ( ( isset( $value['id']) ) && ( isset( $_POST[$value["id"]] ) ) && ( $value['id'] <> "wip_sidebars" ) ) {	
								
								$current[$value["id"]] = $_POST[$value["id"]]; 
								update_option( wip_themename(), array_merge( $wip_setting  ,$current) );
							}
							
						$message_action = 'Options saved successfully.';
					
					}
				
				}
		
			}
		}
}
function message () 

	{
		global $message_action;
		if (isset($message_action))
		echo '<div id="message" class="updated fade message_save voobis_message"><p><strong>'.$message_action.'</strong></p></div>';
	}

function delete_sidebar ()
	
	{
		
		$wip_sidebars = get_option(wip_themename()); /* Carico le sidebar attualmente presenti nel database */
		
		if ( ('delete' == wip_request('action')) && ($wip_sidebars) ) {
			
			echo '<div id="message" class="updated fade message_save voobis_message"><p><strong>'.__('Sidebar deleted successfully.', 'wip' ).'</strong></p></div>';
			unset($wip_sidebars["wip_sidebars"][$_REQUEST['key']]);
			// $lasd["wip_sidebars"] = array_values($lasd["wip_sidebars"]);
			update_option( wip_themename(), $wip_sidebars );
		}

	}


?>
