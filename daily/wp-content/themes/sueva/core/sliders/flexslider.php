<?php function wip_flexslider() {
	
	global $post ;
	
?>
    
<script type="text/javascript">      
	
jQuery(document).ready(function($){ 
	
	jQuery('.flexslider').flexslider({
		animation : '<?php echo wip_setting('wip_flex_animation'); ?>',
		direction : '<?php echo wip_setting('wip_flex_direction'); ?>',
		<?php if (wip_setting('wip_flex_reverse') =="on") { ?>
			reverse : true,
		<?php } else { ?>
			reverse : false,
		<?php } ?>
		<?php if (wip_setting('wip_flex_slideshowspeed')) { ?>
			slideshowSpeed : <?php echo wip_setting('wip_flex_slideshowspeed'); ?>,
		<?php } else { ?>
			slideshowSpeed : 7000,
		<?php } ?>
		<?php if (wip_setting('wip_flex_animationspeed')) { ?>
			animationSpeed : <?php echo wip_setting('wip_flex_animationspeed'); ?>,
		<?php } else { ?>
			animationSpeed : 600,
		<?php } ?>
			

		<?php if (wip_setting('wip_flex_directionnav') =="on") { ?>
			directionNav: true,
		<?php } else { ?>
			directionNav: false,
		<?php } ?>
		
	    controlNav: false,
		keyboardNav: false,
		prevText: "&lt;",
		nextText: "&gt;",
		touch: true,
		
	});

});

</script>

<div class="pin-container slider-flexslider">
	<section class="flexslider">
        <ul class="slides">
          
            <?php 
			
				$wip_gallery = wip_postmeta( 'galleries' );
    			
				foreach ( $wip_gallery as $slide => $input) { 
                    
					echo '<li><img src="'.$input['url'].'" alt="'.$input['title'].'" /> </li>';  

				}
				 
			?>
            
        </ul>
    </section>
</div>

<?php } ?>