<?php

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @theme Sueva
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */
 
/*-----------------------------------------------------------------------------------*/
/* Admin class */
/*-----------------------------------------------------------------------------------*/   

function wip_admin_body_class( $classes ) {
	
	global $wp_version;
	
	if ( ( $wp_version >= 3.8 ) && ( is_admin()) ) {
		$classes .= 'wp-8';
	}
		return $classes;
}
	
add_filter( 'admin_body_class', 'wip_admin_body_class' );

/*-----------------------------------------------------------------------------------*/
/* Localize theme */
/*-----------------------------------------------------------------------------------*/   

load_theme_textdomain('wip', get_template_directory() . '/languages');

/*-----------------------------------------------------------------------------------*/
/* Shortcode in widget */
/*-----------------------------------------------------------------------------------*/   

add_filter('widget_text', 'do_shortcode');

/*-----------------------------------------------------------------------------------*/
/* Shortcodes */
/*-----------------------------------------------------------------------------------*/ 

remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 12);

/*-----------------------------------------------------------------------------------*/
/* Post formats */
/*-----------------------------------------------------------------------------------*/   

add_theme_support( 'post-formats', array( 'aside','gallery','quote','video','audio','link' ) );

/*-----------------------------------------------------------------------------------*/
/* Main menu */
/*-----------------------------------------------------------------------------------*/         

function wip_main_menu() {
	register_nav_menu( 'main-menu', 'Menu principale' );
}

add_action( 'init', 'wip_main_menu' );

/*-----------------------------------------------------------------------------------*/
/* Theme name */
/*-----------------------------------------------------------------------------------*/ 

function wip_themename() {
	
	$themename = "sueva_theme_settings";
	return $themename;	
	
}

/*-----------------------------------------------------------------------------------*/
/* Theme settings */
/*-----------------------------------------------------------------------------------*/ 

function wip_setting($id) {

	$wip_setting = get_option(wip_themename());
	if(isset($wip_setting[$id]))
		return $wip_setting[$id];

}

/*-----------------------------------------------------------------------------------*/
/* Post meta */
/*-----------------------------------------------------------------------------------*/ 

function wip_postmeta($id) {

	global $post;
	
	$val = get_post_meta( $post->ID , $id, TRUE);
	if(isset($val))
		return $val;

}

/*-----------------------------------------------------------------------------------*/
/* Require */
/*-----------------------------------------------------------------------------------*/ 

function wip_require($folder) {

	if (isset($folder)) : 

		if ( ( !wip_setting('wip_loadsystem') ) || ( wip_setting('wip_loadsystem') == "mode_a" ) ) {
	
			$folder = dirname(dirname(__FILE__)) . $folder ;  
			
			$files = scandir($folder);  
			  
			foreach ($files as $key => $name) {  
				if (!is_dir($name)) { 
					require_once $folder . $name;
				} 
			}  
		
		} else if ( wip_setting('wip_loadsystem') == "mode_b" ) {


			$dh  = opendir(get_template_directory().$folder);
			
			while (false !== ($filename = readdir($dh))) {
			   
				if ( strlen($filename) > 2 ) {
				require_once get_template_directory()."/".$folder.$filename;
				}
			}
		}
	
	endif;
	
}

/*-----------------------------------------------------------------------------------*/
/* Scripts */
/*-----------------------------------------------------------------------------------*/ 

function wip_enqueue_script($folder) {

	if (isset($folder)) : 

		if ( ( !wip_setting('wip_loadsystem') ) || ( wip_setting('wip_loadsystem') == "mode_a" ) ) {
	
		
			$dir = dirname(dirname(__FILE__)) . $folder ;  
			
			$files = scandir($dir);  
			  
			foreach ($files as $key => $name) {  
				if (!is_dir($name)) { 
					
					wp_enqueue_script( str_replace('.js','',$name), get_template_directory_uri() . $folder . "/" . $name , array('jquery'), FALSE, TRUE ); 
					
				} 
			}  
		
		} else if ( wip_setting('wip_loadsystem') == "mode_b" ) {

			$dh  = opendir(get_template_directory().$folder);
			
			while (false !== ($filename = readdir($dh))) {
			   
				if ( strlen($filename) > 2 ) {
						wp_enqueue_script( str_replace('.js','',$filename), get_template_directory_uri() . $folder . "/" . $filename , array('jquery'), FALSE, TRUE ); 
				}
			}
	
		}
		
	endif;

}

/*-----------------------------------------------------------------------------------*/
/* Styles */
/*-----------------------------------------------------------------------------------*/ 

function wip_enqueue_style($folder) {

	if (isset($folder)) : 

		if ( ( !wip_setting('wip_loadsystem') ) || ( wip_setting('wip_loadsystem') == "mode_a" ) ) {
	
		
			$dir = dirname(dirname(__FILE__)) . $folder ;  
			
			$files = scandir($dir);  
			  
			foreach ($files as $key => $name) {  
				
				if (!is_dir($name)) { 
					
					wp_enqueue_style( str_replace('.css','',$name), get_template_directory_uri() . $folder . "/" . $name ); 
					
				} 
			}  
		
		
		} else if ( wip_setting('wip_loadsystem') == "mode_b" ) {

		
			$dh  = opendir(get_template_directory().$folder);
			
			while (false !== ($filename = readdir($dh))) {
			   
				if ( strlen($filename) > 2 ) {
						wp_enqueue_style( str_replace('.css','',$filename), get_template_directory_uri() . $folder . "/" . $filename ); 
				}
			}
		

		}
	
	endif;

}

/*-----------------------------------------------------------------------------------*/
/* TWITTER STATUS */
/*-----------------------------------------------------------------------------------*/ 

function status($status){
								 
	$status = preg_replace("/((http:\/\/|https:\/\/)[^ )
		]+)/e", "'<a href=\"$1\" title=\"$1\" target=\"_blank\" >$1</a>'", $status);
									 
	$status = preg_replace("/(@([_a-z0-9\-]+))/i","<a href=\"http://twitter.com/$2\" title=\"Follow $2\" target=\"_blank\">$1</a>",$status);
									 
	$status = preg_replace("/(#([_a-z0-9\-]+))/i","<a href=\"https://twitter.com/search?q=$2\" title=\"Search $1\" target=\"_blank\">$1</a>",$status);
									 
	return $status;
			
}

/*-----------------------------------------------------------------------------------*/
/* FUNCTIONS */
/*-----------------------------------------------------------------------------------*/ 

wip_require('/core/shortcodes/');
wip_require('/core/widgets/');
wip_require('/core/templates/');
wip_require('/core/classes/');
wip_require('/core/functions/');
wip_require('/core/custom-post/');
wip_require('/core/metaboxes/');
wip_require('/core/sliders/');
wip_require('/core/oauth/');


/*-----------------------------------------------------------------------------------*/
/* Request */
/*-----------------------------------------------------------------------------------*/ 

function wip_request($id) {
	
	if ( isset ( $_REQUEST[$id])) 
	return $_REQUEST[$id];	
	
}

/*-----------------------------------------------------------------------------------*/
/* Theme path */
/*-----------------------------------------------------------------------------------*/ 

function wip_theme_data($id) {
	
	 global $wp_version;	
	 if ( $wp_version <= 3.4 ) :
	 	$themedata = get_theme_data(TEMPLATEPATH. '/style.css');
		return $themedata[$id];
	 else:
		$themedata = wp_get_theme();
		return $themedata->get($id);
	 endif;
	
}

/*-----------------------------------------------------------------------------------*/
/* Content template */
/*-----------------------------------------------------------------------------------*/ 

function wip_template($id) {

	$template = array ("full" => "span12" , "left-sidebar" => "span8" , "right-sidebar" => "span8" );

	$span = $template["full"];
	$sidebar =  "full";

	if  ( ( (is_category()) || (is_tag()) || (is_tax()) || (is_month() ) ) && (wip_setting('wip_category_layout')) ) {
		
		$span = $template[wip_setting('wip_category_layout')];
		$sidebar =  wip_setting('wip_category_layout');
			
	} else if ( (is_home()) && (wip_setting('wip_home')) ) {
		
		$span = $template[wip_setting('wip_home')];
		$sidebar =  wip_setting('wip_home');
		
	} else if  ( ( (is_single()) || (is_page()) ) && (wip_postmeta('wip_template')) ) {
		
		$span = $template[wip_postmeta('wip_template')];
		$sidebar =  wip_postmeta('wip_template');
			
	}

	return ${$id};
	
}

/*-----------------------------------------------------------------------------------*/
/* Sidebar list */
/*-----------------------------------------------------------------------------------*/ 

function wip_sidebar_list($sidebar_type) {
	
		$wip_sidebars = get_option(wip_themename());

		$default = array($sidebar_type."_sidebar_area" => "Default");

		if(!empty($wip_sidebars["wip_sidebars"])):
		
			foreach ($wip_sidebars["wip_sidebars"] as $sidebar_item => $sidebar_items) {
				
				$sidebar = explode("_", $sidebar_items);
				
				if ($sidebar[0] == $sidebar_type)
					$sidebar_list[str_replace(" ","",strtolower($sidebar_items))] =  $sidebar[1];
				}
			
			return array_merge($default, $sidebar_list);
		
		else:
			
			return $default;
			
		endif;
		
}

/*-----------------------------------------------------------------------------------*/
/* GET PAGED */
/*-----------------------------------------------------------------------------------*/ 

function wip_paged() {
	
	if ( get_query_var('paged') ) {
		$paged = get_query_var('paged');
	} elseif ( get_query_var('page') ) {
		$paged = get_query_var('page');
	} else {
		$paged = 1;
	}
	
	return $paged;
	
}

/*-----------------------------------------------------------------------------------*/
/* OEMBED */
/*-----------------------------------------------------------------------------------*/   

function wip_oembed_soundcloud(){
	wp_oembed_add_provider( 'http://soundcloud.com/*', 'http://soundcloud.com/oembed' );
	wp_oembed_add_provider( 'https://soundcloud.com/*', 'http://soundcloud.com/oembed' );
	wp_oembed_add_provider('#https?://(?:api\.)?soundcloud\.com/.*#i', 'http://soundcloud.com/oembed');
}

add_action('init','wip_oembed_soundcloud');

/*-----------------------------------------------------------------------------------*/
/* Thumbnails */
/*-----------------------------------------------------------------------------------*/         

function wip_get_thumbs($type) {
	
	if (wip_setting('wip_'.$type.'_thumbinal')):
		return wip_setting('wip_'.$type.'_thumbinal');
	else:
		return "429";
	endif;

}

add_theme_support( 'post-thumbnails' );

add_image_size( 'blog', 940,wip_get_thumbs('blog'), TRUE ); 
add_image_size( 'portfolio', 940,wip_get_thumbs('portfolio'), TRUE ); 
add_image_size( 'slide', 940,wip_get_thumbs('slide'), TRUE ); 

add_image_size( 'large', 449,304, TRUE ); 
add_image_size( 'medium', 290,220, TRUE ); 
add_image_size( 'small', 211,150, TRUE ); 

/*-----------------------------------------------------------------------------------*/
/* Add default style, at theme activation */
/*-----------------------------------------------------------------------------------*/         

if ( is_admin() && isset($_GET['activated'] ) && $pagenow == 'themes.php' ) {
	
	$wip_setting = get_option(wip_themename());

	if (!$wip_setting) {	
		
		$skins = array( 
		
		"wip_loadsystem" => "mode_a",
		"wip_skins" => "Orange", 
		"wip_logo_font" => "Allura", 
		"wip_logo_font_size" => "70px", 
		"wip_logo_description_font" => "Droid Sans", 
		"wip_logo_description_font_size" => "14px", 
		
		"wip_menu_font" => "Droid Sans", 
		"wip_menu_font_size" => "16px", 

		"wip_content_font" => "Droid Sans", 
		"wip_content_font_size" => "14px", 

		"wip_titles_font" => "Oswald", 

		"wip_h1_font_size" => "28px", 
		"wip_h2_font_size" => "26px", 
		"wip_h3_font_size" => "24px", 
		"wip_h4_font_size" => "18px", 
		"wip_h5_font_size" => "16px", 
		"wip_h6_font_size" => "14px", 

		"wip_text_font_color" => "#616161", 
		"wip_copyright_font_color" => "#ffffff", 
		"wip_link_color" => "#ff6644", 
		"wip_link_color_hover" => "#d14a2b", 
		"wip_border_color" => "#ff6644", 
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
		
		"wip_footer_background" => "/images/background/patterns/pattern11.jpg",
		"wip_footer_background_repeat" => "repeat",
		"wip_footer_background_color" => "#f3f3f3",
	
		"wip_home" => "full",
		"wip_footer_facebook_button" => "http://www.facebook.com/WpInProgress",
		"wip_footer_twitter_button" => "https://twitter.com/#!/WPinProgress",
		"wip_footer_skype_button" => "alexvtn",
		"wip_view_comments" => "on",
		"wip_view_social_buttons" => "on",
		"wip_nivo_effect" => "random",
		"wip_flex_directionnav" => "on",
		
		);
	
		update_option( wip_themename(), $skins ); 
		
	}
}

/*-----------------------------------------------------------------------------------*/
/* Admin menu */
/*-----------------------------------------------------------------------------------*/   

function wip_option_panel() {
        global $wp_admin_bar, $wpdb;
    	$wp_admin_bar->add_menu( array( 'id' => 'theme_options', 'title' => '<span> Theme Options </span>', 'href' => get_admin_url() . 'themes.php?page=themeoption' ) );
    	$wp_admin_bar->add_menu( array( 'id' => 'getfeatures', 'title' => '<span> Get Features </span>', 'href' => get_admin_url() . 'themes.php?page=getfeatures' ) );
}
add_action( 'admin_bar_menu', 'wip_option_panel', 1000 );

/*-----------------------------------------------------------------------------------*/
/* Prettyphoto at post gallery */
/*-----------------------------------------------------------------------------------*/   

function wip_prettyPhoto( $html, $id, $size, $permalink, $icon, $text ) {
	
    if ( ! $permalink )
        return str_replace( '<a', '<a data-rel="prettyPhoto" ', $html );
    else
        return $html;
}

add_filter( 'wp_get_attachment_link', 'wip_prettyPhoto', 10, 6);

/*-----------------------------------------------------------------------------------*/
/* Excerpt more */
/*-----------------------------------------------------------------------------------*/

function wip_hide_excerpt_more() {
	return '';
}

add_filter('the_content_more_link', 'wip_hide_excerpt_more');
add_filter('excerpt_more', 'wip_hide_excerpt_more');

function wip_excerpt() {
	
	global $post,$more;
	
	$more = 0;
	
	if ($pos=strpos($post->post_content, '<!--more-->')): 
		$content = the_content();
	else:
		$content = the_excerpt();
	endif;
	
	echo '<p>' . $content . ' <a class="button" href="'.get_permalink($post->ID).'" title="More">  ' . __( "Read More","wip") . '</a> </p>';

}

/*-----------------------------------------------------------------------------------*/
/* Remove category list rel */
/*-----------------------------------------------------------------------------------*/   

function wip_remove_category_list_rel($output) {
	$output = str_replace('rel="category"', '', $output);
	return $output;
}

add_filter('wp_list_categories', 'wip_remove_category_list_rel');
add_filter('the_category', 'wip_remove_category_list_rel');

/*-----------------------------------------------------------------------------------*/
/* Remove thumbnail dimensions */
/*-----------------------------------------------------------------------------------*/ 

function wip_remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

add_filter( 'post_thumbnail_html', 'wip_remove_thumbnail_dimensions', 10, 3 );
  
/*-----------------------------------------------------------------------------------*/
/* Remove css gallery */
/*-----------------------------------------------------------------------------------*/ 

add_filter( 'gallery_style', 'wip_my_gallery_style', 99 );

function wip_my_gallery_style() {
    return "<div class='gallery'>";
}

/*-----------------------------------------------------------------------------------*/
/* Thematic dropdown options */
/*-----------------------------------------------------------------------------------*/ 

function wip_childtheme_dropdown_options($dropdown_options) {
	$dropdown_options = '<script type="text/javascript" src="'. get_stylesheet_directory_uri() .'/scripts/thematic-dropdowns.js"></script>';
	return $dropdown_options;
}

add_filter('thematic_dropdown_options','wip_childtheme_dropdown_options');

/*-----------------------------------------------------------------------------------*/
/* POST ICON */
/*-----------------------------------------------------------------------------------*/ 

function wip_posticon() {

	$icons = array ("video" => "fa fa-film" , "gallery" => "fa fa-camera" , "audio" => "fa fa-music" );

	if (get_post_format()) { 
		$icon = '<span><i class="'.$icons[get_post_format()].'"></i>'.ucfirst(strtolower(get_post_format())).'</span>'; 
	} else {
		$icon = '<span><i class="fa fa-pencil-square-o"></i>'.__( "Article","wip").'</span>'; 
	}

	return $icon;
	
}

?>