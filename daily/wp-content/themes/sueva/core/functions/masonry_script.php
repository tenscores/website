<?php 

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

function wip_masonry_script_function() { ?>

	<script type="text/javascript">
    
    jQuery(document).ready(function($){
    
    var $container = jQuery('#masonry');
    
    $container.imagesLoaded(function(){
        $container.masonry({
            itemSelector: '.pin-article',
            isAnimated: true
        });
    });
    
    });
    
    </script>

<?php 

} 

add_action( 'wip_masonry_script', 'wip_masonry_script_function', 10, 2 );

?>