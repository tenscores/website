<footer id="footer">
<div class="container">
	
		<?php if ( is_active_sidebar('bottom-sidebar-area') ) : ?>
        
            <!-- FOOTER WIDGET BEGINS -->
            
                <section class="row widget">
                    <?php dynamic_sidebar('bottom-sidebar-area') ?>
                </section>
                
            <!-- FOOTER WIDGET END -->
            
        <?php endif; ?>

         <div class="row copyright" >
            <div class="span5" >
				<?php if (wip_setting('wip_copyright_text')): ?>
                   <p> <?php echo stripslashes(wip_setting('wip_copyright_text')); ?> </p>
                <?php else: ?>
                   <p> Copyright <?php echo get_bloginfo("name"); ?> <?php echo date("Y"); ?> - Powered by <a href="http://www.wpinprogress.com/" target="_blank">WP in Progress</a> </p>
                <?php endif; ?>
            </div>
            <div class="span7" >
                <!-- start social -->
                <div class="socials">
                    <?php do_action( 'wip_socials' ); ?>
                </div>
                <!-- end social -->

            </div>
		</div>
	</div>
</footer>

<?php wp_footer() ?>   
</body>

</html>