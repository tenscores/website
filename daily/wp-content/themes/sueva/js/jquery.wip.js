jQuery(document).ready(function(){
	
	if ( jQuery('nav#mainmenu ul:first .current-menu-item').length ) { 
	
		jQuery('nav#mainmenu ul:first').tinyNav({
			active: 'current-menu-item',
		});

	} else {
	
		jQuery('nav#mainmenu ul:first').tinyNav({
			header: 'Select an item',
		});

	}

	jQuery('.filterable-grid li').live('mouseover',function(){
			
		var imgw = jQuery('.overlay',this).prev().width();
		var imgh = jQuery('.overlay',this).prev().height();
			
		jQuery('.overlay',this).css({'width':imgw,'height':imgh});	
			
		jQuery('.overlay',this).animate({ opacity : 0.6 },{queue:false});
	
	});

	jQuery('.filterable-grid li').live('mouseout',function(){
		
		jQuery('.overlay', this).animate({ opacity: 0}, { queue:false });
		
	});

	jQuery('.skills .views').click(function(){
	
	if(jQuery(this).next('ul').css('display')=='none') {	
			jQuery(this).addClass('active');
			jQuery('.skills .views i').addClass('open');
		}
	else {	
			jQuery(this).removeClass('active');
			jQuery('.skills .views i').removeClass('open');
		}
				
			jQuery(this).next('ul').stop(true,false).slideToggle('hight');

	});
	
	jQuery(".buttons").click(function() {
		
			var hasError = false;
	
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			
			if(jQuery("input#contact-name").val() == '') {
				hasError = true;
			}
			if(jQuery("input#contact-subject").val() == '') {
				hasError = true;
			}
			if(jQuery("#contact-message").val() == '') {
				hasError = true;
			}
			if( (!emailReg.test( jQuery("#contact-email").val() )) || (jQuery("#contact-email").val() == '') ) {
				hasError = true;
			}
	
			if(hasError == true)
			{
				jQuery('span.error').css({'display':'block'});
				return false;
	
			}else {
				jQuery('span.error').css({'display':'none'});
				return true;
			}
	
	});

	jQuery('a.social').tipsy({fade:true, gravity:'s'});
	
	jQuery('nav#mainmenu li').hover(
			function () {
				jQuery(this).children('ul').stop(true, true).fadeIn(100);
	 
			}, 
			function () {
				jQuery(this).children('ul').stop(true, true).fadeOut(400);		
			}
			
	
	);

	jQuery('nav#widgetmenu ul > li').each(function(){
    	if( jQuery('ul', this).length > 0 )
        jQuery(this).children('a').append('<span class="sf-sub-indicator"> &raquo;</span>').removeAttr("href");
	}); 

	jQuery('nav#widgetmenu ul > li ul').click(function(e){
		e.stopPropagation();
    })
	
    .filter(':not(:first)')
    .hide();
    
	jQuery('nav#widgetmenu ul > li, nav#widgetmenu ul > li > ul > li').click(function(){

		var selfClick = jQuery(this).find('ul:first').is(':visible');
		if(!selfClick) {
		  jQuery(this).parent().find('> li ul:visible').slideToggle('low');
		  
		
		}
		
		jQuery(this).find('ul:first').stop(true, true).slideToggle();
	
	});

	// TABS
	jQuery( ".tabs" ).tabs({ hide: { effect: "fadeOut", duration: 300 } });
	
	// TOGGLE
	
	jQuery('.toggle_container h5.element').last().css('border-bottom', 'none' , 'border-top', 'none');
	jQuery('.toggle_container h5.element').click(function(){		
		if(jQuery(this).next('.toggle').css('display')=='none')
			{	
				jQuery(this).addClass('inactive');
				jQuery(this).children('img').addClass('inactive');
						
			}
		else
			{	jQuery(this).removeClass('inactive');
						jQuery(this).children('img').removeClass('inactive');			
			}
					
				jQuery(this).next('.toggle').stop(true,false).slideToggle('slow');
	
	
		});

	/* Overlay  */
	
	jQuery('.overlay-image').hover(function(){
		
		var imgwidth = jQuery(this).children('img').width();
		var imgheight = jQuery(this).children('img').height();
		jQuery(this).children('.zoom').css({'width':imgwidth,'height':imgheight});	
		jQuery(this).children('.link').css({'width':imgwidth,'height':imgheight});		
		jQuery(this).css({'width':imgwidth+10});		
		
		jQuery('.overlay',this).animate({ opacity : 0.6 },{queue:false});
		}, 
		function() {
		jQuery('.overlay',this).animate({ opacity: 0.0 },{queue:false});
	
	});
	
	jQuery('.gallery img').hover(function(){
		jQuery(this).animate({ opacity: 0.50 },{queue:false});
	}, 
	function() {
		jQuery(this).animate({ opacity: 1.00 },{queue:false});
	});
	
	
	jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
	
				animationSpeed:'fast',
				slideshow:5000,
				theme:'pp_default',
				show_title:false,
				overlay_gallery: false,
				social_tools: false
	});

});          