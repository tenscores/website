<?php get_header(); ?>

<div class="container">
	<div class="row" id="blog" >
    
	<?php if ( ( wip_template('sidebar') == "left-sidebar" ) || ( wip_template('sidebar') == "right-sidebar" ) ) : ?>
        
        <div class="<?php echo wip_template('span') .' '. wip_template('sidebar'); ?>"> 
        <div class="row"> 
        
    <?php endif; ?>
        

		<?php if ( have_posts() ) :  ?>
		
        <div <?php post_class(array('pin-article', 'span12' )); ?> >

			<article class="article category">
				
                    <h1><?php _e( 'Skill','wip'); ?> : <strong> <?php echo $wp_query->queried_object->name . '</h1>';
 ?> </strong> </h1>

			</article>

    	</div>
		
		<?php while ( have_posts() ) : the_post(); ?>

        <div <?php post_class(array('pin-article', 'span12' )); ?> >
    
				<?php do_action('wip_postformat'); ?>
        
                <div style="clear:both"></div>
            
            </div>
		
		<?php endwhile; else:  ?>

        <div <?php post_class(array('pin-article', 'span12' )); ?> >
    
                <article class="article category">
                    
                    <h1> Not found </h1>
                    <p><?php _e( 'Sorry, no posts matched into ',"wip" ) ?> <strong>: <?php the_category(' '); ?></strong></p>
     
                </article>
    
            </div>
	
		<?php endif; ?>
        
	<?php if ( ( wip_template('sidebar') == "left-sidebar" ) || ( wip_template('sidebar') == "right-sidebar" ) ) : ?>
        
        </div>
        </div>
        
    <?php endif; ?>

	<?php if ( ( is_active_sidebar('category-sidebar-area') ) && ( wip_template('span') == "span8" ) ) : ?>
        
        <section id="sidebar" class="pin-article span4">
            <div class="sidebar-box">
            	<?php dynamic_sidebar('category-sidebar-area') ?>
            </div>
        </section>
    
	<?php endif; ?>
           
    </div>
</div>

<?php
	
	get_template_part('pagination');
	get_footer(); 

?>