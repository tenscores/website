<?php 

	get_header();
	
	if ( (wip_setting('wip_home') == "span6") || (wip_setting('wip_home') == "span4") ) {
	
		do_action('wip_masonry',wip_setting('wip_home'));
		
	} else {
		
		get_template_part('home-blog');

	}
	
	get_template_part('pagination');
	
	get_footer(); 

?>