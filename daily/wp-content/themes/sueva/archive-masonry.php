<div class="container">
	
    <div class="row">

        <div <?php post_class(array('pin-article', 'span12') ); ?> >

			<article class="article category">
				
				<?php if (is_tag()) : ?>

                    <h1> <strong> <?php _e( 'Tag','wip'); ?> :  </strong> <?php echo get_query_var('tag');  ?></h1>
				
				<?php else : ?>
				
                    <h1> <strong> <?php _e( 'Category','wip'); ?> :  </strong> <?php single_cat_title(); ?></h1>

				<?php endif; ?>
                
			</article>

    	</div>
    
    </div>

    <div class="row" id="masonry" >
		
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
           
		<div <?php post_class(array('pin-article', wip_setting('wip_category_layout') )); ?>>
				
			<?php do_action('wip_postformat'); ?>
                
		</div>

    <?php endwhile; else: ?>

    	<div class="pin-article span12">
    
        	<article class="article">
                    
            	<h1> Not found </h1>
                <p><?php _e( 'Sorry, no posts matched into ',"wip" ) ?> <strong>: <?php single_cat_title(); ?> </strong></p>
     
            </article>
    
        </div>
	
	<?php endif; ?>
                
	</div>
            
</div>
    
<?php do_action('wip_masonry_script'); ?>
    
<?php get_template_part('pagination'); ?>