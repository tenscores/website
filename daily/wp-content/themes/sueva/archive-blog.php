<div class="container">

	<div class="row" id="blog" >
    
	<?php if ( ( wip_template('sidebar') == "left-sidebar" ) || ( wip_template('sidebar') == "right-sidebar" ) ) : ?>
        
        <div class="<?php echo wip_template('span') .' '. wip_template('sidebar'); ?>"> 
        
        <div class="row"> 
        
    <?php endif; ?>
        
		<?php if ( have_posts() ) :  ?>
		
        <div <?php post_class(array('pin-article', wip_template('span') )); ?> >

			<article class="article category">
				
				<?php if (is_tag()) : ?>

                    <h1> <strong> <?php _e( 'Tag','wip'); ?> :  </strong> <?php echo get_query_var('tag');  ?></h1>
				
				<?php else : ?>
				
                    <h1> <strong> <?php _e( 'Category','wip'); ?> :  </strong> <?php single_cat_title(); ?></h1>

				<?php endif; ?>
                
			</article>

    	</div>
		
		<?php while ( have_posts() ) : the_post(); ?>

        <div <?php post_class(array('pin-article', wip_template('span') )); ?> >
    
				<?php do_action('wip_postformat'); ?>
        
                <div style="clear:both"></div>
            
            </div>
		
		<?php endwhile; else:  ?>

            <div class="pin-article span12">
    
                <article class="article">
                    
                    <h1> Not found </h1>
                    <p><?php _e( 'Sorry, no posts matched into ',"wip" ) ?> <strong>: <?php single_cat_title(); ?> </strong></p>
     
                </article>
    
            </div>
	
		<?php endif; ?>
        
	<?php if ( ( wip_template('sidebar') == "left-sidebar" ) || ( wip_template('sidebar') == "right-sidebar" ) ) : ?>
        
        </div>
        </div>
        
    <?php endif; ?>

	<?php if ( ( is_active_sidebar('category-sidebar-area') ) && ( wip_template('span') == "span8" ) ) : ?>
        
        <section id="sidebar" class="pin-article span4">
            <div class="sidebar-box">
            	<?php dynamic_sidebar('category-sidebar-area') ?>
            </div>
        </section>
    
	<?php endif; ?>
           
    </div>
    
</div>

<?php get_template_part('pagination'); ?>