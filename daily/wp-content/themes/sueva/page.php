<?php get_header(); ?>

<!-- start content -->

<div class="container">
    <div class="row" >
    
    <div class="pin-article <?php echo wip_template('span') . " ". wip_template('sidebar'); ?>" >
		
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
       
    <?php do_action('wip_postformat'); ?> 

	<?php wp_link_pages(array('before' => '<div class="wip-pagination">' . __('Pages:','wip'), 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>') ); ?>

	<?php endwhile; endif;?>
    
    </div>
    
    <?php get_sidebar(); ?>

    </div>
</div>

<?php get_footer(); ?>