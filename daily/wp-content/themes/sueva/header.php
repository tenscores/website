<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
   
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<?php if (wip_setting('wip_custom_favicon')) : ?>
	<link rel="shortcut icon" href="<?php echo wip_setting('wip_custom_favicon'); ?>"/>
<?php endif; ?>

<?php do_action( 'wip_metatags' ); ?> 

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.2, user-scalable=yes" />

<!--[if IE 8]>
    <link href='http://fonts.googleapis.com/css?family=Gudea' rel='stylesheet' type='text/css'>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php do_action( 'wip_head' ); ?>

</head>

<body <?php body_class(); ?>>

<header class="header container" >

	<div class="row">
    	<div class="span12" >
        	<div id="logo">
                    
            	<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name') ?>">
                        
                	<?php 
					   				
                    	if ( (wip_setting('wip_custom_logo')) && (wip_setting('wip_view_custom_logo') == "on")):
                        	echo "<img src='".wip_setting('wip_custom_logo')."' alt='logo'>"; 
                        else: 
                            bloginfo('name');
							echo "<span>".get_bloginfo('description')."</span>";
                        endif; 
						
					?>
                            
                </a>
                        
			</div>

            <nav id="mainmenu">
                <?php wp_nav_menu( array('theme_location' => 'main-menu', 'container' => 'false','depth' => 3  )); ?>
            </nav> 
                           
        </div>
	</div>

</header>