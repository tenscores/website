<?php get_header(); ?>

<div class="container content">

<div class="my-headline" style="display:none;">
<h1>Daily Pay Per Click News And Tips</h1>
<h2>Short, Sweet And Entertaining Reads To Enjoy With Your Morning Coffee</h2>

<!-- Begin MailChimp Signup Form -->
<form action="//tenscores.us5.list-manage.com/subscribe/post?u=70e48df660281be38b3086bf1&amp;id=63f07d6e7b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	<input type="email" placeholder="Enter your email" name="EMAIL" class="input-text">
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_70e48df660281be38b3086bf1_63f07d6e7b" tabindex="-1" value=""></div>
    <input type="submit" value="Get A Weekly Digest" name="subscribe" class="input-submit">
</form>
<!--End mc_embed_signup-->

<div style="max-width:200px; margin: auto; display:none;">
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_sharing_toolbox"></div>
Send it to a friend.
</div>


</div>

	<div class="row" id="masonry">
                
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
           
			<article <?php post_class(array('post-container',wip_template('wip_home') )); ?>>
				
				<?php do_action('wip_postformat'); ?>

			</article>
			
		<?php endwhile; else: ?>
        
			<article class="post-container col-md-12">
                
                <div class="post-article">
    
                    <h1><?php _e( 'Content not found',"wip" ) ?></h1>           
                    
                    <p> <?php _e( 'No article found in this blog.','wip'); ?> </p>
    
                    <h2> <?php _e( 'What can i do?',"wip" ) ?> </h2>           
    
                    <p> <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name') ?>"> <?php _e( 'Back to the homepage','wip'); ?> </a> </p>
                  
                    <p> <?php _e( 'Make a search, from the below form:','wip'); ?> </p>
                    
                    <section class="contact-form">
                    
                        <form method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                             <input type="text" value="<?php _e( 'Search', 'wip' ) ?>" name="s" id="s" onblur="if (this.value == '') {this.value = '<?php _e( 'Search', 'wip' ) ?>';}" onfocus="if (this.value == '<?php _e( 'Search', 'wip' ) ?>') {this.value = '';}" class="input-search"/>
                             <input type="submit" id="searchsubmit" class="button-search" value="<?php _e( 'Search', 'wip' ) ?>" />
                        </form>
                        
                        <div class="clear"></div>
                        
                    </section>
    
                </div>
			
            </article>

		<?php endif; ?>

	</div>
            
	<?php 
		
		if ( wip_setting('wip_home') <> 'col-md-12' ):
			wip_masonry_scripts(); 
		endif;
		
		get_template_part('pagination'); 
		
	?>
		
</div>

<?php get_footer(); ?>