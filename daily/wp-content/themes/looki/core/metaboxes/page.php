<?php

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

$wip_new_metaboxes = new wip_metaboxes ('page', array (

array( "name" => "Navigation",  
       "type" => "navigation",  
       "item" => array( "setting" => __( "Setting","wip") , "gallery" => __( "Gallery","wip") , "video" => __( "Video","wip") , "maps" => __( "Google maps","wip")),   
       "start" => "<ul>", 
       "end" => "</ul>"),  

array( "type" => "begintab",
	   "tab" => "setting",
	   "element" =>

		array( "name" => __( "Setting","wip"),
			   "type" => "title",
			  ),

		array( "name" => __( "Sidebar","wip"),
			   "desc" => __( "Select side sidebar","wip"),
			   "id" => "wip_sidebar",
			   "type" => "select",
			   "options" => wip_sidebar_list('side'),
			),
			
		array( "name" => __( "Content type","wip"),
			   "desc" => __( "Select a type for this page","wip"),
			   "id" => "wip_content_type",
			   "type" => "select",
			   "options" => array(
				   "standard" => __( "Standard","wip"),
				   "gallery" =>  __( "Gallery","wip"),
				   "video" => __( "Video","wip"),
				   "maps" => __( "Google Maps","wip"),
			  ),
		),
			  
),

array( "type" => "endtab"),

array( "type" => "begintab",
	   "tab" => "gallery",
	   "element" =>

		array( "name" => __( "Gallery","wip"),
			   "type" => "title",
			  ),

		array( "name" => __( "Gallery type","wip"),
			   "desc" => __( "Select one of these slideshows","wip"),
			   "id" => "wip_slider_type",
			   "type" => "select",
			   "options" => array(
				   "nivoslider" => "Nivo Slider",
				   "flexslider" => "Flex Slider" ),
			  ),

		array( "name" => __( "Add a new photo","wip"),
			   "type" => "title",
			  ),

	    array(  "name" => __( "Crea Gallery","wip"),
			   "desc" => __( "Upload the images for gallery","wip"),
			   "id" => "galleries",
			   "type" => "galleries",
       		  ),
			  
),

array( "type" => "endtab"),

array( "type" => "begintab",
	   "tab" => "video",
	   "element" =>

		array( "name" => __( "Video","wip"),
			   "type" => "title",
			  ),

		array( "name" => __( "Video Services","wip"),
			   "desc" => __( "Select one of this video services","wip"),
			   "id" => "wip_video_type",
			   "type" => "select",
			   "options" => array(
				   "http://player.vimeo.com/video/" => "Vimeo",
				   "http://www.youtube.com/embed/" => "Youtube" ),
			  ),
			  
		array( "name" => __( "ID Services","wip"),
			   "desc" => __( "Insert the ID of Youtube or Vimeo Video","wip"),
			   "id" => "wip_video_id",
			   "type" => "text",
			  ),
			  
),

array( "type" => "endtab"),

array( "type" => "begintab",
	   "tab" => "maps",
	   "element" =>

		array( "name" => __( "Google maps","wip"),
			   "type" => "title",
			  ),

		array( "name" => __( "Address","wip"),
			   "desc" => __( "Insert the address for Google maps","wip"),
			   "id" => "wip_contact_address",
			   "type" => "text",
			  ),
			  
),

array( "type" => "endtab"),

array( "type" => "endtab")
)

);


?>