<?php function wip_nivoslider() {
	
	global $post ;
	
?>
    
<script type="text/javascript">      
	
jQuery(document).ready(function($){ 

	jQuery('.slider-wrapper').nivoSlider({
		effect : '<?php echo wip_setting('wip_nivo_effect'); ?>',
		<?php if (wip_setting('wip_nivo_animationspeed')) { ?>
			animSpeed : <?php echo wip_setting('wip_nivo_animationspeed'); ?>,
		<?php } else { ?>
			animSpeed : 500,
		<?php } ?>
		<?php if (wip_setting('wip_nivo_pause_time')) { ?>
			pauseTime : <?php echo wip_setting('wip_nivo_pause_time'); ?>,
		<?php } else { ?>
			pauseTime : 3000,
		<?php } ?>
        directionNav: true,
        controlNav: false,
        controlNavThumbs: false,
        manualAdvance: false,
        pauseOnHover: false
	});
	

});          
</script>

<div class="pin-container" >
	<div class="slider-wrapper theme-default">
          
    	<?php 
			
			$wip_gallery = wip_postmeta( 'galleries' );
    		
			foreach ( $wip_gallery as $slide => $input ) { 
                    
				echo '<img src="'.$input['url'].'" alt="'.$input['title'].'" /> '; 
				
			}
			
		?> 
            
    </div>
</div>

<?php } ?>