<?php 

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

function wip_masonry($span) { ?>

	<div class="row" id="masonry">
        
		<?php

			$maxpost = get_option('posts_per_page');
			$offset = wip_paged()*get_option('posts_per_page')-get_option('posts_per_page');

			$query = new WP_Query( 'post_type=post&posts_per_page='.$maxpost.'&offset='.$offset.'&paged='.wip_paged() );
			if ( $query->have_posts() ) : while ( $query-> have_posts() ) : $query->the_post(); 

        ?>
   
        <article <?php post_class(array('post-container',$span)); ?>>
			<?php do_action('wip_postformat'); ?>
    	</article>

		<?php endwhile; endif; ?>
        
    </div>
	
    <?php wip_masonry_scripts('','');  ?>

<?php } ?>