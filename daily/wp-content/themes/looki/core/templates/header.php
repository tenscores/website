<?php 

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

/*-----------------------------------------------------------------------------------*/
/* Header and fonts */
/*-----------------------------------------------------------------------------------*/ 

function wip_header() {
	
	$fonts = array ( wip_setting("wip_logo_font"),wip_setting("wip_menu_font"),wip_setting("wip_logo_description_font"),wip_setting("wip_titles_font"),wip_setting("wip_content_font"));
	
	$fonts = array_unique($fonts); 
	
	echo "<link href='http://fonts.googleapis.com/css?family=Fjalla+One|Roboto+Condensed";
	
	foreach ( $fonts as $font ) {
		if ($font) { 
			echo "|".str_replace(" ","+",$font); 
		}
	}
	
	echo "' rel='stylesheet' type='text/css'>";
		
	wp_head(); 

}

add_action( 'wip_head', 'wip_header', 10, 2 );

function wip_header_content() {
	
	if ( ( is_page()) && (wip_postmeta('wip_slogan')) ) : ?>
	
		<section id="subheader">
		
			<div class="container">
			
				<div class="row">
				
					<div class="col-md-12">
					
						<p> <?php echo wip_postmeta('wip_slogan'); ?> </p>
                        
					</div>
					
				</div>
				
			</div>
			
		</section>
		
<?php 

	endif;

}

?>