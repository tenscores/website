<?php 

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

function social_buttons_function() { 

	global $post;
	
?>
    
	<div class="socials share">

		<a href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php echo get_permalink($post->ID);?>&p[images][0]=<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>&p[title]=<?php echo get_the_title(); ?>&p[summary]=<?php echo get_the_excerpt(); ?>" target="_blank" class="social" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=220,width=600');return false;"> 
        	<i class="fa fa-facebook"></i> 
        </a>
        
		<a href="https://twitter.com/share?text=<?php echo get_the_title(); ?>&url=<?php echo get_permalink($post->ID);?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=260,width=600');return false;" target="_blank" class="social">
			<i class="fa fa-twitter"></i> 
        </a> 
        
		<a href="https://plus.google.com/share?url=<?php echo get_permalink($post->ID);?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=260,width=600');return false;" target="_blank" class="social">
			<i class="fa fa-google-plus"></i> 
        </a> 

		<a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID));?>&media=<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>&description=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=260,width=600');return false;" target="_blank" class="social">
			<i class="fa fa-pinterest"></i> 
        </a> 

    </div>

<?php 

} 

add_action( 'wip_social_buttons', 'social_buttons_function', 10, 2 );

?>