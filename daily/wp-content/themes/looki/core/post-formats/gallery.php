<?php 

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

	wip_get_title(); 

	if (wip_postmeta('wip_slider_type')):
		call_user_func('wip_'.wip_postmeta('wip_slider_type'));
	else:
		wip_flexslider();
	endif;
	
?>
    
<div class="post-article">

	<?php do_action('wip_after_content'); ?>

</div>