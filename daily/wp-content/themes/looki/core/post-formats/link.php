<?php 

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

?>

<div class="post-article post-link">
	<div class="link">
		<a href="<?php echo wip_postmeta('wip_url_link' ); ?>" target="_blank">
            <span class="link"> <?php _e('Post link','wip'); ?> </span>
            <?php echo wip_postmeta( 'wip_url_link_name' ); ?>
		</a>
	</div>
</div>
