<?php 

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */

	wip_get_title(); 

	wip_video(); 
	
?>
    
<div class="post-article">

	<?php do_action('wip_after_content'); ?>

</div>