<?php

/**
 * Wp in Progress
 * 
 * @package Wordpress
 * @author WPinProgress
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
 */


$skins = array( 

	"orange" => array(
	
		"wip_skins" => "orange", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#eb984e", 
		"wip_link_color_hover" => "#e67e22",

		"wip_bars_background_color" => "#2D3032", 
		"wip_bars_text_color" => "#ffffff",
		"wip_bars_borders_color" => "#444649",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),
	
	"turquoise" => array(
	
		"wip_skins" => "turquoise", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#48c9b0", 
		"wip_link_color_hover" => "#1abc9c",

		"wip_bars_background_color" => "#2D3032", 
		"wip_bars_text_color" => "#ffffff",
		"wip_bars_borders_color" => "#444649",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),
	
	"yellow" => array(
	
		"wip_skins" => "yellow", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#f4d03f", 
		"wip_link_color_hover" => "#f1c40f",

		"wip_bars_background_color" => "#2D3032", 
		"wip_bars_text_color" => "#ffffff",
		"wip_bars_borders_color" => "#444649",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),
	
	"red" => array(
	
		"wip_skins" => "red", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#ec7063", 
		"wip_link_color_hover" => "#e74c3c",

		"wip_bars_background_color" => "#2D3032", 
		"wip_bars_text_color" => "#ffffff",
		"wip_bars_borders_color" => "#444649",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),
	
	"purple" => array(
	
		"wip_skins" => "purple", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#af7ac5", 
		"wip_link_color_hover" => "#9b59b6",

		"wip_bars_background_color" => "#2D3032", 
		"wip_bars_text_color" => "#ffffff",
		"wip_bars_borders_color" => "#444649",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),
	
	"green" => array(
	
		"wip_skins" => "green", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#58d68d", 
		"wip_link_color_hover" => "#2ecc71",

		"wip_bars_background_color" => "#2D3032", 
		"wip_bars_text_color" => "#ffffff",
		"wip_bars_borders_color" => "#444649",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),
	
	"blue" => array(
	
		"wip_skins" => "blue", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#5dade2", 
		"wip_link_color_hover" => "#3498db",

		"wip_bars_background_color" => "#2D3032", 
		"wip_bars_text_color" => "#ffffff",
		"wip_bars_borders_color" => "#444649",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),

	"light_orange" => array(
	
		"wip_skins" => "light_orange", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#eb984e", 
		"wip_link_color_hover" => "#e67e22",

		"wip_bars_background_color" => "#ffffff", 
		"wip_bars_text_color" => "#333333",
		"wip_bars_borders_color" => "#dddddd",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),

	"light_turquoise" => array(
	
		"wip_skins" => "light_turquoise", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#48c9b0", 
		"wip_link_color_hover" => "#1abc9c",

		"wip_bars_background_color" => "#ffffff", 
		"wip_bars_text_color" => "#333333",
		"wip_bars_borders_color" => "#dddddd",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),

	"light_yellow" => array(
	
		"wip_skins" => "light_yellow", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#f4d03f", 
		"wip_link_color_hover" => "#f1c40f",

		"wip_bars_background_color" => "#ffffff", 
		"wip_bars_text_color" => "#333333",
		"wip_bars_borders_color" => "#dddddd",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",

	),

	"light_red" => array(
	
		"wip_skins" => "light_red", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#ec7063", 
		"wip_link_color_hover" => "#e74c3c",

		"wip_bars_background_color" => "#ffffff", 
		"wip_bars_text_color" => "#333333",
		"wip_bars_borders_color" => "#dddddd",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),

	"light_purple" => array(
	
		"wip_skins" => "light_purple", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#af7ac5", 
		"wip_link_color_hover" => "#9b59b6",

		"wip_bars_background_color" => "#ffffff", 
		"wip_bars_text_color" => "#333333",
		"wip_bars_borders_color" => "#dddddd",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),

	"light_green" => array(
	
		"wip_skins" => "light_green", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#58d68d", 
		"wip_link_color_hover" => "#2ecc71",

		"wip_bars_background_color" => "#ffffff", 
		"wip_bars_text_color" => "#333333",
		"wip_bars_borders_color" => "#dddddd",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),

	"light_blue" => array(
	
		"wip_skins" => "light_blue", 
		
		"wip_text_font_color" => "#616161", 
		
		"wip_link_color" => "#5dade2", 
		"wip_link_color_hover" => "#3498db",

		"wip_bars_background_color" => "#ffffff", 
		"wip_bars_text_color" => "#333333",
		"wip_bars_borders_color" => "#dddddd",
	
		"wip_body_background" => "/images/background/patterns/pattern12.jpg",
		"wip_body_background_repeat" => "repeat",
		"wip_body_background_color" => "#f3f3f3",
	
	),

);

	$current = $skins[$_REQUEST["wip_skins"]];
	$message_action = 'Options saved successfully.';

?>