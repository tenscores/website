<?php

function dropcap_code ($atts,  $content = null) {
	
	$html =  '<p class="dropcap">' . $content . '</p>';
	
	return $html;

}

add_shortcode('dropcap','dropcap_code');

?>
