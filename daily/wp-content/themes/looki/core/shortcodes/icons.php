<?php

function icon_function($atts,  $content = null) {
	extract(shortcode_atts(array(
		'name' => '',
	), $atts));

	$icon = '<i class="fa '.$name.'"></i>'; 
	
	return do_shortcode($icon.$content);
}

add_shortcode('icon','icon_function');


?>
