<?php

function tabs_container_code($atts, $content = null) {

	$html = '<div class="tabs"> <ul> ';
		
    foreach($atts as $id => $title)
        $html .= '<li><a href="#'.$id.'" title="'.$title.'">'.$title.'</a></li>';

	$html .= '<div class="clear"></div></ul><div class="tabs-container">';
	
	$html .= do_shortcode($content);

	$html .= '<div class="clear"></div></div></div>';

	return $html;

}

add_shortcode('tabs_container','tabs_container_code');

?>
