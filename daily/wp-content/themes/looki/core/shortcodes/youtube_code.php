<?php

function youtube_code ($atts,  $content = null) {
	
	extract(shortcode_atts(array(
		'id_video' => '',
		'autoplay' => '0',
	), $atts));
	
	if ($autoplay == "true" ) $autoplay = "1";
	
	$html =  '<div class="video-container"><div class="video-thumb"><iframe src="http://www.youtube.com/embed/' . $id_video . '?wmode=opaque&amp;autoplay=' . $autoplay . '"></iframe></div></div>';
	
	return $html;

}

add_shortcode('youtube','youtube_code');

?>
