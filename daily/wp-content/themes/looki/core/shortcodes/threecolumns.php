<?php

function threecolumns($atts,  $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'icon' => '',
		'span' => 'true'
	), $atts));

	if ($icon) { $icon = '<i class="fa '.$icon.'"></i>'; } 

	if ($title) { $title = '<h3 class="title">'.$icon.$title.'</h3>'; }
	
	$content = "<div class='col-md-4'>" . $title . $content . "</div>" ;
	
	return do_shortcode($content);
}

add_shortcode('three_columns','threecolumns');

?>
