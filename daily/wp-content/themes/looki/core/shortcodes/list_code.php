<?php

function list_code ($atts,  $content = null) {
	
	extract(shortcode_atts(array(
		'icon' => '',
	), $atts));
	
	$content = str_replace('<li>','<li><i class="fa ' . $icon . '"></i>',$content);
	
	$html =  '<ul class="icons">' . $content . '</ul>';
	
	return $html;

}

add_shortcode('list','list_code');

?>
