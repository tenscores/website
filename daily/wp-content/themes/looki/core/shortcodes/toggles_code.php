<?php

function toggles_code($atts,  $content = null) {
	extract(shortcode_atts(array(
		'title' => '',

	), $atts));

	$content = '<div class="toggle_container">' . $content . '</div>' ;
	
	return do_shortcode($content);
}

add_shortcode('toggle_container','toggles_code');

?>
