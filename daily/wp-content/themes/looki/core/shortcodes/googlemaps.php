<?php

function maps($atts,  $content = null) {
	extract(shortcode_atts(array(
		'src' => '',
		'width' => '',
		'height' => ''
	), $atts));
	
	$map = '<div class="maps-container"><div class="maps-thumb">';
	$map .= '<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q='.$src.'&amp;hnear='.$src.'&amp;output=embed"></iframe>';
	$map .= '</div></div>';
	
	return do_shortcode($map);
	
}

add_shortcode('googlemaps','maps');

?>
