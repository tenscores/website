<?php

	register_sidebar(array(
	
		'name' => 'Sidebar',
		'id'   => 'side-sidebar-area',
		'description'   => 'This sidebar will be shown after the content.',
		'before_widget' => '<div class="post-article">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title">',
		'after_title'   => '</h3>'
	
	));

	register_sidebar(array(
	
		'name' => 'Home Sidebar',
		'id'   => 'home-sidebar-area',
		'description'   => 'This sidebar will be shown in the homepage.',
		'before_widget' => '<div class="post-article">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title">',
		'after_title'   => '</h3>'
	
	));

	register_sidebar(array(
	
		'name' => 'Category Sidebar',
		'id'   => 'category-sidebar-area',
		'description'   => 'This sidebar will be shown at the side of content.',
		'before_widget' => '<div class="post-article">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title">',
		'after_title'   => '</h3>'
	
	));

	register_sidebar(array(
	
		'name' => 'Search Sidebar',
		'id'   => 'search-sidebar-area',
		'before_widget' => '<div class="post-article">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title">',
		'after_title'   => '</h3>'
	
	));

	$wip_sidebars = get_option(wip_themename());
	 
	if((isset($wip_sidebars["wip_sidebars"]))) :
	 
	 foreach ($wip_sidebars["wip_sidebars"] as $sidebar ) {
	
	 $sidebar_type = explode("_", $sidebar);
	 
	 register_sidebar(array(
		'name' => $sidebar_type[1],
		'id'   => $sidebar_type[0]."_".str_replace(" ","",strtolower($sidebar_type[1])),
		'description'   => $sidebar_type[0]."_".str_replace(" ","",strtolower($sidebar_type[1])),
		'before_widget' => '<div class="post-article">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="title">',
		'after_title'   => '</h3>'
		));
	 }
	 
	 endif;

?>