<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Blog" <?php language_attributes(); ?>>
<head>
   
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<?php if (wip_setting('wip_custom_favicon')) : ?>
	<link rel="shortcut icon" href="<?php echo wip_setting('wip_custom_favicon'); ?>"/>
<?php endif; ?>

<title><?php wp_title( '|', true, 'right' ); ?></title>


<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.2.1/css/bootstrap-combined.min.css" rel="stylesheet">	

<link media="all" rel="stylesheet" href="http://tenscores.com/css/all.css">

<script src="http://tenscores.com/js/jquery-1.8.3.min.js"></script>

<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script> 

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />


<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.2, user-scalable=yes" />

<!--[if IE 8]>
    <script src="<?php echo get_template_directory_uri(); ?>/scripts/html5.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/scripts/selectivizr-min.js" type="text/javascript"></script>
<![endif]-->

    <!-- Google Analytics (Universal) -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-5126444-17', 'auto');
      ga('require', 'displayfeatures');
      ga('send', 'pageview');

    </script>
    <!-- Analytics -->

<!-- Heap Analytics -->
<script type="text/javascript">
  window.heap=window.heap||[];heap.load=function(a){window._heapid=a;var b=document.createElement("script");b.type="text/javascript",b.async=!0,b.src=("https:"===document.location.protocol?"https:":"http:")+"//cdn.heapanalytics.com/js/heap.js";var c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c);var d=function(a){return function(){heap.push([a].concat(Array.prototype.slice.call(arguments,0)))}},e=["identify","track"];for(var f=0;f<e.length;f++)heap[e[f]]=d(e[f])};
  heap.load("781427032");
</script>


<?php do_action( 'wip_head' ); ?>

</head>

<body <?php body_class(); ?> id="daily">

<?php include '../includes/nav-bar-2.php'; ?>

<div style="height: 56px; background: white;"></div>

<header id="header" style="display:none;">

    <div class="container">
        <div class="row">
        
        	<div class="col-md-12">
            
            	<div id="logo">
                
            	<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name') ?>">
                        
                	<?php 
					   				
                    	if ( (wip_setting('wip_custom_logo')) && (wip_setting('wip_view_custom_logo') == "on")):
                        	echo "<img src='".wip_setting('wip_custom_logo')."' alt='logo'>"; 
                        else: 
                        	echo "<img src='".get_template_directory_uri()."/images/logo/logo.png' alt='logo'>"; 
                        endif; 
						
					?>
                            
                </a>
                
                </div>
                
		<div class="subscribe">



<!-- Begin MailChimp Signup Form -->
<form action="//tenscores.us5.list-manage.com/subscribe/post?u=70e48df660281be38b3086bf1&amp;id=63f07d6e7b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	<input type="email" placeholder="Enter your email" name="EMAIL" class="input-text">
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_70e48df660281be38b3086bf1_63f07d6e7b" tabindex="-1" value=""></div>
    <input type="submit" value="Get A Weekly Digest" name="subscribe" class="input-submit">
</form>
<!--End mc_embed_signup-->




		</div>
                <div class="navigation"><i class="fa fa-bars"></i> </div>
				
                <div class="clear"></div>
            
            </div>
            
        </div>
    </div>
</header>



<div class="mobile-menu-container">
	<button class="menu-toggle">Menu</button>
</div>

<?php get_sidebar(); ?>

<div id="wrapper">