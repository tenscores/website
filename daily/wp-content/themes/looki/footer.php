<footer id="footer">
	<div class="container">
		<div class="row" >
             
			<div class="col-md-12" >
               
                <div class="copyright left">
                
					<?php if (wip_setting('wip_copyright_text')): ?>
                    <p> <?php echo stripslashes(wip_setting('wip_copyright_text')); ?> </p>
                    <?php else: ?>
                    <p> Copyright <?php echo get_bloginfo("name"); ?> <?php echo date("Y"); ?> </p>
                    <?php endif; ?>
                    
				</div>
            
                <div class="back-to-top"><i class="fa fa-chevron-up"></i> </div>
                
                <div class="clear"></div>
                
			</div>
                
		</div>
	</div>
</footer>

</div>

<?php wp_footer() ?>  


<script type="text/javascript">    	(function($) {		$('.menu-toggle').bind('click', function() {			$('.nav').toggleClass('toggled-on');		});	})(jQuery);</script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54249781255ad5ef" async></script>
</body>

</html>