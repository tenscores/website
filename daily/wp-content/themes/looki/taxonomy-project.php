<?php get_header(); ?>

<section id="subheader">
	
    <div class="container">
    	
        <div class="row">
            
            <div class="col-md-12">
            
            	<p class="left"> <?php echo $wp_query->queried_object->name; ?> </p>

                <?php
                
                    $terms = get_terms('project');
				   
				    $count = count($terms); 
				    
					if(!empty($terms)) {
					
                ?>

                <div class="skills right">
                
                <div class="views"> <?php _e( 'View projects ',"wip" ) ?> <i class="fa fa-chevron-down"></i> </div>
                
                <ul class="filter" style="list-style:none; padding:0; margin:0">
                
				<?php
				   
					foreach ($terms  as $term) {
						
						if ($wp_query->get_queried_object_id() == $term->term_id ) { $class="class='active'"; } else { $class=""; }
							
						$term_list  .= '<li '.$class.' style="display:block"><a href="'.get_term_link($term).'">' . $term->name .' </a></li> ';
                    }
						
                        echo $term_list; 
				?>

                </ul>
                
                </div>

                <?php } ?>
                
				<div class="clear"></div>

            </div>   
                 
		</div>
        
    </div>
    
</section>

<div class="container content">

	<div class="row" id="masonry">
                
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
           
			<article <?php post_class(array('post-container',wip_template('wip_portfolio_layout') )); ?>>
				
				<?php 
				
					wip_get_title(); 
					
					if ( ( !wip_postmeta('wip_content_type')) || ( wip_postmeta('wip_content_type') == "standard" ) ) {
						wip_thumbnail('blog'); 
					} else if ( wip_postmeta('wip_content_type') == "video" )  {
						wip_video(); 
					} else if ( wip_postmeta('wip_content_type') == "gallery" )  {
						wip_flexslider(); 
					}
				
				?>

            </article>

		<?php endwhile; ?>
        
		<?php else: ?>

			<article class="post-container col-md-12">
             
                <div class="post-article">
                
                    <h1><?php _e( 'Not found',"wip" ) ?></h1>  
                             
                    <p><?php _e( 'Sorry, no items matched into ',"wip" ); echo ": ".$wp_query->queried_object->name;; ?></p>

                    <h2> <?php _e( 'What can i do?',"wip" ) ?> </h2>           
    
                    <p> <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name') ?>"> <?php _e( 'Back to the homepage','wip'); ?> </a> </p>
                  
                    <p> <?php _e( 'Make a search, from the below form:','wip'); ?> </p>
                    
                    <section class="contact-form">
                    
                        <form method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                             <input type="text" value="<?php _e( 'Search', 'wip' ) ?>" name="s" id="s" onblur="if (this.value == '') {this.value = '<?php _e( 'Search', 'wip' ) ?>';}" onfocus="if (this.value == '<?php _e( 'Search', 'wip' ) ?>') {this.value = '';}" class="input-search"/>
                             <input type="submit" id="searchsubmit" class="button-search" value="<?php _e( 'Search', 'wip' ) ?>" />
                        </form>
                        
                        <div class="clear"></div>
                        
                    </section>

                </div>	

			</article>

		<?php endif; ?>
                
	</div>
		
	<?php 
		
		if ( wip_setting('wip_portfolio_layout') <> 'col-md-12' ):
			wip_masonry_scripts(); 
		endif;
		
		get_template_part('pagination'); 
		
	?>
		
</div>

<?php get_footer(); ?>