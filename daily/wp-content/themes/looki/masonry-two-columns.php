<?php

	/**
	 * Wp in Progress
	 * 
	 * @author WPinProgress
	 *
	 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
	 * It is also available at this URL: http://www.gnu.org/licenses/gpl-3.0.txt
	 */
	
	/* Template Name: Masonry Two Columns */

	get_header(); 
	wip_header_content();

?> 

<div class="container content">
	
	<?php wip_masonry('col-md-6'); ?>
	<?php wp_reset_query(); ?>
    <?php get_template_part('pagination'); ?>

</div>

<?php get_footer(); ?>