<?php
//VAR SETUP
$logo = get_theme_mod('themolitor_customizer_logo');
$twitter = get_theme_mod('themolitor_customizer_twitter');
$facebook = get_theme_mod('themolitor_customizer_facebook');
$rss = get_theme_mod('themolitor_customizer_rss');
$googleApi = get_theme_mod('themolitor_customizer_google_api');
$googleKeyword = get_theme_mod('themolitor_customizer_google_key');
$linkColor = get_theme_mod('themolitor_customizer_link_color');
$secondarySidebar = get_theme_mod('themolitor_customizer_smallsidebar_onoff');
$responsive = get_theme_mod('themolitor_customizer_responsive_onoff');
$googleAdSense = get_theme_mod('themolitor_customizer_google_adsense');
if($googleAdSense == ''){
	$topAdUrl = get_theme_mod('themolitor_customizer_top_ad_url');
	$topAdImg = get_theme_mod('themolitor_customizer_top_ad_imgurl');
	$newWindowAd = get_theme_mod('themolitor_customizer_newwindow');
}
$customCSS = get_theme_mod('themolitor_customizer_css');
$favicon = get_theme_mod('themolitor_customizer_favicon');

//WordPress VAR setup
$templateUrl = get_template_directory_uri();
$siteUrl = home_url();
$siteName = get_bloginfo('name');
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script src="//cdn.optimizely.com/js/3064490269.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1.0,width=device-width" />
<title><?php wp_title('&laquo;', true, 'right'); ?> ~<?php echo $siteName; ?></title>
<link rel="alternate" type="application/rss+xml" title="<?php echo $siteName; ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php echo $siteName; ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if($favicon) { ?><link rel="icon" href="<?php echo $favicon; ?>" type="image/x-icon" /><?php } ?>
<?php if($googleApi) { echo $googleApi; } ?>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<?php if($secondarySidebar){ ?><link rel="stylesheet" href="<?php echo $templateUrl; ?>/sidebar.css" type="text/css" media="screen" /><?php } ?>
<?php if($responsive){ ?><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/responsive.css" type="text/css" media="screen" /><?php } ?>
<link rel="stylesheet" href="<?php echo $templateUrl; ?>/scripts/prettyPhoto.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $templateUrl; ?>/font-awesome-4.0.3/css/font-awesome.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $templateUrl; ?>/print.css" type="text/css" media="print" />

<!--[if IE]>
<link rel="stylesheet" href="<?php echo $templateUrl; ?>/ie.css" type="text/css" media="screen" />
<![endif]-->

<style>
<?php if($googleKeyword){?>
* {font-family: '<?php echo $googleKeyword;?>', sans-serif;}
<?php } ?>

/*-----------------------------*/
/*--CUSTOM HEADER BACKGROUND--*/
/*---------------------------*/
#header {
	background-image: url("<?php echo header_image(); ?>");
}

/*--FONT COLOR STUFF--*/
.cn_content h2 a:hover,
.catName span,
.shareThis .stButton .chicklets,
.posttitle a:hover,
.entrytitle a:hover,
h1 a:hover,
h2 a:hover,
h3 a:hover,
h4 a:hover,
h5 a:hover,
h6 a:hover,
a {color:<?php echo $linkColor;?>;}

/*--BACKGROUND COLOR STUFF--*/
#commentform input[type="submit"],
input[type="submit"],
.navigation .wp-paginate .current,
.selected:hover,
.selected,
.cn_item:active,
#sidebar .widget_tag_cloud a,
#wp-calendar a,
.cancel-comment-reply a,
.reply a {background-color:<?php echo $linkColor;?>;}

/*--BORDER COLOR STUFF--*/
.selected:after,
.cn_item:active:after {border-color:transparent <?php echo $linkColor;?> transparent transparent;}

/*--CUSTOM CSS STUFF--*/
<?php echo $customCSS;?>
</style>

<?php
wp_enqueue_script('jquery');
wp_head();
if ( is_singular() ) wp_enqueue_script( "comment-reply" );
?>

<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-544ec17b57e3e17e" async="async"></script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P6TPWD');</script>
<!-- End Google Tag Manager -->

</head>

<body <?php body_class();?>>

<div id="topBannerContainer">
	<div id="topBanner">
		<?php if (has_nav_menu( 'topLeft' ) ) { wp_nav_menu(array('depth'=> 1,'theme_location' => 'topLeft', 'container_id' => 'topLeftNav', 'menu_id' => 'leftDropmenu')); }?>
		<?php if (has_nav_menu( 'topRight' ) ) { wp_nav_menu(array('depth'=> 1,'theme_location' => 'topRight', 'container_id' => 'topRightNav', 'menu_id' => 'rightDropmenu')); }?>
	</div><!--end topBanner-->
</div><!--end topBannerContainer-->

<div id="wrapper">

<?php if($googleAdSense){?>
<div class="advertising">
	<?php echo $googleAdSense; ?>
</div><!--end advertisting-->
<?php } elseif($topAdImg && $topAdUrl){?>
<div class="advertising">
	<a <?php if($newWindowAd){ echo 'target="_blank"'; } ?> href="<?php echo $topAdUrl; ?>"><img src="<?php echo $topAdImg; ?>" alt="" /></a>
</div><!--end advertisting-->
<?php } ?>

<div id="header">
	<div id="dateSocial">
		<?php if($twitter || $facebook || $rss){?>
		<div id="socialStuff"><?php _e('Follow Us:','themolitor');?> &nbsp;<?php if($twitter){?><a id="twitterLink" title="<?php _e('Follow Us on Twitter','themolitor');?>" href="<?php echo $twitter;?>"><i class="fa fa-twitter-square"></i></a><?php } if($facebook){?> <a id="facebookLink" title="<?php _e('Like Us on Facebook','themolitor');?>" href="<?php echo $facebook;?>"><i class="fa fa-facebook-square"></i></a><?php } if($rss){?> <a id="rssLink" title="<?php _e('Subscribe via RSS','themolitor');?>" href="<?php echo $rss;?>"><i class="fa fa-rss-square"></i></a><?php } ?></div>
		<?php } ?>
		<?php echo date('l F jS, Y');?>
	</div><!--end dateSocial-->

	<div id="logoTagline">
		<a class="logo" href="<?php echo $siteUrl; ?>">
			<?php if($logo){?>
			<img src="<?php echo $logo; ?>" alt="<?php echo $siteName; ?>" />
			<?php } else { ?>
			<img src="<?php echo $templateUrl; ?>/images/logo.png" alt="<?php echo $siteName; ?>" />
			<?php } ?>
		</a><!--end logo-->
		<h3 id="tagline"><?php echo get_bloginfo('description');?></h3>
	</div><!--end logoTagline-->

	<form method="get" id="headerSearch" action="<?php echo $siteUrl; ?>/">
		<input type="image" src="<?php echo $templateUrl; ?>/images/magglass.png" id="searchBtn" alt="Go" />
		<input type="text" value="<?php _e('Search Site','themolitor');?>" onfocus="this.value=''; this.onfocus=null;" name="s" id="s" />
	</form>

	<?php
	//NORMAL MENU FOR DESKTOP SCREENS
	if (has_nav_menu( 'main' ) ) { wp_nav_menu(array('theme_location' => 'main', 'container_id' => 'navigation', 'menu_id' => 'dropmenu')); }
    ?>

</div><!--end header-->

<?php if(is_home()){ ?>
<div id="featuredItemContainer">
	<?php
	$showPostsInCategory = new WP_Query(); $showPostsInCategory->query('showposts=1');
	if ($showPostsInCategory->have_posts()) :
	while ($showPostsInCategory->have_posts()) : $showPostsInCategory->the_post();
	?>
		<div id="featuredItem">
			<h1 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title();?></a></h1>
			<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('large'); ?></a>
		</div>
	<?php endwhile; endif; ?>
</div><!--end featuredItemContainer-->
<?php } ?>

<?php if( is_category() ) { ?>
	<div id="categoryIntroContainer">
		<h1 class="entry-title">
			<?php single_cat_title(); ?>
		</h1>
	</div>
<?php } ?>

<div id="content">
