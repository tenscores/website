<?php get_header();?>

<div id="main">

	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs();?>

	<div class="listing">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class(); ?>>

		<div class="latest"><?php _e('Latest','themolitor');?></div>
		<h2 class="posttitle"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to ','themolitor'); the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		<p class="metaStuff"><?php _e('By','themolitor');?> <?php the_author_posts_link();?>&nbsp; / &nbsp;<?php echo get_the_date(); ?>&nbsp; / &nbsp;<?php the_category(', '); ?>&nbsp; / &nbsp;<?php comments_popup_link(__('No Comments','themolitor'), __('1 Comment','themolitor'), __('% Comments','themolitor') ); ?></p>

		<a class="thumbLink" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
		<?php the_post_thumbnail(); ?>
		</a>

		<?php the_excerpt(); ?>
		<a class="readMore" href="<?php the_permalink() ?>"><?php _e('Read More','themolitor');?> &rarr;</a>

        <div class="clear"></div>
		</div><!--end post-->

		<?php endwhile;

		get_template_part('navigation');

	else : ?>
		<br />
		<?php if(is_search()){ ?>
		<h2 class="entrytitle"><?php _e('Nothing Found for','themolitor');?> "<?php the_search_query();?>". <?php _e('Try searching again...','themolitor');?></h2>
		<?php get_template_part('searchform');
		} else { ?>
		<h2 class="entrytitle"><?php _e('Nothing Found. Try searching below...','themolitor');?></h2>
		<?php get_template_part('searchform');
		}
	endif; ?>

	</div><!--end listing-->

</div><!--end main-->

<?php
get_sidebar();
get_footer();
?>
