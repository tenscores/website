<?php
///////////////////////
// Localization Support
///////////////////////
load_theme_textdomain( 'themolitor', get_template_directory().'/languages' );
$locale = get_locale();
$locale_file = get_template_directory().'/languages/$locale.php';
if ( is_readable($locale_file) )
    require_once($locale_file);

////////////////////////////
//CUSTOM HEADER IMAGE
////////////////////////////
$headerArgs = array(
	'default-image' => '',
	'header-text'   => false,
	'uploads'       => true
);
add_theme_support( 'custom-header', $headerArgs );

///////////////////////
//CONTENT WIDTH
///////////////////////
if ( ! isset( $content_width ) ) $content_width = 565;

///////////////////////
//CUSTOM USER DATA
///////////////////////
add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>

	<h3><?php _e('Extra profile information','themolitor');?></h3>

	<table class="form-table">

		<tr>
			<th><label for="twitter"><?php _e('Position Title','themolitor');?></label></th>

			<td>
				<input type="text" name="position" id="position" value="<?php echo esc_attr( get_the_author_meta( 'position', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your position title.','themolitor');?></span>
			</td>
		</tr>

	</table>
<?php }

///////////////////////
//SAVE CUSTOM USER DATA
///////////////////////
add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_user_meta( $user_id, 'position', $_POST['position'] );
}

/*
pass Parameter with url
Plugin URI: http://webopius.com/
Description: A plugin to allow parameters to be passed in the URL and recognized by WordPress
Author: Adam Boyse
Version: 1.0
Author URI: http://www.webopius.com/
*/
add_filter('query_vars', 'parameter_queryvars' );
function parameter_queryvars( $qvars )
{
$qvars[] = 'all';
return $qvars;
}

///////////////////////
//EXCLUDE PAGES FROM SEARCH
///////////////////////
if (!is_admin())	{
function SearchFilter($query) {
if ($query->is_search) {
$query->set('post_type', 'post');
}
return $query;
}
add_filter('pre_get_posts','SearchFilter');
}

///////////////////////
//INCLUDE PROTECTED POSTS IN SEARCH
///////////////////////
add_filter( 'posts_search', 'include_password_posts_in_search' );
function include_password_posts_in_search( $search ) {
	global $wpdb;
	if( !is_user_logged_in() ) {
		$pattern = " AND ({$wpdb->prefix}posts.post_password = '')";
		$search = str_replace( $pattern, '', $search );
	}
	return $search;
}

///////////////////////
//REMOVE PROTECT TEXT FROM TITLE
///////////////////////
function the_title_trim($title) {
	$title = esc_attr($title);
	$findthese = array(
		'#Protected:#',
		'#Private:#'
	);
	$replacewith = array(
		'', // What to replace "Protected:" with
		'' // What to replace "Private:" with
	);
	$title = preg_replace($findthese, $replacewith, $title);
	return $title;
}
add_filter('the_title', 'the_title_trim');

///////////////////////
//EXCERPT STUFF FOR PASSWORD PROTECTION
///////////////////////
function display_protected_excerpts($excerpt) {
	global $post;
    if (!empty($post->post_password)) {
    	$output = $post->post_excerpt;
        $output = apply_filters('get_the_excerpt', $output);
        return $output;
    }
    return $excerpt;
}
add_filter('the_excerpt','display_protected_excerpts', 0);

///////////////////////
//PASSWORD PROTECTED FORM STUFF
///////////////////////
function my_password_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$o = '<form class="protected-post-form" action="' . get_option( 'siteurl' ) . '/wp-pass.php" method="post">
	'.__('To view this article, enter the password below:','themolitor').'
	<input name="post_password" id="' . $label . '" type="password" size="20" /><input type="submit" name="Submit" value="' . esc_attr__( 'Unlock','themolitor' ) . '" />
	</form>
	<br />
	';
	return $o;
}
add_filter( 'the_password_form', 'my_password_form' );
///////////////////////
//MISC
///////////////////////
add_theme_support('automatic-feed-links' );

///////////////////////
//BACKGROUND
///////////////////////
add_theme_support( 'custom-background');

///////////////////////
//FEATURED IMAGE SUPPORT
///////////////////////
add_theme_support( 'post-thumbnails', array( 'post' ) );
set_post_thumbnail_size( 300, 224, true );
add_image_size( 'slider',260 ,194, true );

///////////////////////
//ADD MENU SUPPORT
///////////////////////
add_theme_support( 'menus' );
register_nav_menu('main', __('Main Navigation Menu','themolitor'));
register_nav_menu('topLeft',  __('Top Left Section','themolitor'));
register_nav_menu('topRight',  __('Top Right Section','themolitor'));
register_nav_menu('footer',  __('Footer Menu','themolitor'));



///////////////////////
//SIDEBAR GENERATOR
///////////////////////
if ( function_exists('register_sidebar') )
register_sidebar(array('name'=> __('Primary Widgets','themolitor'),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => '</h2>',
		'description' => __('NOTE: These are the main widgets. The width is 260px.','themolitor')
));
register_sidebar(array('name'=> __('Secondary Widgets','themolitor'),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => '</h2>',
		'description' => __('NOTE: These appear in the footer when there is not enough horizontal space. The width is 180px.','themolitor')
));

///////////////////////
//BREADCRUMBS
///////////////////////
include(TEMPLATEPATH . '/include/breadcrumbs.php');

///////////////////////
//THEME OPTIONS
///////////////////////
include(TEMPLATEPATH . '/include/theme-options.php');

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
