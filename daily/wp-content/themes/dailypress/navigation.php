<?php if(function_exists('wp_paginate')) { wp_paginate(); } else { ?>
<div class="navigation">
	<div id="nextpage" class="pagenav alignright"><?php next_posts_link(__('Next Page &rarr;','themolitor')) ?></div>
	<div id="backpage" class="pagenav alignleft"><?php previous_posts_link(__('&larr; Previous Page','themolitor')) ?></div>
</div><!--end navigation-->
<?php } ?>
