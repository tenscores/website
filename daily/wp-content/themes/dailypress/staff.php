<?php
/*
Template Name: Staff List
*/

get_header(); 
?>

<div id="main">

	<?php 
	if (have_posts()) : while (have_posts()) : the_post();
	if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs();
	?>
	
	<h2 class="entrytitle"><?php the_title();?></h2>
	
		<div class="entry">
		
		<?php 
		the_content(); edit_post_link(__('Edit This Page','themolitor'),'','<br />');		
		echo "<br />";
		
		$blogusers = get_users('orderby=registered&who=authors');
  		foreach ($blogusers as $user) {
    		$displayName = $user->display_name;
    		$userUrl = $user->user_url;
    		$userLogin = $user->user_login;
    		
    		echo get_avatar( $user->ID, 80 );
   			echo '<h2 class="authorName">' . $displayName . '</h2>';
    		if (get_the_author_meta('position', $user->ID)) {echo '<h3 class="positionTitle">'.get_the_author_meta('position',$user->ID).'</h3>';}			
    		if (get_the_author_meta('description', $user->ID)) {echo '<p class="listBio">'.get_the_author_meta('description',$user->ID).'</p>';}
			
    		$args=array(
      			'author' => $user->ID,
      			'post_type' => 'post',
      			'post_status' => 'publish',
      			'posts_per_page' => 5,
      			'caller_get_posts'=> 1
    		);
    		$my_query = null;
    		$my_query = new WP_Query($args);
    		if( $my_query->have_posts() ) {
    			echo '<ul class="columnists">';
      			while ($my_query->have_posts()) : $my_query->the_post(); ?>
       				<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
        		<?php endwhile;
        		echo '<li><a href="'. get_author_posts_url(get_the_author_meta( 'ID' )).'"><strong>'.__('View All','themolitor').' &raquo;</strong></a></li>';
    			echo '</ul>';//END COLUMNISTS
    		}
  			wp_reset_query();
  		}
		?>
		
		</div><!--end entry-->
 
	<div class="clear"></div>
	<?php endwhile; endif; ?>


</div><!--end main-->

<?php 
get_sidebar();
get_footer(); 
?>