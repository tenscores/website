<?php
////////////////////////////
//THEME CUSTOMIZER MENU ITEM
////////////////////////////
function themolitor_customizer_admin() {
    add_theme_page( __('Theme Options','themolitor'),  __('Theme Options','themolitor'), 'edit_theme_options', 'customize.php' ); 
}
add_action ('admin_menu', 'themolitor_customizer_admin');

////////////////////////////
//THEME CUSTOMIZER SETTINGS
////////////////////////////
add_action( 'customize_register', 'themolitor_customizer_register' );

function themolitor_customizer_register($wp_customize) {

	//CREATE TEXTAREA OPTION
	class Example_Customize_Textarea_Control extends WP_Customize_Control {
    	public $type = 'textarea';
 
    	public function render_content() { ?>
        	<label>
        	<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
        	<textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
        	</label>
        <?php }
	}
	
	//CREATE CATEGORY DROP DOWN OPTION
	$options_categories = array();  
	$options_categories_obj = get_categories();
	$options_categories[''] = 'Select a Category';
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	//-------------------------------
	//TITLE & TAGLINE SECTION
	//-------------------------------
	
	//LOGO
	$wp_customize->add_setting( 'themolitor_customizer_logo');
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themolitor_customizer_logo', array(
    	'label'    => __('Logo', 'themolitor'),
    	'section'  => 'title_tagline',
    	'settings' => 'themolitor_customizer_logo',
    	'priority' => 1
	)));
	
	//-------------------------------
	//COLORS SECTION
	//-------------------------------
	
	//LINK COLOR
	$wp_customize->add_setting( 'themolitor_customizer_link_color', array(
		'default' => '#0A67B3'
	));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'themolitor_customizer_link_color', array(
		'label'   => __( 'Link Color', 'themolitor'),
		'section' => 'colors',
		'settings'   => 'themolitor_customizer_link_color'
	)));
	
	//-------------------------------
	//GENERAL SECTION
	//-------------------------------
	
	//ADD GENERAL SECTION
	$wp_customize->add_section( 'themolitor_customizer_general_section', array(
		'title' => __( 'General', 'themolitor' ),
		'priority' => 195
	));
	
	//RESPONSIVE OF/OFF
	$wp_customize->add_setting( 'themolitor_customizer_responsive_onoff', array(
    	'default' => 1
	));
	$wp_customize->add_control( 'themolitor_customizer_responsive_onoff', array(
    	'label' => 'Responsive Layout',
    	'type' => 'checkbox',
    	'section' => 'themolitor_customizer_general_section',
    	'settings' => 'themolitor_customizer_responsive_onoff',
    	'priority' => 1
	));

	
	//DISPLAY SMALL SIDEBAR
	$wp_customize->add_setting( 'themolitor_customizer_smallsidebar_onoff', array(
    	'default' => 1
	));
	$wp_customize->add_control( 'themolitor_customizer_smallsidebar_onoff', array(
    	'label' => 'Display Small Sidebar (when enough space)',
    	'type' => 'checkbox',
    	'section' => 'themolitor_customizer_general_section',
    	'settings' => 'themolitor_customizer_smallsidebar_onoff',
    	'priority' => 4
	));
	
	//DISPLAY YOU MIGHT LIKE BOX
	$wp_customize->add_setting( 'themolitor_customizer_likebox_onoff', array(
    	'default' => 1
	));
	$wp_customize->add_control( 'themolitor_customizer_likebox_onoff', array(
    	'label' => 'Display "You Might Also Like" Box',
    	'type' => 'checkbox',
    	'section' => 'themolitor_customizer_general_section',
    	'settings' => 'themolitor_customizer_likebox_onoff',
    	'priority' => 5
	));
		
	//FAVICON URL
    $wp_customize->add_setting( 'themolitor_customizer_favicon');
	$wp_customize->add_control('themolitor_customizer_favicon', array(
   		'label'   => __( 'Favicon URL (optional)', 'themolitor'),
    	'section' => 'themolitor_customizer_general_section',
    	'settings'   => 'themolitor_customizer_favicon',
    	'type' => 'text',
    	'priority' => 8
	));		
		
	//-------------------------------
	//ADVERTISING SECTION
	//-------------------------------

	//ADD AD SECTION
	$wp_customize->add_section( 'themolitor_customizer_ad_section', array(
		'title' => __( 'Advertising Settings', 'themolitor' ),
		'priority' => 196
	));
	
	//GOOGLE ADSENSE
	$wp_customize->add_setting('themolitor_customizer_google_adsense');
	$wp_customize->add_control( new Example_Customize_Textarea_Control($wp_customize,  'themolitor_customizer_google_adsense', array(
   		'label'   => __( "Google AdSense Code - overrides custom", 'themolitor'),
    	'section' => 'themolitor_customizer_ad_section',
    	'settings'   => 'themolitor_customizer_google_adsense',
    	//'type' => 'text',
    	'priority' => 1
	)));
	
	//TOP BANNER AD LINK URL
    $wp_customize->add_setting( 'themolitor_customizer_top_ad_url');
	$wp_customize->add_control('themolitor_customizer_top_ad_url', array(
   		'label'   => __( 'Custom Top Banner Ad Link URL', 'themolitor'),
    	'section' => 'themolitor_customizer_ad_section',
    	'settings'   => 'themolitor_customizer_top_ad_url',
    	'type' => 'text',
    	'priority' => 2
	));
	
	//TOP BANNER AD IMAGE URL
    $wp_customize->add_setting( 'themolitor_customizer_top_ad_imgurl');
	$wp_customize->add_control('themolitor_customizer_top_ad_imgurl', array(
   		'label'   => __( 'Custom Top Banner Ad Image URL', 'themolitor'),
    	'section' => 'themolitor_customizer_ad_section',
    	'settings'   => 'themolitor_customizer_top_ad_imgurl',
    	'type' => 'text',
    	'priority' => 3
	));
	
	//BOTTOM BANNER AD LINK URL
    $wp_customize->add_setting( 'themolitor_customizer_bottom_ad_url');
	$wp_customize->add_control('themolitor_customizer_bottom_ad_url', array(
   		'label'   => __( 'Custom Bottom Banner Ad Link URL', 'themolitor'),
    	'section' => 'themolitor_customizer_ad_section',
    	'settings'   => 'themolitor_customizer_bottom_ad_url',
    	'type' => 'text',
    	'priority' => 4
	));
	
	//BOTTOM BANNER AD IMAGE URL
    $wp_customize->add_setting( 'themolitor_customizer_bottom_ad_imgurl');
	$wp_customize->add_control('themolitor_customizer_bottom_ad_imgurl', array(
   		'label'   => __( 'Custom Bottom Banner Ad Image URL', 'themolitor'),
    	'section' => 'themolitor_customizer_ad_section',
    	'settings'   => 'themolitor_customizer_bottom_ad_imgurl',
    	'type' => 'text',
    	'priority' => 5
	));
	
	//OPEN LINKS IN NEW WINDOW
	$wp_customize->add_setting( 'themolitor_customizer_newwindow', array(
    	'default' => 1
	));
	$wp_customize->add_control( 'themolitor_customizer_newwindow', array(
    	'label' => 'Open Ad Links in New Window',
    	'type' => 'checkbox',
    	'section' => 'themolitor_customizer_ad_section',
    	'settings' => 'themolitor_customizer_newwindow',
    	'priority' => 6
	));
	
	//-------------------------------
	//SOCIAL SECTION
	//-------------------------------

	//ADD SOCIAL SECTION
	$wp_customize->add_section( 'themolitor_customizer_social_section', array(
		'title' => __( 'Social Links', 'themolitor' ),
		'priority' => 197
	));
	
	//RSS 
    $wp_customize->add_setting( 'themolitor_customizer_rss');
	$wp_customize->add_control('themolitor_customizer_rss', array(
   		'label'   => __( 'RSS Page URL', 'themolitor'),
    	'section' => 'themolitor_customizer_social_section',
    	'settings'   => 'themolitor_customizer_rss',
    	'type' => 'text',
    	'priority' => 2
	));	
	
	//TWITTER
    $wp_customize->add_setting( 'themolitor_customizer_twitter');
	$wp_customize->add_control('themolitor_customizer_twitter', array(
   		'label'   => __( 'Twitter URL', 'themolitor'),
    	'section' => 'themolitor_customizer_social_section',
    	'settings'   => 'themolitor_customizer_twitter',
    	'type' => 'text',
    	'priority' => 3
	));
	
	//FACEBOOK
    $wp_customize->add_setting( 'themolitor_customizer_facebook');
	$wp_customize->add_control('themolitor_customizer_facebook', array(
   		'label'   => __( 'Facebook URL', 'themolitor'),
    	'section' => 'themolitor_customizer_social_section',
    	'settings'   => 'themolitor_customizer_facebook',
    	'type' => 'text',
    	'priority' => 4
	));


	//-------------------------------
	//HOME SECTION
	//-------------------------------
	
	//ADD HOME SECTION
	$wp_customize->add_section( 'themolitor_customizer_home_section', array(
		'title' => __( 'Home Page', 'themolitor' ),
		'priority' => 198
	));
	
	//NUMBER OF ITEMS
    $wp_customize->add_setting( 'themolitor_customizer_items',array(
    	'default' => 8
    ));
	$wp_customize->add_control('themolitor_customizer_items', array(
   		'label'   => __( 'Number of items to display', 'themolitor'),
    	'section' => 'themolitor_customizer_home_section',
    	'settings'   => 'themolitor_customizer_items',
    	'type' => 'text',
    	'priority' => 1
	));
			
	//CATEGORY 1
	$wp_customize->add_setting('themolitor_events_category_1', array(
	    'capability'     => 'edit_theme_options',
	    'type'           => 'option'
	));
	$wp_customize->add_control( 'themolitor_events_category_1', array(
 	   'settings' => 'themolitor_events_category_1',
 	   'label'   => __('Category 1','themolitor'),
   	 	'section' => 'themolitor_customizer_home_section',
   	 	'type'    => 'select',
   	 	'choices' => $options_categories,
   	 	'priority' => 2
	));
	
	//CATEGORY 2
	$wp_customize->add_setting('themolitor_events_category_2', array(
	    'capability'     => 'edit_theme_options',
	    'type'           => 'option'
	));
	$wp_customize->add_control( 'themolitor_events_category_2', array(
 	   'settings' => 'themolitor_events_category_2',
 	   'label'   => __('Category 2','themolitor'),
   	 	'section' => 'themolitor_customizer_home_section',
   	 	'type'    => 'select',
   	 	'choices' => $options_categories,
   	 	'priority' => 3
	));
	
	//CATEGORY 3
	$wp_customize->add_setting('themolitor_events_category_3', array(
	    'capability'     => 'edit_theme_options',
	    'type'           => 'option'
	));
	$wp_customize->add_control( 'themolitor_events_category_3', array(
 	   'settings' => 'themolitor_events_category_3',
 	   'label'   => __('Category 3','themolitor'),
   	 	'section' => 'themolitor_customizer_home_section',
   	 	'type'    => 'select',
   	 	'choices' => $options_categories,
   	 	'priority' => 4
	));
	
	//CATEGORY 4
	$wp_customize->add_setting('themolitor_events_category_4', array(
	    'capability'     => 'edit_theme_options',
	    'type'           => 'option'
	));
	$wp_customize->add_control( 'themolitor_events_category_4', array(
 	   'settings' => 'themolitor_events_category_4',
 	   'label'   => __('Category 4','themolitor'),
   	 	'section' => 'themolitor_customizer_home_section',
   	 	'type'    => 'select',
   	 	'choices' => $options_categories,
   	 	'priority' => 5
	));
	
	//CATEGORY 5
	$wp_customize->add_setting('themolitor_events_category_5', array(
	    'capability'     => 'edit_theme_options',
	    'type'           => 'option'
	));
	$wp_customize->add_control( 'themolitor_events_category_5', array(
 	   'settings' => 'themolitor_events_category_5',
 	   'label'   => __('Category 5','themolitor'),
   	 	'section' => 'themolitor_customizer_home_section',
   	 	'type'    => 'select',
   	 	'choices' => $options_categories,
   	 	'priority' => 6
	));
	
	//CATEGORY 6
	$wp_customize->add_setting('themolitor_events_category_6', array(
	    'capability'     => 'edit_theme_options',
	    'type'           => 'option'
	));
	$wp_customize->add_control( 'themolitor_events_category_6', array(
 	   'settings' => 'themolitor_events_category_6',
 	   'label'   => __('Category 6','themolitor'),
   	 	'section' => 'themolitor_customizer_home_section',
   	 	'type'    => 'select',
   	 	'choices' => $options_categories,
   	 	'priority' => 7
	));
	
	//CATEGORY 7
	$wp_customize->add_setting('themolitor_events_category_7', array(
	    'capability'     => 'edit_theme_options',
	    'type'           => 'option'
	));
	$wp_customize->add_control( 'themolitor_events_category_7', array(
 	   'settings' => 'themolitor_events_category_7',
 	   'label'   => __('Category 7','themolitor'),
   	 	'section' => 'themolitor_customizer_home_section',
   	 	'type'    => 'select',
   	 	'choices' => $options_categories,
   	 	'priority' => 8
	));
	
	//CATEGORY 8
	$wp_customize->add_setting('themolitor_events_category_8', array(
	    'capability'     => 'edit_theme_options',
	    'type'           => 'option'
	));
	$wp_customize->add_control( 'themolitor_events_category_8', array(
 	   'settings' => 'themolitor_events_category_8',
 	   'label'   => __('Category 8','themolitor'),
   	 	'section' => 'themolitor_customizer_home_section',
   	 	'type'    => 'select',
   	 	'choices' => $options_categories,
   	 	'priority' => 9
	));
	
	//CATEGORY 9
	$wp_customize->add_setting('themolitor_events_category_9', array(
	    'capability'     => 'edit_theme_options',
	    'type'           => 'option'
	));
	$wp_customize->add_control( 'themolitor_events_category_9', array(
 	   'settings' => 'themolitor_events_category_9',
 	   'label'   => __('Category 9','themolitor'),
   	 	'section' => 'themolitor_customizer_home_section',
   	 	'type'    => 'select',
   	 	'choices' => $options_categories,
   	 	'priority' => 10
	));
	
	//CATEGORY 10
	$wp_customize->add_setting('themolitor_events_category_10', array(
	    'capability'     => 'edit_theme_options',
	    'type'           => 'option'
	));
	$wp_customize->add_control( 'themolitor_events_category_10', array(
 	   'settings' => 'themolitor_events_category_10',
 	   'label'   => __('Category 10','themolitor'),
   	 	'section' => 'themolitor_customizer_home_section',
   	 	'type'    => 'select',
   	 	'choices' => $options_categories,
   	 	'priority' => 11
	));
		
	//-------------------------------
	//FOOTER SECTION
	//-------------------------------

	//ADD FOOTER SECTION
	$wp_customize->add_section( 'themolitor_customizer_footer_section', array(
		'title' => __( 'Footer', 'themolitor' ),
		'priority' => 199
	));
	
	//FOOTER TEXT
    $wp_customize->add_setting( 'themolitor_customizer_footer',array(
    	'default' => 'Site by <a title="Site" href="http://themolitor.com/portfolio">THE MOLITOR</a>'
    ));
	$wp_customize->add_control('themolitor_customizer_footer', array(
   		'label'   => __( 'Footer Text', 'themolitor'),
    	'section' => 'themolitor_customizer_footer_section',
    	'settings'   => 'themolitor_customizer_footer',
    	'type' => 'text',
    	'priority' => 1
	));
			
	//-------------------------------
	//GOOGLE FONT SECTION
	//-------------------------------

	//ADD GOOGLE FONT SECTION
	$wp_customize->add_section( 'themolitor_customizer_googlefont_section', array(
		'title' => __( 'Google Custom Font', 'themolitor' ),
		'priority' => 200
	));
	
	//GOOGLE API
    $wp_customize->add_setting( 'themolitor_customizer_google_api');
	$wp_customize->add_control('themolitor_customizer_google_api', array(
   		'label'   => __( 'Google Font API Link', 'themolitor'),
    	'section' => 'themolitor_customizer_googlefont_section',
    	'settings'   => 'themolitor_customizer_google_api',
    	'type' => 'text',
    	'priority' => 1
	));
	
	//GOOGLE KEYWORD
    $wp_customize->add_setting( 'themolitor_customizer_google_key');
	$wp_customize->add_control('themolitor_customizer_google_key', array(
   		'label'   => __( 'Google Font Keyword', 'themolitor'),
    	'section' => 'themolitor_customizer_googlefont_section',
    	'settings'   => 'themolitor_customizer_google_key',
    	'type' => 'text',
    	'priority' => 2
	));
		
	//-------------------------------
	//CUSTOM CSS SECTION
	//-------------------------------
	
	//ADD CSS SECTION
	$wp_customize->add_section( 'themolitor_customizer_custom_css', array(
		'title' => __( 'CSS', 'themolitor' ),
		'priority' => 201
	));
			
	//CUSTOM CSS
    $wp_customize->add_setting( 'themolitor_customizer_css');
	$wp_customize->add_control( new Example_Customize_Textarea_Control( $wp_customize, 'themolitor_customizer_css', array(
   		'label'   => __( 'Custom CSS', 'themolitor'),
    	'section' => 'themolitor_customizer_custom_css',
    	'settings'   => 'themolitor_customizer_css'
	)));
	
	//-------------------------------
	//POST FORM SECTION
	//-------------------------------
	
	//ADD POST FORM SECTION
	$wp_customize->add_section( 'themolitor_customizer_form_section', array(
		'title' => __( 'Front-end Submission Form', 'themolitor' ),
		'priority' => 201
	));
	
	//SEND ADMIN EMAIL NOTICE FOR NEW SUBMISSION
	$wp_customize->add_setting( 'themolitor_send_email', array(
    	'default' => 1
	));
	$wp_customize->add_control( 'themolitor_send_email', array(
    	'label' => 'Send email notice',
    	'type' => 'checkbox',
    	'section' => 'themolitor_customizer_form_section',
    	'settings' => 'themolitor_send_email',
    	'priority' => 1
	));
	
	//EMAIL TO USE
	$adminEmail = get_option('admin_email');
	$wp_customize->add_setting( 'themolitor_alt_email',array(
		'default' => $adminEmail
	));
	$wp_customize->add_control( 'themolitor_alt_email', array(
    	'label' => 'Email notice goes to:',
    	'type' => 'text',
    	'section' => 'themolitor_customizer_form_section',
    	'settings' => 'themolitor_alt_email',
    	'priority' => 2
	));
	
	//REMOVE STUFF
	$wp_customize->remove_section('static_front_page');
}