<?php
/*
Template Name: Sitemap
*/

get_header(); 
?>

<div id="main">

	<?php 
	if (have_posts()) : while (have_posts()) : the_post();
	if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs();
	?>
	<h2 class="entrytitle"><?php the_title(); ?></h2>
	
		<div class="entry">
		
		<?php the_content();?><?php edit_post_link(__('Edit This Page','themolitor'),'','<br />'); ?>
		
		<br />
		
		<h2><?php _e('Pages','themolitor');?></h2>
        <ul><?php wp_list_pages("title_li=" ); ?></ul>
        
        <h2><?php _e('Feeds','themolitor');?></h2>
        <ul>
        	<li><a title="<?php _e('Full content','themolitor');?>" href="<?php bloginfo('rss2_url'); ?>"><?php _e('Main RSS','themolitor');?></a></li>
            <li><a title="<?php _e('Comment Feed','themolitor');?>" href="<?php bloginfo('comments_rss2_url'); ?>"><?php _e('Comments RSS','themolitor');?></a></li>
        </ul>
       
       	<h2><?php _e('Categories','themolitor');?></h2>
       	<ul><?php wp_list_categories('title_li=&sort_column=name&optioncount=1&hierarchical=true&feed=RSS'); ?></ul>

		<h2><?php _e('Archives','themolitor');?></h2>
    	<ul>
       		<?php wp_get_archives('type=monthly&show_post_count=true'); ?>
     	</ul>

   		<h2><?php _e('Last 50 Articles','themolitor');?></h2>
     	<ul><?php $archive_query = new WP_Query('showposts=50'); while ($archive_query->have_posts()) : $archive_query->the_post(); ?>
      		<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a> (<?php comments_number('0', '1', '%'); ?>)</li>
            <?php endwhile; ?>
       	</ul>
		
		<div class="clear"></div>			
		</div><!--end entry-->
 
	<br />
	<?php endwhile; endif; ?>

</div><!--end main-->

<?php 
get_sidebar();
get_footer(); 
?>