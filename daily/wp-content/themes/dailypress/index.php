<?php get_header(); ?>

<div id="main">

	<?php
	$showposts = get_theme_mod('themolitor_customizer_items');
	$homeCats = array(
		$cat1 = get_option('themolitor_events_category_1'),
		$cat2 = get_option('themolitor_events_category_2'),
		$cat3 = get_option('themolitor_events_category_3'),
		$cat4 = get_option('themolitor_events_category_4'),
		$cat5 = get_option('themolitor_events_category_5'),
		$cat6 = get_option('themolitor_events_category_6'),
		$cat7 = get_option('themolitor_events_category_7'),
		$cat8 = get_option('themolitor_events_category_8'),
		$cat9 = get_option('themolitor_events_category_9'),
		$cat10 = get_option('themolitor_events_category_10')
	);
	foreach($homeCats as $homeCat) { if($homeCat){
	$catname = get_cat_name( $homeCat );
	?>

	<h2 class="catName"><a title="<?php _e('View All','themolitor');?> <?php echo $catname; ?> <?php _e('Articles','themolitor');?>" href="<?php echo get_category_link($homeCat); ?>"><?php echo $catname; ?> <span><?php _e('View All','themolitor');?> &rarr;</span></a></h2>
	<div class="cn_wrapper">
		<div class="cn_preview">
			<?php
			$showPostsInCategory = new WP_Query(); $showPostsInCategory->query('cat='.$homeCat.'&showposts='.$showposts);
			if ($showPostsInCategory->have_posts()) :
			while ($showPostsInCategory->have_posts()) : $showPostsInCategory->the_post();
			?>
				<div class="cn_content">
					<h2><a href="<?php the_permalink() ?>"><?php the_title();?></a></h2>
					<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('slider'); ?></a>
					<!--
					<p class="metaStuff"><?php _e('By','themolitor');?> <?php the_author_posts_link();?>&nbsp; <?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' '.__('ago','themolitor').''; ?></p>
					<?php the_excerpt(); ?>
					<a class="readMore" href="<?php the_permalink() ?>"><?php _e('Read More','themolitor');?> &rarr;</a>
				  -->
				</div>
			<?php endwhile; endif; ?>
		</div><!--end preview-->
		<ul class="cn_list"></ul>
	</div><!--end wrapper-->

	<?php }} ?>

</div><!--end main-->

<?php
get_sidebar();
get_footer();
?>
