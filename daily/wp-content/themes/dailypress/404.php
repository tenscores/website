<?php get_header(); ?>

<div id="main">
	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs();?>
	<br />
	<h2 class="entrytitle"><?php _e('Error 404 - Not Found. The page you were looking for does not exist. Try searching below...','themolitor')?></h2>
	<?php get_template_part('searchform');?>
</div><!--end main-->

<?php 
get_sidebar();
get_footer(); 
?>