<?php
//VAR SETUP
$secondarySidebar = get_theme_mod('themolitor_customizer_smallsidebar_onoff');
?>

<div id="sidebar">
<ul>
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(__('Primary Widgets','themolitor')) ) : endif; ?>
</ul>
</div><!--end sidebar-->

<?php if($secondarySidebar){ ?>
<div id="secondarySidebar">
<ul>
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(__('Secondary Widgets','themolitor')) ) : endif; ?>
</ul>
<div class="clear"></div>
</div><!--end sidebar-->
<?php } ?>