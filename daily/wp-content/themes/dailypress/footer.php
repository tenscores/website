<?php
//VAR SETUP
$logo = get_theme_mod('themolitor_customizer_logo');
$googleAdSense = get_theme_mod('themolitor_customizer_google_adsense');
if($googleAdSense == ''){
	$bottomAdUrl = get_theme_mod('themolitor_customizer_bottom_ad_url');
	$bottomAdImg = get_theme_mod('themolitor_customizer_bottom_ad_imgurl');
	$newWindowAd = get_theme_mod('themolitor_customizer_newwindow');
}
$footer = get_theme_mod('themolitor_customizer_footer');
$popOut = get_theme_mod('themolitor_customizer_likebox_onoff');

//WordPress VAR setup
$templateUrl = get_template_directory_uri();
$siteUrl = home_url();
$siteName = get_bloginfo('name');
?>

<div class="clear"></div>
</div><!--end content-->

<?php if($googleAdSense){?>
<div class="advertising" id="footerAd">
	<?php echo $googleAdSense; ?>
</div><!--end advertisting-->
<?php } elseif($bottomAdImg && $bottomAdUrl){?>
<div class="advertising" id="footerAd">
	<a <?php if($newWindowAd){ echo 'target="_blank"'; } ?> href="<?php echo $bottomAdUrl; ?>"><img src="<?php echo $bottomAdImg; ?>" alt="" /></a>
</div><!--end advertisting-->
<?php } ?>

<div id="footer">
	<?php if (has_nav_menu( 'footer' ) ) { wp_nav_menu(array('depth'=> 1,'theme_location' => 'footer', 'container_id' => 'footerNav', 'menu_id' => 'footerDropmenu')); }?>
	<div id="copyright">
		<?php if($logo){?><a class="logo" href="<?php echo $siteUrl; ?>"><img src="<?php echo $logo; ?>" alt="<?php echo $siteName; ?>" /></a><?php } ?> &copy; <?php echo date("Y "); echo $siteName; ?>. <?php echo $footer;?>
	</div>
</div><!--end footer-->

</div><!--end wrapper-->

<?php if(is_single() && $popOut){
//YOU MIGHT LIKE BOX
$categories = get_the_category($post->ID);
if ($categories) {
	$category_ids = array();
	foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

	$args=array(
		'category__in' => $category_ids,
		'post__not_in' => array($post->ID),
		'showposts'=>1, // Number of related posts that will be shown.
		'caller_get_posts'=>1
	);
	$my_query = new wp_query($args);
	if( $my_query->have_posts() ) {
		echo '<div id="slidebox">';
		echo '<a class="close"></a>';
		while ($my_query->have_posts()) {
			$my_query->the_post();
		?>
			<p id="moreCat"><?php _e('You might also like...','themolitor');?></p>
			<a class="moreThumb" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail(); ?>
			</a>
    		<h2><a title="<?php _e('Permanent Link to ','themolitor'); the_title_attribute(); ?>" href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
    		<a class="more" title="<?php _e('Permanent Link to ','themolitor'); the_title_attribute(); ?>" href="<?php the_permalink() ?>"><?php _e('Read More','themolitor');?> &rarr;</a>
		<?php
		}
		echo '</div>';
	}
}
}?>

<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/prettyphoto.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/sticky.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/respond.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/custom.js"></script>
<?php if(is_single() && $popOut){?>
<script>
jQuery.noConflict(); jQuery(document).ready(function(){

//VAR SETUP
var theWindow = jQuery(window);

//YOU MIGHT ALSO LIKE BOX STUFF
theWindow.scroll(function(){
	var distanceTop = jQuery('#authorBio').offset().top - theWindow.height() + 300,
		slideBox = jQuery('#slidebox');

	if(theWindow.scrollTop() > distanceTop){
		slideBox.animate({'right':'0px'},300);
	} else {
		slideBox.stop(true).animate({'right':'-450px'},100);
	}
});
jQuery('#slidebox .close').bind('click',function(){
	jQuery(this).parent().remove();
});

});
</script>
<?php
}
wp_footer();
?>




</body>
</html>
