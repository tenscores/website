//MOLITOR SCRIPTS
this.molitorscripts = function () {

	var theWindow = jQuery(window);

	//////////////////
	//ipad and iphone
	//////////////////
	var deviceAgent = navigator.userAgent.toLowerCase(),
    	agentID = deviceAgent.match(/(iphone|ipod|ipad)/);
    
    //////////////////
	//STICKY WIDGET
	//////////////////
	jQuery("#sidebar li.widget:last-child,#secondarySidebar li.widget:last-child, #crumbs").sticky({ topSpacing: -4, bottomSpacing: 210, className: 'sticky'});
	
	//////////////////
	//PRETTY PHOTO
	//////////////////
	function prettyPhotoFunc(){
	jQuery("a[href$='jpg'],a[href$='png'],a[href$='gif']").attr({rel: "prettyPhoto"});
	jQuery(".gallery-icon > a[href$='jpg'],.gallery-icon > a[href$='png'],.gallery-icon > a[href$='gif']").attr({rel: "prettyPhoto[pp_gal]"});
	jQuery("a[rel^='prettyPhoto']").prettyPhoto({
		animation_speed: 'normal', // fast/slow/normal 
		opacity: 0.810, // Value betwee 0 and 1 
		show_title: false, // true/false 
		allow_resize: true, // true/false 
		overlay_gallery: false,
		counter_separator_label: ' of ', // The separator for the gallery counter 1 "of" 2 
		//theme: 'light_square', // light_rounded / dark_rounded / light_square / dark_square 
		hideflash: true, // Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto 
		modal: false // If set to true, only the close button will close the window 
	});
	}
	prettyPhotoFunc();

	//////////////////
	//AJAX PAGINATION
	//////////////////
	jQuery(".postPages a").live('click',function() {    
		var url = jQuery(this).attr('href'),
			height = jQuery("#entryContainer").outerHeight(),
			entryTop = jQuery(".posttitle").offset().top -70,
			entry = jQuery(".entry");
				
		jQuery("#entryContainer").css({height:height});	
		
		jQuery("html,body").animate({scrollTop:entryTop},800);
		
    	entry.fadeOut(500,function(){
    		jQuery("#entryContainer").html("<div id='pageLoading'>Loading...</div>").load(url + " .entry",'', function() {
        		entry.hide().fadeIn(500);
        		jQuery(this).css({height:"auto"});
        		prettyPhotoFunc();
       		});
    	});
    	return false;
	});
	
	//////////////////
	//SCROLL TO COMMENTS
	//////////////////
	jQuery('.commentsLink').click(function(){
		var commentTop = jQuery("#commentsection").offset().top + 5 - 40;
		jQuery("html,body").animate({scrollTop:commentTop},800);
		return false;
	});
	
	//////////////////
	//CRUMBS FUNCTION
	//////////////////
	function showCrumbs(){
		jQuery("#crumbs span").each(function() {
			var delayAmount = jQuery(this).index() * 150;
    		jQuery(this).delay(delayAmount).animate({top:"0px"},500);
		});
	}

	//////////////////
	//REMOVE TITLE ATTRIBUTE
	//////////////////
	jQuery("#dropmenu a").removeAttr("title");
	
	//////////////////
	//MENU
	//////////////////
	jQuery("#dropmenu ul a").hover(function(){
		jQuery(this).stop(true,true).animate({paddingLeft:"5px"},100);
	},function(){
		jQuery(this).stop(true,true).animate({paddingLeft:"0px"},200);
	});	
	
	////////////////
	//RESPONSIVE MENU
	////////////////
	// Create the dropdown base
	jQuery("<select id='selectMenu'><select />").appendTo("#navigation");

	// Create default option "Go to..."
	jQuery("<option />", {
	   "selected": "selected",
	   "value"   : "",
	   "text"    : "Menu"
	}).appendTo("#navigation select");

	// Populate dropdown with menu items
	jQuery("#dropmenu a, #topLeftNav a, #topRightNav a").removeAttr("title").each(function() {
	 	var el = jQuery(this);
	 	
	 	el.parents('.sub-menu').each(function(){
	 		el.prepend('<span class="navDash">-</span>');
	 	});
	 	
	 	jQuery("<option />", {
	  	   "value"   : el.attr("href"),
	     	"text"    : el.text()
	 	}).appendTo("#navigation select");
	});
	
	// Load url when selected
	jQuery("#selectMenu").change(function() {
  		window.location = jQuery(this).find("option:selected").val();
	});
	
	//////////////////
	//HEADER SEARCH
	//////////////////
    if (agentID) {
    	jQuery("#headerSearch").addClass('mobileOs');
    } else {
    	var headerSearch = jQuery('#headerSearch input[type="text"]');
		jQuery("#headerSearch").hover(function(){
			headerSearch.stop(true,true).fadeIn(100);
		},function(){
			headerSearch.stop(true,true).delay(800).fadeOut(350);
		});		
	}
	
	//////////////////
	//GRAB H1 TAG AND COPY IT INTO ITEM LIST
	//////////////////
	var newsItems = jQuery('.cn_preview').find('.cn_content');
	newsItems.each(function(){
		jQuery(this).find('img,h2').clone().appendTo(jQuery(this).parents('.cn_wrapper').find('.cn_list')).wrapAll('<li class="cn_item">');
	});

	//////////////////
	//CLICKING NEWS LIST ANIMATIONS
	//////////////////
	jQuery('.cn_wrapper').each(function(){
		var cn_list 	= jQuery(this).find('.cn_list'),
			items 		= cn_list.find('.cn_item'),
			cn_preview = jQuery(this).find('.cn_preview'),
			current		= 1;
			
		items.each(function(i){
			var item = jQuery(this);
			item.data('idx',i+1);
			
			item.bind('click',function(){
				if(theWindow.width() >= 550){
			
				var thisItem 		= jQuery(this);
				cn_list.find('.selected').removeClass('selected');
				thisItem.addClass('selected');
				var idx			= jQuery(this).data('idx');
				var newcurrent 	= cn_preview.find('.cn_content:nth-child('+current+')');
				var newnext		= cn_preview.find('.cn_content:nth-child('+idx+')');
				
				/*
				if(idx > current){
					newcurrent.stop().animate({'top':'-550px'},500,function(){
						jQuery(this).css({'top':'550px'});
					});
					newnext.css({'top':'550px'}).stop().animate({'top':'0px'},500);
				}
				else if(idx < current){
					newcurrent.stop().animate({'top':'550px'},500,function(){
						jQuery(this).css({'top':'550px'});
					});
					newnext.css({'top':'-550px'}).stop().animate({'top':'0px'},500);
				}
				*/
				
				newcurrent.stop().hide();
				newnext.stop().fadeIn(150);
				
				current = idx;
				return false;
				}
			});
		});
	});	
	
	//////////////////
	//FORM VALIDATION
	//////////////////
	jQuery("#primaryPostForm").submit(function(event) {  		
	        			
	    var postTest = jQuery('#postTest'),
	    	requiredField = jQuery("#primaryPostForm .required").not('#primaryPostForm #postTest');
	    		
	    //CHECK REQUIRED FIELDS
		requiredField.each(function(){
			var imRequired = jQuery(this);
			if (imRequired.val() === "" || imRequired.val() === "-1") {
				imRequired.css({background:"#fcd1c8"});
				event.preventDefault();
			} else {
				imRequired.css({background:"#fff"});
			}
		});
		
		//CHECK FORM TEST
		if(postTest.val() != "102"){
			postTest.css({background:"#fcd1c8"});
			event.preventDefault();
			alert('Test answer incorrect. Please try again.');
		} else {
			postTest.css({background:"#fff"});
		}
	});

	
	//////////////////
	//WHEN PAGE LOADS
	//////////////////
	theWindow.load(function(){
	
		//SHOW CRUMBS
		jQuery('#crumbs #loading').delay(200).fadeOut(200,function(){
			showCrumbs();
		});
		
		//CLICK FIRST SLIDE
		jQuery('.cn_list').addClass('afterLoad');
		jQuery('.cn_item').each(function(){
			var loadMe = jQuery(this).index();
			jQuery(this).delay(250*loadMe).fadeIn(250,function(){
				//jQuery('.cn_item:first-child').click();
				jQuery('.cn_item:first-child').addClass('selected');
			});
		});
	});
}	
//END MYSCRIPTS
jQuery.noConflict(); jQuery(document).ready(function(){molitorscripts();});