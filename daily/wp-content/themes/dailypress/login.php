<?php
/*
Template Name: Login
*/

get_header(); 
?>

<div id="main">

	<?php 
	if (have_posts()) : while (have_posts()) : the_post();
	if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs();
	if(!is_user_logged_in()) { ?>
	<h2 class="entrytitle"><?php the_title(); ?></h2>
	<?php } else { ?>
	<h2 class="entrytitle"><?php _e('Hi','themolitor');?>, <?php echo($current_user->display_name);?></h2>
	<?php } ?>
	
		<div class="entry">
		
		<?php 
		if(is_user_logged_in()) { //LOGGED IN STUFF
			the_content();
			global $current_user; get_currentuserinfo();?>
			<div id="loginstuff">
				<h3><a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Logout"><?php _e('Logout','themolitor');?></a></h3>
				<h3><a rel="nofollow" href="<?php echo home_url(); ?>/wp-admin/" rel="nofollow"><?php _e('Dashboard','themolitor');?></a></h3>
				<h3><a rel="nofollow" href="<?php echo home_url(); ?>/wp-admin/profile.php" rel="nofollow"><?php _e('Profile Page','themolitor');?></a></h3>
			<?php if($current_user->user_level > 1) { ?>
				<h3><a rel="nofollow" href="<?php echo home_url(); ?>/wp-admin/post-new.php" rel="nofollow"><?php _e('New Article','themolitor');?></a></h3>
				<h3><a rel="nofollow" href="<?php echo home_url(); ?>/wp-admin/post-new.php?post_type=page" rel="nofollow"><?php _e('New Page','themolitor');?></a></h3>
			<?php } if($current_user->user_level > 7) { ?>
				<h3><a rel="nofollow" href="<?php echo home_url(); ?>/wp-admin/widgets.php" rel="nofollow"><?php _e('Manage Widgets','themolitor');?></a></h3>
				<h3><a rel="nofollow" href="<?php echo home_url(); ?>/wp-admin/admin.php?page=option_tree" rel="nofollow"><?php _e('Theme Options','themolitor');?></a></h3>
				<h3><?php edit_post_link(__('Edit This Page','themolitor'),'',''); ?></h3>
			<?php } ?>
			</div><!--end loginstuff-->
		<?php } //END LOGGED IN STUFF
		
		if(!is_user_logged_in()) { //NOT LOGGED IN STUFF?>
			<form action="<?php echo wp_login_url( get_permalink() ); ?>" method="post">
			<p>
			<label for="log"><?php _e('Username','themolitor');?></label><br />
			<input type="text" name="log" id="log" value="<?php echo esc_html(stripslashes($user_login)) ?>" size="22" /><br />
			</p>
			<p>
			<label for="pwd"><?php _e('Password','themolitor');?></label><br />
			<input type="password" name="pwd" id="pwd" size="22" /><br /> 
			</p>
			<input type="submit" name="submit" value="Log in" class="button" />
			</form>		
        
			<?php
			global $wpdb;
			$where = 'WHERE comment_approved = 1 AND user_id <> 0';
			$comment_counts = (array) $wpdb->get_results(
				"SELECT user_id, COUNT( * ) AS total FROM {$wpdb->comments}{$where} GROUP BY user_id", object
			);
			foreach ( $comment_counts as $count ) {
  				$user = get_userdata($count->user_id);
			}
			wp_register('<br />', ' '.__('for this site.','themolitor').' ' . $count->total . ' '.__('Registered.','themolitor').'<br />'); 
		} //END NOT LOGGED IN STUFF?>
						
		</div><!--end entry-->
 
	<div class="clear"></div>
	<?php endwhile; endif; ?>
		

</div><!--end main-->

<?php 
get_sidebar();
get_footer(); 
?>