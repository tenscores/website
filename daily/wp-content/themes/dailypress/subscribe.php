<?php
/*
Template Name: Subscribe
*/

get_header(); 
?>

<div id="main">

	<?php 
	if (have_posts()) : while (have_posts()) : the_post();
	if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs();
	?>
	<h2 class="entrytitle"><?php the_title(); ?></h2>
	
		<div class="entry">
		
		<?php the_content();?><?php edit_post_link(__('Edit This Page','themolitor'),'','<br />'); ?>
		
		<br />
		       
        <ul>
        	<li><a title="<?php _e('Full content','themolitor');?>" href="<?php bloginfo('rss2_url'); ?>"><?php _e('All Articles','themolitor');?> <img src="/wp-includes/images/rss.png" alt="RSS" /></a></li>
            <li><a title="<?php _e('Comment Feed','themolitor');?>" href="<?php bloginfo('comments_rss2_url'); ?>"><?php _e('Comments','themolitor');?>  <img src="/wp-includes/images/rss.png" alt="RSS" /></a></li>
        </ul>
       
       	<ul><?php wp_list_categories('title_li=&sort_column=name&optioncount=0&hierarchical=true&feed=RSS&feed_image=/wp-includes/images/rss.png'); ?></ul>

		
		<div class="clear"></div>			
		</div><!--end entry-->
 
	<br />
	<?php endwhile; endif; ?>

</div><!--end main-->

<?php 
get_sidebar();
get_footer(); 
?>