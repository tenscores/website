<?php
//WordPress VAR setup
$templateUrl = get_template_directory_uri();

get_header();
echo '<div id="main">';
if (have_posts()) : while (have_posts()) : the_post();
if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs();
?>

	<div <?php post_class(); ?>>

		<h1 class="posttitle entry-title"><?php the_title(); ?></h1>

		<p class="metaStuff">
			<?php
			_e('By','themolitor'); echo '&nbsp;<span class="vcard author"><span class="fn">'; the_author_posts_link();
			echo '</span></span>&nbsp; / &nbsp;<span class="date updated">'.get_the_date().'</span>&nbsp; / &nbsp;';
			comments_popup_link(__('No Comments','themolitor'), __('1 Comment','themolitor'), __('% Comments','themolitor'), 'commentsLink');
			edit_post_link(__('Edit Post','themolitor'),'&nbsp; / &nbsp;','');
			?>
		</p><!--end metaStuff-->

		<div id="postExcerpt">
			<?php if(!empty($post->post_excerpt)) { the_excerpt(); }?>
		</div><!--end postExcerpt-->

		<!--SHARE, PRINT, EMAIL BUTTONS-->
        <?php if ( !post_password_required() ) { ?>
        <div class="shareThis" id="topShare">
			<a href="javascript:window.print()" title="<?php _e('Print Article','themolitor');?>"><i class="fa fa-print"></i>&nbsp; <?php _e('Print','themolitor');?></a> &nbsp;&nbsp;&nbsp;
			<a href="mailto:?subject=<?php urlencode(the_title()) ?>&amp;body=<?php urlencode(the_permalink()) ?>" title="<?php _e('Email Article','themolitor');?>"><i class="fa fa-envelope-o"></i>&nbsp; <?php _e('Email','themolitor');?></a>
		</div><!--end shareThis-->
		<?php }?>

		<div id="entryContainer">
		<div class="entry">

		<?php
		//PAGE LINKS
		if ( !post_password_required() ) { wp_link_pages(array('before' => '<p id="topLinks" class="postPages">'.__('Pages:','themolitor').' ', 'after' => '</p>','pagelink' => '<span>%</span>'));}
		echo '<div class="clear"></div>';
		//CONTENT
		the_content();
		//PAGE LINKS
		if ( !post_password_required() ) {wp_link_pages(array('before' => '<p class="postPages">'.__('Pages:','themolitor').' ', 'after' => '</p>','pagelink' => '<span>%</span>'));}
		?>

		<div class="clear"></div>
        </div><!--end entry-->
        </div><!--end entryContainer-->

        <!--SHARE, PRINT, EMAIL BUTTONS-->
        <?php if ( !post_password_required() ) { ?>
        <div class="shareThis" id="bottomShare">
			<a href="javascript:window.print()" title="<?php _e('Print Article','themolitor');?>"><i class="fa fa-print"></i>&nbsp; <?php _e('Print','themolitor');?></a> &nbsp;&nbsp;&nbsp;
			<a href="mailto:?subject=<?php urlencode(the_title()) ?>&amp;body=<?php urlencode(the_permalink()) ?>" title="<?php _e('Email Article','themolitor');?>"><i class="fa fa-envelope-o"></i>&nbsp; <?php _e('Email','themolitor');?></a>
		</div><!--end shareThis-->
		<?php }?>


        <?php if ( !post_password_required() ) { ?>
		<ul id="metaStuff">
			<li><?php _e('Published:','themolitor');?> <?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' '.__('ago on','themolitor').' '; echo get_the_date(); ?></li>
       		<li><?php _e('By:','themolitor');?> <?php the_author_posts_link();?></li>
       		<li><?php _e('Last Modified:','themolitor');?> <?php the_modified_time('F j, Y @ g:i a'); ?></li>
       		<li><?php _e('Filed Under:','themolitor');?> <?php the_category(', '); ?></li>
        	<li><?php the_tags(__('Tagged With: ','themolitor'));?></li>
        </ul><!--end metaStuff-->
        <?php } ?>

        <div id="nextPrevPosts">
        	<p class="alignright"><?php previous_post_link('%link', '<small>'.__('NEXT ARTICLE','themolitor').' &rarr;</small><br />%title', TRUE);?></p>
        	<p><?php next_post_link('%link', '<small>&larr; '.__('PREVIOUS ARTICLE','themolitor').'</small><br />%title ', TRUE); ?></p>
        	<div class="clear"></div>
        </div><!--end nextPrevPosts-->

		<?php if ( !post_password_required() ) { ?>
        <div id="authorBio">
        	<h3><?php _e('About the author','themolitor');?></h3>
        	<?php echo get_avatar( $post->post_author, 56 ); ?>
        	<h4><?php the_author_posts_link();?></h4>
        	<?php
        	if ( get_the_author_meta('position')) { echo '<p class="positionTitle">'. get_the_author_meta('position').'</p>';}
        	if ( get_the_author_meta('description')) { echo '<p class="authorBioP">'. get_the_author_meta('description').'</p>';}
        	?>
			<p id="bioLink"><a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php _e('View all articles by','themolitor');?> <?php the_author_meta('display_name'); ?>  &raquo;</a></p>

		</div><!--end authorBio-->


		<?php
		$tags = wp_get_post_tags($post->ID);
		if($tags){
		?>
      	<div id="relatedPosts">
      		<h3><?php _e('Related Articles','themolitor');?></h3>
			<div id="postHolder">
			<?php
			$orig_post = $post;
			global $post;

			$tag_ids = array();
			foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
			$args=array(
				'tag__in' => $tag_ids,
				'post__not_in' => array($post->ID),
				'posts_per_page'=>3,
				'caller_get_posts'=>1
			);
			$my_query = new wp_query( $args );
			while( $my_query->have_posts() ) {
			$my_query->the_post();
			?>

			<div class="relatedItem">
				<?php if ( has_post_thumbnail() ) { ?>
				<a class="featuredImage" href="<?php the_permalink() ?>">
					<?php the_post_thumbnail('slider'); ?>
				</a>
				<?php } ?>
				<span class="relatedMeta"><?php echo get_the_date(); ?></span>
				<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
			</div><!--end relatedItem-->

			<?php } $post = $orig_post; wp_reset_query();?>
			</div><!--postHolder-->
			<div class="clear"></div>
      	</div><!--end relatedPosts-->
      	<?php }} ?>

      <div id="commentsection">
        <?php comments_template(); ?>
      </div><!--end commentsection-->


	</div><!--end post-->

<?php
endwhile; endif;
echo '</div><!--end main-->';
get_sidebar();
get_footer();
?>
