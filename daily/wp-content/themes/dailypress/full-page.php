<?php 
/*
Template Name: Full Width Page
*/

get_header(); 
?>

<div id="main">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs();?>
	
	<h2 class="entrytitle"><?php the_title(); ?></h2>
	
		<div class="entry">
		
		<?php the_content(); ?>	
		<?php edit_post_link(__('Edit this page','themolitor'),'',''); ?>			
		<br />
								
		<?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:','themolitor').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
		</div>
 
	<div class="clear"></div>
	<?php endwhile; endif; ?>
		
    <div id="commentsection">
	<?php comments_template(); ?>
    </div>

</div><!--end main-->

<?php get_footer(); ?>