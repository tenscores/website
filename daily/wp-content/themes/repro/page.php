<?php 

$tz_image_display = get_option('tz_image_display');

?>

<?php get_header(); ?>

<div id="the_body">
    
        <div class="container_12">
            
            <div class="grid_8" id="archive">
            	
                <?php if (have_posts()) : while (have_posts()) : the_post();?>
                
                <div class="grid_8 alpha omega">
                    
                    <div class="description">
                    
                        <h1><?php the_title(); ?></h1>
                         
                    </div><!--description-->
                    
                    <div class="clear"></div>
                    
                </div><!--grid_8 alpha omeg-->
            	
                <div id="post-<?php the_ID(); ?>" <?php post_class("grid_8 alpha omega"); ?>>
                
                	<div id="content">
                    	
                        <?php if($tz_image_display == 'true') : ?>
                        
                    	<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) : /* if post has post thumbnail */ ?>
        
                        <?php the_post_thumbnail('single-large'); ?>
                        
                        <?php endif; ?>
                        
                        <?php endif; ?>
                        
                        <?php the_content(); ?>
                        <?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:', 'framework').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
                        
                        <div class="clear"></div>
                    
                    </div><!--content-->

                </div><!--grid_8 alpha omega-->
                
                <?php endwhile; endif; ?>
               
               <?php wp_reset_query(); ?>
                                
                <?php comments_template('', true); ?>

            </div><!--grid_8-->
            
            <div class="grid_4">
            
				<?php get_sidebar(); ?>
              
            </div><!--grid_4-->
    
            <div class="clear"></div>
            
        </div><!--container_12-->
    
    </div><!--the_body-->

<?php get_footer(); ?>