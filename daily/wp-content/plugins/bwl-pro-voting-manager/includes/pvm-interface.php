<?php

/*
 * @Description: Display Voting Interface Just below the Post/Page/Custom Post Type Single Content.
 * @Created By: Md Mahbub Alam Khan
 * @Since: 1.0.0
 * @Created At: 30-04-2014 
 * @Last Update: 20-02-2015 
 *  */

function display_pvm_interface( $content ) {

    $post_id = get_the_ID();
    
    $pvm_data = get_option('bwl_pvm_options'); // retrive all options from plugin option panel.
    
    $bwl_pvm_display_status = get_post_meta($post_id, "bwl_pvm_display_status", true);
    
    // retrive the status of display current 
    
    $status = 1;
    
    if ( isset( $pvm_data ["pvmpt_".get_post_type( $post_id )] ) && $pvm_data ["pvmpt_".get_post_type( $post_id )] == 0 ) {
        
        $status = 0;  
        
    }
     
     if ( ! is_singular( get_post_type( $post_id ) ) ) {
        
        return $content;
        
    }
    
    if ( $status == 0 ) {
        
        return $content;
        
    }
    
    /*------------------------------ Voting Range Status: Introduced in version  1.0.7 ---------------------------------*/
    
    // Check Date Status In here.
 
    $bpvm_vote_start_date = get_post_meta($post_id, "bpvm_vote_start_date", true);
    $bpvm_vote_end_date = get_post_meta($post_id, "bpvm_vote_end_date", true);
    $bpvm_current_date = date("m").'/'.date("d").'/'.date("Y");
    
    // voting time is not started yet.
    if ( $bpvm_vote_start_date !="" && $bpvm_vote_end_date !="" && $bpvm_current_date < $bpvm_vote_start_date && $bwl_pvm_display_status == 4 ) {
        $bwl_pvm_display_status = 4;
    }
    
    // Voting time has been closed.
    if ( $bpvm_vote_start_date !="" && $bpvm_vote_end_date !="" && $bpvm_current_date > $bpvm_vote_end_date && $bwl_pvm_display_status == 4  ) {
        $bwl_pvm_display_status = 5;
    }
    
    
    $content .= do_shortcode('[bwl_pvm status=' . $bwl_pvm_display_status .' /]');
    
    return $content;
    
}

add_filter('the_content', 'display_pvm_interface');