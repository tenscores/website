<?php
 
 function bpvm_clean_custom_post_types() {

    $available_bpvm_post_types = get_post_types();
    // Some unset function.
    
    if ( class_exists( 'TribeEvents' ) ) {
        
        $removed_items = array( 'attachment', 'revision', 'nav_menu_item', 'tribe_venue', 'tribe_organizer');
        
    } else {
        
        $removed_items = array( 'attachment', 'revision', 'nav_menu_item');
        
    }

    foreach ($removed_items as $rm_post_types_key => $rm_post_types_vlaue) {

        foreach (array_keys($available_bpvm_post_types, $rm_post_types_vlaue) as $key) {
            unset($available_bpvm_post_types[$key]);
        }
    }

    if ( class_exists( 'TribeEvents' ) ) {
        
        $available_bpvm_post_types = array_merge( array('tribe_events'=>'tribe_events'), $available_bpvm_post_types );
        
    }

    return $available_bpvm_post_types;
    
}


function bpvm_get_all_post_types() {
    
    if ( class_exists( 'TribeEvents' ) ) {
        
        $bpvm_default_post_types = array('post'=> 'post', 'tribe_events'=>'tribe_events');
        
    } else {
        
        $bpvm_default_post_types = array('post'=> 'post');
        
    }
    
    $bpvm_custom_post_types = array_merge( $bpvm_default_post_types, bpvm_clean_custom_post_types() );
    
    return $bpvm_custom_post_types;
    
}

function bpvm_get_custom_column_post_types() {
    
     if ( class_exists( 'TribeEvents' ) ) {
        
        $bpvm_default_post_types = array('posts'=> 'posts', 'tribe_events'=>'tribe_events');
        
    } else {
        
        $bpvm_default_post_types = array('post'=> 'post');
        
    }
    
    
    $bpvm_custom_post_types = array_merge( $bpvm_default_post_types, bpvm_clean_custom_post_types() );
    
    return $bpvm_custom_post_types;
    
}

function bpvm_get_widget_custom_post_types() {
    
    $bpvm_default_post_types = array('post'=> 'post');
    
    $bpvm_custom_post_types = array_merge( $bpvm_default_post_types, bpvm_clean_custom_post_types() );
    
    return $bpvm_custom_post_types;
    
}

/*
 * @Description: Post Share Button.
 * @Created by: Md Mahbub Alam Khan
 * @Since: 1.0.7
 * @Created at: 20-02-2015
 * @Last Update: 20-02-2015
 *  */

add_shortcode('bpvm_share', 'bpvm_share');

function bpvm_share( $atts ) {
    
    extract(shortcode_atts(array(
        'post_id' => ''
    ), $atts));

    $bpvm_post_title = get_the_title( $post_id );
    $bpvm_post_url = get_permalink( $post_id );
    
    return '<br /><span class="bpvm-share-links"> <strong>' .__('Share : ', 'bwl-pro-voting-manager') .'</strong>&nbsp;  

                        <a class="btn-share" href="http://twitter.com/home?status='.$bpvm_post_url.'" title="Tweet It">
                           <i class="fa fa-twitter"></i>
                        </a>

                        <a class="btn-share" href="http://www.facebook.com/sharer.php?u='.$bpvm_post_url.'" title="Share at Facebook">
                             <i class="fa fa-facebook"></i>
                        </a>

                        <a class="btn-share" href="http://plus.google.com/share?url='.$bpvm_post_url.'" title="Share at Google+">
                           <i class="fa fa-google-plus"></i>
                        </a>

                        <a class="btn-share" href="http://pinterest.com/pin/create/button/?url='.$bpvm_post_url.'" title="Share at Pinterest">
                            <i class="fa fa-pinterest"></i>
                        </a>

                        <a class="btn-share" href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.$bpvm_post_url.'" title="Share at LinkedIn">
                             <i class="fa fa-linkedin"></i>
                        </a>

                        <a class="btn-share" href="http://www.tumblr.com/share/link?url='.$bpvm_post_url.'&amp;name='.$bpvm_post_title.'" title="Share at Tumblr">
                            <i class="fa fa-tumblr"></i>
                        </a>

                        <a class="btn-share" href="mailto:?subject='.$bpvm_post_title.'&amp;body='.$bpvm_post_url.'" title="Share via Email">
                             <i class="fa fa-envelope-o"></i>
                        </a>

            </span>';
    
}

function bpvm_change_date_format( $custom_date ) {
    
    $explode_custom_date = explode("/", $custom_date);
    return $explode_custom_date[1].'/'.$explode_custom_date[0].'/'.$explode_custom_date[2];
    
}