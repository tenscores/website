(function($) {
    
    /*------------------------------ Voting Date Range Picker ---------------------------------*/
    
    if ( $("#bpvm_vote_start_date").length && $("#bpvm_vote_end_date").length ) {
        
         var $bwl_pvm_display_status = $("#bwl_pvm_display_status"),
              $bpvm_vote_start_date = $("#bpvm_vote_start_date"),
              $bpvm_vote_end_date = $("#bpvm_vote_end_date"),
              $pvm_date_range_field = $([]).add($bpvm_vote_start_date).add($bpvm_vote_end_date);
              
              
        if ( $bwl_pvm_display_status.val() == 4 ) {
            $pvm_date_range_field.parent('p').show("slow");
        } else {
            $pvm_date_range_field.parent('p').hide("slow");
        }
        
        // On change action

        $bwl_pvm_display_status.on("change",function(){
            
            if ($(this).val() == 4) {
                $pvm_date_range_field.parent('p').show("slow");
            } else {
                $pvm_date_range_field.parent('p').hide("slow");
            }
            
        });
        
        
         $bpvm_vote_start_date.datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function(selectedDate) {

                $bpvm_vote_end_date.datepicker( "option", "minDate", selectedDate );
                
            }
        });
        
        $bpvm_vote_end_date.datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onSelect: function(selectedDate) {
                
                $bpvm_vote_start_date.datepicker({ maxDate: selectedDate});
                
            }
        });
        
        
    }
    
    /*------------------------------Meta Box (Introduced in version 1.0.5)  ---------------------------------*/
    
    if ( $("#bwl_pvm_voting_bar_type").length ) {

        var $bwl_pvm_voting_bar_type = $("#bwl_pvm_voting_bar_type"),
              $bwl_pvm_like_bar_theme = $("#bwl_pvm_like_bar_theme"),
              $bwl_pvm_dislike_bar_theme = $("#bwl_pvm_dislike_bar_theme"),
              $pvm_bar_theme_field = $([]).add($bwl_pvm_like_bar_theme).add($bwl_pvm_dislike_bar_theme);

        if ( $bwl_pvm_voting_bar_type.val() == 1 ) {
            $pvm_bar_theme_field.parent('p').show();
        } else {
            $pvm_bar_theme_field.parent('p').hide();
        }
        
        // On change action

        $bwl_pvm_voting_bar_type.on("change",function(){
            
            if ( $(this).val() == 1 ) {
                $pvm_bar_theme_field.parent('p').show();
            } else {
                $pvm_bar_theme_field.parent('p').hide();
            }
            
        })

    }
    
    if ( typeof(inlineEditPost) == 'undefined') {
        return '';
    }
    
    // we create a copy of the WP inline edit post function
    var $wp_inline_edit = inlineEditPost.edit;
    
    // and then we overwrite the function with our own code
    inlineEditPost.edit = function(id) {

        // "call" the original WP edit function
        // we don't want to leave WordPress hanging
        $wp_inline_edit.apply(this, arguments);

        // now we take care of our business

        // get the post ID
        
        var $post_id = 0;
        
        if (typeof(id) == 'object')
            
            $post_id = parseInt(this.getId(id));

        if ($post_id > 0) {

            // define the edit row
            var $edit_row = $('#edit-' + $post_id);

            // get the breaking new status.
            
            var bwl_pvm_display_status = $('#bwl_pvm_display_status-' + $post_id).data('status_code');
            
            // populate the release date
            
            $edit_row.find('select[name="bwl_pvm_display_status"]').val( ( bwl_pvm_display_status > 0 ) ? bwl_pvm_display_status : 0 );
            
        }

    };
    
    /*------------------------------ Bulk Edit Settings ---------------------------------*/
    
    $( '#bulk_edit' ).live( 'click', function() {

    // define the bulk edit row
    var $bulk_row = $( '#bulk-edit' );

    // get the selected post ids that are being edited
    var $post_ids = new Array();
    $bulk_row.find( '#bulk-titles' ).children().each( function() {
       $post_ids.push( $( this ).attr( 'id' ).replace( /^(ttle)/i, '' ) );
    });

    // get the $bwl_pvm_display_status
    
    var $bwl_pvm_display_status = $bulk_row.find( 'select[name="bwl_pvm_display_status"]' ).val();
    var $bwl_pvm_like_votes_count = $bulk_row.find( 'input[name="pvm_like_votes_count"]:checked' ).length;
    var $bwl_pvm_dislike_votes_count = $bulk_row.find( 'input[name="pvm_dislike_votes_count"]:checked' ).length;
    
        $bwl_pvm_like_votes_count = ( $bwl_pvm_like_votes_count == 1 ) ? 0 : "";
        $bwl_pvm_dislike_votes_count = ( $bwl_pvm_dislike_votes_count ==  1 ) ? 0 : "";
    
    // save the data
    $.ajax({
       url: ajaxurl, // this is a variable that WordPress has already defined for us
       type: 'POST',
       async: false,
       cache: false,
       data: {
          action: 'manage_wp_posts_using_bulk_edit_bkb', // this is the name of our WP AJAX function that we'll set up next
          post_ids: $post_ids, // and these are the 2 parameters we're passing to our function
          bwl_pvm_display_status: $bwl_pvm_display_status,
          pvm_like_votes_count: $bwl_pvm_like_votes_count,
          pvm_dislike_votes_count: $bwl_pvm_dislike_votes_count
       }
    });

 });
 
})(jQuery);