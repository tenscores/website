<?php

  //include the main class file
  if ( ! class_exists( 'BF_Admin_Page_Class') ) :
    
    require_once("admin-page-class/admin-page-class.php");

 endif;
  
  
  /**
   * configure your admin page
   */
  $config = array(    
    'menu'           => 'bwl-pvm',             //sub page to settings page
    'page_title'     => __('Option Panel','bwl-pro-voting-manager'),       //The name of this page 
    'capability'     => 'manage_options',         // The capability needed to view the page 
    'option_group'   => 'bwl_pvm_options',       //the name of the option to create in the database [do change]
    'id'             => 'bwl_pvm_admin_page',            // meta box id, unique per page[do change]
    'fields'         => array(),            // list of fields (can be added by field arrays)
    'local_images'   => false,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => false          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );  
  
  /**
   * instantiate your admin page
   */
  $options_panel = new BF_Admin_Page_Class($config);
  $options_panel->OpenTabs_container('');
  
  /**
   * define your admin page tabs listing
   */
  $options_panel->TabsListing(array(
    'links' => array(
      'options_1' =>  __('General','bwl-pro-voting-manager'),
      'bpvm_post_type_settings' =>  __('Post Type Settings','bwl-pro-voting-manager'),
      'options_2' =>  __('Theme Options','bwl-pro-voting-manager'),
      'options_4' =>  __('Tipsy Options','bwl-pro-voting-manager'),
      'options_5' =>  __('Voting Filter','bwl-pro-voting-manager'),
      'bpvm_user_role_filter' =>  __('User Role Filter','bwl-pro-voting-manager'),
      'options_3' =>  __('Advanced Options','bwl-pro-voting-manager'),
    )
  ));
  
  /**
   * Open admin page for General Settings
   */
  $options_panel->OpenTab('options_1');

  //title
  $options_panel->Title(__("General Options",'bwl-pro-voting-manager'));

    
    
   //Enable Font Awesome
  $options_panel->addCheckbox('pvm_fontawesome_status',array('name'=> __('Load Font Awesome ','bwl-pro-voting-manager'), 'std' => true, 'desc' => __('If your theme already loaded Font Awesome, then turn it OFF.', 'bwl-pro-voting-manager')));
  
  //Title Of Feedback From
  $options_panel->addText('pvm_feedback_form_title', array('name'=> __('Feedback Form Title','bwl-pro-voting-manager'), 'std'=> __('Tell us how can we improve this post?', 'bwl-pro-voting-manager') , 'desc' => __('Set custom Feedback Form title for dislike vote!', 'bwl-pro-voting-manager')));
  
  //Feedback Email to Admin
  $options_panel->addCheckbox('pvm_feedback_email_status',array('name'=> __('Send Feedback Message To Admin','bwl-pro-voting-manager'), 'std' => true, 'desc' => __('Allow users to submit feedbacks for dislike votes.', 'bwl-pro-voting-manager')));
  
  //Admin Email Address
  $options_panel->addText('pvm_feedback_admin_email', array('name'=> __('Admin Email ','bwl-pro-voting-manager'), 'std'=> get_bloginfo( 'admin_email' ), 'desc' => __('Set a valid Email address to get feedback message notification.', 'bwl-pro-voting-manager')));
  
  //Enable RTL Support
  $options_panel->addCheckbox('pvm_rtrl_support',array('name'=> __('Enable RTL Support? ','bwl-kb'), 'std' => true, 'desc' => __('Load Right to Left support stylesheet for Arabic language.', 'bwl-pro-voting-manager')));

  
  /**
   * Close first tab
   */   
  $options_panel->CloseTab();
  
  /**
   * Open admin page Advance TAB
   */
  $options_panel->OpenTab('bpvm_post_type_settings');

  
  //title
 
  $options_panel->Title(__("Available Post Types:",'bwl-pro-voting-manager'));
  
  
  
  /**
   * To Create a Conditional Block first create an array of fields (just like a repeater block
   * use the same functions as above but add true as a last param
   */
  
  $options_panel->addParagraph(__("You can filter where you want to show automated voting interface.", 'bwl-pro-voting-manager'));

  $available_bpvm_post_types = bpvm_clean_custom_post_types();
 
 foreach( $available_bpvm_post_types as $bpvm_post_type_key=> $bpvm_post_type_value) :
    
    $bpvm_post_type_value = strtolower($bpvm_post_type_value);

    $bpvm_post_type_title = ucfirst ( str_replace('_', ' ', $bpvm_post_type_value) );

    $options_panel->addCheckbox('pvmpt_'.$bpvm_post_type_value,array('name'=> $bpvm_post_type_title, 'std' => true ));
 
 endforeach;
 
  /**
   * Close first tab
   */   
  $options_panel->CloseTab();


  /**
   * Open admin page Second tab
   */
  $options_panel->OpenTab('options_2');
  /**
   * Add fields to your admin page 2nd tab

   */
  //title
  $options_panel->Title(__('Theme Settings','bwl-pro-voting-manager'));
  
  //Enable/Disable Like/Dislike Bar
//  $options_panel->addCheckbox('pvm_like_dislike_bar_status',array('name'=> __('Enable Like/Dislike Bar ','bwl-pro-voting-manager'), 'std' => true, 'desc' => '' ));

  //Like Thumb Icon.

  $options_panel->addSelect('pvm_like_thumb_icon',array('fa-thumbs-o-up'=>'Transparent Thumbs Up', 
                                                                             'fa-thumbs-up'=>'Filled Thumbs Up',
                                                                             'fa-heart-o'=>'Transparent Heart',
                                                                             'fa-heart'=>'Filled Heart',
                                                                             'fa-smile-o'=>'Smile Face',
                                                                             'fa-level-up'=>'Level up',
                                                                             'fa-arrow-circle-up'=>'Circle up',
                                                                             'fa-arrow-up'=>'Arrow up',
                                                                             'fa-angle-up'=>'Angle up',
                                                                             'fa-angle-double-up'=>'Double Angle up'),
          
                                array('name'=> __('Like Thumb Icon','bwl-pro-voting-manager'), 'std'=> array('fa-thumbs-o-up'), 'desc' => __('Select like vote icon.', 'bwl-pro-voting-manager')));
 
  
  //Like Thumb Color
  $options_panel->addColor('pvm_like_thumb_color',array('name'=> __('Like Thumb Color','bwl-pro-voting-manager'), 'std' => '#559900',  'desc' => __('Choose like vote icon color.', 'bwl-pro-voting-manager')));

  //Display share button
   $options_panel->addCheckbox('pvm_disable_share_button',array('name'=> __('Disable Share Button For Like Vote? ','bwl-pro-voting-manager'), 'std' => FALSE, 'desc' => __('You can disable share button for like vote.', 'bwl-pro-voting-manager')));
  
  //Like Custom Icon.
  $pvm_like_conditinal_fields[] = $options_panel->addImage('pvm_custom_like_icon',array('name'=> __('Upload Like Icon ','bwl-pro-voting-manager')),true);
  
 
  //conditinal block
  $options_panel->addCondition('pvm_like_conditinal_fields',
      array(
        'name' => __('Upload Custom Like Icon? ','bwl-pro-voting-manager'),
        'desc' => __('You can upload custom icon for like button. Best size 16x16 PX','bwl-pro-voting-manager'),
        'fields' => $pvm_like_conditinal_fields,
        'std' => false
      ));
  
   //Disable Feedback From For Down Vote
   $options_panel->addCheckbox('pvm_disable_feedback_status',array('name'=> __('Disable Feedback From? ','bwl-pro-voting-manager'), 'std' => FALSE, 'desc' => 'You can disable feedback from for down vote.'));
  
   //Disable Down Vote
   $options_panel->addCheckbox('pvm_disable_down_vote_status',array('name'=> __('Disable Down Vote? ','bwl-pro-voting-manager'), 'std' => FALSE, 'desc' => 'You can disable down voting option.'));
   
  //dislike Thumb Icon.

  $options_panel->addSelect('pvm_dislike_thumb_icon',array('fa-thumbs-o-down'=>'Transparent Thumbs Down', 
                                                                                'fa-thumbs-down'=>'Filled Thumbs Down',
                                                                                'fa-frown-o'=>'Sad Face ',
                                                                                'fa-level-down'=>'Level Down',
                                                                                'fa-arrow-circle-down'=>'Circle Down',
                                                                                'fa-arrow-down'=>'Arrow Down',
                                                                                'fa-angle-down'=>'Angle Down',
                                                                                'fa-angle-double-down'=>'Double Angle Down'),
          
                                array('name'=> __('Dislike Thumb Icon','bwl-pro-voting-manager'), 'std'=> array('fa-thumbs-o-down')));
  
  
  
  //Dislike Thumb field
  $options_panel->addColor('pvm_dislike_thumb_color',array('name'=> __('Dislike Thumb Color ','bwl-pro-voting-manager'), 'std' => '#C9231A',  'desc' => __('Choose dislike vote icon color.', 'bwl-pro-voting-manager')));
  
  //Dislike Custom Icon.
  $pvm_dislike_conditinal_fields[] = $options_panel->addImage('pvm_custom_dislike_icon',array('name'=> __('Upload Dislike Icon ','bwl-pro-voting-manager')),true);

  //conditinal block
  $options_panel->addCondition('pvm_dislike_conditinal_fields',
      array(
        'name' => __('Upload Custom Dislike Icon? ','bwl-pro-voting-manager'),
        'desc' => __('You can upload custom icon for dislike button. Best size 16x16 PX','bwl-pro-voting-manager'),
        'fields' => $pvm_dislike_conditinal_fields,
        'std' => false
      ));
  
  //conditinal block
  $options_panel->addCheckbox('pvm_disable_voting_bar_status',array('name'=> __('Disable Voting Bar? ','bwl-pro-voting-manager'), 'std' => FALSE, 'desc' => __('You can disable display voting bar.','bwl-pro-voting-manager')));

  //Like Bar Color
  $options_panel->addColor('pvm_like_bar_color',array('name'=> __('Like Bar Color','bwl-pro-voting-manager'), 'std' => '#559900',  'desc' => __('Set custom color for like voting bar.','bwl-pro-voting-manager')));
  
 //Dislike Color field
  $options_panel->addColor('pvm_dislike_bar_color',array('name'=> __('Dislike Bar Color ','bwl-pro-voting-manager'), 'std' => '#C9231A',  'desc' => __('Set custom color for dislike voting bar.','bwl-pro-voting-manager')));
  
  /**
   * Close second tab
   */ 
  $options_panel->CloseTab();
  
   /**
   * Open admin page Advance TAB
   */
  $options_panel->OpenTab('options_3');

  
  //title
 
  $options_panel->Title(__("Advance Setting",'bwl-pro-voting-manager'));
 
 $options_panel->addCode('pvm_custom_css',array('name'=> __('Custom CSS ','bwl-pro-voting-manager'), 'syntax' => 'css', 'desc' => __('You can write custom css code in here.','bwl-pro-voting-manager')));
 
 //Auto Update Notification
  $options_panel->addCheckbox('pvm_auto_update_status',array('name'=> __('Auto Update Notification: ','bwl-pro-voting-manager'), 'std' => FALSE, 'desc' => 'If you enable this option then you will get notification while we release new version. <b style="color: #e32e31;">We strongly recommend to take a backup of your language file/custom css code/custom scripts before applying updates.</b>'));
  
  /**
   * Close first tab
   */   
  $options_panel->CloseTab();
  
  
  /**
   * Open admin page for General Settings
   */
  $options_panel->OpenTab('options_4');

  //title
  $options_panel->Title(__("Tool Tip Options",'bwl-pro-voting-manager'));
  
  //Disable Tipsy
  $options_panel->addCheckbox('pvm_tipsy_status',array('name'=> __('Show Tool Tip ','bwl-pro-voting-manager'), 'std' => TRUE, 'desc' => __('You can disable Like/Dislike tooltip text.','bwl-pro-voting-manager')));

  //Tipsy Like Hover Title
  $options_panel->addText('pvm_tipsy_like_title', array('name'=> __('Like Hover Title','bwl-pro-voting-manager'), 'std'=> __('Like The Post', 'bwl-pro-voting-manager') , 'desc' => __('Set custom tooltip text for like button.','bwl-pro-voting-manager')));
  
  //Tipsy Dislike Hover Title
  $options_panel->addText('pvm_tipsy_dislike_title', array('name'=> __('Dislike Hover Title','bwl-pro-voting-manager'), 'std'=> __('Dislike The Post', 'bwl-pro-voting-manager') , 'desc' => __('Set custom tooltip text for dislike button.','bwl-pro-voting-manager')));
  
  //Tipsy Background
  $options_panel->addColor('pvm_tipsy_bg',array('name'=> __('Tool Tip Background','bwl-pro-voting-manager'), 'std' => '#000000',  'desc' => __('Set tooltip background color.','bwl-pro-voting-manager')));

   //Tipsy Background
   $options_panel->addColor('pvm_tipsy_text_color',array('name'=> __('Tool Tip Text Color','bwl-pro-voting-manager'), 'std' => '#FFFFFF',  'desc' => __('Set tooltip text color.','bwl-pro-voting-manager')));
  
  /**
   * Close first tab
   */   
  $options_panel->CloseTab();
  
  
  /**
   * Open admin page for General Settings
   */
  $options_panel->OpenTab('options_5');
  
  //Filter IP Address.
  $options_panel->addCheckbox('pvm_ip_filter_status',array('name'=> __('IP Filter Status: ','bwl-pro-voting-manager'), 'std' => TRUE, 'desc' => 'If you disable this option then your can submit multiple votes from single IP address.'));
  
  //is_numeric
  $options_panel->addText('pvm_vote_interval',
    array(
      'name'   => __('Voting Interval ','bwl-pro-voting-manager'),
      'std'     => '120',
      'desc'   => __("e.g: we set 120 min(2 hours) interval between repeated votes in a same post. <br /><br /><b>Requirement Notes : </b><br /> 1. Turn <b>ON</b>  IP Filter Status.",'bwl-pro-voting-manager'),
      'validate' => array(
          'numeric' => array('param' => '','message' => __("must be number. e.g: 120",'bwl-pro-voting-manager'))
      )
    )
  );
  
  //is_numeric
  $options_panel->addText('pvm_max_vote_submission',
    array(
      'name'     => __('Maximum no of Votes between Interval ','bwl-pro-voting-manager'),
      'std'      => '1',
      'desc'     => __("You can set no of repeat votes that user can submit from same IP address between interval time. <br /><br /> <b>For example</b>- If you set Maximum Vote value <b>3</b> and Voting Interval value <b>120 MIN</b>, then user can submit maximum 3 votes from same IP address within 120 MIN from hisr first vote submission. <br /><br /> <b>Requirement Notes : </b><br /> 1. Turn <b>ON</b>  IP Filter Status <br />  2. Voting Interval Value.",'bwl-pro-voting-manager'),
      'validate' => array(
          'numeric' => array('param' => '','message' => __("must be number. e.g: 1",'bwl-pro-voting-manager'))
      )
    )
  );
  
    /**
   * Close first tab
   */   
  $options_panel->CloseTab();
  
  
  
  
  /**
   * Open admin page for General Settings
   */
  $options_panel->OpenTab('bpvm_user_role_filter');
  
  //Required Login To Submit Vote
    
  $options_panel->addCheckbox('pvm_login_status',array('name'=> __('Login Required? ','bwl-pro-voting-manager'), 'std' => FALSE, 'desc' => __('Allow to submit votes only for registered users and logged in users.','bwl-pro-voting-manager')));
  
  $pvm_role_filter_fields[] = $options_panel->addRoles('pvm_enabled_roles',array('type' => 'checkbox_list' ),array('name'=> __('Available Roles','bwl-pro-voting-manager'), 'desc' => __('Turn ON Roles, those you want to allow for submit votes.','bwl-pro-voting-manager')),true);

  /**
   * Then just add the fields to the repeater block
   */
  //conditinal block 
  $options_panel->addCondition('pvm_role_status',
      array(
        'name'   => __('Enable Role Filter','bwl-pro-voting-manager'),
        'desc'   => '',
        'fields' => $pvm_role_filter_fields,
        'std'    => false
      ));
  
  
    /**
   * Close first tab
   */   
  $options_panel->CloseTab();