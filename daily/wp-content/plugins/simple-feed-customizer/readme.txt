=== Plugin Name ===
Contributors: collizo4sky
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=HAAAMDMXMSP58
Tags: feed, feed cache, RSS, Atom, RSS feed, feedburner
Requires at least: 3.5
Tested up to: 4.2
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add copyright text at the end of each feed item, display feature image and set the cache duration of feed.

== Description ==

Simple Feed Customizer is use in customizing WordPress feed such as adding a read-more link at the end of a Summary feed, adding copyright text at the bottom of each feed item, and including feature image to each and every feed post.

I built this plugin in order to deter scrapers who were constantly a torn in my flesh stealing my blog articles via some [special kind of software]( http://w3guy.com/build-scraper-site-blogger-wordpress/) that rip-off RSS feed.

More information on how this plugin works can be found [Here]( http://w3guy.com/simple-feed-customizer-wordpress-plugin/)

== Installation ==

= Step 1 =

Plugin installation is quite easy.

1. Download zip file and Extract it
2. Upload `simple-feed-customizer` folder to your /wp-content/plugins/ folder.
2. Go to the 'Plugins' page in the menu and activate the plugin.

= Step 2 =

1. Go to WordPress Admin Panel > Setting > Simple Feed Customizer,
2. set your feed copyright text and other options and update it

== Screenshots ==

1. screenshot-1.jpg
2. screenshot-2.png

== Frequently Asked Questions ==

= Can i add a read-more link to the Summary feed article? =

Yes, just add the anchor text you want to use as the read-more text E.g Continue reading... and the plugin will add the HTML link to the article.

More information on how this plugin works can be found at this post on my [blog]( http://w3guy.com/simple-feed-customizer-wordpress-plugin/)

== Changelog ==

= 1.2 =
* Added option to change feature image size

= 1.1 =
* Stable version

= 1.0 =
* Beta version.
