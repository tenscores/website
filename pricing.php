<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" class=" js flexbox canvas canvastext postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache">
  <meta charset="utf-8">
   <title>Pricing starting from $25/month - TenScores</title>
    <meta name="description" content="Plans ranging from $25 to $1000 to accomodated businesses of all sizes. Even yours.">
    <link href="http://tenscores.com/pricing.php" rel="canonical">
    <?php include 'files/includes/meta.php'; ?>
    <?php include 'files/includes/tags.php'; ?>
  </meta>
</head>
<body id="pricing">
  <?php include 'files/includes/nav.php'; ?>
  <article id="content">
    <div class="visual">
      <h1>Simple pricing.</h1>
      <p class="new-pricing-h2">Pay per individual Google Ads account. Unlimited spend. 14 days free trial.</p>
    </div>


    <div id="mc-container">

          <a class="no-hover" href="https://app.tenscores.com/register?lp=pricing">
            <div class="pricing-new slideup">

              <div class="pricing-top">
                <div class="plan-name">
                  <span>SMB</span>
                </div>
                <div class="plan-amount">
                  <span class="large">$25</span>
                  <span class="smaller">/account/month</span>
                </div>
              </div>

              <div class="pricing-bottom">
                <div class="structure-head">
                  <h4><span>Restructuring fee</span> per account</h4>
                  <p>Required on all accounts to identify adgroup structure problems when they occur.
                  </p>
                </div>
                <div class="structure-details">
                  <div class="structure-plan">
                    <span class="structure-quantity">Up to 1,000 enabled keywords</span>
                    <span class="structure-price free">Free</span>
                  </div>
                  <div class="structure-plan">
                    <span class="structure-quantity">Up to 5,000 enabled keywords</span>
                    <span class="structure-price">+$19/mo</span>
                  </div>
                  <div class="structure-plan">
                    <span class="structure-quantity">Up to 10,000 enabled keywords</span>
                    <span class="structure-price">+$99/mo</span>
                  </div>
                  <div class="structure-plan">
                    <span class="structure-quantity more">
                      Got an account with more keywords? <br />Send us an email at
                      <span class="email">support@tenscores.com</span>
                    </span>
                  </div>
                </div>
              </div>

            </div>
          </a>



          <div class="container">

            <a class="btn btn-in btn-danger btn-new-pricing" href="https://app.tenscores.com/register?lp=pricing&ft=1">Try it free for 14 days</a>
            <h3 class="title">Your restructuring fee will be automatically calculated on the payment page. <a class="blue" href="https://app.tenscores.com/register?lp=pricing&ft=1">Click here to get started.</a></h3>
          </div>

    </div>



  </article>
    	<div id="press" style="display:none;">
		<div id="inner-press">
			<!--
			<img src="images/press/techvibes-50.gif" alt="Tech Vibes">
			<img src="images/press/lapresse-50.gif"  alt="La Presse">
			<img src="images/press/next-montreal-50.gif"  alt="Next Montreal">
			<img src="images/press/metro-50.gif"  alt="Metro">
		  -->
		  <span style="margin-top: 15px; display:inline-block;"><strong>Featured on</strong></span>
			<img src="files/sej-logo.png" width="232" height="50" alt="Search Engine Journal">
			<img src="files/searchengineland-50.gif" width="215" height="50" alt="Search Engine Land">
	    </div>
	</div>

  <?php include 'files/includes/footer.php'; ?>

</body>
</html>
