<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tenscores');

/** MySQL database username */
define('DB_USER', 'b783ee4f36a286');

/** MySQL database password */
define('DB_PASSWORD', 'd58c697b');

/** MySQL hostname */
define('DB_HOST', 'us-cdbr-azure-east-a.cloudapp.net');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Temp limit */
define('WP_MEMORY_LIMIT', '256M');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?-X<D7S}gWoZ0-R%b7mU$028w;{^QC$B)a6SD4R{e4<I`:&I!j<z0jE__ |6}0H%');
define('SECURE_AUTH_KEY',  'p+4m6V}`,#uBPk~%j(q lgq^=b}b)3f+ELr#wYz:$;-&!$!]2|X(Hy3`q@.bm 00');
define('LOGGED_IN_KEY',    '(+5UH!Sx&^$~?6?Sd3U-b)^w>rX:5v/-X:BON1E(7|=~q,;~}.cp6t;AaIn:oF/%');
define('NONCE_KEY',        'j%4-Tv<[m&/I38aL@bW}w&i1Y|C[ .5B&bQ|8|}JO(ia[DZX=>`}^po/(oU=8jqE');
define('AUTH_SALT',        'uLJS5)KnH7.4t3~!=z*qYZ4|_cGdtIhEb6UQ}Sb6{NN+tVOjMFl!AV^&OAoad5GX');
define('SECURE_AUTH_SALT', 'a5S6TCd*ctkdPjBNgW:/[--t6:Xy;H/kfA]O-#l`m8XIvW6,)k5QawyDXWPl#l6H');
define('LOGGED_IN_SALT',   '3X^NK=NpWX^0YOi(c^P|On/-&3U{zogd-R(6EaK-@(>|_I&Sez29rF?u0YlU4:U ');
define('NONCE_SALT',       '9A1N?E|`kK#u^mz]D$8c^7fj=*}WWM%?9YdY<F9cTHn:-^-{e&`3eiB#oP2hav4Q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_blog4_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
