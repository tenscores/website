
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>What Is Quality Score? It Sets Ad Prices On Google [Infographic]</title>
	<meta name"description" content="What is quality score? A metric used to determine how much you'll pay for each click when advertising on Google.">
	<link media="all" rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script> 
	<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
	<script src="js/bootstrap-popover.js"></script> 
	<script type="text/javascript" src="js/main.js"></script>
	<link media="all" rel="stylesheet" type="text/css" href="css/all.css" />

	
	<script>  
		$(function ()  
		{ $( '#bar-one' ).popover();
		  $( '#bar-two' ).popover();
		  $( '#bar-three' ).popover();
		  $( '#bar-four' ).popover();
		  $( '#bar-five' ).popover();
		  $( '#bar-six' ).popover();
		  $( '#bar-seven' ).popover();
		  $( '#bar-eight' ).popover();
		  $( '#bar-nine' ).popover();
		  $( '#bar-last' ).popover();
		  $( '#cpc-equation' ).popover();
		  $( '#adrank-equation' ).popover();
		  $( '#rankbar-one' ).popover();
		  $( '#rankbar-two' ).popover();
		  $( '#rankbar-three' ).popover();
		  $( '#rankbar-four' ).popover();
		  $( '#rankbar-five' ).popover();
		  $( '#rankbar-six' ).popover();
		  $( '#rankbar-seven' ).popover();
		  $( '#rankbar-eight' ).popover();
		  $( '#rankbar-nine' ).popover();
		  $( '#rankbar-last' ).popover();
		  $( '#embed' ).popover();
		});  
	</script>
	
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-5126444-17']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
</head>
<body>
<!-- AddThis Button #1 BEGIN -->
<div class="addthis_toolbox addthis_floating_style addthis_counter_style" 
	 style="
	 left:20px;
	 top:100px; 
	 background:none;
	 "> 
	    <a class="addthis_button_facebook_like" fb:like:layout="box_count"></a>
		<a class="addthis_button_tweet" tw:count="vertical"></a>
		<a class="addthis_button_linkedin_counter" li:counter="top"></a>
		<a class="addthis_button_google_plusone" g:plusone:size="tall"></a>
		<a class="addthis_counter"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50f25e53735f346f"></script>
<!-- AddThis Button END -->
<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=christianziza"></script>
<script type="text/javascript">
  addthis.layers({
    'theme' : 'transparent', 
    'follow' : {
      'services' : [
        {'service': 'facebook', 'id': 'tenscores'},
        {'service': 'twitter', 'id': 'tenscores'},
        {'service': 'google_follow', 'id': '117096530344301301599'}
      ]
    }   
  });
</script>
<!-- AddThis Smart Layers END -->

	<header id="header">
		<div class="navbar navbar-fixed-top">
		  <div class="navbar-inner">
		    <a class="brand" href="/"><img src="http://tenscores.com/images/logo-v2.png" border="0" alt="Tenscores" align="middle" height="25"></a>
		    <ul class="nav">
		      <li id="home"><a href="http://tenscores.com">Home</a></li>
		      <li id="tour"><a href="http://tenscores.com/tour.php">Tour</a></li>
		      <li id="benefits"><a href="http://tenscores.com/benefits.php">Benefits</a></li>
		      <li id="pricing"><a href="http://tenscores.com/pricing.php">Pricing</a></li>
		      <li id="blog"><a href="http://tenscores.com/blog">Blog</a></li>
		      <li id="signup"><a class="btn btn-signup btn-primary"href="https://account.tenscores.com/register?lp=qs-infographic">Signup</a></li>
		      <li id="login"><a class="btn btn-login" href="https://account.tenscores.com/login">Login</a></li>
		    </ul>
		  </div>
		</div>
	</header>



	<div id="wrapper">
		<div class="wrapper-holder">
			<div class="wrapper-frame">
				<div class="top-area">
					<img src="images/img-1.png" class="img-top" width="50" height="51" alt="Tenscores" />
					
					<h1><span class="mark">WHAT IS</span>QUALITY SCORE?</h1>
					<p>A metric used by Google to determine if your ad is eligible to be shown in the ads section of the search results, at what position it will be shown and how much you'll pay for each click.</p>
				</div>
				<div class="img-area" style="display:none;">
					<div class="img-holder">
<iframe src="http://player.vimeo.com/video/59311012?title=0&byline=0&portrait=0" width="411" height="240" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>					
 					</div>
				</div>
				<div class="area">
				
					<div class="arrow">arrow</div>
		
					<h2><span class="mark">WHY DOES IT</span>  REALLY MATTER?</h2>
					<p>
					Your ad's cost-per-click (CPC) <br />depends on it.
					</p>
					
					<div class="scale-1">
						<div class="cpc-chart">
							<a href="#" 
							    onClick="return false;"
								class="bar one" 
								id="bar-one" 
								rel="popover"  
								data-animation="true" 
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='red'>a=10</span> and <span class='red'>QS=1</span> then <span class='red'>CPC=10</span></div> All other things equal, you'd pay $10 for each click with a QS of 1/10." 
								data-original-title="High Penalty">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar two" 
								id="bar-two" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='red'>a=10</span> and <span class='red'>QS=2</span> then <span class='red'>CPC=5</span></div> All other things equal, you'd pay $5 for each click with a QS of 2/10." 
								data-original-title="High Penalty">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar three" 
								id="bar-three" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='red'>a=10</span> and <span class='red'>QS=3</span> then <span class='red'>CPC=3.33</span></div> All other things equal, you'd pay $3.33 for each click with a QS of 3/10." 
								data-original-title="High Penalty">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar four" 
								id="bar-four" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='orange'>a=10</span> and <span class='orange'>QS=4</span> then <span class='orange'>CPC=2.5</span></div> All other things equal, you'd pay $2.5 for each click with a QS of 4/10. It's getting better." 
								data-original-title="Medium Penalty">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar five" 
								id="bar-five" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='orange'>a=10</span> and <span class='orange'>QS=5</span> then <span class='orange'>CPC=2</span></div> All other things equal, you'd pay $2 for each click with a QS of 5/10." 
								data-original-title="Medium Penalty">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar six" 
								id="bar-six" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='orange'>a=10</span> and <span class='orange'>QS=6</span> then <span class='orange'>CPC=1.67</span></div> All other things equal, you'd pay $1.67 for each click with a QS of 6/10." 
								data-original-title="Medium Penalty">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar seven" 
								id="bar-seven" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='green'>a=10</span> and <span class='green'>QS=7</span> then <span class='green'>CPC=1.42</span></div> All other things equal, you'd pay $1.42 for each click with a QS of 7/10." 
								data-original-title="No Penalty">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar eight" 
								id="bar-eight" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='green'>a=10</span> and <span class='green'>QS=8</span> then <span class='green'>CPC=1.25</span></div> All other things equal, you'd pay $1.25 for each click with a QS of 8/10." 
								data-original-title="Discount">
							</a>
							<a href="#"
							    onClick="return false;" 
								class="bar nine" 
								id="bar-nine" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='green'>a=10</span> and <span class='green'>QS=9</span> then <span class='green'>CPC=1.11</span> </div> All other things equal, you'd pay $1.11 for each click with a QS of 9/10." 
								data-original-title="Discount">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar last" 
								id="bar-last" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='green'>a=10</span> and <span class='green'>QS=10</span> then <span class='green'>CPC=1</span></div> All other things equal, you'd pay just $1 for each click with a QS of 10/10." 
								data-original-title="Discount">
							</a>
						</div>
						<span class="left-info">Cost-Per-Click<br />(CPC)</span>
						<span class="bottom-info">Quality Score (QS)</span>
						<div class="example">
							<a href="#" 
							   class="holder"
							   onClick="return false;"
							   id="cpc-equation" 
							   rel="popover"  
							   data-animation="true"
							   data-placement="bottom" 
							   data-html="true"
							   data-trigger="click"
							   data-content="<span class='formula'>CPC = Cost-Per-Click <br /><br /> a = AdRank of advertiser below <br /> <br />QS = Quality Score <br /><br /> <iframe width='220' height='124' src='http://www.youtube.com/embed/qwuUe5kq_O8?rel=0&start=328' frameborder='0' allowfullscreen></iframe></span>" 
								data-original-title="Cost Of A Click">
								CPC =
								<span class="box">
									<span class="top-example">a</span>
									<span class="bottom-example">QS</span>
								</span>
								<span class="side-arrow"><img src="images/down-arrow.png" /></span>
							</a>
						</div>
					</div>
					<p>Your ad position (Ad Rank) on Google<br /> depends on it. </p>
					
					<div class="scale-2">
					     <div class="adrank-chart">
							<a href="#" 
							    onClick="return false;"
								class="bar one" 
								id="rankbar-one" 
								rel="popover"  
								data-animation="true" 
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='red'>b=2</span> and <span class='red'>QS=1</span> then <span class='red'>AdRank=2</span></div> In an ad auction of 10 advertisers all bidding $2, with a QS of 1/10 your ad would be in the last position (or may not even show at all." 
								data-original-title="Low AdRank">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar two" 
								id="rankbar-two" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='red'>b=2</span> and <span class='red'>QS=2</span> then <span class='red'>AdRank=4</span></div> In an ad auction of 10 advertisers all bidding $2, with a QS of 2/10 your ad would be in the 9th position as it would have the lowest AdRank(or may not even show at all)." 
								data-original-title="Low AdRank">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar three" 
								id="rankbar-three" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='red'>b=2</span> and <span class='red'>QS=3</span> then <span class='red'>AdRank=6</span></div> In an ad auction of 10 advertisers all bidding $2, with a QS of 3/10 your ad would be in the 8th position (or may not show at all)." 
								data-original-title="Low AdRank">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar four" 
								id="rankbar-four" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='orange'>b=2</span> and <span class='orange'>QS=4</span> then <span class='orange'>AdRank=8</span></div> In an ad auction of 10 advertisers all bidding $2, with a QS of 4/10 your ad would be in the 7th position." 
								data-original-title="Medium AdRank">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar five" 
								id="rankbar-five" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='orange'>b=2</span> and <span class='orange'>QS=5</span> then <span class='orange'>AdRank=10</span></div> In an ad auction of 10 advertisers all bidding $2, with a QS of 4/10 your ad would be in the 6th position." 
								data-original-title="Medium AdRank">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar six" 
								id="rankbar-six" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='orange'>b=2</span> and <span class='orange'>QS=6</span> then <span class='orange'>AdRank=12</span></div> In an ad auction of 10 advertisers all bidding $2, with a QS of 6/10 your ad would be in the 5th position." 
								data-original-title="Medium AdRank">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar seven" 
								id="rankbar-seven" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='green'>b=2</span> and <span class='green'>QS=7</span> then <span class='green'>AdRank=14</span></div> In an ad auction of 10 advertisers all bidding $2, with a QS of 7/10 your ad would be in the 4th position." 
								data-original-title="High AdRank">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar eight" 
								id="rankbar-eight" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='green'>b=2</span> and <span class='green'>QS=8</span> then <span class='green'>AdRank=16</span></div> In an ad auction of 10 advertisers all bidding $2, with a QS of 8/10 your ad would be in the 3rd position." 
								data-original-title="High AdRank">
							</a>
							<a href="#"
							    onClick="return false;" 
								class="bar nine" 
								id="rankbar-nine" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='green'>b=2</span> and <span class='green'>QS=9</span> then <span class='green'>AdRank=18</span></div> In an ad auction of 10 advertisers all bidding $2, with a QS of 9/10 your ad would be in the 2nd position as it would have the second highest AdRank." 
								data-original-title="High AdRank">
							</a>
							<a href="#" 
							    onClick="return false;"
								class="bar last" 
								id="rankbar-last" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-content="<div class='popover-math'>With <span class='green'>b=2</span> and <span class='green'>QS=10</span> then <span class='green'>AdRank=20</span></div> In an ad auction of 10 advertisers all bidding $2, with a QS of 10/10 your ad would be in the 1st position as it would have the highest AdRank."
								data-original-title="High AdRank">
							</a>
					     </div>						
						
						<span class="right-info">AdRank</span>
						<span class="bottom-info">Quality Score (QS)</span>
						<div class="example">
							<a href="#" 
							   class="holder"
							   onClick="return false;"
							   id="adrank-equation" 
							   rel="popover"  
							   data-animation="true"
							   data-placement="bottom" 
							   data-html="true"
							   data-trigger="click"
							   data-content="<span class='formula'>AdRank = Metric used to rank ads on Google <br /><br /> b = MaxCPC (what you bid on a keyword) <br /> <br />QS = Quality Score <br /><br /> <iframe width='220' height='124' src='http://www.youtube.com/embed/qwuUe5kq_O8?rel=0&start=257' frameborder='0' allowfullscreen></iframe></span>"  
								data-original-title="AdRank Formula">
								AdRank = b <img src="images/img-operation.png" alt="image description" /> QS
								<span class="side-arrow-2"><img src="images/down-arrow.png" /></span>
							</a>
						</div>
					</div>
				</div>
				
				<div class="arrow">arrow</div>
				
				<div class="area">
				
				<h2><span class="mark">WHERE</span> CAN I FIND IT?</h2>
					<div class="text-holder">
						<p>In campaigns that are targeted on Google Search, each keyword has a Quality Score. But it is hidden by default. Here's how to show it:</p>
						<ol style="text-align:left; margin:0 auto; width:310px;">
						<li>Click the "Keywords" tab in your Adwords account. </li>
						<li>Click "Columns" then "Customize columns". </li>
						<li>In the section that opens up, choose "Attributes". </li>
						<li>Click on the "Add" link in the Quality Score row.</li>
						<li>Click "Apply" to save. You should now have a QS column in your data.</li>
						</ol>
						 
						<div class="adwords-qs"></div>
						
						
					</div>
					
					<br />
					<br />
					<br />
					<br />
					<br />
					
				</div>
				
				<div class="arrow">arrow</div>
				
				<div class="section">
					<h2><span class="mark">WHAT ARE THE FACTORS </span>INFLUENCING IT?</h2>
					<div class="text-holder">
						<p>External factors are the things that happen on your website. Internal factors are what goes on in your Adwords account.</p>
						
					</div>
					<span class="number">1.</span>
					<h3><span>EXTERNAL FACTORS</span> <br /> LANDING PAGE QUALITY <br />& Relevance</h3>
					<p>There are two sides to a good landing page: quality and relevance. "Quality" means that users have a great experience when they reach your website. "Relevance" means they find what was promised in the ad.</p>
			        
			       <div class="LP-Quality">
					<strong class="star-title quality">LP QUALITY</strong>
					<div class="experience-block">
					
						<div class="holder">
							<ul class="pointers">
								<li class="red"></li>
								<li class="yellow"></li>
								<li class="green"></li>
							</ul>
							<div class="box"></div>
						</div>
						<ul class="boxes">
							<li class="blue"></li>
							<li class="red"></li>
							<li class="yellow"></li>
							<li class="green"></li>
						</ul>
						<div class="box-content">
							<div class="square green"></div>
							<div class="text-holder">
								<span class="heading">Amazing Product</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ullamcorper nisl ac mauris convallis luctus.</p>
								<span class="call blue">Call To Action</span>
							</div>
						</div>
						<div class="bottom-block">
							<div class="load"><div class="high blue"></div></div>
							<ul class="nav">
								<li>About</li>
								<li>Contact</li>
								<li>Privacy</li>
								<li>Terms</li>
							</ul>
						</div>
						<span class="info first blue">
							<span class="connect"></span>
							<span><span class="mark">CLEAR PURPOSE</span>(Recommended)</span>
						</span>
						<span class="info second red">
							<span class="connect"></span>
							<span class="mark">HIGH PAGE LOAD SPEED</span>(Rarely an issue)
						</span>
						<span class="info third green">
							<span class="connect"></span>
							<span class="mark">LINKS TO BUSINESS INFO PAGES</span>(Required)
						</span>
						<span class="info fourth yellow">
							<span class="connect"></span>
							<span class="mark"><a href="https://www.google.ca/search?q=crawlable+definition" target="_blank">CRAWLABLE TEXT</a></span>(Required)
						</span>
						<span class="info fifth green">
							<span class="connect"></span>
							<span class="mark">EASY NAVIGATION</span>(Recommended)
						</span>
					</div>
			       </div>
				   <div class="LP-Relevance">	
					<strong class="star-title">LP RELEVANCE</strong>
					<div class="lp-relevance-box">
						<div class ="ad-wrap">
						    <div class="ad">
						    	<span class="ad-title">Amazing Product XYZ</span>
						    	<span class="ad-url">www.productxyz.com</span>
						    	<span class="ad-line1">Find XYZ In Your Favourite</span>
						    	<span class="ad-line2">Colour - 10% Off.</span>
							</div>
							<span class="legend">Ad</span>
						</div>
						<div class ="lp-wrap">
								<!-- LP2 -->
								<div class="experience-block2">
								<div class="holder">
									<ul class="pointers">
										<li class="red"></li>
										<li class="yellow"></li>
										<li class="green"></li>
									</ul>
									<div class="box"></div>
								</div>
								<ul class="boxes">
									<li class="blue"></li>
									<li class="red"></li>
									<li class="yellow"></li>
									<li class="green"></li>
								</ul>
								<div class="box-content">
									<div class="square green"></div>
									<div class="text-holder">
										<span class="heading">Product XYZ</span>
										<p>In your favourite colour, 10% off.</p>
										<span class="call blue">Buy Now</span>
									</div>
								</div>
								<div class="bottom-block">
									<ul class="nav">
										<li>About</li>
										<li>Contact</li>
										<li>Privacy</li>
										<li>Terms</li>
									</ul>
								</div>
							</div>
						    <!--- END LP2 -->							
							<span class="legend">Landing Page</span>
						</div>
						<div class="relevance-connect">
								<div class="inline circle-left"></div>
							    <div class="inline line"></div>
								<div class="inline circle-right"></div>
						</div>
					</div>
				   </div>
				</div>
				<div class="container">
				    <br />
				    <br />
					<span class="number">2.</span>
					<h3><span>INTERNAL FACTORS</span> <br />CLICK-THROUGH RATES<br />& KEYWORD RELEVANCE</h3>
					<div class="text-holder">
							<p>Historical click-through-rates have an overwhelming impact on Quality Score. Users vote on the quality of your ads with their clicks.  </p>
				    </div>
					<strong class="star-title">SEARCH</strong>
					<div class="circle-scale-1">
						<div class="img-holder">
							<div class="img-circle">
								
								<span class="first">80%</span>
								<span class="second">15%</span>
								<span class="third">5%</span>
								
							</div>
							<div class="google-search"><a href="#" onClick="return false">google search</a><p>Google.com and other country specific such as google.ca, google.ru, etc</p></div>
							<ul class="notes">
								<li class="first">Historical CTR Factors</li>
								<li class="second">Relevance Factors</li>
								<li class="third">Other</li>
							</ul>
						</div>
						<div class="holder">
							<div class="box">
								<div class="connect"></div>
								<ul class="history-list">
									<li>
										<a href="#" onClick="return false;" class="history-title blue">Ad/Keyword<br />History</a>
										<div class="bullet-circle first"><span class="blue"></span></div>
										<span class="about">Historical click-through rate (CTR) of the keyword and the matched ad on the Google domain.</span>
									</li>
									<li>
										<a href="#" onClick="return false;" class="history-title ">Account History</a>
										<div class="bullet-circle second"><span class=""></span></div>
										<span class="about">Account history, which is measured by the CTR of all the ads and keywords in your account.</span>
									</li>
									<li>
										<a href="#" onClick="return false;" class="history-title ">URL History</a>
										<div class="bullet-circle third"><span class=""></span></div>
										<span class="about">The historical CTR of the display URLs in the ad group.</span>
									</li>
									<li>
										<a href="#" onClick="return false;" class="history-title ">Geo History</a>
										<div class="bullet-circle fourth"><span class=""></span></div>
										<span class="about">Account's performance (in terms of CTR) in the geographical region where the ad will be shown.</span>
									</li>
								</ul>
								<span class="mark">When there's no history in your account, Google will use other advertisers' historical performance on specific keywords to evaluate what Quality Score you should be granted. It is up to you to exceed performance.</span>
							</div>
							<div class="block">
								<div class="connect"></div>
								<ul class="block-list">
									<li>
										<em class="for-connection"></em>
										<span class="block-text">Relevance of the keyword to the ads in its ad group.</span>
										<div class="bullet-circle first"><span class="yellow"></span></div>
										<a href="#" onClick="return false;" class="keyword-link yellow">Ad/Keyword Relevance</a>
									</li>
									<li class="last">
										<em class="for-connection"></em>
										<span class="block-text">Relevance of the ad to the search query.</span>
										<div class="bullet-circle second"><span class=""></span></div>
										<a href="#" onClick="return false;" class="keyword-link ">Keyword/Query Relevance</a>
									</li>
								</ul>
								<span class="opener" style="display:none;">
									<span class="block-arrow">
										LITTLE SECRET
									</span>
									<span class="drop"><span class="drop-holder"></span></span>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="black-area">
					<div class="holder">
						<a href="#" class="search">SEARCH PARTNERS</a>
						<p>Sites such as AOL.com, Google Images, etc</p>
					</div>
					<div class="circle-area">
						<span class="first">80%</span>
						<span class="second">15%</span>
						<span class="third">5%</span>
					</div>
					<span class="text-holder">Quality score for Search Partners works the same way as Google.com. Although performance on these sites varies considerably from performance on Google Seach, it has NO impact on quality scores of your Google Search campaigns. It usually is good practice to separate your Google Search campaigns from Search Partners campaigns. <a target="_blank" href="http://www.tenscores.com/blog/the-4-adwords-campaigns-every-adwords-account-must-have/">Learn how</a>.</span>
				</div>
				<div class="network-area">
					<strong class="star-title">DISPLAY</strong>
					<div class="holder">
						<div class="box">
							<a href="#" class="heading">AUTO Placements</a>
							<div class="img-holder">
								<div class="img-circle pie-3"></div>
								<ul class="notes">
									<li class="first">Historical CTR Factors</li>
									<li class="second">Relevance Factors</li>
									<li class="third">Other</li>
								</ul>
								<span class="first-number">80%</span>
								<span class="second-number">15%</span>
								<span class="third-number">5%</span>
								<span class="about-first">
									Semantic relevance of the ads and keywords in the ad group to the site.
									<span class="connect">connect</span>
								</span>
								<span class="about-second">
									The ad's past performance on similar sites in terms of CTR.
									<span class="connect">connect</span>
								</span>
							</div>
						</div>
						<div class="block">
							<a href="#" class="heading">Managed PLACEMENTS</a>
							<div class="block-holder">
								<strong class="add-heading">CPC BIDDING</strong>
								<div class="img-section">
									<div class="pie-4"></div>
									<span class="first">90%</span>
									<span class="second">10%</span>
									<span class="first-note">Landing page quality.<span class="connect"></span></span>
									<span class="second-note"><span class="connect"></span></span>
								</div>
								<span class="img-name">The historical CTR of the ad on similar sites.</span>
								<strong class="add-heading alt">CPM BIDDING</strong>
								<div class="img-container">
									<div class="pie-5"></div>
									<span class="first">100%</span>
								</div>
								<span class="img-name alt">Landing page quality.</span>
							</div>
						</div>
					</div>
					<ul class="black-box">
						<li>Landing page quality plays an on/off swith role in the display network campaigns as well.</li>
						<li>Quality score for display network campaigns is not visible anywhere in the AdWords interface.</li>
					</ul>
				</div>
				
				
				<div class="numbers-block">
				
					<div class="divider"></div>
					
						<div class="arrow">arrow</div>


					<h2><span class="mark">WHAT DO THE</span>NUMBERS MEAN?</h2>
					<span class="text-holder">A 7/10 Quality Score is the recommended number and is sufficient. Going above 7 is great but not always achievable and may not be worth the effort. Anything below 7 is a sign that something is wrong and should be worked on.</span>
					<div class="numbers-section">
						<div class="box">
							<ul class="numbers-list">
								<li class="one">
									1
									<span class="mark short">Dead<span></span></span>
								</li>
								<li class="two">
									2
									<span class="mark middle">Sick<span></span></span>
								</li>
								<li class="three">3</li>
								<li class="four">
									4
									<span class="mark long">Weak<span></span></span>
								</li>
								<li class="five">5</li>
								<li class="six">6</li>
								<li class="seven">
									7
									<span class="mark short">Good<span></span></span>
								</li>
							</ul>
							<ul class="numbers-list alt">
								<li class="eight">
									8
									<span class="mark long">Excellent<span></span></span>
								</li>
								<li class="nine">9</li>
								<li class="last ten">10</li>
							</ul>
						</div>
						<div class="connect"></div>
						<div class="divider-2"></div>
						<div class="black-section">
							<div class="connection"></div>
							<ul class="box-area">
								<li>
									<span class="title dead">Dead</span>
									If you get a 1/10, you're in trouble. Google has or is about to ban your website. You'll hardly get any traffic and your costs will be exorbitant.
								</li>
								<li>
									<span class="title sick">Sick</span>
									At 2/10 and 3/10, your keywords are suffering. You're not getting all the traffic you could and you'll be charged very high prices for such bad scores. To go from 2 or 3 to 7/10 will require quite some expertise.
								</li>
								<li>
									<span class="title weak">Weak</span>
									These keywords need your attention and getting them above 7 should be accomplished without much trouble. Simply follow the flow chart at the bottom of this page. You'll most likely need to regroup your ad groups and write better ads.
								</li>
							</ul>
							<ul class="box-area alt">
								<li>
									<span class="title good">Good</span>
									You've accomplished what you needed in terms of quality. Google likes what you're doing and you're not being penalized in terms of traffic or costs. 
								</li>
								<li>
									<span class="title excellent">Excellent</span>
									The holy grail. At this point, you've surpassed Google's expectations and your performance is above average. Your costs are being discounted and your ads are appearing in prominent positions and more frequently.
								</li>
								<li class="last">
									<em class="vertical"></em>
									<span>The jump from 7/10 to anything above is not linear and sometimes doesn't make sense. Keywords often go from 7/10 immediately to 10/10 with no transition to 8 and 9.</span>
								</li>
							</ul>
						</div>
						<div class="divider-2"></div>
					</div>
				</div>
				<div class="bottom-area">
					<div class="divider"></div>
					
					<div class="arrow"></div>

					
					<h2><span class="mark">HOW DO I INCREASE</span>MY QUALITY SCORES?</h2>
					<span class="text-holder">Follow the flow chart. </span>
				</div>
				<div class="block-download">
					<div class="holder">
						<p>The following will make sense if you have a Tenscores account. <br />You can also download it in PDF format. </p>
						<a href="https://www.tenscores.com/pdf/tenscores-flow-chart.pdf" target="_blank" class="btn-grey">Download PDF</a>
					</div>
					<span>Don’t have a Tenscores account? <a href="http://account.tenscores.com/signup?lp=tenscores.com/quality-score" class="link" target="_blank">Get one</a>, or use  <a href="http://www.tenscores.com/pdf/QS-flow-chart.pdf" target="_blank" class="link">this</a> instead.</span>
				</div>
				<div class="scheme-block">
					<div class="row row-start">
						<span class="black-box">START HERE</span>
						<span class="border"></span>
					</div>
					<div class="row row-check-2">
						<div class="white-box box-2">Check Account Quality Score <br />(All campaigns)</div>
						<span class="border-2"></span>
					</div>
					<div class="row row-2">
						<div class="dark-box box scheme-red">It's below or equal to 3.9/10</div>
						<div class="grey-box box scheme-orange">It's between 4.0/10 &amp; 6.9/10</div>
						<div class="white-box box scheme-green">It's above or equal to 7.0/10</div>
					</div>
					<div class="row row-3">
						<span class="black-box done">YOU'RE DONE!</span>
						<div class="dark-box box-2 scheme-purple-2">You might have a Landing <br />Page problem. <a href="http://support.google.com/adwords/answer/2404197/?hl=en" target="_blank">Follow these <br />guidelines</a> then continue.</div>
						<div class="circle box-2">Click first <br />losing Campaign <br />then first losing <br />Ad Group.</div>
					</div>
					<div class="border-3"></div>
					<div class="row">
						<div class="white-box check">Check trend in <br />timeline</div>
					</div>
					<div class="border-3"></div>
					<div class="row row-answer">
						<div class="white-box box-4">Ad Group is getting fine. Pick <br />next losing ad group in the same <br />campaign or next losing <br />campaign.</div>
						<span class="yes">yes</span>
						<div class="diamond box-4">Is trend up?</div>
						<span class="yes-2">no</span>
						<div class="dark-box box-4 scheme-purple-2">You need to increase <br />Ad Group CTR and <br />relevance.</div>
					</div>
					<div class="row">
						<span class="how">how?</span>
						<div class="border-5"></div>
					</div>
					<div class="row row-area">
						<div class="border-6"></div>
						<div class="row row-1">
							<div class="dark-box scheme-purple-2">Your ad copy sucks. <br />Write better ads.</div>
							<span class="no">no</span>
							<div class="diamond box-5 box-6">Mostly broad?</div>
							<div class="white-box box-5">Check match <br />types.</div>
							<div class="diamond diamond-2 box-5">Ad group <br />more than 10 <br />keywords?</div>
							<span class="no-2">no</span>
						</div>
						<div class="row row-02">
							<span class="how-2">how?</span>
							<span class="yes-3">yes</span>
							<span class="yes-4">yes</span>
							<div class="dark-box box-6 scheme-purple-2">Regroup! Divide it smaller <br />tighter Ad Groups of no more <br />than 10 keywords.</div>
							<div class="white-box box-6">Add exact, phrase and modified <br /> broad match. Add negative <br /> keywords.</div>
						</div>
					</div>
					<div class="row row-dark">
						<div class="dark-box scheme-purple-2">Figure out exactly what the <br />person behind the specific <br />keyword is really searching for.</div>
					</div>
					<div class="border-4"></div>
					<div class="row">
						<div class="white-box type">Type keyword in Google &amp; <br />analyse natural search results.</div>
					</div>
					<div class="border-4"></div>
					<div class="row row-last">
						<span class="arrow-down-2"></span>
						<div class="white-box">Mostly websites <br />teaching stuff.</div>
						<div class="diamond">What do <br />you see?</div>
						<div class="white-box box-3">Mostly websites <br />selling stuff.</div>
					</div>
					<div class="columns">
						<div class="col">
							<div class="heading">
								<h3 class="info">INFORMATIONAL INTENT</h3>
							</div>
							<div class="small-box">The person behind your keyword is <br /> educating herself. Promise <br />information.</div>
							<div class="holder">
								<p>What kind of information are the websites in the search results giving away? What are they not providing?</p>
								<ul class="list scheme-orange">
									<li>Give away information similar but better than your competiton.</li>
									<li>Give away information that your customers want but competition is not providing.</li>
								</ul>
								<p>How and in what format?</p>
								<ul class="list scheme-orange">
									<li>Don't just give it away for 'FREE', ask for an email adress in exchange so you can drive  them to a sale later on.</li>
									<li>Give it away in video, PDF, audio, webpage or or any other form of consumption that will make your infomation more valuable to your prospects (not you).</li>
								</ul>
								<span>If your information is great, not only will you get higher CTR but they will be more likely to buy from you.</span>
							</div>
						</div>
						<div class="col col-2">
							<div class="heading">
								<h3 class="cart">COMMERCIAL INTENT</h3>
							</div>
							<div class="small-box">The person behind your keyword <br /> wants to buy. Promise a great deal.</div>
							<div class="holder">
								<p>What kinds of deals are the websites in the search results promoting?  Promote a better deal in your ad:</p>
								<ul class="list list-2 scheme-green">
									<li>free shipping</li>
									<li>faster deliver</li>
									<li>cheaper price</li>
									<li>better support</li>
									<li>money back guarantee</li>
									<li>more features</li>
									<li>free trial</li>
									<li>lots of hugs and kisses</li>
								</ul>
								<p>Or help the searcher make a better buying decision: </p>
								<ul class="list scheme-green">
									<li>Promise a review or comparison table of you against your competitors.</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="finish-box">
						<span class="border-finish"></span>
						These are the two most frequent remedies to<br /> bad click-through-rates. But it's just the tip of the iceberg. Visit <a href="http://tenscores.com/blog/what-i-want" target="_blank">our blog</a> for more.
					</div>
				</div>
				<div class="social-block">
					<div class="social-networks">
						
						<!-- AddThis Button BEGIN -->
							<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
							<a class="addthis_button_google_plusone_share"></a>
							<a class="addthis_button_twitter"></a>
							<a class="addthis_button_facebook"></a>
							<a class="addthis_button_linkedin"></a>
							<a class="addthis_button_compact"></a>
							</div>
							<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
							<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50f25e53735f346f"></script>
						<!-- AddThis Button END -->
					
					<p>If you like it, why not<br />share it?</p>
					</div>
					
				</div>
				<div id="footer">
					<div class="divider-2"></div>
					<div class="holder">
						<span class="title">Embed This On Your Site</span>
						<span class="text-box">The button below will provide html code that you can copy & paste in order to embed an image version of this page.</span>
						<a href="#" 
							    onClick="return false;"
								class="btn-grey"
								id="embed" 
								rel="popover"  
								data-animation="true"
								data-placement="top" 
								data-html="true"
								data-trigger="click"
								data-original-title="Embed code"
								data-content="<textarea cols='40' rows='3' style='font-family: Consolas,Monaco,monospace; overflow:scroll; word-wrap: break-word; resize: horizontal; height: 195px;'>
<a href='http://www.tenscores.com/quality-score' target='_blank'><img style='border:1px solid #ccc;' alt='What Is Quality Score?' src='http://www.tenscores.com/quality-score/quality-score-infographic.png' width='650' border='0'></a><br>&copy;<a href='http://tenscores.com'>Tenscores.com</a>, view the original interactive<a href='http://tenscores.com/quality-score'>quality score</a> infographic. 
</textarea>">Embed</a>
					</div>
				</div>
			</div>
		</div>
	</div>	



</body>
</html>