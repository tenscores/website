<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Please confirm</title>
<meta name="description" content="Use the Quality Score Monitor to track the evolution of your keywords quality and the Bid Optimizer to uncover the bids that yield maximum profits." />
<meta name="keywords" content="adwords quality score tool, quality score monitor, bid optimizer, free, tenscores" />
<meta name="author" content="Chris Thunder" />

<style type="text/css" media="all"> @import "css/index-v6.css"; </style>


<?php include 'files/includes/tags.php'; ?>


</head>

<body>

<div id="mainWrap">

<?php include('includes/body.roof.php'); ?>

<div id="mainFrame">
  <table width="975" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td  valign="top" style="height: 7px; background:url(images/border_main.gif) no-repeat top;"></td>
    </tr>
    <tr>
      <td  height="400" valign="middle" style="border-left:1px #ccc solid; border-right:1px #ccc solid; background:url(images/login_bg.png) bottom repeat-x;">
          <div id="contentRegister">
            <h1 align="center" class="one">Almost there...</h1>
            <p align="center" class="feature">We've sent you a short message to confirm your email. Simply click on <br />
              the link in that email and you'll be set. </p>
            <p align="center" class="feature">(If you can't find the message, please check your spam folder or <a href="register.php">try again</a>).</p>
          </div></td>
    </tr>
    <tr>
      <td valign="bottom" style="height: 7px; background:#E4E5FF url(images/border_main.gif) no-repeat bottom;"></td>
    </tr>
  </table>
</div>

<?php include('includes/body.footer.php'); ?>

</div><!--end mainWrap -->

</body>

</html>
