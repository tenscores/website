<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>
  <meta charset="utf-8">
   <title>Features For Higher Quality Scores. - TenScores</title>
    <meta name="description" content="Here are the complete features that TenScores offers.">
    <link href="http://tenscores.com/tour.php" rel="canonical">
    <?php include 'files/includes/meta.php'; ?>
    <?php include 'files/includes/tags.php'; ?>
  </meta>
</head>

<body id="tour">
  <?php include 'files/includes/nav.php'; ?>
	<article id="content">
		<div class="visual">
			<h1>Built for results.</h1>
			<p>Find out what's really going on with your Adwords Quality Scores<br> and learn how to improve them.</p>
		</div>
		<div id="mc-container">
			<div class="w1">
				<div class="w2">
					<div class="screen slideup">
						<img src="files/dashboard-2015-2.png" width="835" height="528" alt="Tenscores Dashboard">
						<span class="hr"></span>
					</div>
					<div class="features">
						<div class="container">
							<ul>
								<li class="i1">
									<em>1<span class="line"><span class="s1 horizontal"></span><span class="s2 vertical">
									</span><span class="s3 horizontal"></span><span class="bull"></span></span></em>
									<h3>Quality Score where it really matters</h3>
									<p>Quality Scores are only visible at the keyword level in AdWords,
										and their evolution is not provided. With Tenscores you'll see your Quality Scores
										at the account, campaign and ad group levels. Additionally, we track their values on a daily basis
									  and alert you by email about major changes.</p>
								</li>
								<li class="i2">
									<em>2<span class="line"><span class="s1 horizontal"></span><span class="s2 vertical">
									</span><span class="s3 horizontal"></span><span class="bull"></span></span></em>
									<h3>Recommendations</h3>
									<p>If your Quality Scores are low, Tenscores will tell you what you need to do to improve them.
									It will show you ad groups that have too many keywords or those that have an unhealthy heavy use of
									broad match keywords as well as those that have CTR problems. </p>
								</li>
								<li class="i3">
									<em>3<span class="line"><span class="s1 horizontal"></span><span class="s2 vertical"></span><span class="s3 horizontal"></span><span class="bull"></span></span></em>
									<h3>Apply recommendations in a few clicks</h3>
									<p>The automatic restructuring feature allows you to fix ad group size problems and match type problems
									in a few clicks. This way you can focus on your ads which are the most important part of the equation.
									The Ad Analysis feature will show you ad groups that only have one ad so you can write a second and start
									testing; it will show you where you have winning ads by CTR or conversions so you can pause losing ads.</p>
								</li>
								<li class="i4">
									<em>4<span class="line"><span class="s1 horizontal"></span><span class="s2 vertical"></span><span class="s3 horizontal"></span><span class="bull"></span></span></em>
									<h3>Metrics Google doesn’t show you</h3>
									<p>See an estimate of the penalty being accrued in your account due to low scores. Or how much is being saved on high scores. In context with your total costs pulled from Adwords, it gives you an idea of how much impact higher scores will have on you bottom line.</p>
								</li>
								<li class="i5">
									<em>5<span class="line"><span class="s1 horizontal"></span><span class="s2 vertical"></span><span class="s3 horizontal"></span><span class="bull"></span></span></em>
									<h3>Priority</h3>
									<p>Your campaigns are ordered by priority, the ones wasting you more money due to low scores being
										at the top. When they have been optimized, they shift to the winning area where you can see how much
										you're saving due to their now acquired higher scores.</p>
								</li>
								<li class="i6">
									<em>6<span class="line"><span class="s1 horizontal"></span><span class="s2 vertical"></span><span class="s3 horizontal"></span><span class="bull"></span><span class="s4 vertical"></span></span></em>
								</li>
							</ul>
							<div class="go-back">
								<h3>Budget</h3>
								<p>On top of detailed Quality Score analysis, Tenscores will show you how well you're spending your money
								on Adwords. It is often the case that you have a vast majority of keywords costing you money without ever
								generating conversions. We'll show you which they are so you can evaluate them and act accordingly.</p>
								<img src="files/restructuring-tool.png" style="margin: -5px auto 70px auto;">
								<a class="btn btn-in btn-danger" href="https://app.tenscores.com/register?lp=tour&ft=1">Show Me My Dashboard</a>
							</div>
							<strong class="pricing"><a href="http://tenscores.com/pricing.php">View plans <span class="and">and</span> pricing</a></strong>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
		<div id="press" style="display:none;">
		<div id="inner-press">
			<!--
			<img src="images/press/techvibes-50.gif" alt="Tech Vibes">
			<img src="images/press/lapresse-50.gif"  alt="La Presse">
			<img src="images/press/next-montreal-50.gif"  alt="Next Montreal">
			<img src="images/press/metro-50.gif"  alt="Metro">
		  -->
		  <span style="margin-top: 15px; display:inline-block;"><strong>Featured on</strong></span>
			<img src="files/sej-logo.png" width="232" height="50" alt="Search Engine Journal">
			<img src="files/searchengineland-50.gif" width="215" height="50" alt="Search Engine Land">
	    </div>
	</div>

  <?php include 'files/includes/footer.php'; ?>

</div>
