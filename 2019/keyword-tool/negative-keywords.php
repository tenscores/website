<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">
    <title>Google Ads Negative Keyword Tool - Tenscores</title>
    <meta name"description" content="Reduce wasted spent by adding negatives from low performing search terms and n-grams.">
	  <link rel="canonical" href="https://www.tenscores.com/quality-score-tool/negative-keywords/" />
  </head>
  <body class="km km-negatives">

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">Find your negative keywords<span class="ts-red">.</span></h1>
        <h2 class="lead">Reduce wasted spent by adding negatives from low performing search terms and n-grams.</h2>
      </div>
    </section>

    <?php include 'km-nav.php'; ?>

    <div class="container content">
      <div class="row">
        <div class="col-sm-4 content-item">
          <div class="content-text">
            <h4>Low performing search terms</h4>
            <p>Search terms that are proven to waste budget should be excluded from triggering ads. </p>
          </div>
        </div>
        <div class="col-sm-8 content-item">
          <div class="placeholder">
            <div class="content-img ">
              <img src="../images/negative-search-term.png" alt="Negative Search Terms" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text">
            <h4>N-grams</h4>
            <p>Single words — in search terms or keywords — that are associated with low performance should be added as negatives. </p>
          </div>
        </div>
        <div class="col-sm-8 content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/negative-n-grams.png" alt="Negative N-Grams" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <h4>From new keywords</h4>
            <p>Keywords can be sent here from the new keywords area to be published as negatives. </p>
          </div>
        </div>
        <div class="col-sm-8  content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/manual-negatives.png" alt="Negatives added fro the new keywords tab" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="container-fluid bg-slant">
    </div>

    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-issam.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
