<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">
    <title>Google Ads Automated Keyword Research Tool - Tenscores</title>
    <meta name"description" content="New keywords are automatically fetched from your search terms reports and Google's keyword planner.">
	  <link rel="canonical" href="https://www.tenscores.com/keyword-tool/new-keywords" />
  </head>
  <body class="km km-new">

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">Automated keyword research<span class="ts-red">.</span></h1>
        <h2 class="lead">New keywords are automatically fetched from your search terms reports and Google's keyword planner.</h2>
      </div>
    </section>

    <?php include 'km-nav.php'; ?>

    <div class="container content">
      <div class="row">
        <div class="col-sm-4 content-item">
          <div class="content-text">
            <h4>From Search terms reports</h4>
            <p>High performing terms from your search terms reports are promoted to be keywords. </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/search-terms-report.png" alt="New keywords from search terms reports" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm-4 order-sm-2 content-item">
          <div class="content-text">
            <h4>From keyword planner</h4>
            <p>The keyword planner is used to find keywords that are related to your highly performing terms. </p>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder">
            <div class="content-img ">
              <img src="../images/keyword-planner.png" alt="New keywords from Keyword Planner" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <h4>In well structured ad groups</h4>
            <p>You don't have to worry about creating ad groups, your new keywords are arlready structured. </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/adgroup.png" alt="New Adgroup" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text">
            <h4>Edit match types</h4>
            <p>Exact match is the default but you can add or remove match types. </p>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/change-matchtype.gif" alt="Editing Match Types Animation" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <h4>Or send to negatives</h4>
            <p>If a keyword looks like it doesn't belong, send it to the negatives. </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/send-to-negatives.gif" alt="Send To Negatives" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm-12 order-sm-2 content-item" style="max-width: 700px;
    margin: auto;">
          <div class="content-text text-center">
            <h4>Approve ads & bids before publishing</h4>
            <p>Ads and bids are automatically added to your new ad groups, you can edit them before publishing. </p>
          </div>
        </div>
        <!--
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/new-adgroup.png" alt="New Adgroup" class="img-fluid" />
            </div>
          </div>
        </div>
        -->
      </div>
    </div>




    <div class="container-fluid bg-slant">
    </div>

    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-issam.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
