<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">
    <title>Google Ads Duplicate Keywords Tool - Tenscores</title>
    <meta name"description" content="The same search terms are being triggered by different keywords, in ad groups you didn't intend them to show.">
	  <link rel="canonical" href="https://www.tenscores.com/keyword-tool/duplicate-keywords" />
  </head>
  <body class="km km-duplicates">

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">Fix rampant duplication<span class="ts-red">.</span></h1>
        <h2 class="lead">The same search terms are being triggered by different keywords, in ad groups you didn't intend them to.</h2>
      </div>
    </section>

    <?php include 'km-nav.php'; ?>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <h4>Search term duplication</h4>
            <p>Search terms that are being triggered by different keywords are spotted. </p>
          </div>
        </div>
        <div class="col-sm-8 content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/duplicate-search-term.png" alt="Duplicate Search Term" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm-4 order-sm-2 content-item">
          <div class="content-text">
            <h4>Keyword duplication</h4>
            <p>Keywords that appear multiple times in the same campaign or overlaping campaigns are uncovered. </p>
          </div>
        </div>
        <div class="col-sm-8 order-sm-1 content-item">
          <div class="placeholder">
            <div class="content-img ">
              <img src="../images/duplicate-keyword.png" alt="Duplicate Keyword" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm-4 content-item">
          <div class="content-text">
            <h4>Choose the one to keep</h4>
            <p>Selections are automatically made for you based on performance, but you can select your own. </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img" style="padding: 1rem;">
              <img src="../images/duplicate-winner-selection.gif" alt="Duplicate Winner Selection" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content last">
      <div class="row">
        <div class="col-12 content-item">
          <div class="content-text text-center">
            <h4>Paused or added as negative</h4>
            <p>Negative keywords are used to stop search term duplication, and duplicate keywords are simply paused. </p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid bg-slant">
    </div>

    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-issam.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
