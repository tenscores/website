<div class="pl-1 mb-5">
    <ul class="nav nav-pills justify-content-center ">
      <li class="nav-item text-center">
        <a class="nav-link km-overview" href=".">
          <i class="ts-nav-icon icon-km-overview"></i>
          <span>Overview</span>
        </a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link km-new" href="new-keywords">
          <i class="ts-nav-icon icon-km-new"></i>
          <span>New Keywords</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link km-duplicates text-center" href="duplicate-keywords">
          <i class="ts-nav-icon icon-km-duplicates"></i>
          <span>Duplicates</span>
        </a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link km-negatives" href="negative-keywords">
          <i class="ts-nav-icon icon-km-negatives"></i>
          <span>Negatives</span>
        </a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link km-bids" href="bid-optimization">
          <i class="ts-nav-icon icon-km-bids"></i>
          <span>Bid Optimization</span>
        </a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link km-cleanup" href="cleanup">
          <i class="ts-nav-icon icon-km-cleanup"></i>
          <span>Cleanup</span>
        </a>
      </li>
    </ul>
</div>
