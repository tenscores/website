<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">
    <title>Google Ads Bid Optimization Tool - Tenscores</title>
    <meta name"description" content="No more guesswork or blind automation, your most profitable bids are waiting for you to approve.">
	  <link rel="canonical" href="https://www.tenscores.com/quality-score-tool/analysis/" />
  </head>
  <body class="km km-bids">

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">Bid for maximum profitability<span class="ts-red">.</span></h1>
        <h2 class="lead">No more guesswork or blind automation, your most profitable bids are waiting for you to approve.</h2>
      </div>
    </section>

    <?php include 'km-nav.php'; ?>

    <div class="container content text-center">
      <div class="row">
        <div class="col-12 order-2 content-item">
          <div class="content-text">
            <h4>Maximum profitablity bids</h4>
            <p>From Google's own traffic estimates at various bids, and a Max CPA computed by Tenscores, now you can see the bids that lead to maximum profitablity.</p>
          </div>
        </div>
        <div class="col-12 order-1 content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/profitable-bids.png" alt="Bids For Maximum Profits" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container text-center">
      <h4>Here's how it works:</h4>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <span>Step 1</span>
            <h4>Google's bid simulator</h4>
            <p>We start with Google's traffic and cost simulations of different proposed bids. </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/google-ads-bid-simulator.png" alt="Google Ads Bid Simulator" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <span>Step 2</span>
            <h4>Estimated Max. CPA</h4>
            <p>From your data, we estimate the maximum amount you should pay for a conversion (can be edited). </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img ">
              <img src="../images/estimated-max-cpa.png" alt="Estimated Max CPA" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <span>Step 3<span>
            <h4>Bids of maximum profitablity</h4>
            <p>With your max CPA and Google's simulation, we show you the bids that will yield maximum profitability.</p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/profitable-bids.png" alt="Bids For Maximum Profits" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content last">
      <div class="row">
        <div class="col-12 content-item">
          <div class="content-text text-center">
            <h4>Ad group or keyword level</h4>
            <p>Profitable bids are suggested at the ad group or keyword level.</p>
          </div>
        </div>
      </div>
    </div>


    <div class="container-fluid bg-slant">
    </div>

    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-issam.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
