<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">

    <title>Google Ads Keyword Management Tool | Tenscores</title>
    <meta name"description" content="Keyword research, duplicates identification, bid optimization & cleanup operations done for you">
	  <link rel="canonical" href="https://www.tenscores.com/keyword-tool/" />

  </head>
  <body class="km km-overview">
    <div class="container-fluid bg-slant"></div>

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">Manage your keywords effortlessly.</h1>
        <h2 class="lead">Keyword research, duplicates identification, bid optimization & cleanup operations done for you to approve or reject.</h2>
      </div>
    </section>

    <?php include 'km-nav.php'; ?>

    <div class="container-fluid text-center hero-container">
        <img src="../images/keyword-tool-hero2.png" class="hero-img shadow-big" alt="Tenscores Keyword Tool" />
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <span class="number">1</span>
            <h4>Keyword research, <span class="slimmed">automated.</span></h4>
            <p>Get new keywords from search terms report and Keyword Planner. </p>
            <span class="dotted-line km-1a d-xl-block"></span>
            <span class="dotted-line km-1b d-xl-block"></span>
            <span class="dotted-line km-1c d-xl-block"></span>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img">
              <a href="new-keywords.php">
                <img src="../images/search-terms-report.png" alt="New keywords from search terms reports" class="img-fluid" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm  content-item">
          <div class="content-text">
            <span class="number">2</span>
            <h4>Duplicate keywords, <span class="slimmed">exposed.</span></h4>
            <p>Duplicate search terms are lurking unrestranined in your account. It's time to stop them. </p>
            <span class="dotted-line km-2a d-xl-block"></span>
            <span class="dotted-line km-2b d-xl-block"></span>
            <span class="dotted-line km-2c d-xl-block"></span>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img">
              <a href="duplicates.php">
                <img src="../images/duplicate-search-term.png" alt="Duplicate Search Term" class="img-fluid" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <span class="number">3</span>
            <h4>Negative keywords, <span class="slimmed">extracted.</span></h4>
            <p>Reduce wasted spent by adding negative keywords extracted from your low performing terms. </p>
            <span class="dotted-line km-3a d-xl-block"></span>
            <span class="dotted-line km-3b d-xl-block"></span>
            <span class="dotted-line km-3c d-xl-block"></span>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img">
              <a href="negatives.php">
                <img src="../images/negative-search-term.png" alt="Negative Search Terms" class="img-fluid" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <span class="number">4</span>
            <h4>Manual bidding<span class="slimmed">, maximized.</span></h4>
            <p>Bid for highest profitablity using Google's own engine, in your control. </p>
            <span class="dotted-line km-4a d-xl-block"></span>
            <span class="dotted-line km-4b d-xl-block"></span>
            <span class="dotted-line km-4c d-xl-block"></span>
          </div>
        </div>
        <div class="col-sm  content-item">
          <div class="placeholder">
            <div class="content-img">
              <a href="bid-optimization.php">
                <img src="../images/profitable-bids.png" alt="Bids For Maximum Profits" class="img-fluid" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <span class="number">5</span>
            <h4>Everything else, <span class="slimmed">cleaned out.</span></h4>
            <p>Keyword with no impressions, clicks, conversions, profits are unproductive. Deal with them. </p>
            <span class="dotted-line km-5a d-xl-block"></span>
            <span class="dotted-line km-5b d-xl-block"></span>
            <span class="dotted-line km-5c d-xl-block"></span>
            <span class="dotted-line km-5d d-xl-block"></span>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
              <div class="content-img">
                <a href="cleanup.php">
                  <img src="../images/no-impressions-keywords.png" alt="Keywords with zero impressions" class="img-fluid" />
                </a>
              </div>
          </div>
        </div>

      </div>
    </div>


    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-issam.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
