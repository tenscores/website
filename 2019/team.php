<!doctype html>
<html lang="en">
  <head>
    <?php include 'includes/meta.php'; ?>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <title>Tenscores Team at your service</title>
    <meta name"description" content="A small team trying to make a big difference.">
	  <link rel="canonical" href="https://tenscores.com/contact" />

  </head>
  <body class="team">

    <?php include 'includes/main-nav.php'; ?>
    <section class="jumbotron mb-3 text-center animated fadeInDown">
      <div class="container">
        <h1 class="jumbotron-heading">Small team. Big changes<span class="ts-red">.</span></h1>
        <h2 class="lead">Freedom.</h2>
      </div>
    </section>
    <div class="container team">
      <div class="row">
        <div class="col-sm-4 mt-3">
          <div class="team-mate chris animated fadeInLeft slower">
            <div class="name">
              <span>Christian Nkurunziza</span>
              <span>Fisherman</span>
            </div>
          </div>
        </div>
        <div class="col-sm-4 mt-3">
          <div class="team-mate chretien animated fadeInLeft  slow">
            <div class="name">
              <span>Chretien Mwizerwa</span>
              <span>Blacksmith</span>
            </div>
          </div>
        </div>
        <div class="col-sm-4 mt-3">
          <div class="team-mate prynce animated fadeInLeft fast">
            <div class="name">
              <span>Prynce Comiso</span>
              <span>Junior Blacksmith</span>
            </div>
          </div>
        </div>
      </div>
    </div>





    <?php include 'includes/footer.php'; ?>

  </body>
</html>
