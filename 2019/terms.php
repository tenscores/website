<!doctype html>
<html lang="en">
  <head>
    <?php include 'includes/meta.php'; ?>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <title>Tenscores: Terms & conditions</title>
    <meta name"description" content="Terms and conditions of Tenscores use.">
	  <link rel="canonical" href="https://tenscores.com/terms" />

  </head>
  <body class="terms">

    <div class="container">
      <div class="row">
        <div class="col-12" style="padding: 5rem;">

  <article id="content">
  <div class="visual">
  <h1 class="align-left">Terms Of Use</h1>
  </div>
  <div id="mc-container">
    <div class="container">
      <p>Welcome to Tenscores.com (the “Website”).</p>
      <p>This Terms of Use Agreement (the “Terms of Use” or the “Agreement”) describes
         the terms and conditions applicable to your access and use of the Website.</p>
      <p>The terms “TenScores” and “We” refer to the owner of the Website. The term
        “You” refers to the users or viewers of the Website.</p>
      <p>You accept this Agreement by using the Website or accessing any content
        available through the Website and will be bound by the Terms of Use which
        together with our Privacy Policy govern TenScores’ relationship with you in
        relation to the Website. If you disagree with any part of these Terms of Use,
        please do not use our Website.</p>
      <p>
        <ol>
        <li><strong>Changes:</strong> The content of the pages of the Website is
          for your general information and use only. TenScores may make changes to
          the Website without notice and the content and/or the services described
          on the Website at any time.</li>
        <li><strong>Proprietary Rights:</strong> The Website and its content
          are the sole and exclusive property of TenScores. This material, which
          is owned or licensed to TenScores includes, but is not limited to, the
          design, layout, look, appearance and graphics. All trademarks reproduced
          in the Website which are not the property of, or licensed to the TenScores,
           are acknowledged on the Website. You agree not to reproduce, duplicate,
           copy, sell, resell or exploit for any commercial purpose, any portion
           of the Website or its content other than in accordance with the copyright
           notice, which forms part of these Terms of Use. You hereby acknowledge and
           agree that, as between TenScores and you, all rights, title, and interest
           in and to the Website and its content shall be owned exclusively by TenScores.
            Use of the Website or its content in any way not expressly permitted by
            this Agreement is prohibited. Unauthorized use of the Website may give
            rise to a claim for damages and/or constitute a criminal offense.</li>
        <li><strong>Links to Other Sites:</strong> The Website may contain links to websites operated by other parties. TenScores provides these links to other websites as a convenience, and use of these sites is at your own risk. The linked sites are not under the control of TenScores, and TenScores is not responsible for the content available on such linked websites. Such links do not imply TenScores’ endorsement of the information or materials on any other website and TenScores disclaims all liability with regard to your access to and use of such linked websites.</li>
        <li><strong>Additional Restrictions:</strong> You agree not to access the Website by any means other than through a standard web browser on a computer or mobile device. You further agree that you will not damage, disable, overburden, or impair the Website or interfere with any other party’s use and enjoyment of it.</li>
        <li><strong>Termination:</strong> This Agreement will remain in full force and effect while you use the Website and/or are a Member. You may cancel your Membership at any time, for any reason. TenScores may terminate your Membership at any time for any reason. All decisions regarding the termination of accounts shall be made in the sole discretion of TenScores. TenScores is not required, and may be prohibited, from disclosing a reason for the termination of your account. Even after your membership is terminated, this Agreement will remain in effect. All terms that by their nature may survive termination of this Agreement shall be deemed to survive such termination. Such termination will result in the deactivation or deletion of your account or your access to your account, and the forfeiture and relinquishment of all content in your account. TenScores reserves the right to refuse service to anyone for any reason at any time.</li>
        <li><strong>Disclaimers and Limitation of Liability:</strong>
          <ul>
            <li>(i) The Website and all content on the Website are provided to you on an “as is” “as
              available” basis without warranty of any kind either express or implied, including but not limited to the
              implied warranties of merchantability, fitness for a particular purpose, and non-infringement. TenScores
              makes no warranty as to the accuracy, timeliness, performance, completeness or reliability of any materials
              and tools found or offered on the Website for any particular purpose. You acknowledge that such
              information and materials may contain inaccuracies or errors. You are responsible for verifying
              any information before relying on it. Use of the Website and the content available on the
              Website is at your sole risk and TenScores explicitly excludes liability for any such
              inaccuracies or errors to the fullest extent permitted by the law.</li>
            <li>(ii) TenScores cannot guarantee and does not promise any specific results from the use of the Website. It shall be your own responsibility to ensure that any products, services or information available through the Website meet your specific requirements.</li>
            <li>(iii) TenScores makes no representations or warranties that use of the Website will be uninterrupted or error-free. You are responsible for taking all necessary precautions to ensure that any content you may obtain from the Website is free of viruses or other harmful code.</li>
            <li>(iv) To the maximum extent permitted by law, TenScores disclaims all liability, whether based in contract, tort (including negligence), strict liability or otherwise, and further disclaims all losses, including without limitation indirect, incidental, consequential, or special damages arising out of or in any way connected with access to or use of the Website, the content, or the services offered or sold through Website, even if TenScores has been advised of the possibility of such damages.</li>
          </ul>
        </li>
        <li><strong>Exclusion and Limitations:</strong> Some jurisdictions do not allow the exclusion of certain warranties or the limitation or exclusion of liability for incidental or consequential damages. Accordingly, some of the above indications may not apply to you.</li>
        <li><strong>Indemnity:</strong> You agree to indemnify, defend and hold TenScores and its respective officers, agents, partners and employees, harmless from any loss, liability, claim, or demand, including reasonable attorney’s fees, due or arising out of your use of the Website and/or breach of this Agreement.</li>
        <li><strong>Confidentiality:</strong> Except as otherwise expressly provided herein, the Parties agree not to disclose confidential information of the other party without their prior written consent (“Confidential Information”). Confidential Information shall mean any confidential, proprietary or trade secret information relating to the Service or disclosed by one party (the "Disclosing Party") to the other party (the "Receiving Party") during the Term, including, but not limited to, business, financial or technical information that is not (a) disclosed in public materials or otherwise in the public domain through no fault of the Receiving Party; (b) lawfully obtained by the Receiving Party from a third party without any obligation of confidentiality; (c) lawfully known to the Receiving Party prior to disclosure by the Disclosing Party; (d) independently developed by the Receiving Party; or (e) required or reasonably advised to be disclosed by law as long as the Receiving Party affords the Disclosing Party a reasonable opportunity to seek protective legal treatment of the Confidential Information and reimburses the Disclosing Party for its reasonable cost of compiling and providing secure access to such Confidential Information.</li>
        <li><strong>Choice of Law and Forum:</strong> This Agreement shall be governed by, and will be construed under the laws of the Province of Québec and the laws of Canada applicable therein. The Courts of the Province of Québec, judicial district of Montreal, will have exclusive jurisdiction to adjudicate any dispute arising under or in connection with this Agreement.</li>
        <li><strong>Miscellaneous</strong>
          <ul>
            <li>(i) If any provision of this Agreement is held to be illegal, invalid or unenforceable, such
            provision shall be struck and the remaining provisions shall remain in full force.</li>
            <li>(ii) Headings are for reference purposes only and in no way define, limit, construe or
            describe the scope or extent of such section.</li>
            <li>(iii) TenScores’ failure to act with respect to any failure by you or others to comply with these
            Terms of Use does not waive TenScores’ right to act with respect to subsequent or
            similar failures.</li>
            <li>(iv) These Terms of Use set forth the entire understanding and agreement between you and
            TenScores with respect to the subject matter hereof.</li>
            <li>(v) You may not assign or transfer your rights or obligations under this Agreement without
            the prior written consent of TenScores, and any assignment or transfer in violation of this
            provision shall be null and void.</li>
            <li>(vi) TenScores reserves the right to seek all remedies available at law and in equity for
            violations of this Agreement and/or the rules and regulations set forth on the Website, including without limitation the right to block access from a particular internet address.</li>
          </ul>
        </li>
        </ol>
        <p>Any questions regarding our Terms Of Use should be sent to support@tenscores.com</p>
      </p>
    </div>
  </div>
</article>





        </div>
      </div>
    </div>

  </body>
</html>
