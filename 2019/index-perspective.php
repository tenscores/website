<!doctype html>
<html lang="en">
  <head>
    <?php include 'includes/meta.php'; ?>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <title>Tenscores - Google Ads Optimization Platform For SMBs & Agencies</title>
    <meta name"description" content="Optimize your Google Ads, reduce acquistion costs with Quality Score optimization tool and get more conversions with keyword management.">
	  <link rel="canonical" href="https://www.tenscores.com" />

  </head>
  <body class="home">

    <?php include 'includes/main-nav.php'; ?>
    <div class="container-fluid bg-slant"></div>

    <section class="jumbotron mb-3 text-center animated fadeInDown">
      <div class="container">
        <h1 class="jumbotron-heading">Optimize your Google Ads<span class="ts-red">.</span></h1>
        <h2 class="lead">Reduce acquisition costs with Quality Score optimization. Get more conversions with Keyword Management.</h2>
      </div>
    </section>
    <div class="hero-perspective">
      <div class="row no-gutters">
        <div class="col-6">
          <div class="hero-left">
            <a href="quality-score-tool/">
              <img src="images/quality-score-tool-hero-3.png" alt="Google Ads Quality Score Tool" class="hero img-fluid animated fadeInLeft" />
            </a>
          </div>
        </div>
        <div class="col-6">
          <div class="hero-right">
            <a href="keyword-tool/">
              <img src="images/keyword-tool-hero2.png" alt="Google Ads Keyword Tool" class="hero img-fluid animated fadeInRight" />
            </a>
          </div>
        </div>
      </div>
    </div>

    <section class="ts-CTA text-center">
      <?php include 'includes/cta-button.php'; ?>
      <?php include 'includes/issam-testimonial.php'; ?>
    </section>

    <?php include 'includes/footer.php'; ?>


  </body>
</html>
