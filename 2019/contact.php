<!doctype html>
<html lang="en">
  <head>
    <?php include 'includes/meta.php'; ?>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <title>How To Contact The Tenscores Team</title>
    <meta name"description" content="The best way to contact us is by email.">
	  <link rel="canonical" href="https://tenscores.com/contact" />

  </head>
  <body class="contact">

    <?php include 'includes/main-nav.php'; ?>
    <section class="jumbotron mb-3 text-center animated fadeInDown">
      <div class="container">
        <h1 class="jumbotron-heading">Contact us<span class="ts-red">.</span></h1>
        <h2 class="lead">The best way to reach our team is by email: support@tenscores.com</h2>
      </div>
    </section>
    <div class="container shadow-big" style="padding:0;">
      <div class="row no-gutters">
        <div class="col-12 text-center">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2796.273945109594!2d-73.57398168480395!3d45.50456373877077!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cc91a467b0e97af%3A0x7be4c1375659ee4d!2s2001+Boulevard+Robert-Bourassa%2C+Montr%C3%A9al%2C+QC+H3A+2A5%2C+Canada!5e0!3m2!1sen!2sth!4v1554786906794!5m2!1sen!2sth"  width="100%" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>

        </div>
      </div>
    </div>

    <div class="container mt-5">
      <div class="row ">
        <div class="col-12">
          <address>
            <strong>Tenscores, Inc.</strong><br>
            2001 Boulevard Robert-Bourassa, Suite 1700<br>
            Montreal, QC, H3A 2A6 <br>
            Canada <br>
            P: +1-855-330-2671<br>
            E: support@tenscores.com<br>
          </address>
        </div>
      </div>
    </div>



    <?php include 'includes/footer.php'; ?>

  </body>
</html>
