<!doctype html>
<html lang="en">
  <head>
    <?php include 'includes/meta.php'; ?>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <title>Tenscores - Google Ads Optimization Platform For SMBs & Agencies</title>
    <meta name"description" content="Optimize your Google Ads, reduce acquistion costs with Quality Score optimization tool and get more conversions with keyword management.">
	  <link rel="canonical" href="https://tenscores.com" />

  </head>
  <body class="home">

    <?php include 'includes/main-nav.php'; ?>
    <div class="container-fluid bg-slant"></div>

    <section class="jumbotron mb-3 text-center animated fadeInDown">
      <div class="container">
        <h1 class="jumbotron-heading">Get Google Ads results<span class="ts-red">.</span></h1>
        <h2 class="lead">Get cheaper clicks with Quality Score optimization and more volume with Keyword management.</h2>
      </div>
    </section>
    <div class="container-fluid">
      <div class="row no-gutters">
        <div class="col-12 text-center">
            <a href="quality-score-tool/">
              <img style="border: 3px solid #000;" src="images/quality-score-tool-hero-3.png" alt="Google Ads Quality Score Tool" class="hero img-fluid animated fadeInLeft" />
            </a>
        </div>
      </div>
    </div>

    <section class="ts-CTA text-center">
      <?php include 'includes/cta-button.php'; ?>
      <?php include 'includes/testimonial-issam.php'; ?>
    </section>

    <?php include 'includes/footer.php'; ?>

  </body>
</html>
