<!doctype html>
<html lang="en">
  <head>
    <?php include 'includes/meta.php'; ?>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <title>Scientific Advertising by Claude Hopkins Free Instant PDF Download</title>
    <meta name"description" content="Scientific Advertising by Claude Hopkins Free Instant PDF Download - no email signup!">
	  <link rel="canonical" href="https://tenscores.com/scientific-advertising" />

  </head>
  <body class="scientific-advertising">

    <section class="jumbotron mb-3 text-center animated fadeInDown">
      <div class="container">
        <h1 class="jumbotron-heading">Scientific Advertising<span class="ts-red">.</span></h1>
        <h2 class="lead">"The time has come when advertising has in some hands reached the status of a science. It is based on ﬁxed principles and is reasonably exact. The causes and eﬀects have been analyzed until they are well understood."</h2>
        <p class="muted italic mt-3" style="">Written by claude Hopkins, 1923</p>
      </div>
    </section>
    <div class="container" style="padding:0;">
      <div class="row">
        <div class="col-12 text-center">
          <a href="pdf/scientific-advertising.pdf">
            <img src="images/scientific-advertising-pdf.png" class="shadow-big" style="border: 1px solid rgba(0,0,0,0.1)" alt="Scientific Advertersing PDF Download">
          </a>
        </div>
      </div>
    </div>
    <div class="container text-center">
      <p>
        <a href="pdf/scientific-advertising.pdf" class="btn btn-lg mt-5" style="background:#E74C3C;color:white;">Download PDF</a>
      </p>
    </div>
  </body>
</html>
