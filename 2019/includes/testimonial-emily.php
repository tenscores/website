<div class="container testimonial text-center">
  <div class="testimonial-img emily">
  </div>
  <span class="testimonial-txt">
      <span class="braket">&ldquo;</span>I used to manually download reports. Then, I’d have to do a lot of reformatting in Excel to get any insights about my quality scores. Tenscores does this all for me, and has saved several hours of my time!.&rdquo;
   </span>
  <span class="testimonial-sig">Emily Vetter, Search Analyst At Fraser Communications</span>
</div>
