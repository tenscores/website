<div class="container testimonial text-center">
  <div class="testimonial-img issam">
  </div>
  <span class="testimonial-txt">
      <span class="braket">&ldquo;</span>Many struggle to gain this level of Quality Score analysis because Google’s current set-up doesn’t provide anything like this, so it has previously been a very painful and time-consuming process to try and assess it. TenScores provides recommendations on how you can optimise and further improve quality score in a really useful way.&rdquo;
   </span>
  <span class="testimonial-sig">Issam Tidjani, Global SEM Lead At Nokia</span>
</div>
