<div class="container testimonial text-center" style="max-width: 500px;">
  <span class="testimonial-txt">
      <span class="braket">&ldquo;</span>Thanks! That's all I have to say... I just hit a 7.5/10 score, my CPC has dropped 80% in a year!&rdquo;
   </span>
  <span class="testimonial-sig">Anonymous Customer</span>
</div>
