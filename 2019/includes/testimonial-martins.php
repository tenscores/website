<div class="container testimonial text-center">
  <div class="testimonial-img martins">
  </div>
  <span class="testimonial-txt">
      <span class="braket">&ldquo;</span>... I have a question regarding this new feature I see called adgroup regrouping. I tried it and got quite interesting results that improved campaign I had on average at 3.1 QS up to 7 right away. I had managed to improve CTR for those keywords from 2% up to 10% but for some reason QS didn't change much so I decided to try this regrouping and the results quite surprised me...&rdquo;
   </span>
  <span class="testimonial-sig">Mārtiņš Kažemaks, CEO at Top Media</span>
</div>
