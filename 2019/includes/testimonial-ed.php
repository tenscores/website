<div class="container testimonial text-center">
  <div class="testimonial-img ed">
  </div>
  <span class="testimonial-txt">
      <span class="braket">&ldquo;</span>Coming up on the end of my 4th month with TenScores, things look good and I'm getting better with my Google Ads account. Quality Score has gone up from 5.4/10 to bouncing in the range of 7.7/10 and 8.1/10. CTR is up from 0.8% to 1.7%. CPC cut in half from $2.80 to under $1.50. All good.&rdquo;
   </span>
  <span class="testimonial-sig">Ed West, Small Business Owner</span>
</div>
