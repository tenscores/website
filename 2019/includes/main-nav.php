<?php
  $path = "http://localhost:8888/php-site/2019/";
  //$path = "http://tenscores.com/2019/";
  //$path = "https://tenscores.today/";
?>

<nav class="navbar navbar-light navbar-expand-md justify-content-center">
    <a href='<?php echo "$path";?>' class="navbar-brand d-flex w-50 mr-auto">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar3">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse text-center collapse w-100" id="collapsingNavbar3">
        <ul class="navbar-nav w-100 justify-content-center">
            <li class="nav-item">
                <a class="nav-link qs" href='<?php echo "$path";?>quality-score-tool/'>Quality Score</a>
            </li>
            <li class="nav-item">
                <a class="nav-link km" href='<?php echo "$path";?>keyword-tool/'>Keywords</a>
            </li>
            <li class="nav-item">
                <a class="nav-link pricing" href="<?php echo "$path";?>pricing">Pricing</a>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto w-100 justify-content-end">
            <li class="nav-item">
                <a class="nav-link ts-red" href="https://app.tenscores.com/signup">Signup</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://app.tenscores.com/login">Signin</a>
            </li>
        </ul>
    </div>
</nav>
