<div class="container customer-logos text-center ">
  <div class="row logo-row">
    <div class="col-sm nokia">
    </div>
    <div class="col-sm iprospect">
    </div>
    <div class="col-sm thomas-cook">
    </div>
    <div class="col-sm trivago">
    </div>
    <div class="col-sm aexp">
    </div>
    <div class="col-sm walgreens">
    </div>
  </div>
</div>

<footer class="container-fluid">
  <div class="row">
    <div class="col-12 col-md">
      <div class="footer-logo"></div>
      <small class="d-block mb-3 text-muted">&copy; 2013-2019</small>
    </div>
    <div class="col-6 col-md">
      <h5>Product</h5>
      <ul class="list-unstyled text-small">
        <li><a class="text-muted" href="<?php echo "$path";?>quality-score-tool/">Quality Score Tool</a></li>
        <li><a class="text-muted" href="<?php echo "$path";?>keyword-tool/">Keyword Tool</a></li>
        <li><a class="text-muted" href="<?php echo "$path";?>quality-score-tool/regrouper">Regrouper</a></li>
        <li><a class="text-muted" href="<?php echo "$path";?>pricing">Pricing</a></li>
        <li><a class="text-muted" href="<?php echo "$path";?>reviews">Reviews</a></li>
      </ul>
    </div>
    <div class="col-6 col-md">
      <h5>Free Resources</h5>
      <ul class="list-unstyled text-small">
        <li><a class="text-muted" href="<?php echo "$path";?>blog">Read Our Blog</a></li>
        <li><a class="text-muted" href="<?php echo "$path";?>quality-score/">Quality Score Infographic</a></li>
        <li><a class="text-muted" href="<?php echo "$path";?>calibrate">Calibrate: Free Bid Optimization Tool</a></li>
        <li><a class="text-muted" href="<?php echo "$path";?>scientific-advertising">Scientific Advertising</a></li>
      </ul>
    </div>
    <div class="col-6 col-md">
      <h5>Popular Articles</h5>
      <ul class="list-unstyled text-small">
        <li><a class="text-muted" href="<?php echo "$path";?>blog/empathy">Empathy: Beyond search intent</a></li>
        <li><a class="text-muted" href="<?php echo "$path";?>blog/keywords">Keywords: Treat them like employees</a></li>
        <li><a class="text-muted" href="<?php echo "$path";?>blog/increase-that-rate">105% conversion rate increase</a></li>
      </ul>
    </div>
    <div class="col-6 col-md">
      <h5>Company</h5>
      <ul class="list-unstyled text-small">
        <li><a href="<?php echo "$path";?>team">Team</a></li>
        <li><a href="<?php echo "$path";?>contact">Contact</a></li>
        <li><a href="<?php echo "$path";?>terms" target="_blank">Terms</a></li>
        <li><a href="https://www.iubenda.com/privacy-policy/86948563" class="iubenda-nostyle no-brand iubenda-embed " title="Privacy Policy">Privacy Policy</a> <script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script></li>
        <li><a href="https://www.iubenda.com/privacy-policy/86948563/cookie-policy" class="iubenda-nostyle no-brand iubenda-embed " title="Cookie Policy">Cookie Policy</a> <script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script></li>
      </ul>
    </div>
  </div>
</footer>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
