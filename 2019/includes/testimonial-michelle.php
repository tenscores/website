<div class="container testimonial text-center">
  <div class="testimonial-img michelle">
  </div>
  <span class="testimonial-txt">
      <span class="braket">&ldquo;</span>My Quality Scores have increased daily since I started using Tenscores. The 14 day trial is long enough to ensure it is a quality program. I would also recommend it because, for a small/micro business, the monthly fee is affordable, unlike other tools. I tried other tools and while they were ok, the price was too expensive to justify continued use. I have gotten better results with Tenscores and will continue past the trial.&rdquo;
   </span>
  <span class="testimonial-sig">Michelle Lee, Sales Manager At Junk It Junk Removal</span>
</div>
