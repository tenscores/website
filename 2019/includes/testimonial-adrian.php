<div class="container testimonial text-center">
  <div class="testimonial-img adrian">
  </div>
  <span class="testimonial-txt">
      <span class="braket">&ldquo;</span>Quality Score has always been the recognition of how search practitioners are optimising their accounts. By using TenScores to capture the trend, we can see which of the optimisations have had the biggest impact.&rdquo;
   </span>
  <span class="testimonial-sig">Adrian Cutler, Formerly Head Of Performance Products At iProspect</span>
</div>
