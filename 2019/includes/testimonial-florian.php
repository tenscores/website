<div class="container testimonial text-center">
  <div class="testimonial-img florian">
  </div>
  <span class="testimonial-txt">
      <span class="braket">&ldquo;</span>I’m falling in love with your tool, I'm going to test and improve one of our best performing Google As accounts. I think it should be possible to get some budget from my client when I can show them how good Tenscores works and saves a lot of time in optimizing the whole account.&rdquo;
   </span>
  <span class="testimonial-sig">Florian Muff, Head Of Advertising At Hutter Consult</span>
</div>
