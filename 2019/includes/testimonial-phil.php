<div class="container testimonial text-center">
  <div class="testimonial-img phil">
  </div>
  <span class="testimonial-txt">
      <span class="braket">&ldquo;</span>We've made a lot of improvements since using Tenscores - 63% increase in a very bad account from 3.3/10 to 5.4/10 Quality Score in just a few days and reducing cost-per-click by 31%.&rdquo;
   </span>
  <span class="testimonial-sig">Phil Sander Nielsen, Head Of Search At Deducta</span>
</div>
