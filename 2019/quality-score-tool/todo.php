<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">
    <title>Quality Score Tool: All Your Optimization Tasks In One Place</title>
    <meta name"description" content="All your optimization tasks can be found in one simple to do list so you can focus on one issue at a time with no disctractions.">
    <link rel="canonical" href="https://www.tenscores.com/quality-score-tool/todo/" />
  </head>
  <body class="qs qs-todo">

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">All tasks in one simple list<span class="ts-red">.</span></h1>
        <h2 class="lead">All your optimization tasks can be found in one simple to do list so you can focus on one issue at a time with no disctractions.</h2>
      </div>
    </section>

    <?php include 'qs-nav.php'; ?>

    <div class="container content last">
      <div class="row">
        <div class="col-12 order-sm-2 content-item">
          <div class="content-text text-center">
            <h4>Optimize one step at a time</h4>
            <p>You don't have to hunt them down, every issue found by Tenscores is exposed. Deal with them in no time.</p>
          </div>
        </div>
        <div class="col-12 order-sm-1 content-item">
          <div class="placeholder">
            <div class="content-img todo">
              <img src="../images/todo-list.png" alt="Google Ads Todolist" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="container-fluid bg-slant">
    </div>

    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-anonymous.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
