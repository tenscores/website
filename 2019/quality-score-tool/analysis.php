<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">
    <title>Quality Score Analysis Charts: Gauge, Distributions, Prioritization, etc</title>
    <meta name"description" content="Find out what your account Quality Score really is and view charts that will help you understand what it means.">
	  <link rel="canonical" href="https://www.tenscores.com/quality-score-tool/analysis/" />
  </head>
  <body class="qs qs-analysis">

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">Analyse Your Quality Scores<span class="ts-red">.<span></h1>
        <h2 class="lead">Find out what your account Quality Score really is and view charts that will help you understand what it means.</h2>
      </div>
    </section>

    <?php include 'qs-nav.php'; ?>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <h4>Account, campaign and ad group quality score</h4>
            <p>Quality Score is aggregated up to the account level for an instant view of how you're doing. </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img account-qs">
              <img src="../images/account-quality-score-3.png" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text">
            <h4>Components</h4>
            <p>Analyse the factors that influence Quality Score and learn where to optimize. </p>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder">
            <div class="content-img components">
              <img src="../images/quality-score-components.gif" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <h4>Distributions</h4>
            <p>See how your keywords and metrics are distributed along the Quality Score scale. </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img distributions">
              <img src="../images/quality-score-distribution.gif" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text">
            <h4>Penalties and discounts</h4>
            <p>Learn how much you're wasting on poor scores and how much you're gaining on high scores. </p>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/quality-score-discount.png" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <h4>Winners and losers</h4>
            <p>Campaign, ad groups, keywords are segmented into winners and losers. You'll know where to start. </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img regrouper-settings">
              <img src="../images/quality-score-losers.png" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>



    <div class="container-fluid bg-slant">
    </div>

    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-emily.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
