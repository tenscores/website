<div class="pl-1 mb-5">
    <ul class="nav nav-pills justify-content-center">
      <li class="nav-item text-center">
        <a class="nav-link qs-overview" href=".">
          <i class="ts-nav-icon icon-overview"></i>
          <span>Overview</span>
        </a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link qs-analysis" href="analysis">
          <i class="ts-nav-icon icon-gauge"></i>
          <span>Analysis</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link qs-regrouper text-center" href="regrouper">
          <i class="ts-nav-icon icon-regrouper"></i>
          <span>Regrouper</span>
        </a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link qs-ads" href="ads">
          <i class="ts-nav-icon icon-ads"></i>
          <span>Losing Ads</span>
        </a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link qs-history" href="history">
          <i class="ts-nav-icon icon-timeline"></i>
          <span>History</span>
        </a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link qs-todo" href="todo">
          <i class="ts-nav-icon icon-todo"></i>
          <span>Todo List</span>
        </a>
      </li>
    </ul>
</div>
