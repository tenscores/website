<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">

    <title>Google Ads Quality Score Optimization Tool | Tenscores</title>
    <meta name"description" content="The most in-depth Quality Score analysis and optimization tool to keep your Google Ads accounts healthy.">
	  <link rel="canonical" href="https://www.tenscores.com/quality-score-tool/" />

  </head>
  <body class="qs qs-overview">
    <div class="container-fluid bg-slant"></div>

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">Increase your Quality Scores<span class="ts-red">.</span></h1>
        <h2 class="lead">The most in-depth Quality Score analysis and optimization tool to keep your Google Ads accounts healthy.</h2>
      </div>
    </section>

    <?php include 'qs-nav.php'; ?>

    <div class="container-fluid text-center hero-container">
        <img src="../images/quality-score-tool-hero-3.png" class="hero-img shadow-big" alt="Quality Score Tool" />
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <span class="number">1</span>
            <h4>Analyse <span class="slimmed">your scores</span></h4>
            <p>Find out how healthy your Google Ads account really is and where the problems are. </p>
            <span class="dotted-line a1 d-xl-block"></span>
            <span class="dotted-line b1 d-xl-block"></span>
            <span class="dotted-line c1 d-xl-block"></span>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <a href="analysis.php">
              <img src="../images/quality-score-analysis-charts.png" class="img-fluid" />
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text text-xl-right">
            <span class="number">2</span>
            <h4>Optimize <span class="slimmed">by restructuring</span></h4>
            <p>Fix structure problems with our unique AI powered regrouping feature. </p>
            <span class="dotted-line a2 d-xl-block"></span>
            <span class="dotted-line b2 d-xl-block"></span>
            <span class="dotted-line c2 d-xl-block"></span>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder">
            <a href="regrouper.php">
              <img src="../images/regrouper-options.png" class="img-fluid" alt="Regrouper Options" />
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <span class="number">3</span>
            <h4>Track <span class="slimmed">your results</span></h4>
            <p>View historical Quality Scores to see if your optimizations are making an impact. </p>
            <span class="dotted-line a3 d-xl-block"></span>
            <span class="dotted-line b3 d-xl-block"></span>
            <span class="dotted-line c3 d-xl-block"></span>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <a href="history.php">
              <img src="../images/quality-score-timeline.png" class="img-fluid" alt="Quality Score History" />
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text text-xl-right">
            <span class="number">4</span>
            <h4>Uncover <span class="slimmed">losing ads</span></h4>
            <p>Instantly see campaigns and ad groups that have low performing ads. </p>
            <span class="dotted-line a4 d-xl-block"></span>
            <span class="dotted-line b4 d-xl-block"></span>
            <span class="dotted-line c4 d-xl-block"></span>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <a href="ads.php">
            <div class="placeholder container">
              <div class="row">
                <div class="col-6 loser-by-CTR">
                  <img src="../images/CTR-winning-ad.png" alt="Winner By CTR" class="img-fluid" />
                </div>
                <div class="col-6 loser-by-CTR">
                  <img src="../images/CTR-losing-ad.png" alt="Loser By CTR" class="img-fluid" />
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>

    <div class="container content focus">
      <div class="row">
        <div class="col-12 content-item">
          <div class="content-text text-center">
            <span class="number">5</span>
            <h4>Focus <span class="slimmed">without distractions</span></h4>
            <p>All issues are listed in one simple to-do list so you can go through them in a breeze. </p>
            <span class="dotted-line a5 d-xl-block"></span>
            <span class="dotted-line b5 d-xl-block"></span>
            <span class="dotted-line c5 d-xl-block"></span>
          </div>
        </div>
        <div class="col-12 content-item">
          <div class="placeholder">
            <a href="todo.php">
              <img src="../images/todo-list-1.png" class="img-fluid mx-auto d-block todo" alt="Todo List" />
            </a>
          </div>
        </div>
      </div>
    </div>


    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-phil.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
