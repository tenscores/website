<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">
    <title>Quality Score Tool: Uncover Losing Ads Driving Down Your CTR</title>
    <meta name"description" content="Ad CTR is the second step to increasing Quality Score. Find the ads that are pulling down your  account, in an instant.">
    <link rel="canonical" href="https://www.tenscores.com/quality-score-tool/ads/" />
  </head>
  <body class="qs qs-ads">

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">Uncover your losing ads<span class="ts-red">.</span></h1>
        <h2 class="lead">Ad CTR is the second step to increasing Quality Score. Find the ads that are pulling down your  account, in an instant.</h2>
      </div>
    </section>

    <?php include 'qs-nav.php'; ?>


    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-1 content-item">
          <div class="content-text">
            <h4>Find losers at a glance</h4>
            <p>Instantly see which campaigns and ad groups contain losing ads. </p>
          </div>
        </div>
        <div class="col-sm order-sm-2 content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/ctr-losers.png" alt="Adgroups containing CTR losing ads" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm-5 order-sm-2 content-item">
          <div class="content-text">
            <h4>Losers by CTR</h4>
            <p>CTR losers are driving down your account's overall CTR, find them. </p>
          </div>
        </div>
        <div class="col-sm-7 order-sm-1 content-item">
          <div class="placeholder container">
            <div class="row">
              <div class="col-6 loser-by-CTR">
                <img src="../images/CTR-winning-ad.png" alt="Winner By CTR" class="img-fluid" />
              </div>
              <div class="col-6 loser-by-CTR">
                <img src="../images/CTR-losing-ad.png" alt="Loser By CTR" class="img-fluid" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm-5 order-sm-1 content-item">
          <div class="content-text">
            <h4>Losers by conversions</h4>
            <p>Low converting ads are robbing you of new customers, find them instantly.</p>
          </div>
        </div>
        <div class="col-sm-7 order-sm-2 content-item">
          <div class="placeholder container">
            <div class="row">
              <div class="col-6 loser-by-CTR">
                <img src="../images/conv-winning-ad.png" alt="Winner By Conversion Rate" class="img-fluid" />
              </div>
              <div class="col-6 loser-by-CTR">
                <img src="../images/conv-losing-ad.png" alt="Loser By Conversion Rate" class="img-fluid" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text">
            <h4>Pause them easily</h4>
            <p>Every loser you find can be paused from the Tenscores interface. </p>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder">
            <div class="content-img pause-ads">
              <img src="../images/pause-them.gif" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm-5 order-sm-1 content-item">
          <div class="content-text">
            <h4>Replace them</h4>
            <p>Write new ads to replace the ones you've just paused. </p>
          </div>
        </div>
        <div class="col-sm-7 order-sm-2 content-item">
          <div class="placeholder">
            <div class="content-img write-ads">
              <img src="../images/write-new-ads.png" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text">
            <h4>Uncover missing ads</h4>
            <p>When an ad group only has one ad, you'll know and be able to give it a companion. </p>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder text-right">
            <div class="content-img">
              <img src="../images/no-ad-test.png" alt="Adgroups containing less than 2 ads" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>



    <div class="container-fluid bg-slant">
    </div>

    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-florian.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
