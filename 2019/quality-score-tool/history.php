<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">
    <title>Quality Score History: Timeline & Daily Tracking </title>
    <meta name"description" content="See the history of your Quality Scores daily from the time you joined Tenscores. Follow their progress as you optimize.">
    <link rel="canonical" href="https://www.tenscores.com/quality-score-tool/history/" />
  </head>
  <body class="qs qs-history">

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">Track your results<span class="ts-red">.</span></h1>
        <h2 class="lead">See the history of your Quality Scores daily from the time you joined Tenscores. Follow their progress as you optimize.</h2>
      </div>
    </section>

    <?php include 'qs-nav.php'; ?>

    <div class="container content timeline">
      <div class="row">
        <div class="col-12 content-item">
          <div class="content-text text-center">
            <h4>Historical timeline</h4>
            <p>View historical account, campaign, ad group or keyword Quality Score in beautiful timeline, not a data table.</p>
          </div>
        </div>
        <div class="col-12 content-item">
          <div class="placeholder">
            <div class="content-img history">
              <img src="../images/quality-score-history.png" alt="Google Ads Quality Score History" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm-7 order-sm-2 content-item" style="height: 23rem;">
          <div class="content-text">
            <h4>Compare with other metrics</h4>
            <p>See how it improves your metrics, from clicks to avg. CPC.  </p>
          </div>
        </div>
        <div class="col-sm-5 order-sm-1 content-item">
          <div class="placeholder text-right">
            <div class="content-img history">
              <img src="../images/compare-metrics.png" alt="Compare metrics" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <h4>Updated daily</h4>
            <p>Everyday is a new day, and so your Quality Scores are automatically fetched daily. </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img daily-updates">
              <img src="../images/daily-updates.png" alt="Daily Updates" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text" style="height:23rem;">
            <h4>Can be updated manually</h4>
            <p>When you've just made important changes to your account, you can update them manually and get fresh data.</p>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder text-right">
            <div class="content-img history">
              <img src="../images/manual-updates-2.gif" alt="Manual Updates" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm content-item">
          <div class="content-text">
            <h4>Export to csv</h4>
            <p>Get the flexibility to use your data as you wish with a CSV export. </p>
          </div>
        </div>
        <div class="col-sm content-item">
          <div class="placeholder">
            <div class="content-img history">
              <img src="../images/timeline-export.png" alt="Export to csv" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>



    <div class="container-fluid bg-slant">
    </div>

    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-adrian.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
