<!doctype html>
<html lang="en">
  <head>
    <?php include '../includes/meta.php'; ?>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">
    <title>Tenscores Regrouper: Restrucuting For Google Ads Campaigs and Ad groups</title>
    <meta name"description" content="Structuring ad groups in your campaigns in closely related families is the first step to Quality Score optimization.">
    <link rel="canonical" href="https://www.tenscores.com/quality-score-tool/regrouper/" />
  </head>
  <body class="qs qs-regrouper">

    <?php include '../includes/main-nav.php'; ?>

    <section class="jumbotron mb-0 text-center">
      <div class="container">
        <h1 class="jumbotron-heading">Restructure your campaigns<span class="ts-red">.</span></h1>
        <h2 class="lead">Structuring ad groups in your campaigns in closely related families is the first step to Quality Score optimization.</h2>
      </div>
    </section>

    <?php include 'qs-nav.php'; ?>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-1 content-item">
          <div class="content-text">
            <h4>See structure problems at a glance</h4>
            <p>Instantly see how many ad groups have structure problems in a campaign. </p>
          </div>
        </div>
        <div class="col-sm order-sm-2 content-item">
          <div class="placeholder">
            <div class="content-img">
              <img src="../images/issues-to-fix.png" alt="Google ads structure problems" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text">
            <h4>Fix them in a few steps</h4>
            <p>Restructure entire campaigns in an intuitive interface. </p>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder">
            <div class="content-img regrouper">
              <img src="../images/regrouper.png" alt="Google Ads Regrouper" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-1 content-item">
          <div class="content-text">
            <span>With SMAGs (Recommended)</span>
            <h4>Single match type ad groups</h4>
            <p>Stop match types from competing with each other with single match type ad groups. </p>
          </div>
        </div>
        <div class="col-sm order-sm-2 content-item">
          <div class="placeholder">
            <div class="content-img smag">
              <img src="../images/single-match-type-adgroup-smag.png" alt="Single Match Type Adgroups" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-2 content-item">
          <div class="content-text">
            <span>Or SKAGs</span>
            <h4>Single keyword ad groups</h4>
            <p>Go granular by chosing single keyword ad groups that will allow you to write hyper focused ads. </p>
          </div>
        </div>
        <div class="col-sm order-sm-1 content-item">
          <div class="placeholder">
            <div class="text-right">
              <img src="../images/single-keyword-adgroups-skag.png" alt="Single Keyword Adgroups (SKAGs)" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content">
      <div class="row">
        <div class="col-sm order-sm-1 content-item">
          <div class="content-text">
            <h4>Choose your settings</h4>
            <p>You can change how your ad groups are created by making them tighter or less.</p>
          </div>
        </div>
        <div class="col-sm order-sm-2 content-item">
          <div class="placeholder">
            <div class="content-img regrouper-settings">
              <img src="../images/regrouper-settings.png" alt="Regrouper Settings" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container content ai">
      <div class="row">
        <div class="col-5 order-sm-1 content-item bg-white rounded-left">
          <div class="content-text bg-white text-center">
            <h4>Ai</h4>
          </div>
        </div>
        <div class="col-7 order-sm-2 bg-white content-item">
          <div class="content-text">
            <p>The regrouper uses Google's machine learning technology to group keywords by search intent and in highly related families. </p>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid bg-slant">
    </div>

    <section class="ts-CTA text-center">
      <?php include '../includes/cta-button.php'; ?>
      <?php include '../includes/testimonial-martins.php'; ?>
    </section>

    <?php include '../includes/footer.php'; ?>

  </body>
</html>
