<!doctype html>
<html lang="en">
  <head>
    <?php include 'includes/meta.php'; ?>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <title>Tenscores Pricing: $25/month/account | 14-Days Free Trial</title>
    <meta name"description" content="Optimize your Google Ads. Pay per individual Google Ads account. Unlimited spend. 14 days free trial">
	  <link rel="canonical" href="https://www.tenscores.com" />

  </head>
  <body class="pricing">

    <?php include 'includes/main-nav.php'; ?>
    <div class="container-fluid bg-slant"></div>

    <section class="jumbotron mb-3 text-center animated fadeInDown">
      <div class="container">
        <h1 class="jumbotron-heading">One price<span class="ts-red">.</span> All features<span class="ts-red">.</span></h1>
        <h2 class="lead">Pay per individual Google Ads account. Unlimited spend. 14-days free trial.</h2>
      </div>
    </section>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm text-right">

        </div>
        <div class="col-sm text-center">

          <div class="card pricing-box" onclick="location.href='https://app.tenscores.com/signup';">
            <div class="card-body price-body">
              <div class="price-amount">
                <h3 class="card-title">$25</h3>
                <span class="price-package">/account/month</span>
              </div>
            </div>
            <div class="structure-fee">
              <h5 class="card-title"><span class="ts-gold">Structure Fee</span> per account</h5>
              <p class="card-text">Required to identify adgroup structure problems and fix them effortlessly.</p>
              <ul class="list-group list-group-flush">
                <li class="list-group-item d-flex justify-content-between">
                  <div>Up to 1,000 unique keywords</div>
                  <div><span class="ts-gold">Free</span></div>
                </li>
                <li class="list-group-item d-flex justify-content-between">
                  <div>Up to 5,000 unique keywords</div>
                  <div>$19/mo</div>
                </li>
                <li class="list-group-item d-flex justify-content-between">
                  <div>Up to 10,000 unique keywords</div>
                  <div>$99/mo</div>
                </li>
                <li class="list-group-item d-flex justify-content-center">
                  <div>More keywords? <a class="contact" href="mailto:support@tenscores.com">Contact us!</a></div>
                </li>
              </ul>
            </div>
          </div>
        </a>
        </div>

        <div class="col-sm text-left">
          <div class="pricing-detail d-sm-none d-md-block">
            <p><strong>Why is there a structure fee?</strong> For AI powered restructuring, required on unique keywords only. Match-types and duplicates are not counted.</p>
          </div>
        </div>

      </div>
    </div>

    <section class="ts-CTA text-center">
      <?php include 'includes/cta-button.php'; ?>
      <?php include 'includes/testimonial-michelle.php'; ?>
    </section>

    <?php include 'includes/footer.php'; ?>

  </body>
</html>
