<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" class=" js flexbox canvas canvastext postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache">
  <meta charset="utf-8">
   <title>Tenscores Team</title>
    <meta name="description" content="Who are the people behind Tenscores?">
    <link href="http://tenscores.com/team.php" rel="canonical">
    <?php include 'files/includes/meta.php'; ?>
    <?php include 'files/includes/tags.php'; ?>
  </meta>
</head>
<body id="team">
  <?php include 'files/includes/nav.php'; ?>
  <div class="header-img">
    <img src="files/montreal3.jpg">
    <div class="header-txt">
      <p> A small team on a mission to making pay-per-click advertising simple for the average business owner.</p>
    </div>
  </div>
  <article id="content">
    <!--
    <div class="visual">
      <h1></h1>
    </div>
  -->
    <div id="mc-container">
      <div class="container">
        <div class="small-container">

          <div class="team-member">
            <img src="files/chris.png" class="img-circle">
            <h4>Christian Nkurunziza</h4>
            <p>Marketing</p>
          </div>

          <div class="team-member">
            <img src="files/chret.png" class="img-circle">
            <h4>Chrétien Mwizerwa</h4>
            <p>Technology</p>
          </div>

          <div class="team-member">
           <img src="files/prynce.jpeg" class="img-circle">
           <h4>Prynce Comiso</h4>
           <p>Development</p>
          </div>

        </div>
      </div>
    </div>
  </article>

  <?php include 'files/includes/footer.php'; ?>

</body>
</html>
