<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" class=" js flexbox canvas canvastext postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache">
  <meta charset="utf-8">
   <title>Contact us</title>
    <meta name="description" content="How to get in touch with us.">
    <link href="http://tenscores.com/contact.php" rel="canonical">
    <?php include 'files/includes/meta.php'; ?>
    <?php include 'files/includes/tags.php'; ?>

  </meta>
</head>
<body id="legal">
  <?php include 'files/includes/nav.php'; ?>
  <article id="content">
    <div class="visual">
      <h1 class="align-left"></h1>
    </div>
    <div id="mc-container">
      <div class="container">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2796.273945109594!2d-73.57398168480395!3d45.50456373877077!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cc91a467b0e97af%3A0x7be4c1375659ee4d!2s2001+Boulevard+Robert-Bourassa%2C+Montr%C3%A9al%2C+QC+H3A+2A5%2C+Canada!5e0!3m2!1sen!2sth!4v1554786906794!5m2!1sen!2sth" width="800" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>


<p style="margin-top:50px"><strong>Canada</strong></p>
<p>2001 Boulevard Robert-Bourassa, Suite 1700 <br />
Montreal, QC, H3A 2A6 <br />
Canada <br />
</p>
<p>Phone: +1-855-330-2671</p>

<p style="margin-top:50px"><strong>The best way to reach us is by email:</strong></p>
<p>support@tenscores.com</p>


      </div>
    </div>
  </article>

  <?php include 'files/includes/footer.php'; ?>

</body>
</html>
