<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Join the notification list.</title>
<link rel="stylesheet" href="style.css">


</head>

<body>
        <div id="topWrapper"  style="margin:0 0 60px 0;">
            <div id="inWrap">
       
          <div id="beta">
          <h1>You're all set!</h1>
          <p>You've been successfully added to our notification list.
Thank you for your interest.</p>
          </div>

                  
        </div><!--inWrap-->
   </div><!--topWrapper-->
        
        <div id="footer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top" width="180">
                          <ul class="footerLinks">
                            <li class="title">Quality Score</li>
                                <li><a href="http://www.tenscores.com/book/quality-score-introduction/" target="_blank">What is Quality Score?</a></li>
                            <li><a href="http://www.tenscores.com/book/the-adwords-ad-auction/" target="_blank">What are its impacts?</a></li>
                            <li><a href="http://www.tenscores.com/book/lesson-1-get-highest-click-through-rates-ctr/" target="_blank">How do I increase it?</a></li>
                          </ul>
                        </td>
                        <td valign="top" width="120">
                            <ul class="footerLinks">
                                <li class="title">The Tool</li>
                                <li><a href="http://tenscores.com/overview.php" target="_blank">Overview</a></li>
                                <li><a href="http://tenscores.com/features.php" target="_blank">Features</a></li>
                              <li><a href="http://tenscores.com/pricing.php" target="_blank">Pricing</a></li>
                              <li><a href="http://tenscores.com/faq.php" target="_blank">F.A.Q</a></li>
                                <li><a href="http://tenscores.com/register.php" target="_blank">Sign-up</a></li>
                                <li><a href="http://tenscores.com/login.php" target="_blank">Login</a></li>
                                <li><a href="http://tenscores.com/support.php" target="_blank">Support</a></li>
                            </ul>
                        </td>
                        <td valign="top" width="120">
                            <ul class="footerLinks">
                                <li class="title">Tenscores</li>
                                <li><a href="http://tenscores.com/about.php" target="_blank">About</a></li>
                                <li><a href="http://tenscores.com/contact.php" target="_blank">Contact</a></li>
                              <li><a href="http://tenscores.com/terms.php" target="_blank">Terms</a></li>
                                <li><a href="http://tenscores.com/disclaimer.php" target="_blank">Disclaimer</a></li>
                            </ul>
                        </td>
                        <td valign="top" width="150">
                            <ul class="footerLinks">
                                <li class="title">Adwords Tips</li>
                                <li><a href="http://tenscores.com/blog" target="_blank">On the blog</a></li>
                              <li><a href="http://tenscores.com/book" target="_blank">In the book</a></li>
                            </ul>
                        </td>
                        <td align="right" valign="top">
                            <a href="https://adwords.google.com/professionals/profile/org?id=06066492393613028737&amp;hl=en" target="_blank">
                                <img src="images/adwords_certified_partner.gif" alt="Adwords Certified Partner" class="opacity" width="75" height="75" border="0" />
                               </a>
                         </td>
                      </tr>
              </table>
          </div><!--footer-->       

</body>
</html>
