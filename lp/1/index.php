<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Quality Score? There's a tool for it!</title>
<link rel="stylesheet" href="style.css">
<script type="text/javascript">
var addthis_share = {
  templates: { twitter: '[VIDEO] {{title}} : {{url}} via @tenscores' }
}
</script>

</head>

<body>
        <div id="topWrapper">
            <div id="inWrap">
                <div id="login">
                    <div align="center">
                        <a href="https://tenscores.com/login.php" title="Login">
                        <img src="images/key-hole.png" alt="Login" height="15" width="10" border="0" style="align:center; margin:0 3px 3px 0; vertical-align:middle;" />
                        </a>
                        <a href="https://tenscores.com/login.php" title="Login">login</a>                    
                    </div><!--center-->
          		</div><!--login-->
          
                  <a href="#" title="Play Video" 
                  	onClick="document.getElementById('video').style.display ='inline-block'; 
                  		   document.getElementById('ten').style.display ='none';">
                      <div id="ten" style="display:inline;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                                    <td width="499" align="right">
                                   
                                    <img src="images/10c.png" alt="10" border="0" class="opacity"  height="282" width="330" style="margin-top:100px;" /></td>
                            <td width="181" valign="bottom"><img src="images/play-video.png"  border="0" style="margin-bottom:50px;" /></td>
                              </tr>
                                </table>
                      </div>
                  <!--ten-->
                  </a>
          
                  <div id="video" style="display:none;">
                    <object width="640" height="360">
                        <param name="movie" value="https://www.youtube.com/v/c_5h5vSlhHE?version=3&amp;hl=en_US&amp;rel=0&autoplay=1"></param>
                        <param name="allowFullScreen" value="true"></param>
                        <param name="allowscriptaccess" value="always"></param>
                        <embed src="https://www.youtube.com/v/c_5h5vSlhHE?version=3&amp;hl=en_US&amp;rel=0&autoplay=1" type="application/x-shockwave-flash" width="640" height="360" 
                                    allowscriptaccess="always" allowfullscreen="true"></embed>
                    </object>
                    	<!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style " style="z-index:10;">
                        <a class="addthis_button_preferred_1"></a>
                        <a class="addthis_button_preferred_2"></a>
                        <a class="addthis_button_preferred_3"></a>
                        </div>
                        <script type="text/javascript" src="https://s7.addthis.com/js/250/addthis_widget.js#pubid=christianziza"></script>
                        <!-- AddThis Button END -->
                  </div>
                <!--video-->
          
                  <div id="join">  
                        <div align="center">
                            <a href="join.php">
                            <img src="images/join_f.png" alt="Join Tenscores" border="0" class="opacity" />                            </a> 
                          <p class="demo"><a href="http://tenscores.com/demo.php" target="_blank">Or play with the free demo account.</a></p>
               		</div><!--center-->
           		</div><!--join-->
        </div><!--inWrap-->
   </div><!--topWrapper-->
   
        
        <div id="footer">
        
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top" width="180">
                          <ul class="footerLinks">
                            <li class="title">Quality Score</li>
                                <li><a href="http://www.tenscores.com/book/quality-score-introduction/" target="_blank">What is Quality Score?</a></li>
                            <li><a href="http://www.tenscores.com/book/the-adwords-ad-auction/" target="_blank">What are its impacts?</a></li>
                            <li><a href="http://www.tenscores.com/book/lesson-1-get-highest-click-through-rates-ctr/" target="_blank">How do I increase it?</a></li>
                          </ul>
                        </td>
                        <td valign="top" width="120">
                            <ul class="footerLinks">
                                <li class="title">The Tool</li>
                                <li><a href="http://tenscores.com/overview.php" target="_blank">Overview</a></li>
                                <li><a href="http://tenscores.com/features.php" target="_blank">Features</a></li>
                              <li><a href="http://tenscores.com/pricing.php" target="_blank">Pricing</a></li>
                              <li><a href="http://tenscores.com/faq.php" target="_blank">F.A.Q</a></li>
                                <li><a href="http://tenscores.com/register.php" target="_blank">Sign-up</a></li>
                                <li><a href="http://tenscores.com/login.php" target="_blank">Login</a></li>
                                <li><a href="http://tenscores.com/support.php" target="_blank">Support</a></li>
                            </ul>
                        </td>
                        <td valign="top" width="120">
                            <ul class="footerLinks">
                                <li class="title">Tenscores</li>
                                <li><a href="http://tenscores.com/about.php" target="_blank">About</a></li>
                                <li><a href="http://tenscores.com/contact.php" target="_blank">Contact</a></li>
                              <li><a href="http://tenscores.com/terms.php" target="_blank">Terms</a></li>
                                <li><a href="http://tenscores.com/disclaimer.php" target="_blank">Disclaimer</a></li>
                            </ul>
                        </td>
                        <td valign="top" width="150">
                            <ul class="footerLinks">
                                <li class="title">Adwords Tips</li>
                                <li><a href="http://tenscores.com/blog" target="_blank">On the blog</a></li>
                              <li><a href="http://tenscores.com/book" target="_blank">In the book</a></li>
                            </ul>
                        </td>
                        <td align="right" valign="top">
                            <a href="https://adwords.google.com/professionals/profile/org?id=06066492393613028737&amp;hl=en" target="_blank">
                                <img src="images/adwords_certified_partner.gif" alt="Adwords Certified Partner" class="opacity" width="75" height="75" border="0" />
                               </a>
                         </td>
                      </tr>
              </table>
          </div><!--footer-->       

</body>
</html>
