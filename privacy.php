<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" class=" js flexbox canvas canvastext postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache">
  <meta charset="utf-8">
   <title>Privacy policy</title>
    <meta name="description" content="Tenscores Privacy Policy">
    <link href="http://tenscores.com/privacy.php" rel="canonical">
    <?php include 'files/includes/meta.php'; ?>
    <?php include 'files/includes/tags.php'; ?>
  </meta>
</head>
<body id="legal">
  <?php include 'files/includes/nav.php'; ?>
  <article id="content">
    <div class="visual">
      <h1 class="align-left">Privacy Policy</h1>
    </div>
    <div id="mc-container">
      <div class="container">
<p>This privacy policy sets out how Tenscores uses and protects any information that you give Tenscores when you use this website.</p>
<p>Tenscores is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.</p>
<p>Tenscores may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 17/09/2010.</p>
<p>
  <ol>
    <li>
      <p><strong>What we collect</strong><p>
      <p> We may collect the following information:</p>
      <p>
        <ul>
          <li>Full name</li>
          <li>Contact information including email address</li>
          <li>AdWords account ID</li>
          <li>AdWords email</li>
          <li>AdWords keyword reports (necessary for the tool to function)</li>
        </ul>
      </p>
    </li>
    <li>
      <p><strong>What we do with the information we gather</strong></p>
      <p>We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:</p>
      <p>
        <ul>
          <li>We mainly use the information to provide the services we offer.</li>
          <li>We may periodically send promotional emails about new products, special offers or other information
              which we think you may find interesting using the email address which you have provided.</li>
          <li>We also use it for Internal record keeping.</li>
          <li>From time to time, we may also use your information to contact you for market research purposes.</li>
        </ul>
      </p>
      <p>We may contact you by email, phone, fax or mail. We may use the information to customise the website according to your interests.</p>
    </li>
    <li><p><strong>Security</strong></p>
      <p>We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>
    </li>
    <li>
      <p><strong>How we use cookies</strong></p>
<p>A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit a
particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.
We use traffic log cookies to identify which pages are being used. This helps us analyse data about webpage traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.
Overall, cookies help us provide you with a better website by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.
You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website.</p>
</li>
<li><p><strong>Links to other websites</strong></p>
<p>Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.</p>
</li>
<li><p><strong>Controlling your personal information</strong></p>
<p>You may choose to restrict the collection or use of your personal information in the following ways:</p>
<p><ul>
  <li>You can unsubscribe from our newsletter any time by clicking on the link provided in each email we send.</li>
  <li>You may terminate your TenScores account anytime by sending us email at support@tenscores.com.</li>
</ul></p>
<p>We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.
You may request details of personal information which we hold about you under the Data Protection Act 1998. A small fee may be payable. If you would like a copy of the information held on you please write to suport@tenscores.com.
If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible at the above address. We will promptly correct any information found to be incorrect.</p>
</li>
    </div>
    </div>
  </article>

  <?php include 'files/includes/footer.php'; ?>

</body>
</html>
