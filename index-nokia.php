<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Optimize your Google Adwords Quality Scores with Tenscores.">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <?php include 'files/includes/tags.php'; ?>

    <title>

        TenScores: The Google Adwords Quality Score Optimization Tool

    </title>


      <link href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">
      <link href="docs/assets/css/toolkit-minimal.css" rel="stylesheet">
      <link href="docs/assets/css/application-minimal.css" rel="stylesheet">






      <style>
        /* note: this is a hack for ios iframe for bootstrap themes shopify page */
        /* this chunk of css is not part of the toolkit :) */
        body {
          width: 1px;
          min-width: 100%;
          *width: 100%;
        }

        /* Place Google Translate Top Bar at Bottom
        ---------------------------------------------------------------------*/
        .goog-te-banner-frame {
          top: initial !important;
          bottom: 0px !important;
        }
      </style>

  </head>


<body>




<nav class="navbar navbar-default navbar-fixed-top text-uppercase app-navbar" style="border-bottom: 1px solid #f1f1f1;">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed p-x-0" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">
        <img class="" alt="Tenscores" src="files/logo-v3.png" width="100">
      </a>
    </div>
    <div class="navbar-collapse collapse" id="navbar-collapse" style="font-weight:400">
      <ul class="nav navbar-nav navbar-right">
        <li >
          <a href="https://account.tenscores.com/login">Login</a>
        </li>
        <li>
          <a style=" margin-right:25px;" href="https://app.tenscores.com/register?ft=1">Signup</a>
        </li>
      </ul>
    </div><!--/.nav-collapse -->
</nav>


<div class="block app-block-intro" style="margin-top:70px; padding-bottom:0;">
  <div class="container text-center">
    <h1 class="block-title m-b-sm text-uppercase app-myphone-brand" style="font-size: 30px;letter-spacing:0;font-weight:600">When Cheap Is Better</h1>
    <div style="max-width:1100px; margin:auto;">
      <h2 class="lead m-b-lg" style="font-size: 32px">Join thousands of Adwords advertisers who get cheaper clicks from Google by improving their Quality Scores.</h2>
    </div>
    <img src="docs/assets/img/heroshot-2015-2.png" style="width:100%; margin-bottom:0%;">
    <a class="btn btn-lg btn-default-outline m-t-lg m-b-lg" style="padding: 15px 60px; text-transform: uppercase;" href="https://app.tenscores.com/register?ft=1">Try it free for 14 days</a>
  </div>
</div>

<div class="block" style="margin-top:0; padding: 0 30px 60px 0;">
  <div class="container text-center app-translate-15" data-transition="entrance">
    <blockquote class="pull-quote">
      <img class="img-circle" src="docs/assets/img/issam.jpg">
      <p>
        “Many struggle to gain this level of quality score analysis because Google’s current set-up doesn’t provide anything like this, so it has
        previously been a very painful and time-consuming process to try and assess it. TenScores provides recommendations on how you can optimise and further improve quality score in a really useful way.”
      </p>
      <cite>Issam Tidjani, Global SEM Lead At Nokia</cite>
    </blockquote>
  </div>
</div>
<div style="text-align:center; margin-bottom:20px;">
<!-- Google Translate -->
<div id="google_translate_element"></div>
<script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
  }
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<!-- end Google Translate -->
</div>


    <script src="docs/assets/js/jquery.min.js"></script>
    <script src="docs/assets/js/toolkit.js"></script>
    <script src="docs/assets/js/application.js"></script>
  </body>
</html>
