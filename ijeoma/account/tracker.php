<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Quality Score Tracker</title>

<!-- CSS Styling files -->
<link href="css/main.css" rel="stylesheet" type="text/css" />
<!-- /css -->

<!--Javascript Stuff -->
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<!--/Javascript -->

<!--Tracking scripts or other miscelenous header codes -->
<?php require_once('includes/header-codes.php'); ?>
<!--/header codes-->

</head>

<body>

    <!--Top Nav Bar -->
        <?php require_once('includes/top-bar.php'); ?>
    <!--/Top Bar-->
    
    <!--Middle Sub Nav Bar-->
        <?php require_once('includes/sub-menu.php'); ?>
    <!--/Sub Bar-->


    <div id="content">
        
        <!--Campaigns Navigation-->
            <?php require_once('includes/campaigns.php'); ?>
        <!--/Campaigns-->
        
        <div id="right-content">
            
            <!--Dashboard top: Breadcrumb & date comparisons-->
            <div id="right-content-topWrap">
                <?php require_once('includes/breadcrumb.php'); ?>
             </div> 
             <!--/Dashboard Top--> 
            
        </div><!--rigth-content ENd-->
        
    </div><!--Content ENd-->

    <!-- Trackings codes that go before the </body> tag -->
            <?php require_once('includes/footer-codes.php'); ?>
    <!-- /footer codes -->

</body>
</html>
