$(document).ready(function(){	
	layout()
	
	var keywords = [
		"project1",
		"project2",
		"project3",
		"project4",
		"keyword1",
		"keyword2",
		"keyword3",
		"keyword14",
		"campaign name",
		"group name",
		"campaing second name",
		"another group",			
	];
	$( "#search" ).autocomplete({
		source: keywords
	});
	
	$("input.clearMe, .clear").click(function(){
        inputValue = $(this).val();
        $(this).val('');
    }).blur(function(){
        if ($(this).val() == '') {
            $(this).val(inputValue);
        }
    });
	
	
	//toggle sub campaings on click on campaign link
	$( "#left-menu a.campaign-name" ).click(function(){
		size = $(this).parent('h3').next('div.sub-campaigns').children('ul').children('li').size()
		height = size * 21 - 1;	
		$(this).parent('h3').next('div.sub-campaigns').siblings('div.sub-campaigns').animate({height:0});	
		$(this).siblings('a.toggle').siblings('a.toggle').removeClass('minus');
		$(this).parent('h3').siblings('h3').removeClass('expanded');
		
		if ($(this).parent('h3').next('div.sub-campaigns').height()!=0) { 	//if the campaign it's visible, we close it and remove the active classes
			if ($(this).parent('h3').hasClass('expanded')) { 					//if the campaign is visible, we check if it's selected or not
				return false
			} else {
				$(this).parent('h3').addClass('expanded');				
			}		
		} else {																//if the campaign it not visible, we open it and add the active classes
			$(this).siblings('a.toggle').addClass('minus');
			$(this).parent('h3').addClass('expanded');
			$(this).parent('h3').next('div.sub-campaigns').animate({height:height});
			$('a#all').parent().removeClass('selected');
		};
		$('a#all').removeCLass('selected')
	});
	
	//toggle sub campaings on click on + sign
	$( "#left-menu a.toggle" ).click(function(){
		size = $(this).parent('h3').next('div.sub-campaigns').children('ul').children('li').size()
		height = size * 21 - 1;		
		
		if ($(this).parent('h3').next('div.sub-campaigns').height()!=0) {
			$(this).siblings('a.add-campaign').removeClass('minus');
			$(this).parent('h3').next('div.sub-campaigns').animate({height:0});
		} else {
			$(this).siblings('a.add-campaign').addClass('minus');
			$(this).parent('h3').next('div.sub-campaigns').animate({height:height});
		}
		$('a#all').parent().removeClass('selected');
		return false
	});
	
	//toggle all campaigns from All Campaigns + button
	$('.main-toggle').click(function(){
		var visible = 0;;
		$('div.sub-campaigns').each(function(){
			if ($(this).height()!=0) {
				visible++				
			}			
		})
		if (visible!=0) {
			$('a.toggle').removeClass('minus');
			$('#left-menu h3').removeClass('expanded');
			$('div.sub-campaigns').animate({height:0});		
		} else {
			$('a.toggle').addClass('minus');
			$('div.sub-campaigns').each(function(){
				size = $(this).children('ul').children('li').size()
				height = size * 21 - 1;		
				$(this).animate({height:height});
			});
		} 
	})
	
	$('a#all').click(function(){
		$(this).parent().addClass('selected');
		$( "#left-menu h3" ).removeClass('expanded')
		$('div.sub-campaigns').animate({height:0});		
	})
		
	$( "#compare input" ).datepicker({ dateFormat: 'M, dd, yy' });
	
})

//call layout function on resize window
$(window).resize(function() {
	layout()
});

//window to calculate the layout dimensions
function layout() {
	bw = $('body').width(); //get page width
	bh = $('body').height(); //get page height
	cw = bw - 246 //content width is page width minus left-menu width
	$('#right-content').css('width', cw + 'px'); //set width to right content div;
}

