$(document).ready(function(){	
	layout()
	
	var keywords = [
		"project1",
		"project2",
		"project3",
		"project4",
		"keyword1",
		"keyword2",
		"keyword3",
		"keyword14",
		"campaign name",
		"group name",
		"campaing second name",
		"another group",			
	];
	$( "#search" ).autocomplete({
		source: keywords
	});
	
	$("input.clearMe, .clear").click(function(){
        inputValue = $(this).val();
        $(this).val('');
    }).blur(function(){
        if ($(this).val() == '') {
            $(this).val(inputValue);
        }
    });
	
	$( "#left-menu" ).accordion();
	
	$( "#compare input" ).datepicker({ dateFormat: 'M, dd, yy' });
	
})

//call layout function on resize window
$(window).resize(function() {
	layout()
});

//window to calculate the layout dimensions
function layout() {
	bw = $('body').width(); //get page width
	bh = $('body').height(); //get page height
	cw = bw - 246 //content width is page width minus left-menu width
	$('#right-content').css('width', cw + 'px'); //set width to right content div;
}

