<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Quality Score Dashboard</title>

<!-- CSS Styling files -->
<link href="css/main.css" rel="stylesheet" type="text/css" />
<!-- /css -->

<!--Javascript Stuff -->
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<!--/Javascript -->

<!--Tracking scripts or other miscelenous header codes -->
<?php require_once('includes/header-codes.php'); ?>
<!--/header codes-->

</head>

<body>

    <!--Top Nav Bar -->
        <?php require_once('includes/top-bar.php'); ?>
    <!--/Top Bar-->
    
    <!--Middle Sub Nav Bar-->
        <?php require_once('includes/sub-menu.php'); ?>
    <!--/Sub Bar-->


    <div id="content">
        
        <!--Campaigns Navigation-->
            <?php require_once('includes/campaigns.php'); ?>
        <!--/Campaigns-->
        
        <div id="right-content">
            
            <!--Dashboard top: Breadcrumb & date comparisons-->
            <div id="right-content-topWrap">
                <?php require_once('includes/breadcrumb.php'); ?>
                <?php require_once('includes/date-comparison.php'); ?>
             </div> 
             <!--/Dashboard Top--> 
             
            <div class="clear"></div>
    
            <!--Dashboard 3 top boxes: CPC, QS, CTR (ATTENION: KEEP IN THIS ORDER: cpc/ctr/qs) -->
            <div id="cpc-qs-ctr-wrap">
                <?php require_once('includes/dashboard/cpc.php'); ?> <!-- leave order as is -->
                <?php require_once('includes/dashboard/ctr.php'); ?> <!-- leave order as is -->
                <?php require_once('includes/dashboard/qs.php'); ?>  <!-- leave order as is -->
            </div>
            <!-- /Top Boxes -->
            
            <!-- Dashboard charts -->
            <div id="charts">	
                <?php require_once('includes/dashboard/distributions.php'); ?>
                <?php require_once('includes/dashboard/priority.php'); ?>
                <?php require_once('includes/dashboard/health.php'); ?>
                <?php require_once('includes/dashboard/rising.php'); ?>
                <?php require_once('includes/dashboard/active-paused.php'); ?>
            </div>
            <!-- /dashbaord charts -->
            
        </div><!--rigth-content ENd-->
        
    </div><!--Content ENd-->

    <!-- Trackings codes that go before the </body> tag -->
            <?php require_once('includes/footer-codes.php'); ?>
    <!-- /footer codes -->

</body>
</html>
