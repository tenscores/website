<div id="left-col">
		<h2>
			<a href="#" class="add-campaign"></a>
			All campaigns
		</h2>
		<div id="left-menu">
			<h3><a href="#" class="active">Campaign 1</a></h3>
			<div class="sub-campaigns">
				<ul>
					<li><a href="#" class="active">Lorem ipsum</a></li>
					<li><a href="#" class="active">Dolor sit amet</a></li>
					<li><a href="#" class="active">Consectetur adipiscing</a></li>
					<li><a href="#" class="paused">Elit Duis sit amet</a></li>
					<li><a href="#" class="paused">Sed ullamcorper ultrices</a></li>
					<li><a href="#" class="paused">Rutrum In porta lectus</a></li>
				</ul>
			</div>
			<h3><a href="#" class="active">Campaign 2</a></h3>
			<div class="sub-campaigns">
				<ul>
					<li><a href="#" class="active">Lorem ipsum</a></li>
					<li><a href="#" class="active">Dolor sit amet</a></li>
					<li><a href="#" class="active">Consectetur adipiscing</a></li>
					<li><a href="#" class="paused">Elit Duis sit amet</a></li>
					<li><a href="#" class="paused">Sed ullamcorper ultrices</a></li>
					<li><a href="#" class="paused">Rutrum In porta lectus</a></li>
				</ul>
			</div>
			<h3><a href="#" class="active">Campaign 3</a></h3>
			<div class="sub-campaigns">
				<ul>
					<li><a href="#" class="active">Lorem ipsum</a></li>
					<li><a href="#" class="active">Dolor sit amet</a></li>
					<li><a href="#" class="active">Consectetur adipiscing</a></li>
					<li><a href="#" class="paused">Elit Duis sit amet</a></li>
					<li><a href="#" class="paused">Sed ullamcorper ultrices</a></li>
					<li><a href="#" class="paused">Rutrum In porta lectus</a></li>
				</ul>
			</div>
			<h3><a href="#" class="active">Campaign 4</a></h3>
			<div class="sub-campaigns">
				<ul>
					<li><a href="#" class="active">Lorem ipsum</a></li>
					<li><a href="#" class="active">Dolor sit amet</a></li>
					<li><a href="#" class="active">Consectetur adipiscing</a></li>
					<li><a href="#" class="paused">Elit Duis sit amet</a></li>
					<li><a href="#" class="paused">Sed ullamcorper ultrices</a></li>
					<li><a href="#" class="paused">Rutrum In porta lectus</a></li>
				</ul>
			</div>
			<h3><a href="#" class="active">Campaign 5</a></h3>
			<div class="sub-campaigns">
				<ul>
					<li><a href="#" class="active">Lorem ipsum</a></li>
					<li><a href="#" class="active">Dolor sit amet</a></li>
					<li><a href="#" class="active">Consectetur adipiscing</a></li>
					<li><a href="#" class="paused">Elit Duis sit amet</a></li>
					<li><a href="#" class="paused">Sed ullamcorper ultrices</a></li>
					<li><a href="#" class="paused">Rutrum In porta lectus</a></li>
				</ul>
			</div>
			<h3><a href="#" class="active">Campaign 6</a></h3>
			<div class="sub-campaigns">
				<ul>
					<li><a href="#" class="active">Lorem ipsum</a></li>
					<li><a href="#" class="active">Dolor sit amet</a></li>
					<li><a href="#" class="active">Consectetur adipiscing</a></li>
					<li><a href="#" class="paused">Elit Duis sit amet</a></li>
					<li><a href="#" class="paused">Sed ullamcorper ultrices</a></li>
					<li><a href="#" class="paused">Rutrum In porta lectus</a></li>
				</ul>
			</div>
			<h3><a href="#" class="active">Campaign 7</a></h3>
			<div class="sub-campaigns">
				<ul>
					<li><a href="#" class="active">Lorem ipsum</a></li>
					<li><a href="#" class="active">Dolor sit amet</a></li>
					<li><a href="#" class="active">Consectetur adipiscing</a></li>
					<li><a href="#" class="paused">Elit Duis sit amet</a></li>
					<li><a href="#" class="paused">Sed ullamcorper ultrices</a></li>
					<li><a href="#" class="paused">Rutrum In porta lectus</a></li>
				</ul>
			</div>
			<h3><a href="#" class="active">Campaign 8</a></h3>
			<div class="sub-campaigns">
				<ul>
					<li><a href="#" class="active">Lorem ipsum</a></li>
					<li><a href="#" class="active">Dolor sit amet</a></li>
					<li><a href="#" class="active">Consectetur adipiscing</a></li>
					<li><a href="#" class="paused">Elit Duis sit amet</a></li>
					<li><a href="#" class="paused">Sed ullamcorper ultrices</a></li>
					<li><a href="#" class="paused">Rutrum In porta lectus</a></li>
				</ul>
			</div>
			<h3><a href="#" class="active">Campaign 9</a></h3>
			<div class="sub-campaigns">
				<ul>
					<li><a href="#" class="active">Lorem ipsum</a></li>
					<li><a href="#" class="active">Dolor sit amet</a></li>
					<li><a href="#" class="active">Consectetur adipiscing</a></li>
					<li><a href="#" class="paused">Elit Duis sit amet</a></li>
					<li><a href="#" class="paused">Sed ullamcorper ultrices</a></li>
					<li><a href="#" class="paused">Rutrum In porta lectus</a></li>
				</ul>
			</div>
			<h3><a href="#" class="active">Campaign 10</a></h3>
			<div class="sub-campaigns">
				<ul>
					<li><a href="#" class="active">Lorem ipsum</a></li>
					<li><a href="#" class="active">Dolor sit amet</a></li>
					<li><a href="#" class="active">Consectetur adipiscing</a></li>
					<li><a href="#" class="paused">Elit Duis sit amet</a></li>
					<li><a href="#" class="paused">Sed ullamcorper ultrices</a></li>
					<li><a href="#" class="paused">Rutrum In porta lectus</a></li>
				</ul>
			</div>
		</div>
	</div>