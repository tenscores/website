<footer id="footer">
  <div class="inner">
    <div class="certified-badge" style="display:none;">
      <a target="_blank" href="https://www.google.com/partners/?hl=en-US#a_profile;idtf=4658125590;"><img src="files/PartnerBadge.png" width="215" height="81" alt="Google Adwords Certified Partner"></a>
      <p>Tenscores is a Certified Google AdWords Partner. <a target="_blank" href="https://www.google.com/partners/?hl=en-US#a_profile;idtf=4658125590;">Click to verify.</a></p>
    </div>
    <div class="product">
      <h4>Product</h4>
      <ul>
        <li><a href="http://www.tenscores.com/tour.php">Tour</a></li>
        <li><a href="http://www.tenscores.com/benefits.php">Benefits</a></li>
        <li><a href="http://www.tenscores.com/pricing.php">Pricing</a></li>
        <li><a href="http://www.tenscores.com/team.php">Team</a></li>
        <li><a href="http://www.tenscores.com/contact.php">Contact Us</a></li>
      </ul>
    </div>
    <div class="popular-articles">
      <h4>Popular Articles</h4>
      <ul>
        <li><a href="http://www.tenscores.com/blog/empathy/">Empathy: Beyond search intent</a></li>
        <li><a href="http://tenscores.com/blog/what-i-want">Keywords: Treat them like employees</a></li>
        <li><a href="http://tenscores.com/blog/increase-that-rate">105% conversion rate increase</a></li>
      </ul>
    </div>
    <div class="free-tools">
      <h4>Free Tools</h4>
      <ul>
        <li><a href="http://www.tenscores.com/calibrate" target="_blank">Calibrate</a></li>
        <li><a href="http://tenscores.com/quality-score">Quality Score Infographic</a></li>
      </ul>
    </div>
    <div class="legal">
      <h4>Legal</h4>
      <ul>
        <li><a href="https://www.iubenda.com/privacy-policy/86948563" class="iubenda-nostyle no-brand iubenda-embed " title="Privacy Policy">Privacy Policy</a> <script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script></li>
        <li><a href="https://www.iubenda.com/privacy-policy/86948563/cookie-policy" class="iubenda-nostyle no-brand iubenda-embed " title="Cookie Policy">Cookie Policy</a> <script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script></li>
      </ul>
    </div>

  </div>
  <div class="copyright">
    <p>©<script>document.write(new Date().getFullYear())</script> Tenscores Inc.</p>

<!-- Google Translate -->
<div id="google_translate_element"></div>
<script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
  }
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<!-- end Google Translate -->

    <!-- Heap Analytics -->
    <a style="margin: 50px 0; display: block;" rel="nofollow" href="https://heapanalytics.com/?utm_source=badge">
      <img width="108" height="41"
      style="width:108px;height:41px; -webkit-filter: grayscale(1); filter: grayscale(1);"
      src="files/badge.png" alt="Heap - iOS and Web Analytics">
    </a>
  </div>
  <!-- End Heap -->


</footer>
