<header id="header">
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
	  <div class="">
	    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </a>
        <a class="brand" href="http://www.tenscores.com/">
          <img src="http://tenscores.com/files/logo-2018-60.png" border="0" alt="Tenscores" align="middle" width="45">
        </a>
        <div class="nav-collapse collapse">
		      <ul class="nav">
            <li id="tour"><a href="http://tenscores.com/tour.php">Tour</a></li>
            <li id="benefits"><a href="http://tenscores.com/benefits.php">Benefits</a></li>
            <!-- <li id="reviews"><a href="http://tenscores.com/reviews.php">Reviews</a></li> -->
            <li id="pricing"><a href="http://tenscores.com/pricing.php">Pricing</a></li>
            <li id="blog"><a href="http://www.tenscores.com/blog/">Blog</a></li>
            <li class="dropdown" id="awesomeness">
              <a href="http://www.tenscores.com/#" class="dropdown-toggle" data-toggle="dropdown">Free <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="http://www.tenscores.com/sucker/">You a sucker?</a></li>
                <li><a href="http://www.tenscores.com/quality-score/">Quality Score Domination</a></li>
                <!-- <li><a target="_blank" href="http://www.tenscores.com/calibrate/">Bid Calibration</a></li> -->
                <!--
                <li><a href="http://www.tenscores.com/book/">Quality Score Mastery</a></li>
                -->
              </ul>
            </li>
            <li id="signup"><a class="btn btn-signup btn-danger" href="https://app.tenscores.com/register?lp=navbar&ft=1">Signup</a></li>
            <li id="login"><a class="btn btn-login" href="https://app.tenscores.com/login">Login</a></li>
          </ul>
		  </div>
	  </div>
    </div>
  </div>
</header>
