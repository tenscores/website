<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" class=" js flexbox canvas canvastext postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache">
  <meta charset="utf-8">
   <title>Disclaimer</title>
    <meta name="description" content="Tescores disclaimer">
    <link href="http://tenscores.com/disclaimer.php" rel="canonical">
    <?php include 'files/includes/meta.php'; ?>
    <?php include 'files/includes/tags.php'; ?>
  </meta>
</head>
<body>
  <?php include 'files/includes/nav.php'; ?>
  <article id="content">
    <div class="visual">
      <h1 class="align-left">Disclaimer</h1>
    </div>
    <div id="mc-container">
      <div class="container">
      <p>The information contained in this website is for general information
        purposes only. The information is provided by TenScores and while we
        endeavour to keep the information up to date and correct, we make no
        representations or warranties of any kind, express or implied, about the
        completeness, accuracy, reliability, suitability or availability with
        respect to the website or the information, products, services, or
        related graphics contained on the website for any purpose. Any reliance
        you place on such information is therefore strictly at your own risk.</p>
      <p>In no event will we be liable for any loss or damage including without
        limitation, indirect or consequential loss or damage, or any loss or damage
        whatsoever arising from loss of data or profits arising out of, or in
        connection with, the use of this website.</p>
      <p>Through this website you are able to link to other websites which are not under
        the control of TenScores. We have no control over the nature, content and
        availability of those sites. The inclusion of any links does not necessarily
        imply a recommendation or endorse the views expressed within them.</p>
      <p>Every effort is made to keep the website up and running smoothly.
        However, TenScores takes no responsibility for, and will not be liable
        for, the website being temporarily unavailable due to technical issues
        beyond our control.</p>
      </div>
    </div>
  </article>

  <?php include 'files/includes/footer.php'; ?>

</body>
</html>
