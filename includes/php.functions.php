<?php

function create_clients_table($mysql_base, $mysql_table){
	mysql_query("CREATE TABLE IF NOT EXISTS $mysql_base.$mysql_table (
  				Id bigint(20) NOT NULL auto_increment,
			  	Client varchar(100) NOT NULL,
			  	Tool varchar(10) NOT NULL,
			  	Plan varchar(10) NOT NULL,
			  	Token varchar(128) NOT NULL,
			  	Status varchar(10) NOT NULL,
			  	TimeZone varchar(10) NOT NULL,
			  	UpdateTime time NOT NULL default '23:00:00',
			  	Notifier tinyint(1) NOT NULL,
			  	SubscriptionId varchar(20) NOT NULL,
			  	PaymentDate varchar(30) NOT NULL,
			  	PayerFirstName varchar(30) NOT NULL,
			  	PayerLastName varchar(30) NOT NULL,
			  	PayerEmail varchar(100) NOT NULL,
			  	Amount varchar(6) NOT NULL,
			  	PayerCurrency varchar(5) NOT NULL,
			  	PayerCountry varchar(3) NOT NULL,
			  	PRIMARY KEY (`Id`))") or die ("MySql Error: ".mysql_error(0));
}

function format_email($email){
	return str_replace(array('_','+','-','@','.'),array('_____','____','___','__','_'),$email);
}

?>