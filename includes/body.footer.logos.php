
<div id="footer">

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px;">
  <tr>
    <td  valign="top" style="height: 7px; background:#E3E3FE url(images/border_main.gif) no-repeat top;"></td>
  </tr>
  <tr>
    <td  valign="top" style="border-left:1px #ccc solid; border-right:1px #ccc solid; background:#E3E3FE;">
    	<table width="100%" border="0" style="margin:5px 15px;">
          <tr>
            <td width="85"><a href="https://adwords.google.com/professionals/profile/org?id=06066492393613028737&amp;hl=en" rel="nofollow" target="_blank" onclick="_gaq.push(['_trackEvent', 'Image Click', 'Footer', 'Certified Partner Logo']);">
                                <img src="images/adwords_certified_partner.gif" alt="Adwords Certified Partner" class="opacity" width="75" height="75" border="0" /></a>
            </td>
            <td width="285" align="left" style="padding:0 10px; border-right:1px dotted #ccc;">
            
            
            <img src="../images/tracked.png" alt="1,500,000 keywords tracked" width="259" height="41" />
             <div>keywords currently being tracked</div>
            </td>
            <td width="597" style="padding-left:10px;">
            
            
            <table width="100%" border="0">
              <tr>
                <td width="241" align="left"><div>As seen on...</div>
                  <a href="http://searchengineland.com/quality-score-tracking-tool-an-interview-with-tenscores-founder-chris-thunder-64744" rel="nofollow" target="_blank" onclick="_gaq.push(['_trackEvent', 'Image Click', 'Footer', 'SEL logo']);"><img src="../images/sel275x65.png" alt="Search Engine Land" width="226" height="43" border="0" /></a></td>
                <td width="89" align="left"><a href="http://www.searchenginejournal.com/3-great-adwords-tools-you%E2%80%99ve-probably-never-heard-of/31830/" target="_blank" rel="nofollow" onclick="_gaq.push(['_trackEvent', 'Image Click', 'Footer', 'SEJ Logo']);"><img src="../images/sej.png" width="66" height="61" border="0" /></a></td>
                <td width="244" align="left"><a href="http://certifiedknowledge.org/blog/improve-quality-score-or-die/" rel="nofollow" target="_blank" onclick="_gaq.push(['_trackEvent', 'Image Click', 'Footer', 'CK Logo']);"><img src="../images/ck-logo-white.png" alt="Certified Knowledge" width="199" height="60" border="0" /></a></td>
              </tr>
            </table></td>
          </tr>
        </table>
    
    </td>
  </tr>
  <tr>
    <td valign="bottom" style="height: 7px; background:#E4E5FF url(images/border_main.gif) no-repeat bottom;"></td>
  </tr>
</table>




<img src="images/thank-you.png" class="thankyou" alt="Thank you!" width="120" height="33" />
  <div id="altnav">
  		<nobr>	&copy; 2011 Tenscores.com |
            <a href="about.php" rel="nofollow">About</a> | 
            <a href="contact.php" rel="nofollow">Contact</a> | 
           	<a href="support.php" rel="nofollow">Support</a> | 
            <a href="privacy.php" rel="nofollow">Privacy</a> | 
            <a href="terms.php" rel="nofollow">Terms Of Use</a> |
            <a href="disclaimer.php" rel="nofollow">Disclaimer</a> 
            </nobr> 
  </div>
</div><!-- End Footer -->

<!-- Piwik -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://www.tenscores.com/piwik/" : "http://www.tenscores.com/piwik/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 1);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://www.tenscores.com/piwik/piwik.php?idsite=1" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->

<!-- Performable -->
<script src="//d2f7h8c8hc0u7y.cloudfront.net/performable/pax/6He6pf.js" type="text/javascript"></script>
<!-- //performable -->