<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     32],
          ['Eat',      2],
          ['Commute',  2]
        ]);

        var options = {
          title: 'My Daily Activities',
          backgroundColor: 'none',
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body style="background:grey;">
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
  </body>
</html>