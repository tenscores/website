<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Here's the question... </title>
<style type="text/css" media="all">

body, html {padding:0; margin:0; height:100%;}

body {background:url(images/contest-bg.png) repeat; font-family:DinPro, 'Lucida Grande', Arial, sans-serif; }

@font-face {
    font-family: 'Bebas';
    src: url('fonts/bebas.eot');
    src: url('fonts/bebas.eot?#iefix') format('embedded-opentype'),
                 url('fonts/bebas.woff') format('woff'),
                 url('fonts/bebas.ttf') format('truetype'),
                 url('fonts/bebas.svg#DinPro') format('svg');
    font-weight: lighter;
    font-style: normal;
}

@font-face {
	font-family: 'DinPro';
    src: url('fonts/dinpromedium.eot');
    src: url('fonts/dinpromedium.eot?#iefix') format('embedded-opentype'), 
		 url('fonts/dinpromedium.woff') format('woff'), 
		 url('fonts/dinpromedium.ttf') format('truetype'), 
		 url('fonts/dinpromedium.svg#DinPro') format('svg');
    font-weight: normal;
    font-style: normal;
}

#centeredBox {
       width: 375px;
       margin:auto;
	   
}

#centeredBox #logo { 
		width: 130px; 
		margin: 130px 0 30px 0 ;
	
		}


#container {
	width: 375px; 
	border:1px solid rgb(128,128,128);

	border-radius:3px;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	background: #ffffff; /* Old browsers */
	background: -moz-linear-gradient(top,  #ffffff 0%, #e6e6e6 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e6e6e6)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  #ffffff 0%,#e6e6e6 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  #ffffff 0%,#e6e6e6 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  #ffffff 0%,#e6e6e6 100%); /* IE10+ */
	background: linear-gradient(top,  #ffffff 0%,#e6e6e6 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e6e6e6',GradientType=0 ); /* IE6-9 */
	}

#form {margin: 15px;}

ul {list-style-type:none; margin:0; padding:0;}

li input.text {
 	width: 303px;
	height:38px; 
	font-size:18px;
	margin: 5px 0; 
	border:1px solid rgb(128,128,128); 
	border-radius:4px; 
	-moz-border-radius:4px; 
	-webkit-border-radius:4px;
	padding:0 0 0 40px;
}

li input.user {
    background:#f8f8f8 url(../images/user.png) 12px no-repeat;
}

li input.email {
    background:#f8f8f8 url(../images/email_bg.png) -5px -2px no-repeat;
}

li input.password {
    background:#f8f8f8 url(../images/password_bg.png) -1px -2px no-repeat;
}

li input.user:hover {background:#fff url(../images/user.png) 12px no-repeat; }

li input.email:hover {
    
}


li input.submit {margin-top:3px;}

li input.submit:hover {opacity:0.6;}


.submit { 
    cursor:pointer;
    width:160px;
	font-size:14px; 
	height:38px;
	margin: 5px 0; 
	border:1px solid rgb(128,128,128); 
	border-radius:4px; 
	-moz-border-radius:4px; 
	-webkit-border-radius:4px;
	padding:0 20px;
	background: #ffffff; /* Old browsers */
	background: -moz-linear-gradient(top,  #ffffff 0%, #e6e6e6 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e6e6e6)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  #ffffff 0%,#e6e6e6 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  #ffffff 0%,#e6e6e6 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  #ffffff 0%,#e6e6e6 100%); /* IE10+ */
	background: linear-gradient(top,  #ffffff 0%,#e6e6e6 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e6e6e6',GradientType=0 ); /* IE6-9 */
}

#form_title { width: 300px; margin:auto;}
 
 h3 {font-family:DinPro, 'Lucida Grande', Arial, sans-serif; font-size:16px; text-transform: uppercase; font-weight:bold; }
 
 .form-textarea { border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; width: 365px; }
 
 .btn-custom {
  background-color: hsl(201, 100%, 30%) !important;
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#00a5ff", endColorstr="#006399");
  background-image: -khtml-gradient(linear, left top, left bottom, from(#00a5ff), to(#006399));
  background-image: -moz-linear-gradient(top, #00a5ff, #006399);
  background-image: -ms-linear-gradient(top, #00a5ff, #006399);
  background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #00a5ff), color-stop(100%, #006399));
  background-image: -webkit-linear-gradient(top, #00a5ff, #006399);
  background-image: -o-linear-gradient(top, #00a5ff, #006399);
  background-image: linear-gradient(#00a5ff, #006399);
  border-color: #006399 #006399 hsl(201, 100%, 25%);
  color: #fff !important;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.33);
  -webkit-font-smoothing: antialiased;
  text-transform: uppercase;
  font-weight: bold;
  border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;
  border:1px solid #333;
}


.form-submit-button {
	width: 370px;
	height: 35px;
	margin: 10px 0;
	}
 
 
 </style>
</head>

<body>
    <div id="centeredBox" >
        <div id="logo">
          <image src="../../images/logo-v2.png" class="logo" align="center" />
        </div>
     <div id="">
     <p>Here's the page again:</p>
     <p>http://tenscores.com/quality-score</p>
     
		
		
		<script src="http://jotform.ca/min/g=jotform?3.1.1466" type="text/javascript"></script>
<script type="text/javascript">
   JotForm.init();
</script>
<style type="text/css">
    .form-label{
        width:150px !important;
    }
    .form-label-left{
        width:150px !important;
    }
    .form-line{
        padding-top:12px;
        padding-bottom:12px;
    }
    .form-label-right{
        width:150px !important;
    }
    .form-all{
        width:650px;
        color:#555 !important;
        font-family:'Lucida Grande';
        font-size:14px;
    }
</style>

<form class="jotform-form" action="http://submit.jotform.ca/submit/30338024691248/" method="post" name="form_30338024691248" id="30338024691248" accept-charset="utf-8">
  <input type="hidden" name="formID" value="30338024691248" />
  <div class="form-all">
    <ul class="form-section">
      <li class="form-line" id="id_1">
        <label class="form-label-top" id="label_1" for="input_1"> What do you think about it? </label>
        <div id="cid_1" class="form-input-wide">
          <div class="form-single-column"><span class="form-radio-item" style="clear:left;"><input type="radio" class="form-radio" id="input_1_0" name="q1_whatDo1" value="Poor" />
              <label for="input_1_0"> Poor </label></span><span class="clearfix"></span><span class="form-radio-item" style="clear:left;"><input type="radio" class="form-radio" id="input_1_1" name="q1_whatDo1" value="Good enough" />
              <label for="input_1_1"> Good enough </label></span><span class="clearfix"></span><span class="form-radio-item" style="clear:left;"><input type="radio" class="form-radio" id="input_1_2" name="q1_whatDo1" value="Awesome!" />
              <label for="input_1_2"> Awesome! </label></span><span class="clearfix"></span><span class="form-radio-item" style="clear:left;"><input type="radio" class="form-radio" id="input_1_3" name="q1_whatDo1" value="Needs more work..." />
              <label for="input_1_3"> Needs more work... </label></span><span class="clearfix"></span>
          </div>
        </div>
      </li>
      <li class="form-line" id="id_3">
        <label class="form-label-top" id="label_3" for="input_3"> If you got two minutes, let me knwo how I could improve it. Thanks. </label>
        <div id="cid_3" class="form-input-wide">
          <textarea id="input_3" class="form-textarea" name="q3_ifYou" cols="40" rows="6"></textarea>
        </div>
      </li>
      <li class="form-line" id="id_2">
        <div id="cid_2" class="form-input-wide">
          <div style="text-align:left" class="form-buttons-wrapper">
            <button id="input_2" type="submit" class="form-submit-button">
              Send feedback
            </button>
          </div>
        </div>
      </li>
      <li style="display:none">
        Should be Empty:
        <input type="text" name="website" value="" />
      </li>
    </ul>
  </div>
  <input type="hidden" id="simple_spc" name="simple_spc" value="30338024691248" />
  <script type="text/javascript">
  document.getElementById("si" + "mple" + "_spc").value = "30338024691248-30338024691248";
  </script>
</form>                
                
                
                
                
                
       </div>            
    </div>
</body>
</html>
