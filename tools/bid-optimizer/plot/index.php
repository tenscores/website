<?php

if(isset($_GET["x"]) && isset($_GET["y"])){

	require('phplot.php');
	$x = explode(",",$_GET["x"]);
	$y = explode(",",$_GET["y"]);
	
	$data = array();
	for ($i = min($x); $i <= max($x); $i++){
		$data[] = array('', $i, $y[$i-min($x)]);
	}	
	
	$plot = new PHPlot(400,300);
	if($_GET["l"]=='C'){$plot->SetTitle('Adwords Costs Interpolation');}else{$plot->SetTitle('Adwords Profits Interpolation');}
	if($_GET["l"]=='C'){$plot->SetYTitle('Incremental Costs');}else{$plot->SetYTitle('Incremental Profits');}
	$plot->SetXTitle('Incremental Clicks');
	$plot->SetPlotType('lines');
	$plot->SetDataType('data-data');
	$plot->SetDataValues($data);
	$plot->DrawGraph();
}

?>