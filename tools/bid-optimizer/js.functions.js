
var cX = 0; var cY = 0; var rX = 0; var rY = 0;

function UpdateCursorPosition(e){ cX = e.pageX-210; cY = e.pageY;}

function UpdateCursorPositionDocAll(e){ cX = event.clientX; cY = event.clientY;}
if(document.all) { document.onmousemove = UpdateCursorPositionDocAll; }
else { document.onmousemove = UpdateCursorPosition; }

function AssignPosition(d) {
if(self.pageYOffset) {
	rX = self.pageXOffset;
	rY = self.pageYOffset;
	}
else if(document.documentElement && document.documentElement.scrollTop) {
	rX = document.documentElement.scrollLeft;
	rY = document.documentElement.scrollTop;
	}
else if(document.body) {
	rX = document.body.scrollLeft;
	rY = document.body.scrollTop;
	}
if(document.all) {
	cX += rX; 
	cY += rY;
	}
d.style.left = (cX+10) + "px";
d.style.top = (cY+10) + "px";
}

function HideContent(d) {
if(d.length < 1) { return; }
document.getElementById(d).style.display = "none";
}

function ShowContent(d) {
if(d.length < 1) { return; }
var dd = document.getElementById(d);
AssignPosition(dd);
dd.style.display = "block";
}

function hideHealth(d) {
if(d.length < 1) { return; }
document.getElementById(d).style.display = "none";
}

function showHealth(d,d1,d2,d3,d4,d5,d6,d7) {
if(d.length < 1) { return; }
var label = 'health_';
document.getElementById(label.concat(d1)).style.display = "none";
document.getElementById(label.concat(d2)).style.display = "none";
document.getElementById(label.concat(d3)).style.display = "none";
document.getElementById(label.concat(d4)).style.display = "none";
document.getElementById(label.concat(d5)).style.display = "none";
document.getElementById(label.concat(d6)).style.display = "none";
document.getElementById(label.concat(d7)).style.display = "none";
document.getElementById(label.concat(d)).style.display = "block";
}

function displayHealthInfo(){
	if(checkHealth.checked==true){document.getElementById('healthInfo').style.display = 'Block';}else{document.getElementById('healthInfo').style.display = 'none';}
}


function confirmSubmit(){
var agree=confirm("Are you sure you wish to continue?");
if (agree)
	return true ;
else
	return false ;
}

function setFocus(){ 
	if(document.getElementById('tables').value==''){
		document.getElementById('tablename').focus();
	}
} 


function progressiveCheck(num){
	if(healthScore.health_1.value <= num){healthScore.health_1.checked = true;}else{healthScore.health_1.checked = false;}
	if(healthScore.health_2.value <= num){healthScore.health_2.checked = true;}else{healthScore.health_2.checked = false;}
	if(healthScore.health_3.value <= num){healthScore.health_3.checked = true;}else{healthScore.health_3.checked = false;}
	if(healthScore.health_4.value <= num){healthScore.health_4.checked = true;}else{healthScore.health_4.checked = false;}
	if(healthScore.health_5.value <= num){healthScore.health_5.checked = true;}else{healthScore.health_5.checked = false;}
	if(healthScore.health_6.value <= num){healthScore.health_6.checked = true;}else{healthScore.health_6.checked = false;}
	if(healthScore.health_7.value <= num){healthScore.health_7.checked = true;}else{healthScore.health_7.checked = false;}
	if(healthScore.health_8.value <= num){healthScore.health_8.checked = true;}else{healthScore.health_8.checked = false;}
}

function uncheckAll(){
	healthScore.health_1.checked = false;
	healthScore.health_2.checked = false;
	healthScore.health_3.checked = false;
	healthScore.health_4.checked = false;
	healthScore.health_5.checked = false;
	healthScore.health_6.checked = false;
	healthScore.health_7.checked = false;
	healthScore.health_8.checked = false;
}

function checkUncheckAll(theElement){
	var theForm = theElement.form, z = 0;
	for(z=0; z<theForm.length;z++){
		if(theForm[z].type == 'checkbox' && theForm[z].name != 'checkAll'){
			theForm[z].checked = theElement.checked;
		}
	}
}