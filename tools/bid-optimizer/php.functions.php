<?php
function mysql_create_tables($mysql_table, $mysql_table_info, $mysql_table_emailtodb_mail, $mysql_table_emailtodb_attach, $page){

	mysql_query("DROP TABLE IF EXISTS $mysql_table") or die ("Request Error".mysql_error(0));
	mysql_query("CREATE TABLE $mysql_table (
	  	id int(11) NOT NULL AUTO_INCREMENT,
	  	KSExact varchar(50) NOT NULL,
	  	R001 int(2) NOT NULL DEFAULT '0',
	  	PRIMARY KEY (id))") or die ("Request Error".mysql_error(0));

	mysql_query("DROP TABLE IF EXISTS $mysql_table_info") or die ("Request Error".mysql_error(0));
	mysql_query("CREATE TABLE $mysql_table_info (
  		id int(11) NOT NULL AUTO_INCREMENT,
  		reportEmail varchar(60) NOT NULL,
  		reportTitle varchar(50) NOT NULL,
  		emailTime datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  		dbTime datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  		PRIMARY KEY (id))") or die ("Request Error".mysql_error(0));

	mysql_query("DROP TABLE IF EXISTS $mysql_table_emailtodb_mail") or die ("Request Error".mysql_error(0));
	mysql_query("CREATE TABLE $mysql_table_emailtodb_mail (
		ID int(11) NOT NULL auto_increment,
		IDEmail varchar(255) NOT NULL default '0',
		EmailFrom varchar(255) NOT NULL default '',
		EmailFromP varchar(255) NOT NULL default '',
		EmailTo varchar(255) NOT NULL default '',
		DateE datetime NOT NULL default '0000-00-00 00:00:00',
		DateDb datetime NOT NULL default '0000-00-00 00:00:00',
		DateRead datetime NOT NULL default '0000-00-00 00:00:00',
		DateRe datetime NOT NULL default '0000-00-00 00:00:00',
		Status tinyint(3) NOT NULL default '0',
		Type tinyint(3) NOT NULL default '0',
		Del tinyint(3) NOT NULL default '0',
		Subject varchar(255) default NULL,
		Message text  NOT NULL,
		Message_html text  NOT NULL,
		MsgSize int(11) NOT NULL default '0',
		Kind tinyint(2) NOT NULL default '0',
		IDre int(11) NOT NULL default '0',
		PRIMARY KEY  (ID),
		KEY IDEmail (IDEmail),
		KEY EmailFrom (EmailFrom))") or die ("Request Error".mysql_error(0));

	mysql_query("DROP TABLE IF EXISTS $mysql_table_emailtodb_attach") or die ("Request Error".mysql_error(0));
	mysql_query("CREATE TABLE $mysql_table_emailtodb_attach (
		ID int(11) NOT NULL auto_increment,
		IDEmail int(11) NOT NULL default '0',
		FileNameOrg varchar(255) NOT NULL default '',
		Filename varchar(255) NOT NULL default '',
		PRIMARY KEY  (ID),
		KEY IDEmail (IDEmail))") or die ("Request Error".mysql_error(0));
	
		
	setcookie(str_replace('_data','',$mysql_table)."_qsm_ids","",time()-3600,"/");
	setcookie(str_replace('_data','',$mysql_table)."_qsm_POSTids","",time()-3600,"/");
	setcookie(str_replace('_info','',$mysql_table_info)."_qsm_cols","",time()-3600,"/");
	setcookie(str_replace('_info','',$mysql_table_info)."_qsm_POSTcols","",time()-3600,"/");
	echo "<script language=\"Javascript\">window.location.href='".$page."';</script>";
}

function setToolMode($page){
	if(isset($_COOKIE['tenscores_qsm_toolMode'])){
		if(isset($_POST['toolMode']) && $_POST['toolMode']=='manual'){
			setcookie("tenscores_qsm_toolMode","manual",time()+365*24*3600,"/");
			unset($_SESSION['table']);
			echo "<script language=\"Javascript\">window.location.href='".$page."';</script>";
		}
		elseif(isset($_POST['toolMode']) && $_POST['toolMode']=='auto'){
			setcookie("tenscores_qsm_toolMode","auto",time()+365*24*3600,"/");
			unset($_SESSION['table']);
			echo "<script language=\"Javascript\">window.location.href='".$page."';</script>";
		}
	}
	else{
		setcookie("tenscores_qsm_toolMode","manual",time()+365*24*3600,"/");
		echo "<script language=\"Javascript\">window.location.href='".$page."';</script>";
	}
}

function showToolMode($mode){
	if($_COOKIE['tenscores_qsm_toolMode']==$mode){
		echo 'style="display: block;"';
	}else{echo 'style="display: none;"';}
}

function getColumns($mysql_table,$mysql_table_info,$page){
	if (isset($_FILES['adwordsReport']) || isset($_POST['reportsMail'])){
		setcookie(str_replace('_info','',$mysql_table_info)."_qsm_cols",'',time()-3600,"/");
		setcookie(str_replace('_info','',$mysql_table_info)."_qsm_POSTcols",'',time()-3600,"/");
	}
	
	$req = mysql_query("SELECT id FROM $mysql_table_info") or die ("Request Error".mysql_error(0));
	$cols = '(id,KSExact,';
	$cols2 = '(id,KSExact,';
	while ($col = mysql_fetch_array($req)){$cols .= "R".sprintf("%03d", $col['id']).',';}
	$cols = $cols.')';

	if (isset($_POST['cols'])){
		switch($_POST['cols']){
							
		case'hide':
		foreach ($_POST as $col){$cols = str_replace($col.',','',$cols);}
		$cols = str_replace(array('(id',',)'),array('id',''),$cols);
		break;
		
		case'show':
		foreach ($_POST as $col){$cols2 .= $col.',';}
		$cols2 = $cols2.')';
		$cols2 = str_replace(array('(id','show,',',)'),array('id','',''),$cols2);
		if($cols2 !='id,KSExact'){
			$cols = $cols2;		
		}
		else{
			$cols = str_replace(array('(id',',)'),array('id',''),$cols);
		}
		break;

		case'all':
		$cols = str_replace(array('(id',',)'),array('id',''),$cols);
		break;
		
		case'delete':
		$delCols = '';
		foreach ($_POST as $delCol){$delCols .= $delCol.',';}
		$delCols = $delCols.')';
		$delCols = str_replace(array('delete,',',)'),array('',''),$delCols);
		if($delCols !=')'){
			function int_alter(&$item){$item = intval(str_replace('R','',$item));} // Function used to convert ex: 006->6
			//Deleting POSTed columns
			$delCols = explode(',',$delCols);
			foreach($delCols as $delCol){
				mysql_query("ALTER TABLE $mysql_table DROP $delCol") or die ("Request Error".mysql_error(0));
			}
			//Renaming all columns after deletion
			$colsIds = str_replace(array('(id,','KSExact,',',)'),array('','',''),$cols);
			$colsIds = explode(',',$colsIds);
			$diffIds = array_diff($colsIds,$delCols);
			$colNum = 1;
			$newCols = '(id,KSExact,';
			foreach($diffIds as $colName){
				$newColName = "R".sprintf("%03d", $colNum);
				mysql_query("ALTER TABLE $mysql_table CHANGE $colName $newColName INTEGER") or die ("Request Error".mysql_error(0));
				$newCols .= $newColName.',';
				$colNum++;
			}
			$newCols = $newCols.')';

			//Updating the info table with reset of ids
			array_walk($delCols,'int_alter');
			$delCols = implode(',',$delCols);
			mysql_query("DELETE FROM $mysql_table_info WHERE id IN ($delCols)") or die ("Request Error".mysql_error(0));
			mysql_query("ALTER TABLE $mysql_table_info DROP id") or die("MySQL Error : ".mysql_error(0));
			mysql_query("ALTER TABLE $mysql_table_info AUTO_INCREMENT = 1") or die("MySQL Error : ".mysql_error(0));
			mysql_query("ALTER TABLE $mysql_table_info ADD id int UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST") or die("MySQL Error : ".mysql_error(0));

			$cols = $newCols;
		}
		
		$cols = str_replace(array('(id',',)'),array('id',''),$cols);
		$_POST['cols'] = 'all';
		break;							
		}
		setcookie(str_replace('_info','',$mysql_table_info)."_qsm_cols",$cols,time()+365*24*3600,"/");
		setcookie(str_replace('_info','',$mysql_table_info)."_qsm_POSTcols",$_POST['cols'],time()+365*24*3600,"/");
		echo "<script language=\"Javascript\">window.location.href='".$page."';</script>";
	}
}


function getRows($mysql_table,$page){
	if (isset($_FILES['adwordsReport']) || isset($_POST['reportsMail'])){
		setcookie(str_replace('_data','',$mysql_table)."_qsm_ids",'',time()-3600,"/");
		setcookie(str_replace('_data','',$mysql_table)."_qsm_POSTids",'',time()-3600,"/");
	}
	$req = mysql_query("SELECT id FROM $mysql_table") or die ("Request Error".mysql_error(0));
	$ids = '(';
	$ids2 = '(';
	while($tab = mysql_fetch_array($req)){$ids .= '['.$tab['id'].']';}
	$ids = $ids.')';

	if (isset($_POST['ids'])){
		switch($_POST['ids']){
							
		case'hide':
		foreach ($_POST as $id){$ids = str_replace('['.$id.']','',$ids);}
		$ids = str_replace('][',',',$ids);
		$ids = str_replace(array('[',']'),array('',''),$ids);
		break;
		
		case'show':
		foreach ($_POST as $id){$ids2 .= '['.$id.']';}
		$ids2 = $ids2.')';
		$ids2 = str_replace(array('[]','[show]'),array('',''),$ids2);
		$ids2 = str_replace('][',',',$ids2);
		$ids2 = str_replace(array('[',']'),array('',''),$ids2);
		if($ids2 !='()'){
			$ids = $ids2;		
		}
		else{
			$ids = str_replace('][',',',$ids);
			$ids = str_replace(array('[',']'),array('',''),$ids);
		}
		break;

		case'all':
		$ids = str_replace('][',',',$ids);
		$ids = str_replace(array('[',']'),array('',''),$ids);
		break;							
		
		case'delete':
		$delRows = '(';
		foreach ($_POST as $delRow){$delRows .= $delRow.',';}
		$delRows = $delRows.')';
		$delRows = str_replace(array('delete,','on,',',)'),array('','',')'),$delRows);
		if($delRows !='()'){
			//Deleting POSTed rows
			mysql_query("DELETE FROM $mysql_table WHERE id IN $delRows") or die ("Request Error".mysql_error());
			//Reset of ids
			mysql_query("ALTER TABLE $mysql_table DROP id") or die("MySQL Error : ".mysql_error(0));
			mysql_query("ALTER TABLE $mysql_table AUTO_INCREMENT = 1") or die("MySQL Error : ".mysql_error(0));
			mysql_query("ALTER TABLE $mysql_table ADD id int UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST") or die("MySQL Error : ".mysql_error(0));

			//Updating the ids cookie
			$reqActualRows = mysql_query("SELECT id FROM $mysql_table") or die ("Request Error".mysql_error(0));
			$actualRows = '(';
			while($actualRow = mysql_fetch_array($reqActualRows)){$actualRows .= '['.$actualRow['id'].']';}
			$actualRows = $actualRows.')';
			
			$ids = $actualRows;
		}
		$ids = str_replace('][',',',$ids);
		$ids = str_replace(array('[',']'),array('',''),$ids);
		$_POST['ids'] = 'all';
		break;							
		}

		setcookie(str_replace('_data','',$mysql_table)."_qsm_ids",$ids,time()+365*24*3600,"/");
		setcookie(str_replace('_data','',$mysql_table)."_qsm_POSTids",$_POST['ids'],time()+365*24*3600,"/");
		echo "<script language=\"Javascript\">window.location.href='".$page."';</script>";
	}
}



function displayTable($mysql_table, $mysql_table_info, $page){
	$cookie_cols = str_replace('_info','',$mysql_table_info)."_qsm_cols";
	$cookie_ids = str_replace('_data','',$mysql_table)."_qsm_ids";
	$cookie_POSTcols = str_replace('_info','',$mysql_table_info)."_qsm_POSTcols";
	$cookie_POSTids = str_replace('_data','',$mysql_table)."_qsm_POSTids";
	if(isset($_COOKIE[$cookie_cols])){
		$colsArray = explode(',',$_COOKIE[$cookie_cols]);
	}else{
		$reqColsArray = mysql_query("SELECT id FROM $mysql_table_info") or die ("Request Error".mysql_error(0));
		$colsArray = '(id,KSExact,';
		while ($colArray = mysql_fetch_array($reqColsArray)){$colsArray .= "R".sprintf("%03d", $colArray['id']).',';}
		$colsArray = $colsArray.')';
		$colsArray = str_replace(array('(id',',)'),array('id',''),$colsArray);
		$colsArray = explode(',',$colsArray);
	}
	$lastColumn = end($colsArray);
	$preLastColumn = $colsArray[count($colsArray)-2];
	if(isset($_GET['health'])){
		if(isset($_GET['sort']) && isset($_GET['for'])){
			if(isset($_COOKIE[$cookie_ids]) || isset($_COOKIE[$cookie_cols])){
				if(isset($_COOKIE[$cookie_ids])){if($_COOKIE[$cookie_ids]!='()'){if(isset($_COOKIE[$cookie_cols])){$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table WHERE id IN ".$_COOKIE[$cookie_ids]." AND (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).") ORDER BY ". $_GET['for']." ".$_GET['sort'];}else{$sqlrows = "SELECT * FROM $mysql_table WHERE id IN ".$_COOKIE[$cookie_ids]." AND (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).") ORDER BY ". $_GET['for']." ".$_GET['sort'];}
					}else{if(isset($_COOKIE[$cookie_cols])){$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table WHERE (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).") ORDER BY ". $_GET['for']." ".$_GET['sort'];}else{$sqlrows = "SELECT * FROM $mysql_table WHERE (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).") ORDER BY ". $_GET['for']." ".$_GET['sort'];}}
				}else{$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table WHERE (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).") ORDER BY ". $_GET['for']." ".$_GET['sort'];}
			}else{$sqlrows = "SELECT * FROM $mysql_table WHERE (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).") ORDER BY ". $_GET['for']." ".$_GET['sort'];}
		}
		else {
			if(isset($_COOKIE[$cookie_ids]) || isset($_COOKIE[$cookie_cols])){
				if(isset($_COOKIE[$cookie_ids])){if($_COOKIE[$cookie_ids]!='()'){if(isset($_COOKIE[$cookie_cols])){$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table WHERE id IN ".$_COOKIE[$cookie_ids]." AND (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).")";}else{$sqlrows = "SELECT * FROM $mysql_table WHERE id IN ".$_COOKIE[$cookie_ids]." AND (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).")";}
					}else{if(isset($_COOKIE[$cookie_cols])){$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table WHERE (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).")";}else{$sqlrows = "SELECT * FROM $mysql_table WHERE (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).")";}}
				}else{$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table WHERE (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).")";}
			}else{$sqlrows = "SELECT * FROM $mysql_table WHERE (".healthMarker($_GET['health'],$lastColumn,$preLastColumn).")";}
		}
	}
	else{
		if(isset($_GET['sort']) && isset($_GET['for'])){
			if(isset($_COOKIE[$cookie_ids]) || isset($_COOKIE[$cookie_cols])){
				if(isset($_COOKIE[$cookie_ids])){if($_COOKIE[$cookie_ids]!='()'){if(isset($_COOKIE[$cookie_cols])){$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table WHERE id IN ".$_COOKIE[$cookie_ids]." ORDER BY ". $_GET['for']." ".$_GET['sort'];}else{$sqlrows = "SELECT * FROM $mysql_table WHERE id IN ".$_COOKIE[$cookie_ids]." ORDER BY ". $_GET['for']." ".$_GET['sort'];}
					}else{if(isset($_COOKIE[$cookie_cols])){$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table ORDER BY ". $_GET['for']." ".$_GET['sort'];}else{$sqlrows = "SELECT * FROM $mysql_table ORDER BY ". $_GET['for']." ".$_GET['sort'];}}
				}else{$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table ORDER BY ". $_GET['for']." ".$_GET['sort'];}
			}else{$sqlrows = "SELECT * FROM $mysql_table ORDER BY ". $_GET['for']." ".$_GET['sort'];}
		}
		else {
			if(isset($_COOKIE[$cookie_ids]) || isset($_COOKIE[$cookie_cols])){
				if(isset($_COOKIE[$cookie_ids])){if($_COOKIE[$cookie_ids]!='()'){if(isset($_COOKIE[$cookie_cols])){$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table WHERE id IN ".$_COOKIE[$cookie_ids];}else{$sqlrows = "SELECT * FROM $mysql_table WHERE id IN ".$_COOKIE[$cookie_ids];}
					}else{if(isset($_COOKIE[$cookie_cols])){$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table";}else{$sqlrows = "SELECT * FROM $mysql_table";}}
				}else{$sqlrows = "SELECT ".$_COOKIE[$cookie_cols]." FROM $mysql_table";}
			}else{$sqlrows = "SELECT * FROM $mysql_table";}
		}
	}
	
	if(isset($_COOKIE[$cookie_cols])){$getid = "(".str_replace(array('id,KSExact,','R',''),array('','',''),$_COOKIE[$cookie_cols]).')';}
	$sqlcols = (isset($_COOKIE[$cookie_cols]))?(($getid!='(id,KSExact)')?("SELECT * FROM $mysql_table_info WHERE id IN ".$getid):("SELECT * FROM $mysql_table_info WHERE id='0'")):("SELECT * FROM $mysql_table_info");
	
	$req = mysql_query($sqlrows) or die ("Request Error".mysql_error(0));
	$reqm = mysql_query($sqlrows) or die ("Request Error".mysql_error(0));
	$reqc = mysql_query($sqlcols) or die ("Request Error".mysql_error(0));
	$reqi = mysql_query($sqlcols) or die ("Request Error".mysql_error(0));
	$reqt = mysql_query($sqlcols) or die ("Request Error".mysql_error(0));
	
	echo "<table cellpadding=\"0\" cellspacing=\"0\">
		<form name=\"displayCols\" method=\"post\" action=\"".$page."\">
		<tr>
			<td colspan=\"2\">
				<table width=\"100%\" cellpadding=\"0\" border=\"0\" cellspacing=\"0\"><tr>
				<td align=\"right\">
					<select name=\"cols\" onChange=\"if(this.value=='delete'){if(confirmSubmit()==true){this.form.submit();}}else{this.form.submit();}\" "; if(!mysql_num_rows($req)){echo "disabled";} echo">
					<option value=\"all\" "; if(isset($_COOKIE[$cookie_POSTcols]) && $_COOKIE[$cookie_POSTcols]=='all'){echo "selected=\"selected\"";} echo ">All</option>
					<option value=\"hide\" "; if(isset($_COOKIE[$cookie_POSTcols]) && $_COOKIE[$cookie_POSTcols]=='hide'){echo "selected=\"selected\"";} echo ">Hide</option>
					<option value=\"show\" "; if(isset($_COOKIE[$cookie_POSTcols]) && $_COOKIE[$cookie_POSTcols]=='show'){echo "selected=\"selected\"";} echo ">Show</option>
					<option value=\"delete\">Delete</option>
					</select>
				</td></tr>
				</table>
			</td>";
			
			// Displaying checkboxes for columns
			while($cbox = mysql_fetch_array($reqc)){
				echo "<td align=\"center\"><input type=\"checkbox\" name=\"R".sprintf("%03d", $cbox['id'])."\" value=\"R".sprintf("%03d", $cbox['id'])."\" /></td>";
			}
			
			echo "<td></td>
		</tr>
		</form>
		<form name=\"displayRows\" method=\"post\" action=\"".$page."\">
		<tr>
			<td colspan=\"2\">
				<select name=\"ids\" onChange=\"if(this.value=='delete'){if(confirmSubmit()==true){this.form.submit();}}else{this.form.submit();}\" "; if(!mysql_num_rows($req)){echo "disabled";} echo">
				<option value=\"all\" "; if(isset($_COOKIE[$cookie_POSTids]) && $_COOKIE[$cookie_POSTids]=='all'){echo "selected=\"selected\"";} echo ">All</option>
				<option value=\"hide\" "; if(isset($_COOKIE[$cookie_POSTids]) && $_COOKIE[$cookie_POSTids]=='hide'){echo "selected=\"selected\"";} echo ">Hide</option>
				<option value=\"show\" "; if(isset($_COOKIE[$cookie_POSTids]) && $_COOKIE[$cookie_POSTids]=='show'){echo "selected=\"selected\"";} echo ">Show</option>
				<option value=\"delete\">Delete</option>
				</select>
			</td>";
			
			// Displaying info layers and sort buttons
			while($ilayer = mysql_fetch_array($reqi)){
				echo "<td align=\"center\"><a onmousemove=\"ShowContent('".sprintf("%03d", $ilayer['id'])."'); return true;\" onmouseover=\"ShowContent('".sprintf("%03d", $ilayer['id'])."'); return true;\" onmouseout=\"HideContent('".sprintf("%03d", $ilayer['id'])."'); return true;\" href=\"javascript:ShowContent('".sprintf("%03d", $ilayer['id'])."')\"><img src=\"info.png\" width=\"15\" height=\"15\" border=\"0\" /></a>
					<div id=\"".sprintf("%03d", $ilayer['id'])."\" style=\"display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;\"><nobr><b>Report name:</b> ".$ilayer['file']."</nobr><br /><nobr><b>Here from:</b> ".$ilayer['time']."</nobr></div></td>";
			}
			
			echo "<td></td>
		</tr>
		<tr>
			<td align=\"center\"><input type=\"checkbox\" name=\"checkAll\" onclick=\"checkUncheckAll(this);\" "; if(!mysql_num_rows($req)){echo "disabled";} echo" /></td>
			
			<td align=\"center\"><a href=\"".$page."&sort="; if(isset($_GET['sort'])){ echo ($_GET['sort']=='ASC')?("DESC"):("ASC"); } else{echo "ASC";} echo "&for=KSExact\">Keyword: Exact</a></td>";
			
			while($sort = mysql_fetch_array($reqt)){
				echo "<td align=\"center\" style=\"padding:0 3px 0 3px;\"><a href=\"".$page."&sort="; if(isset($_GET['sort'])){ echo ($_GET['sort']=='DESC')?("ASC"):("DESC"); } else{echo "DESC";} echo "&for=R".sprintf("%03d", $sort['id'])."\">".sprintf("%03d", $sort['id'])."</a></td>";
			}
		echo "<td align=\"center\" style=\"padding:4px;\"><a href=\"".$page."\"><img src=\"reload.png\" width=\"20\" height=\"20\" border=\"0\" /></a></td>
			</tr>";
	
	// Displaying checkboxes for rows, keywordSite, quality scores and graph buttons
	if(mysql_num_rows($req)){
		while($tab = mysql_fetch_array($req)){
			echo "<tr onMouseOver=\"this.style.backgroundColor='#E9E9E9';\" onMouseOut=\"this.style.backgroundColor='#FFFFFF';\">
					<td align=\"center\"><input type=\"checkbox\" name=\"".$tab[0]."\" id=\"".$tab[0]."\" value=\"".$tab[0]."\" /></td>";
			for($j=1; $j < count($tab)/2; $j++){
				echo "<td align=\"center\" "; if(isset($j) && $j>1){ if($tab[$j]<='3'){echo "style=\"color:red;\"";} elseif($tab[$j]>='7'){echo "style=\"color:green;\"";}else{echo "style=\"color:black;\"";}} echo " nowrap><label for=\"".$tab[0]."\">"; echo ($tab[$j]!='0')?($tab[$j]):("-"); echo"</td></label>";
			}	
			echo "<td align=\"center\"><a href=\"#\" onClick=\"javascript:window.open('graph.php?id=". $tab[0] ."','QS_Graph','menubar=0,resizable=1,status=0,width=550,height=310');\"><img src=\"graph.png\" width=\"16\" height=\"16\" border=\"0\" /></a></td>
				</tr>";
		}

		echo "<tr style=\"height:30px;\">
			<td></td>";
			// Displaying Keywords count and columns AVG
			$p=0; while($tabm = mysql_fetch_array($reqm)){ $avg[$p] = $tabm; $p++;}
			$nbKeywords = count($avg);
			echo "<td align=\"center\"><b>Keywords count: $nbKeywords &nbsp;|&nbsp; Column AVG:&nbsp;</b></td>";
		
			for($c=2; $c < count($avg[0])/2; $c++){
				$colSum=0; $paused=0;
				for($r=0; $r < $nbKeywords; $r++){
					if($avg[$r][$c]=='0'){$paused++;} // Counting paused keywords
					$colSum += $avg[$r][$c];
				}
				if($colSum!=0){echo "<td align=\"center\" class=\"colAvg\"><b>". round($colSum/($nbKeywords-$paused),1) ."</b></td>";}
				else{echo "<td align=\"center\" class=\"colAvg\"><b>0</b></td>";} // Printing AGV with handling exception:0/0
			}
			
			echo "<td></td>
			</tr>";
	}
	else {
		echo "<tr>
				<td colspan=\"2\" align=\"center\"><i>Empty Table</i></td>
			</tr>";
	}
	
	echo "</table>
	</form>";
}


function unzipReports($mysql_table_attach,$folder){

	$req = mysql_query("SELECT * FROM $mysql_table_attach") or die ("Request Error".mysql_error(0));
	
	while($tab = mysql_fetch_array($req)){
	
		$path = $folder.'/'.$tab['Filename'];
		if($tab['FileNameOrg']=='report-xml.zip'){
			// Extracting the .xml report...
			$adwordsZip = new ZipArchive;
			if ($adwordsZip->open($path)) {
				$dir = str_replace("-xml.zip","", $path);
				mkdir($dir);
				$adwordsZip->extractTo($dir);
				$adwordsZip->close();
			} else {
				echo "<div align=\"center\"><font color=\"red\">Sorry, archive extraction failed!</font></div>";
			}
		}
	}
}



function uploadReport($mysql_base, $mysql_table, $mysql_table_info,$page){
	
		if (isset($_FILES['adwordsReport'])){
			$folder = 'upload/';
			$file = basename($_FILES['adwordsReport']['name']);
			$maxSize = 100000;
			$size = filesize($_FILES['adwordsReport']['tmp_name']);
			$extensions = array('.xml');
			$extension = strrchr($_FILES['adwordsReport']['name'], '.'); 
			
			// Security Verifications...
			if (!in_array($extension, $extensions)){$error = "<div align=\"center\"><font color=\"red\">File extension must be: <strong>.xml</strong>!</font></div>";}
			if ($size>$maxSize){$error = "<div align=\"center\"><font color=\"red\">File size exceed <strong>100 Ko</strong>!</font></div>";}
			if (!isset($error)){
				 $file = strtr($file, 
					  '����������������������������������������������������', 
					  'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
				 $file = preg_replace('/([^.a-z0-9]+)/i', '-', $file);
				 if (move_uploaded_file($_FILES['adwordsReport']['tmp_name'], $folder . $file)){
					  $info = "<div align=\"center\"><font color=\"green\">File Uploaded Successfully!</font></div>";
				 }
				 else {$info = "<div align=\"center\"><font color=\"red\">Upload Failed!</font></div>";}
		
		
			// Updating $mysql_table_info...
			mysql_query("INSERT INTO $mysql_table_info (file, time) VALUES ('".$file."', '".date('D, j M y, h:i a')."')") or die("MySQL Error : ".mysql_error(0));
			// RESET ID Column
			mysql_query("ALTER TABLE $mysql_table_info DROP id") or die("MySQL Error : ".mysql_error(0));
			mysql_query("ALTER TABLE $mysql_table_info AUTO_INCREMENT = 1") or die("MySQL Error : ".mysql_error(0));
			mysql_query("ALTER TABLE $mysql_table_info ADD id int UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST") or die("MySQL Error : ".mysql_error(0));
			
			// Updating $mysql_table...
			$adwordsReport = simplexml_load_file($folder.$file);
			$req = mysql_query("SELECT * FROM $mysql_table") or die ("Request Error".mysql_error(0));
			
			if(isset($adwordsReport->children()->table->rows)){ // If report is OLD version
				if(mysql_num_rows($req)){
					$columns = mysql_query("SHOW COLUMNS FROM $mysql_base.$mysql_table") or die ("Request Error".mysql_error(0));	
					$nextColumnName = "R".sprintf("%03d", mysql_num_rows($columns)-1);
					mysql_query("ALTER TABLE $mysql_table ADD $nextColumnName int(2) NOT NULL DEFAULT '0'") or die("MySQL Error : ".mysql_error(0));
					
					// Updating existing keywords...
					foreach ($adwordsReport->children()->table->rows->row as $row){
						if (findAttribute($row,'kwSiteType')=='Exact'){
							mysql_query("UPDATE $mysql_table SET $nextColumnName='".findAttribute($row,'qualityScore')."' WHERE KSExact='".findAttribute($row,'kwSite')."'") or die("MySQL Error : ".mysql_error(0));
						}
					}
					
					// Adding new keywords...
					foreach ($adwordsReport->children()->table->rows->row as $row){
						if (findAttribute($row,'kwSiteType')=='Exact'){
							if (mysql_num_rows(mysql_query("SELECT id FROM $mysql_table WHERE KSExact='".findAttribute($row,'kwSite')."'"))){}
							else {
								mysql_query("INSERT INTO $mysql_table (KSExact, $nextColumnName) VALUES ('".findAttribute($row,'kwSite')."', '".findAttribute($row,'qualityScore')."')") or die("MySQL Error : ".mysql_error(0));
							}
						}
					}
				}
				else {
					foreach ($adwordsReport->children()->table->rows->row as $row){
						if (findAttribute($row,'kwSiteType')=='Exact'){
							mysql_query("INSERT INTO $mysql_table (KSExact, R001) VALUES ('".findAttribute($row,'kwSite')."', '".findAttribute($row,'qualityScore')."')") or die("MySQL Error : ".mysql_error(0));
						}
					}
				}
			}
			else{ // If report is NEW version
				if(mysql_num_rows($req)){
					$columns = mysql_query("SHOW COLUMNS FROM $mysql_base.$mysql_table") or die ("Request Error".mysql_error(0));	
					$nextColumnName = "R".sprintf("%03d", mysql_num_rows($columns)-1);
					mysql_query("ALTER TABLE $mysql_table ADD $nextColumnName int(2) NOT NULL") or die("MySQL Error : ".mysql_error(0));
					
					// Updating existing keywords...
					foreach ($adwordsReport->children()->table->row as $row){
						if (preg_match("/\[(.*)\]/",findAttribute($row,'keyword'))){ // Condition for keywordExact format : "[keyword]"
							mysql_query("UPDATE $mysql_table SET $nextColumnName='".findAttribute($row,'qualityScore')."' WHERE KSExact='".str_replace(array('[',']'),array('',''),findAttribute($row,'keyword'))."'") or die("MySQL Error : ".mysql_error(0));
						}
					}
					
					// Adding new keywords...
					foreach ($adwordsReport->children()->table->row as $row){
						if (preg_match("/\[(.*)\]/",findAttribute($row,'keyword'))){
							if (mysql_num_rows(mysql_query("SELECT id FROM $mysql_table WHERE KSExact='".str_replace(array('[',']'),array('',''),findAttribute($row,'keyword'))."'"))){}
							else {
								mysql_query("INSERT INTO $mysql_table (KSExact, $nextColumnName) VALUES ('".str_replace(array('[',']'),array('',''),findAttribute($row,'keyword'))."', '".findAttribute($row,'qualityScore')."')") or die("MySQL Error : ".mysql_error(0));
							}
						}
					}
				}
				else {
					foreach ($adwordsReport->children()->table->row as $row){
						if (preg_match("/\[(.*)\]/",findAttribute($row,'keyword'))){
							mysql_query("INSERT INTO $mysql_table (KSExact, R001) VALUES ('".str_replace(array('[',']'),array('',''),findAttribute($row,'keyword'))."', '".findAttribute($row,'qualityScore')."')") or die("MySQL Error : ".mysql_error(0));
						}
					}
				}
			}
		
			// Deleting the uploaded file after use... and cookies too..
			unlink($folder.$file);
			echo "<script language=\"Javascript\">window.location.href='".$page."';</script>";
			}
			else {echo $error;}
			
			echo "<script language=\"Javascript\">window.location.href='".$page."';</script>";
		}
		
		else{echo "<div align=\"center\">Please, Upload a file...</div>";}
}


function clearData($table_attach, $table_email, $folder){

	$req = mysql_query("SELECT * FROM $table_attach") or die ("Request Error".mysql_error(0));
	
	while($tab = mysql_fetch_array($req)){
		if($tab['FileNameOrg']=='report-xml.zip'){
			$path = $folder.'/'.$tab['Filename'];
			$subdir = str_replace("-xml.zip","", $path);
			//$dir = str_replace("-xml.zip","", $subdir);
			unlink($path);
			unlink($subdir."/report.xml");
			rmdir($subdir);
		}
	}
	
	mysql_query("TRUNCATE TABLE $table_attach") or die ("Request Error".mysql_error(0));
	mysql_query("TRUNCATE TABLE $table_email") or die ("Request Error".mysql_error(0));
}


function updateKeywords($folder, $mysql_base, $mysql_table_attach, $mysql_table_email, $mysql_table, $mysql_table_info){

	// Updating $mysql_table_info...
	$info = mysql_query("SELECT EmailTo, Subject, DateE, DateDb FROM $mysql_table_email") or die ("Request Error".mysql_error(0));
	while($box = mysql_fetch_array($info)){
		if(strpos($box['Subject'],'|')=='23'){ // Verifying if email is an adwords report
			mysql_query("INSERT INTO $mysql_table_info (reportEmail, reportTitle, emailTime, dbTime) VALUES ('".$box['EmailTo']."', '".str_replace('AdWords Report Request | ','',$box['Subject'])."', '".$box['DateE']."', '".$box['DateDb']."')") or die("MySQL Error : ".mysql_error(0));
		}
	}

	// Updating $mysql_table...
	$attach = mysql_query("SELECT * FROM $mysql_table_attach") or die ("Request Error".mysql_error(0));
	while($ads = mysql_fetch_array($attach)){
		if($ads['FileNameOrg']=='report-xml.zip'){
			$xmlfile = $folder.'/'.str_replace("-xml.zip","/", $ads['Filename']).'report.xml';
			$adwordsReport = simplexml_load_file($xmlfile);
			$req = mysql_query("SELECT * FROM $mysql_table") or die ("Request Error".mysql_error(0));

			if(isset($adwordsReport->children()->table->rows)){ // Old report format actions
				if(mysql_num_rows($req)){
					$columns = mysql_query("SHOW COLUMNS FROM $mysql_base.$mysql_table") or die ("Request Error".mysql_error(0));	
					$nextColumnName = "R".sprintf("%03d", mysql_num_rows($columns)-1);
					mysql_query("ALTER TABLE $mysql_table ADD $nextColumnName int(2) NOT NULL DEFAULT '0'") or die("MySQL Error : ".mysql_error(0));
					
					// Updating existing keywords...
					foreach ($adwordsReport->children()->table->rows->row as $row){
						if (findAttribute($row,'kwSiteType')=='Exact'){
							mysql_query("UPDATE $mysql_table SET $nextColumnName='".findAttribute($row,'qualityScore')."' WHERE KSExact='".findAttribute($row,'kwSite')."'") or die("MySQL Error : ".mysql_error(0));
						}
					}
					
					// Adding new keywords...
					foreach ($adwordsReport->children()->table->rows->row as $row){
						if (findAttribute($row,'kwSiteType')=='Exact'){
							if (mysql_num_rows(mysql_query("SELECT id FROM $mysql_table WHERE KSExact='".findAttribute($row,'kwSite')."'"))){}
							else {
								mysql_query("INSERT INTO $mysql_table (KSExact, $nextColumnName) VALUES ('".findAttribute($row,'kwSite')."', '".findAttribute($row,'qualityScore')."')") or die("MySQL Error : ".mysql_error(0));
							}
						}
					}
				}
				else {
					foreach ($adwordsReport->children()->table->rows->row as $row){
						if (findAttribute($row,'kwSiteType')=='Exact'){
							mysql_query("INSERT INTO $mysql_table (KSExact, R001) VALUES ('".findAttribute($row,'kwSite')."', '".findAttribute($row,'qualityScore')."')") or die("MySQL Error : ".mysql_error(0));
						}
					}
				}
			}
			else{ // New report format actions
				if(mysql_num_rows($req)){
					$columns = mysql_query("SHOW COLUMNS FROM $mysql_base.$mysql_table") or die ("Request Error".mysql_error(0));	
					$nextColumnName = "R".sprintf("%03d", mysql_num_rows($columns)-1);
					mysql_query("ALTER TABLE $mysql_table ADD $nextColumnName int(2) NOT NULL") or die("MySQL Error : ".mysql_error(0));
					
					// Updating existing keywords...
					foreach ($adwordsReport->children()->table->row as $row){
						if (preg_match("/\[(.*)\]/",findAttribute($row,'keyword'))){ // Condition for keywordExact format : "[keyword]"
							mysql_query("UPDATE $mysql_table SET $nextColumnName='".findAttribute($row,'qualityScore')."' WHERE KSExact='".findAttribute($row,'keyword')."'") or die("MySQL Error : ".mysql_error(0));
						}
					}
					
					// Adding new keywords...
					foreach ($adwordsReport->children()->table->row as $row){
						if (preg_match("/\[(.*)\]/",findAttribute($row,'keyword'))){
							if (mysql_num_rows(mysql_query("SELECT id FROM $mysql_table WHERE KSExact='".findAttribute($row,'keyword')."'"))){}
							else {
								mysql_query("INSERT INTO $mysql_table (KSExact, $nextColumnName) VALUES ('".findAttribute($row,'keyword')."', '".findAttribute($row,'qualityScore')."')") or die("MySQL Error : ".mysql_error(0));
							}
						}
					}
				}
				else {
					foreach ($adwordsReport->children()->table->row as $row){
						if (preg_match("/\[(.*)\]/",findAttribute($row,'keyword'))){
							mysql_query("INSERT INTO $mysql_table (KSExact, R001) VALUES ('".findAttribute($row,'keyword')."', '".findAttribute($row,'qualityScore')."')") or die("MySQL Error : ".mysql_error(0));
						}
					}
				}
			}
		}
	}
}



function findAttribute($object, $attribute) { 
  foreach($object->attributes() as $name => $value) { 
    if ($name == $attribute) { 
      $return = $value; 
    } 
  } 
  if(isset($return)) { 
    return $return; 
  }
  else{
  	return false;
  }
}



function healthMarker($condition,$lastColumn,$preLastColumn){
	if($condition=='dead'){
		return "$lastColumn=0 OR $lastColumn=1";
	}
	elseif($condition=='sick'){
		return "$lastColumn=2 OR $lastColumn=3";
	}
	elseif($condition=='weak'){
		return "$lastColumn=4 OR $lastColumn=5 OR $lastColumn=6";
	}
	elseif($condition=='good'){
		return "$lastColumn=7 OR $lastColumn=8";
	}
	elseif($condition=='excellent'){
		return "$lastColumn=9 OR $lastColumn=10";
	}
	elseif($condition=='declining'){
		return "$lastColumn<7 AND $lastColumn<$preLastColumn";
	}
	elseif($condition=='recovering'){
		return "$lastColumn<7 AND $lastColumn>$preLastColumn";
	}
}


# Cubic Spline Interpolation
function spline($x, $y, $xi){

	$nbRows = count($x)-1;
	
	for($i=0; $i<$nbRows; $i++){
		$h[$i] = $x[$i+1] - $x[$i];
		$b[$i] = ($y[$i+1] - $y[$i]) / $h[$i];
	}
		
	$u[0] = 0;
	$v[0] = 0;
	$u[1] = 2*($h[0] + $h[1]);
	$v[1] = 6*($b[1] - $b[0]);
	
	for($i=2; $i<$nbRows; $i++){
		$u[$i] = 2*($h[$i-1] + $h[$i]) - ($h[$i]^2)/$u[$i-1];
		$v[$i] = 6*($b[$i] - $b[$i-1]) - $h[$i-1]*$v[$i-1]/$u[$i-1];
	}

	$z[$nbRows] = 0;
	for($i=$nbRows-1; $i>=1; $i--){
		$z[$i] = ($v[$i] - $h[$i]*$z[$i+1]) / $u[$i];
	}
	$z[0] = 0;
		
	for($i=0; $i<$nbRows; $i++){
		if($xi<= $x[$i+1]){
			break;
		}
	}
	$h[$i] = $x[$i+1] - $x[$i];
		
	$a = $y[$i];
	$b = ($y[$i+1]-$y[$i])/$h[$i] - $h[$i]*$z[$i]/3 - $h[$i]*$z[$i+1]/6;
	$c = $z[$i]/2;
	$d = ($z[$i+1] - $z[$i])/(6*$h[$i]);
	$s = $a + ($xi - $x[$i])*($b + ($xi - $x[$i])*($c + ($xi - $x[$i])*$d));

	return $s;
}

function val_round(&$val){$val = round($val,2);} # Function used to ROUND values 

?>