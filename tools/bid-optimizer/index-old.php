<?php


include_once("php.functions.php");

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Adwords Bid Optimization Tool | TenScores</title>
<meta name="description" content="This Adwords Bid Obtimization Tool allows you to find which bids yield maximum profits with data from the bid simulator in your Adwords account." />
<meta name="keywords" content="adwords, bid optimization, bid optimizer, ppc bids" />
<meta name="author" content="Chris Thunder" />
<style type="text/css" media="all">
	@import "bid-styles.css";
	</style>
<script type="text/javascript" src="js.functions.js"></script>


</head>
<body>
<!-- KISSinsights for tenscores.com -->
<script type="text/javascript">var _kiq = _kiq || [];</script>
<script type="text/javascript" src="//s3.amazonaws.com/ki.js/9245/1zw.js" async="true"></script>

<div id="newtop">
<table width="900" border="0" align="center" cellpadding="3" cellspacing="0" style="padding:5px 0 5px 0;">
  <tr>
    <td width="235" ><a href="http://tenscores.com"><img src="http://tenscores.com/images/topsignup.png" alt="Create A Tenscores Account" width="220" height="33" border="0" /></a></td>
    <td align="left"><p class="toptext">Be the first to test our beta adwords ppc tools, it’s free.</p></td>
    <td align="right"><img src="images/logo-small-black.png" alt="Tenscores" width="109" height="35" /></td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>

<div id="halvideo">
<div id="container1">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="420">
    
<object width="400" height="250"><param name="movie" value="http://www.youtube.com/v/jRx7AMb6rZ0?fs=1&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/jRx7AMb6rZ0?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="400" height="250"></embed></object>    </td>
    <td  align="center">
    <h1>Bid Like A Pro.</h1>
    <p> Watch Hal Varian's video and learn how to bid <br />for maximum profitability.</p>
    
<div style=" width: 220px; margin:30px auto 0 auto; ">

<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a href="http://www.addthis.com/bookmark.php?v=250&amp;username=christianziza" class="addthis_button_compact" style="font-size:12px;">Share</a>
<span class="addthis_separator">|</span>
<a class="addthis_button_email"></a>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_delicious"></a>
<a class="addthis_button_digg"></a>
<a class="addthis_button_stumbleupon"></a>
<a class="addthis_button_google"></a>

</div>
<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=christianziza"></script>
<!-- AddThis Button END -->



</div>
    
    </td>
  </tr>
</table>
</div>
</div>

<div id="tool">

<div style="padding: 30px 0 30px 0;">

<div id="toolwrapper">
<form name="params" action="index.php" method="post">
<input type="hidden" name="bidsimulator" />
<table cellspacing="0" cellpadding=="0">
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0">
	      <tr>
					<td align="left" valign="middle" style="padding-right:10px;"><label for="esbids"><nobr>Estimated Bids <a onmousemove="ShowContent('estbids'); return true;" onmouseover="ShowContent('estbids'); return true;" onmouseout="HideContent('estbids'); return true;" href="javascript:ShowContent('estbids')"><img src="images/info.png" width="15" height="15" border="0" style="vertical-align:top;" /></a>:</nobr></label>
				  <div id="estbids" style="display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;">Insert the estimated <b>bids</b> predictated by the bid simulator available in your adwords account. </div></td>
				  <td align="center"><input type="text" name="esbids" id="esbids" style="width:300px;" value="<?php if(isset($_POST["esbids"])){echo $_POST["esbids"];}else{if(!isset($_GET["reset"])){echo $_SESSION["esbids"];}} ?>" /></td>
				</tr>
                
                <tr>
                	<td></td>
                    <td><p class="example">Example: 6.36, 5.26, 2.10, 1.59, 0.80, 0.36, 0.20</p></td>
                </tr>
                
				<tr>
					<td align="left" valign="middle" style="padding-right:10px;"><label for="esclicks"><nobr>Estimated Clicks <a onmousemove="ShowContent('estclicks'); return true;" onmouseover="ShowContent('estclicks'); return true;" onmouseout="HideContent('estclicks'); return true;" href="javascript:ShowContent('estclicks')"><img src="images/info.png" width="15" height="15" border="0" style="vertical-align:top;" /></a>:</nobr></label>
				  <div id="estclicks" style="display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;">Insert the estimated <b>clicks</b> predictated by the bid simulator available in your adwords account. </div></td>
				  <td align="center"><input type="text" name="esclicks" id="esclicks" style="width:300px;" value="<?php if(isset($_POST["esclicks"])){echo $_POST["esclicks"];}else{if(!isset($_GET["reset"])){echo $_SESSION["esclicks"];}} ?>" /></td>
				</tr>
                
                <tr>
                	<td></td>
                    <td><p class="example">Example: 237, 213, 183, 137, 94, 72, 48</p></td>
                </tr>
                
				<tr>
					<td align="left" valign="middle" style="padding-right:10px;"><label for="escosts"><nobr>Estimated Costs <a onmousemove="ShowContent('estcosts'); return true;" onmouseover="ShowContent('estcosts'); return true;" onmouseout="HideContent('estcosts'); return true;" href="javascript:ShowContent('estcosts')"><img src="images/info.png" width="15" height="15" border="0" style="vertical-align:top;" /></a>:</nobr></label>
				  <div id="estcosts" style="display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;">Insert the estimated <b>costs</b> predictated by the bid simulator available in your adwords account. </div></td>
				  <td align="center"><input type="text" name="escosts" id="escosts" style="width:300px;" value="<?php if(isset($_POST["escosts"])){echo $_POST["escosts"];}else{if(!isset($_GET["reset"])){echo $_SESSION["escosts"];}} ?>" /></td>
				</tr>
                
                <tr>
                	<td></td>
                    <td><p class="example">Example: 418, 275, 161, 75.50, 26.20, 10.90, 4.07</p></td>
                </tr>
                
			</table>
		</td>
		<td width="30"></td>
		<td valign="top">
			<table cellspacing="0" cellpadding="0">
    <tr>
					<td  align="left" valign="middle" style="padding-right:10px;"><label for="CPA"><nobr>Max Profitable CPA <a onmousemove="ShowContent('mpcpa'); return true;" onmouseover="ShowContent('mpcpa'); return true;" onmouseout="HideContent('mpcpa'); return true;" href="javascript:ShowContent('mpcpa')"><img src="images/info.png" width="15" height="15" border="0" style="vertical-align:top;" /></a>:</nobr></label>
				  <div id="mpcpa" style="display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;">CPA: Cost-Per-Acquistions, also called Cost Per Conversion (cost/conv). Enter the maximum price you can pay per conversion and still be able to make a profit.</div></td>
				  <td align="center" valign="top"><input type="text" name="CPA" id="CPA" style="width:50px;" value="<?php if(isset($_POST["CPA"])){echo $_POST["CPA"];}else{if(!isset($_GET["reset"])){echo $_SESSION["CPA"];}} ?>" /></td>
				</tr>
                <tr>
                	<td></td>
                    <td><p class="example">Ex: 100</p></td>
                </tr>
              
				<tr>
					<td  align="left" valign="middle" style="padding-right:10px;"><label for="CR"><nobr>Conversion Rate <a onmousemove="ShowContent('cri'); return true;" onmouseover="ShowContent('cri'); return true;" onmouseout="HideContent('cri'); return true;" href="javascript:ShowContent('cri')"><img src="images/info.png" width="15" height="15" border="0" style="vertical-align:top;" /></a>:</nobr></label>
				  <div id="cri" style="display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;">Enter your website conversion rate.</div></td>
				  <td align="center" valign="top"><input type="text" name="CR" id="CR" style="width:50px;" value="<?php if(isset($_POST["CR"])){echo $_POST["CR"];}else{if(!isset($_GET["reset"])){echo $_SESSION["CR"];}} ?>" /></td>
				</tr>
                 <tr>
                	<td></td>
                    <td><p class="example">Ex: 5</p></td>
                </tr>
                  
				<tr>
				  <td align="left" valign="middle" style="padding-right:10px;"><label for="VPC"><nobr>Value Per Click :</nobr></label></td>
				  <td align="center" valign="top"><input type="text" name="VPC" id="VPC" style="width:50px;" value="<?php if(isset($_POST["CPA"]) && isset($_POST["CR"])){echo round(($_POST["CPA"]*$_POST["CR"])/100,2);} ?>" disabled /></td>
				</tr>
                <tr>
                	<td></td>
                    <td><p class="example">Leave blank</p></td>
                </tr>
                    
			</table>
		</td>
		<td width="30"></td>
		<td width="100" valign="top"><input type="image" src="images/calculate3.png" name="submit" value="Calculate" style=" width:94px; height:116px; border:0;" /></td>
		<td width="10"></td>
		<td width="80" valign="top"><input type="image" src="images/reset2.png" name="reset" value="Reset" style="width:76px; height:116px; font-size:15px; border:0;" onClick="window.location.href='index.php?reset';" /></td>

	</tr>
</table>

</form>
</div>

<?php

if(isset($_GET["reset"])){
	unset($_SESSION["esbids"]);
	unset($_SESSION["esclicks"]);
	unset($_SESSION["escosts"]);
	unset($_SESSION["CPA"]);
	unset($_SESSION["CR"]);
}

if(isset($_POST["bidsimulator"])){ # Checking if form is submitted
	# Using session variables
	if($_POST["esbids"]!=''){$_SESSION["esbids"] = $_POST["esbids"];}
	if($_POST["esclicks"]!=''){$_SESSION["esclicks"] = $_POST["esclicks"];}
	if($_POST["escosts"]!=''){$_SESSION["escosts"] = $_POST["escosts"];}
	if($_POST["CPA"]!=''){$_SESSION["CPA"] = $_POST["CPA"];}
	if($_POST["CR"]!=''){$_SESSION["CR"] = $_POST["CR"];}

	# Putting estimated datas in arrays
	$esBids = explode(',',$_POST["esbids"]);
	$esClicks = explode(',',$_POST["esclicks"]);
	$esCosts = explode(',',$_POST["escosts"]);
	
	# Checking entered values and setting error if needed
	if(count($esBids)!=count($esClicks) || count($esBids)!=count($esCosts) || count($esCosts)!=count($esClicks)){
		$error = "Entered Estimated Datas must have the same count. Please check the entered values.";
	}
	
	if($_POST["CPA"]=='' || $_POST["CR"]==''){
		$error = "Please enter a value for Max Profitable CPA and/or Conversion Rate.";
	}

	if($_POST["esbids"]=='' || $_POST["esclicks"]=='' || $_POST["escosts"]==''){
		$error = "Please enter Estimated Datas for Bids and/or Clicks and/or Costs";
	}
	
	foreach($esBids as $esBid){if(!is_numeric($esBid) && !isset($error)){$error = "Entered Estimated Bids contains non numeric characters. Please check the entered values.";}}
	foreach($esClicks as $esClick){if(!is_numeric($esClick) && !isset($error)){$error = "Entered Estimated Clicks contains non numeric characters. Please check the entered values.";}}
	foreach($esCosts as $esCost){if(!is_numeric($esCost) && !isset($error)){$error = "Entered Estimated Costs contains non numeric characters. Please check the entered values.";}}

	if((!is_numeric($_POST["CPA"]) || !is_numeric($_POST["CR"])) && !isset($error)){
		$error = "Entered value for CPA and/or Conversion Rate is non numeric. Please check the entered values.";
	}
	
	if(isset($error)){
		echo "<div align=\"center\">$error</div>";
	}
	else{ # if no errors found, doing actions

		sort($esBids);
		sort($esClicks);
		sort($esCosts);
		
		$CPA = round($_POST["CPA"],2);			# Maximum Profitable Cost Per Accusation.
		$CR  = round($_POST["CR"],2);			# Conversion rate
		$CR  = $CR/100;
		$VPC = round($CPA*$CR,2);						# Value Per Click
		
		$ck = 0;
		for($click=min($esClicks); $click<=max($esClicks); $click++){
			$costs[$ck] = spline($esClicks,$esCosts,$click);
			$bids[$ck] = spline($esClicks,$esBids,$click);
			$revenues[$ck] = $click*$VPC;
			$profits[$ck] = $revenues[$ck]-$costs[$ck];
			$ck++;
		}
		
		$ICCs[0] = 0;
		for ($i = 1; $i<=count($costs)-1; $i++){
			$ICCs[$i] = $costs[count($costs)-($i)]-$costs[count($costs)-($i+1)];
		}
		sort($ICCs);
		
		# Rounding value to xx.xx
		array_walk($ICCs,'val_round');
		array_walk($bids,'val_round');
		array_walk($costs,'val_round');
		array_walk($profits,'val_round');
		
		# Creating $_GET values fot plot
		$abs = min($esClicks).','.max($esClicks);
		$COrds = 's';
		$POrds = 's';
		for($p=min($esClicks); $p<=max($esClicks); $p++){
			$COrds = $COrds.','.$costs[$p-min($esClicks)];
			$POrds = $POrds.','.$profits[$p-min($esClicks)];
		}
		$COrds = str_replace('s,','',$COrds);
		$POrds = str_replace('s,','',$POrds);
		
		# Getting the uptimum value for bid and setting intervals for displayed datas		
		$optimum = 0; 
		foreach($ICCs as $ICC){
			if($ICC<$VPC){$optimum++;}
		}
		
		$delta = 10; # Max values to be shown before and/or after the optimum value
		
		if($optimum+$delta < count($ICCs)-1){$right = $optimum+$delta;}else{$right = count($ICCs)-1;}
		if($optimum+$delta < count($ICCs)-1){if($optimum-$delta>0){$left = $optimum-$delta;}else{$left = 0;}}else{if($right==count($ICCs)-1){$left = $optimum-$delta;}else{$left = 0;}}
	} 
}

?>
<div align="center" <?php if(!isset($_POST["bidsimulator"]) || isset($error)){echo "style=\"display:none;\" ";} ?>> 

<div align="center" style="padding-top:30px;">
<table cellspacing="0" cellpadding=="0">
	<tr>
	<td width="60" align="left" style="font-size:14px; font-weight:bold; padding-right:10px;">ICCs :</td>
	<?php for($i=$left; $i<$right; $i++){echo "<td width=\"41\" align=\"center\" class=\"iValue\""; if($i==$optimum || $i==$optimum-1){echo "style=\"font-weight:bold;\"";} echo ">$ICCs[$i]</td>";} ?>
	</tr>
</table>
</div>

<div align="center">
<table cellspacing="0" cellpadding=="0">
	<tr>
	<td width="60" align="left" style="font-size:14px; font-weight:bold; padding-right:10px;">Bids :</td>
	<?php for($i=$left; $i<$right; $i++){echo "<td width=\"41\" align=\"center\" class=\"iValue\""; if($i==$optimum || $i==$optimum-1){echo "style=\"font-weight:bold;\"";} echo ">$bids[$i]</td>";} ?>
	</tr>
</table>
</div>

<div align="center">
<table cellspacing="0" cellpadding=="0">
	<tr>
	<td width="60" align="left" style="font-size:14px; font-weight:bold; padding-right:10px;">Profits :</td>
	<?php for($i=$left; $i<$right; $i++){echo "<td width=\"41\" align=\"center\" class=\"iValue\""; if($i==$optimum || $i==$optimum-1){echo "style=\"font-weight:bold;\"";} echo ">$profits[$i]</td>";} ?>
	</tr>
</table>
</div>
<div align="center" style="padding:30px;">
<table cellspacing="0" cellpadding=="0">
	<tr>
		<td><?php echo "<img src=\"plot/index.php?l=C&x=".$abs."&y=".$COrds."\""; ?></td>
		<td width="30"></td>
		<td><?php echo "<img src=\"plot/index.php?l=P&x=".$abs."&y=".$POrds."\""; ?></td>
	</tr>
</table>
</div>
</div> <!-- END OF DATA DIV //-->

</div>
</div>
<div id="footer">
	<div id="foot-nav">
    				&copy; 2010 Tenscores.com - 
     				<a href="http://tenscores.com/about.php">About Us</a> -
        			<a href="http://tenscores.com/contact.php">Contact Us</a>  -
                    <a href="http://tenscores.com/privacy.php">Privacy Policy</a> -
                    <a href="http://tenscores.com/terms.php">Terms Of Use</a> - 
                    <a href="http://tenscores.com/disclaimer.php">Disclaimer</a> - 
                     Our <a href="http://www.tenscores.com/book/">Quality Score Book</a>

                    
                           </div>

</div>
</body>
</html>
