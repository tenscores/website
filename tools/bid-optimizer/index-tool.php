<?php


include_once("php.functions.php");

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>How To Bid On Adwords: Video + Free Tool | TenScores</title>
<meta name="description" content="This Adwords Bid Obtimization Tool allows you to find which bids yield maximum profits with data from the bid simulator in your Adwords account." />
<meta name="keywords" content="adwords, bid optimization, bid optimizer, ppc bids" />
<meta name="author" content="Chris Thunder" />
<style type="text/css" media="all">
	@import "bid-styles-v2.css";
	</style>
<script type="text/javascript" src="js.functions.js"></script>

<script language="javascript">
function toggle() {
	var ele = document.getElementById("instructions");
	var text = document.getElementById("displayText");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "+ show instructions";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "- hide instructions";
	}
}
</script>

<?php include '../../files/includes/tags.php'; ?>

<script language="JavaScript">
function setVisibility(id, visibility) {
document.getElementById(id).style.display = visibility;
}

</script>



</head>
<body>
<!-- KISSinsights for tenscores.com -->
<script type="text/javascript">var _kiq = _kiq || [];</script>
<script type="text/javascript" src="//s3.amazonaws.com/ki.js/9245/1zw.js" async="true"></script>



<div id="newtop">
<table width="900" border="0" align="center" cellpadding="3" cellspacing="0" style="padding:5px 0 5px 0;">
  <tr>
    <td width="117" align="left"><a href="http://tenscores.com"><img src="http://tenscores.com/images/logo.png" alt="Tenscores" width="109" height="35" border="0" /></a></td>
      <td align="right" ><p class="toptext"><strong>Promo:</strong> Increase  Adwords Quality Scores. Get cheaper traffic. $10 only.</p></td>
      <td align="right" width="110" ><a href="http://tenscores.com"><img src="images/click-here.png" alt="Click Here" width="106" border="0" /></a></td>
  </tr>
</table>
</div>

<div id="halvideo">
<div id="container1">
<table width="840" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:auto;" >
  <tr>
    <td width="420">
    <div style="width:400px; border: 5px solid #bbccff; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; -khtml-border-radius:4px;">
<object width="400" height="250"><param name="movie" value="http://www.youtube.com/v/jRx7AMb6rZ0?fs=1&amp;hl=en_US"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/jRx7AMb6rZ0?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="400" height="250"></embed></object>  </div>  </td>
    <td  align="center">
    <h1>Bid Like A Pro.</h1>
    <p> Watch Hal Varian's video to learn how to bid on Adwords then use the tool
       below to discover bids that  yield maximum  profits.</p>

<div style=" width:300px; margin: 30px auto 0 auto;">

<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style " style="text-align:center;">
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=christianziza"></script>
<!-- AddThis Button END -->
</div>
</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td  align="center"><a id="displayText" href="javascript:toggle();">+ show instructions</a></td>
  </tr>
</table>
</div>

<div id="instructions" style="display:none;">
<div style="margin: 0 30px 0 30px;">
<img src="images/bid-simulator.jpg" alt="Adwords Bid Simulator" style="float:right; margin: 20px; border:1px solid #ccc;" />

    <p>The Bid Optimizer tool is located in the box right below these instructions. Simply fill the 5 required fields then click on the calculate bids button. Graphs will be plotted and your most profitable bids will be in bold.</p>
    <p>Here are the complete instructions:</p>

    <dl style="text-align:left;">
    <dt><b>Step 1 - Identify Bid To Optimize</b></dt>
    <dd>Log into your adwords account and identify the keyword bids you want to optimize. They should have enough data so that the bid simulator is active. <a href="http://www.youtube.com/watch?v=b-FzSL66Zjg" target="_blank">Learn more about the bid simulator here.</a></dd>
    <dt><b>Step 2 - Copy Prediction Data From Bid Simulator</b></dt>
    <dd>The Bid Simulator predicts the number of clicks, impressions and your total costs depending on how you bid. Copy the estimated data in a text file. </dd>
    <dt><b>Step 3 - Get Your Conversion Metrics</b></dt>
    <dd>Calculate your maximum profitable CPA and your conversion rate. Watch the video above  to learn how to calculate them. </dd>
    <dt><b>Step 4 - Find The Bids That Yield Maximum Profits</b></dt>
    <dd>In the tool below, enter the data of estimated bids, clicks, costs and conversions in their corresponding fields. The order doesn't matter, separate values by a comma then click on &quot;calcuate bids&quot;. A table will appear with 3 lines and several columns, <b>the bids that yield max profitability will be in bold in the Bids line.</b> The graphs are simple representations of the data generated in the table.</dd>
    <dt><b>Step 5 - Create A Campaign Experiment</b></dt>
    <dd>You are not 100% sure that the predictions are accurate, that's why you should test before adopting them. Learn about<a href="http://www.google.com/ads/innovations/ace.html" target="_blank"> campaign experiments here.</a></dd>
    </dl>

    <div align="center"><img src="images/bid-optimization.png" alt="Bid Optimization" style="margin-bottom:20px; border:1px solid #ccc;" />
    </div>
</div>
</div>

</div>

<div id="tool">

<div style="padding: 30px 0 30px 0;">

<div id="toolwrapper">
<form name="params" action="index.php" method="post" onsubmit="_gaq.push(['_trackEvent', 'Tools', 'Used', 'Bid Optimizer']);">
<input type="hidden" name="bidsimulator" />
<table cellspacing="0" cellpadding=="0">
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0">
	      <tr>
					<td align="left" valign="middle" style="padding-right:10px;"><label for="esbids"><nobr>Max CPC Bids <a onmousemove="ShowContent('estbids'); return true;" onmouseover="ShowContent('estbids'); return true;" onmouseout="HideContent('estbids'); return true;" href="javascript:ShowContent('estbids')"><img src="images/info.png" width="15" height="15" border="0" style="vertical-align:top;" /></a>:</nobr></label>
				  <div id="estbids" style="display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;">Insert the estimated <b>bids</b> predicted by the bid simulator available in your adwords account. </div></td>
				  <td align="center"><input type="text" name="esbids" id="esbids" style="width:300px;" value="<?php if(isset($_POST["esbids"])){echo $_POST["esbids"];}else{if(!isset($_GET["reset"])){echo $_SESSION["esbids"];}} ?>" /></td>
				</tr>

                <tr>
                	<td></td>
                    <td><p class="example">Example: 6.36, 5.26, 2.10, 1.59, 0.80, 0.36, 0.20</p></td>
                </tr>

				<tr>
					<td align="left" valign="middle" style="padding-right:10px;"><label for="esclicks"><nobr>Estimated Clicks <a onmousemove="ShowContent('estclicks'); return true;" onmouseover="ShowContent('estclicks'); return true;" onmouseout="HideContent('estclicks'); return true;" href="javascript:ShowContent('estclicks')"><img src="images/info.png" width="15" height="15" border="0" style="vertical-align:top;" /></a>:</nobr></label>
				  <div id="estclicks" style="display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;">Insert the estimated <b>clicks</b> predicted by the bid simulator available in your adwords account. </div></td>
				  <td align="center"><input type="text" name="esclicks" id="esclicks" style="width:300px;" value="<?php if(isset($_POST["esclicks"])){echo $_POST["esclicks"];}else{if(!isset($_GET["reset"])){echo $_SESSION["esclicks"];}} ?>" /></td>
				</tr>

                <tr>
                	<td></td>
                    <td><p class="example">Example: 237, 213, 183, 137, 94, 72, 48</p></td>
                </tr>

				<tr>
					<td align="left" valign="middle" style="padding-right:10px;"><label for="escosts"><nobr>Estimated Costs <a onmousemove="ShowContent('estcosts'); return true;" onmouseover="ShowContent('estcosts'); return true;" onmouseout="HideContent('estcosts'); return true;" href="javascript:ShowContent('estcosts')"><img src="images/info.png" width="15" height="15" border="0" style="vertical-align:top;" /></a>:</nobr></label>
				  <div id="estcosts" style="display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;">Insert the estimated <b>costs</b> predicted by the bid simulator available in your adwords account. </div></td>
				  <td align="center"><input type="text" name="escosts" id="escosts" style="width:300px;" value="<?php if(isset($_POST["escosts"])){echo $_POST["escosts"];}else{if(!isset($_GET["reset"])){echo $_SESSION["escosts"];}} ?>" /></td>
				</tr>

                <tr>
                	<td></td>
                    <td><p class="example">Example: 418, 275, 161, 75.50, 26.20, 10.90, 4.07</p></td>
                </tr>

			</table>
		</td>
		<td width="30"></td>
		<td valign="top">
			<table cellspacing="0" cellpadding="0">
    <tr>
					<td  align="left" valign="middle" style="padding-right:10px;"><label for="CPA"><nobr>Max Profitable CPA <a onmousemove="ShowContent('mpcpa'); return true;" onmouseover="ShowContent('mpcpa'); return true;" onmouseout="HideContent('mpcpa'); return true;" href="javascript:ShowContent('mpcpa')"><img src="images/info.png" width="15" height="15" border="0" style="vertical-align:top;" /></a>:</nobr></label>
				  <div id="mpcpa" style="display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;">CPA: Cost-Per-Acquistions, also called Cost Per Conversion (cost/conv). Enter the maximum price you can pay per conversion and still be able to make a profit.</div></td>
				  <td align="center" valign="top"><input type="text" name="CPA" id="CPA" style="width:50px;" value="<?php if(isset($_POST["CPA"])){echo $_POST["CPA"];}else{if(!isset($_GET["reset"])){echo $_SESSION["CPA"];}} ?>" /></td>
				</tr>
                <tr>
                	<td></td>
                    <td><p class="example">Ex: 100</p></td>
                </tr>

				<tr>
					<td  align="left" valign="middle" style="padding-right:10px;"><label for="CR"><nobr>Conversion Rate <a onmousemove="ShowContent('cri'); return true;" onmouseover="ShowContent('cri'); return true;" onmouseout="HideContent('cri'); return true;" href="javascript:ShowContent('cri')"><img src="images/info.png" width="15" height="15" border="0" style="vertical-align:top;" /></a>:</nobr></label>
				  <div id="cri" style="display:none; position:absolute; border: solid 1px #0000FF; background-color: white; padding: 5px;">Enter your website conversion rate.</div></td>
				  <td align="center" valign="top"><input type="text" name="CR" id="CR" style="width:50px;" value="<?php if(isset($_POST["CR"])){echo $_POST["CR"];}else{if(!isset($_GET["reset"])){echo $_SESSION["CR"];}} ?>" /></td>
				</tr>
                 <tr>
                	<td></td>
                    <td><p class="example">Ex: 5</p></td>
                </tr>

				<tr>
				  <td align="left" valign="middle" style="padding-right:10px;"><label for="VPC"><nobr>Value Per Click :</nobr></label></td>
				  <td align="center" valign="top"><input type="text" name="VPC" id="VPC" style="width:50px;" value="<?php if(isset($_POST["CPA"]) && isset($_POST["CR"])){echo round(($_POST["CPA"]*$_POST["CR"])/100,2);} ?>" disabled /></td>
				</tr>
                <tr>
                	<td></td>
                    <td><p class="example">Leave blank</p></td>
                </tr>

			</table>
		</td>
		<td width="30"></td>
		<td width="100" valign="top">
        <input type="image" src="images/calculate3.png" name="submit" value="Calculate" style=" width:94px; height:116px; border:0;" />

        </td>
		<td width="10"></td>
		<td width="80" valign="top"><input type="image" src="images/reset2.png" name="reset" value="Reset" style="width:76px; height:116px; font-size:15px; border:0;" onClick="window.location.href='index.php?reset';" /></td>

	</tr>
</table>

</form>
</div>

<?php

if(isset($_GET["reset"])){
	unset($_SESSION["esbids"]);
	unset($_SESSION["esclicks"]);
	unset($_SESSION["escosts"]);
	unset($_SESSION["CPA"]);
	unset($_SESSION["CR"]);
}

if(isset($_POST["bidsimulator"])){ # Checking if form is submitted
	# Using session variables
	if($_POST["esbids"]!=''){$_SESSION["esbids"] = $_POST["esbids"];}
	if($_POST["esclicks"]!=''){$_SESSION["esclicks"] = $_POST["esclicks"];}
	if($_POST["escosts"]!=''){$_SESSION["escosts"] = $_POST["escosts"];}
	if($_POST["CPA"]!=''){$_SESSION["CPA"] = $_POST["CPA"];}
	if($_POST["CR"]!=''){$_SESSION["CR"] = $_POST["CR"];}

	# Putting estimated datas in arrays
	$esBids = explode(',',$_POST["esbids"]);
	$esClicks = explode(',',$_POST["esclicks"]);
	$esCosts = explode(',',$_POST["escosts"]);

	# Checking entered values and setting error if needed
	if(count($esBids)!=count($esClicks) || count($esBids)!=count($esCosts) || count($esCosts)!=count($esClicks)){
		$error = "Entered Estimated Datas must have the same count. Please check the entered values.";
	}

	if($_POST["CPA"]=='' || $_POST["CR"]==''){
		$error = "Please enter a value for Max Profitable CPA and/or Conversion Rate.";
	}

	if($_POST["esbids"]=='' || $_POST["esclicks"]=='' || $_POST["escosts"]==''){
		$error = "Please enter Estimated Datas for Bids and/or Clicks and/or Costs";
	}

	foreach($esBids as $esBid){if(!is_numeric($esBid) && !isset($error)){$error = "Entered Estimated Bids contains non numeric characters. Please check the entered values.";}}
	foreach($esClicks as $esClick){if(!is_numeric($esClick) && !isset($error)){$error = "Entered Estimated Clicks contains non numeric characters. Please check the entered values.";}}
	foreach($esCosts as $esCost){if(!is_numeric($esCost) && !isset($error)){$error = "Entered Estimated Costs contains non numeric characters. Please check the entered values.";}}

	if((!is_numeric($_POST["CPA"]) || !is_numeric($_POST["CR"])) && !isset($error)){
		$error = "Entered value for CPA and/or Conversion Rate is non numeric. Please check the entered values.";
	}

	if(isset($error)){
		echo "<div align=\"center\">$error</div>";
	}
	else{ # if no errors found, doing actions

		sort($esBids);
		sort($esClicks);
		sort($esCosts);

		$CPA = round($_POST["CPA"],2);			# Maximum Profitable Cost Per Accusation.
		$CR  = round($_POST["CR"],2);			# Conversion rate
		$CR  = $CR/100;
		$VPC = round($CPA*$CR,2);						# Value Per Click

		$ck = 0;
		for($click=min($esClicks); $click<=max($esClicks); $click++){
			$costs[$ck] = spline($esClicks,$esCosts,$click);
			$bids[$ck] = spline($esClicks,$esBids,$click);
			$revenues[$ck] = $click*$VPC;
			$profits[$ck] = $revenues[$ck]-$costs[$ck];
			$ck++;
		}

		$ICCs[0] = 0;
		for ($i = 1; $i<=count($costs)-1; $i++){
			$ICCs[$i] = $costs[count($costs)-($i)]-$costs[count($costs)-($i+1)];
		}
		sort($ICCs);

		# Rounding value to xx.xx
		array_walk($ICCs,'val_round');
		array_walk($bids,'val_round');
		array_walk($costs,'val_round');
		array_walk($profits,'val_round');

		# Creating $_GET values fot plot
		$abs = min($esClicks).','.max($esClicks);
		$COrds = 's';
		$POrds = 's';
		for($p=min($esClicks); $p<=max($esClicks); $p++){
			$COrds = $COrds.','.$costs[$p-min($esClicks)];
			$POrds = $POrds.','.$profits[$p-min($esClicks)];
		}
		$COrds = str_replace('s,','',$COrds);
		$POrds = str_replace('s,','',$POrds);

		# Getting the uptimum value for bid and setting intervals for displayed datas
		$optimum = 0;
		foreach($ICCs as $ICC){
			if($ICC<$VPC){$optimum++;}
		}

		$delta = 10; # Max values to be shown before and/or after the optimum value

		if($optimum+$delta < count($ICCs)-1){$right = $optimum+$delta;}else{$right = count($ICCs)-1;}
		if($optimum+$delta < count($ICCs)-1){if($optimum-$delta>0){$left = $optimum-$delta;}else{$left = 0;}}else{if($right==count($ICCs)-1){$left = $optimum-$delta;}else{$left = 0;}}
	}
}

?>
<div align="center" <?php if(!isset($_POST["bidsimulator"]) || isset($error)){echo "style=\"display:none;\" ";} ?>>

<div align="center" style="padding-top:30px;">
<table cellspacing="0" cellpadding=="0">
	<tr>
	<td width="60" align="left" style="font-size:14px; font-weight:bold; padding-right:10px;">ICCs :</td>
	<?php for($i=$left; $i<$right; $i++){echo "<td width=\"41\" align=\"center\" class=\"iValue\""; if($i==$optimum || $i==$optimum-1){echo "style=\"font-weight:bold;\"";} echo ">$ICCs[$i]</td>";} ?>
	</tr>
</table>
</div>

<div align="center">
<table cellspacing="0" cellpadding=="0">
	<tr>
	<td width="60" align="left" style="font-size:14px; font-weight:bold; padding-right:10px;">Bids :</td>
	<?php for($i=$left; $i<$right; $i++){echo "<td width=\"41\" align=\"center\" class=\"iValue\""; if($i==$optimum || $i==$optimum-1){echo "style=\"font-weight:bold;\"";} echo ">$bids[$i]</td>";} ?>
	</tr>
</table>
</div>

<div align="center">
<table cellspacing="0" cellpadding=="0">
	<tr>
	<td width="60" align="left" style="font-size:14px; font-weight:bold; padding-right:10px;">Profits :</td>
	<?php for($i=$left; $i<$right; $i++){echo "<td width=\"41\" align=\"center\" class=\"iValue\""; if($i==$optimum || $i==$optimum-1){echo "style=\"font-weight:bold;\"";} echo ">$profits[$i]</td>";} ?>
	</tr>
</table>
</div>
<div align="center" style="padding:30px;">
<table cellspacing="0" cellpadding=="0">
	<tr>
		<td><?php echo "<img src=\"plot/index.php?l=C&x=".$abs."&y=".$COrds."\""; ?></td>
		<td width="30"></td>
		<td><?php echo "<img src=\"plot/index.php?l=P&x=".$abs."&y=".$POrds."\""; ?></td>
	</tr>
</table>
</div>
</div> <!-- END OF DATA DIV //-->

</div>
</div>
<div id="footer">
	<div id="foot-nav">
    				&copy; 2010 Tenscores.com -
     				<a href="http://tenscores.com/about.php">About Us</a> -
        			<a href="http://tenscores.com/contact.php">Contact Us</a>  -
                    <a href="http://tenscores.com/privacy.php">Privacy Policy</a> -
                    <a href="http://tenscores.com/terms.php">Terms Of Use</a> -
                    <a href="http://tenscores.com/disclaimer.php">Disclaimer</a>


                           </div>

</div>
</body>
</html>
