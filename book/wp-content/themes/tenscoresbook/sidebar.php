<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Tenscores_Book
 */

?>
	<aside id="secondary" class="widget-area" role="complementary">
   	 	 
   		 <nav id="site-navigation" class="table-of-content-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="toc" aria-expanded="false"><?php esc_html_e( 'Show Table Of Content', 'tenscoresbook' ); ?></button>
			<h2 class="table-of-content"><?php  _e( 'Table of Content', 'tenscoresbook'); ?></h2>
   		 	<?php wp_nav_menu( array( 'walker' => new Tenscores_TableOfContentMenu(), 'theme_location' => 'toc', 'menu_id' => 'toc' ) ); ?>
		</nav><!-- #site-navigation -->
 
   		
		<?php tenscoresbook_social_menu(); ?>
		

		<?php 
		if ( ! is_active_sidebar( 'sidebar-1' ) ) {
			dynamic_sidebar( 'sidebar-1' ); 
		}
		
		?>

	</aside>
