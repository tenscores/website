<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" class=" js flexbox canvas canvastext postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache">
  <meta charset="utf-8">
   <title>Pricing starting from $25/month - TenScores</title>
    <meta name="description" content="Plans ranging from $25 to $1000 to accomodated businesses of all sizes. Even yours.">
    <link href="http://tenscores.com/pricing.php" rel="canonical">
    <?php include 'files/includes/meta.php'; ?>
    <?php include 'files/includes/tags.php'; ?>
  </meta>
</head>
<body id="pricing">
  <?php include 'files/includes/nav.php'; ?>
  <article id="content">
    <div class="visual">
      <h1>Fall in love or your money back.</h1>
      <p>We stand behind the quality of our product. If you don't fall in love <br>in 30 days or less, you'll get a full refund.</p>
    </div>
    <div id="mc-container">
      <div class="w1">
        <div class="w2">
          <a class="no-hover" href="https://account.tenscores.com/register?lp=pricing">
          <div class="table-pricing slideup">
            <table>
              <tbody>
                <tr>
                  <td class="cell1">UNLIMITED</td>
                  <td>Unlimited<br> active keywords</td>
                  <td>Unlimited<br> Adwords accounts</td>
                  <td class="cell4"><span>$1,000</span>/month</td>
                </tr>
                <tr>
                  <td class="cell1">EXTRA LARGE</td>
                  <td>Up to 500,000<br> active keywords</td>
                  <td>Up to 50<br> Adwords accounts</td>
                  <td class="cell4"><span>$200</span>/month</td>
                </tr>
                <tr>
                  <td class="cell1">LARGE</td>
                  <td>Up to 200,000<br> active keywords</td>
                  <td>Up to 25<br> Adwords accounts</td>
                  <td class="cell4"><span>$100</span>/month</td>
                </tr>
                <tr>
                  <td class="cell1">MEDIUM</td>
                  <td>Up to 100,000<br> active keywords</td>
                  <td>Up to 10<br> Adwords accounts</td>
                  <td class="cell4"><span>$50</span>/month</td>
                </tr>
                <tr>
                  <td class="cell1">SMALL</td>
                  <td>Up to 50,000<br> active keywords</td>
                  <td>Up to 5<br> Adwords accounts</td>
                  <td class="cell4"><span>$25</span>/month</td>
                </tr>
              </tbody>
            </table>
            <span class="hr"></span>
          </div>
          </a>
          <div class="container" style="text-align:center; margin-bottom: 30px;">
           <p style="font-size:18px;"> Need a custom plan? Contact us: <br />
             <script src="//d2g9qbzl5h49rh.cloudfront.net/static/feedback2.js?3.3.9361" type="text/javascript">
                var JFL_51127875131856 = new JotformFeedback({
                formId:'51127875131856',
                base:'http://jotform.co/',
                windowTitle:'Contact Form',
                background:'#1C1303',
                fontColor:'#FFFFFF',
                type:false,
                height:300,
                width:300,
                openOnLoad:false
                });
                </script>
                <a class="lightbox-51127875131856" style="cursor:pointer;color:#55ACEE;text-decoration:underline;">support@tenscores.com</a>
           </p>
          </div>
          <span class="hr-small"></span>
          <div class="container">
            <a style="display:;" href="https://account.tenscores.com/register?lp=pricing"><img src="files/restructuring-tool.png" style="margin: -5px auto 50px auto; display:block;"></a>
            <span class="hr-small"></span>
            <a class="btn btn-in btn-danger" href="https://account.tenscores.com/register?lp=pricing&ft=1">Try it free for 14 days</a>
            <h3 class="title">Tenscores will automatically calculate the number of active keywords in your account(s) and show you the corresponding plan. <a class="blue" href="https://account.tenscores.com/register?lp=pricing&ft=1">Click here to get started.</a></h3>
          </div>


          <br>
          <br>
          <br>
        </div>
      </div>
    </div>
  </article>
    	<div id="press" style="display:none;">
		<div id="inner-press">
			<!--
			<img src="images/press/techvibes-50.gif" alt="Tech Vibes">
			<img src="images/press/lapresse-50.gif"  alt="La Presse">
			<img src="images/press/next-montreal-50.gif"  alt="Next Montreal">
			<img src="images/press/metro-50.gif"  alt="Metro">
		  -->
		  <span style="margin-top: 15px; display:inline-block;"><strong>Featured on</strong></span>
			<img src="files/sej-logo.png" width="232" height="50" alt="Search Engine Journal">
			<img src="files/searchengineland-50.gif" width="215" height="50" alt="Search Engine Land">
	    </div>
	</div>

  <?php include 'files/includes/footer.php'; ?>

</body>
</html>
