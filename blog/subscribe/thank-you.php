<?php
	$source = 'all';
	if( isset($_GET['from'] ) && $_GET['from'] != '' ) {
		$source = $_GET['from'];
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Thank you for subscribing!</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width">
		<meta name"description" content="Subscribe to Tenscores. ">
		<link media="all" rel="stylesheet" type="text/css" href="style.css" />
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-P6TPWD');</script>
		<!-- End Google Tag Manager -->
		<!-- Global site tag (gtag.js) - Google Ads: 743884132 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-743884132"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-743884132');
</script>

	</head>
	<body class="dark">
		<!-- Event snippet for Email subscribers (May 17) conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-743884132/a082CLHDpZ4BEOSK2-IC'});
</script>

	<div class="container thank-you">
		<div class="logo">
			<img src="images/logo-100.png"/>
		</div>
		<div class="content">


			<h1>Thank you</h1>
			<p>Please download our free gift below:</p>
			<p><a href="http://tenscores.com/blog/subscribe/Quality-Score-Flowchart-2019.pdf">
					How to increase Google Adwords quality score in 2019</a></p>
			<a class="button red" href="http://tenscores.com/blog/subscribe/Quality-Score-Flowchart-2019.pdf">Download PDF</a>
		 </div>
	</div>
	</body>
</html>
