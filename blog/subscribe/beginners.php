<!DOCTYPE html>
<html>
	<head>
		<title>Subscribe to Beginners Blog Posts - Tenscores</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width">
		<meta name"description" content="Subscirbe to Beginners Blog Posts.">
		<link media="all" rel="stylesheet" type="text/css" href="style.css" />
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-P6TPWD');</script>
		<!-- End Google Tag Manager -->
	</head>
	<body>
	<div class="container">
		<div class="logo">
			<img src="images/logo-square.png"/>
		</div>
		<div class="content">
			<span class="badge">Tenscores Blog</span>
			<h1>Beginners.</h1>
			<p>Learn how to optimize your campaigns on a daily basis — profitably.</p>
			<form method="post" class="" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
				  <div style="display: none;">
				  <input type="hidden" name="meta_web_form_id" value="1356231860" />
				  <input type="hidden" name="meta_split_id" value="" />
				  <input type="hidden" name="listname" value="qs-academy" />

				  <input type="hidden" name="redirect" value="http://www.tenscores.com/blog/subscribe/thank-you.php" id="redirect_e190445a9a24e280d9f22877b260fc83" />
				  <input type="hidden" name="meta_adtracking" value="beginners" />

				  <input type="hidden" name="meta_message" value="1" />
				  <input type="hidden" name="meta_required" value="email" />
				  <input type="hidden" name="meta_tooltip" value="" />
				</div>
				<input class="text"  type="text" name="email" value="" tabindex="500"  placeholder="Enter your email ..." />
				<button name="submit" class="" type="submit" value="subscribe" tabindex="501">Subscribe to Beginners Blog Posts</button>
				<div style="display: none;">
				  <img src="https://forms.aweber.com/form/displays.htm?id=jMysbEzMjBxsDA==" alt="" />
				</div>
			</form>
		 </div>
		 <div class="character">
		 	<img src="images/ppc-beginner-min.png"/>
		 </div>
	</div>
	</body>
</html>
