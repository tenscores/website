<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
			<div id="content" role="main">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="nav-above" class="navigation">
					<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'twentyten' ) . '</span> %title' ); ?></div>
					<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'twentyten' ) . '</span>' ); ?></div>
				</div><!-- #nav-above -->

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1 class="entry-title"><?php the_title(); ?></h1>
					 <?php
						$author_id = get_the_author_meta('ID');
						$avatar = get_avatar( $author_id, 60 );
						$display_name = get_the_author_meta( 'display_name' );
 						echo '<div class="post-header-author-meta">';
							if( $avatar ) {
				 			 echo $avatar;
							}
 							echo 'By ' . $display_name;
						echo '</div>';
					 ?>

					<?php
	                	$show_comment_share_container = false;
	                	if( function_exists('tenscores_upwork_share_html') ) {
	                		$show_comment_share_container = true;
	                	}

						$num_comments = get_comments_number();
						$comments_output = '';

						if ( $num_comments > 0 && comments_open() ) {
							if ( $num_comments > 1 ) {
								$comments = $num_comments . __(' Comments');
							} else {
								$comments = __('1 Comment');
							}
							$show_comment_share_container = true;
							$comments_output = '<li><a href="' . get_permalink() . '#disqus_thread">'. $comments.'</a></li>';
						}

					?>
					<?php
						if( $show_comment_share_container ) {
							?>
							<div class="comment-number">
								<ul class="comment-number-list">
									<?php echo $comments_output; ?>
									<?php
										if( function_exists('tenscores_upwork_share_html') ) {
											echo '<li>';
											tenscores_upwork_share_html();
											echo '</li>';
										}
									?>
								</ul>
							</div>
							<?php
						}
					?>
					<div class="entry-meta">
						<?php twentyten_posted_on(); ?>
					</div><!-- .entry-meta -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->

<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
					<div id="entry-author-info">
						<div id="author-avatar">
							<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyten_author_bio_avatar_size', 60 ) ); ?>
						</div><!-- #author-avatar -->
						<div id="author-description">
							<h2><?php printf( esc_attr__( 'About %s', 'twentyten' ), get_the_author() ); ?></h2>
							<?php the_author_meta( 'description' ); ?>
							<div id="author-link">
								<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
									<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentyten' ), get_the_author() ); ?>
								</a>
							</div><!-- #author-link	-->
						</div><!-- #author-description -->
					</div><!-- #entry-author-info -->
<?php endif; ?>

					<div class="entry-utility">
						<?php twentyten_posted_in(); ?>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-utility -->

					<div class="post-subscribe">
						<script src="//fast.wistia.com/embed/medias/1pqw7ictpm.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_1pqw7ictpm videoFoam=true" style="height:100%;width:100%">&nbsp;</div></div></div>						<a class="btn btn-danger btn-large btn-subscribe pull-right" style="margin-top:25px;" target="_blank" href="https://account.tenscores.com/register?ft=1&lp=blog-CTA">Start free trial</a>
						<div style="width:300px; margin-top: 20px; font-size:18px;">
						Track and optimize your Quality Scores for $25/month.
						</div>
					</div>

				</div><!-- #post-## -->

				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'twentyten' ) . '</span> %title' ); ?></div>
					<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'twentyten' ) . '</span>' ); ?></div>
				</div><!-- #nav-below -->

				<?php comments_template( '', true ); ?>
				<?php
				if( in_category( 'beginners' ) ) {
					tenscores_category_teaser( 'beginners' );
				} else {
					tenscores_category_teaser( 'advanced' );
				}
				?>
				<!-- Google Translate -->
				<div id="google_translate_element"></div>
				<script type="text/javascript">
				  function googleTranslateElementInit() {
				    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
				  }
				</script>
				<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
				<!-- end Google Translate -->
<?php endwhile; // end of the loop. ?>


			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
