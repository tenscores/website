<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
			<div id="content" role="main">

			<?php if( is_home() ) : ?>
					<h1 class="home-title">How to get results with Google Ads<span style="font-family:Georgia">.</span></h1>
					<p class="home-description">A blog about getting results from your Google Ads campaigns from their first impressions to squeezing all the traffic and conversions they can offer. Subscribe and get the 2019 flowchart to optimizing Google Adwords Quality Score.</p>

				<form action="https://www.getdrip.com/forms/321489648/submissions" method="post" data-drip-embedded-form="321489648">
					<input type="email" id="drip-email" name="fields[email]" value="" placeholder="Enter your email" />
					<input type="submit" value="Download flowchart" data-drip-attribute="sign-up-button" />
				</form>


					<div class="category-options">
						<div data-tslider="advanced" class="category-option category-option-first active">

							<?php
    							// Get the the ID of a given category
    							$category_id = get_cat_ID( 'Advanced' );

    							// Get the URL of this category
    							$category_link = get_category_link( $category_id );

    							/**
								 * The WordPress Query class.
								 * @link http://codex.wordpress.org/Function_Reference/WP_Query
								 *
								 */
								$category_args_advanced = array(

									//Category Parameters
									'cat'              => $category_id,

									//Type & Status Parameters
									'post_type'   => 'post',


									'post_status' => 'publish',


									//Pagination Parameters
									'posts_per_page'         => 100,

									//Parameters relating to caching
									'no_found_rows'          => true,
									'cache_results'          => true,
									'update_post_term_cache' => false,
									'update_post_meta_cache' => false,

								);


							?>

							<!-- Print a link to this category -->
							<a data-tslider="advanced" href="<?php echo esc_url( $category_link ); ?>" title="Advanced"><h2>Advanced</h2></a>
							<div data-tslider="advanced" class="category-option_description">
								<?php echo category_description( $category_id ); ?>
							</div>
						</div>
						<div data-tslider="beginner" class="category-option category-option-second">
							<?php
    							// Get the ID of a given category
    							$category_id = get_cat_ID( 'Beginners' );

    							// Get the URL of this category
    							$category_link = get_category_link( $category_id );

								/**
								 * The WordPress Query class.
								 * @link http://codex.wordpress.org/Function_Reference/WP_Query
								 *
								 */
								$category_args_beginner = array(

									//Category Parameters
									'cat'              => $category_id,

									//Type & Status Parameters
									'post_type'   => 'post',


									'post_status' => 'publish',


									//Pagination Parameters
									'posts_per_page'         => 100,

									//Parameters relating to caching
									'no_found_rows'          => true,
									'cache_results'          => true,
									'update_post_term_cache' => false,
									'update_post_meta_cache' => false,

								);


							?>

							<!-- Print a link to this category -->
							<a data-tslider="beginner" href="<?php echo esc_url( $category_link ); ?>" title="Beginner"><h2>Beginners</h2></a>
							<div data-tslider="beginner" class="category-option_description">
								<?php echo category_description( $category_id ); ?>
							</div>

						</div>
					</div>
			<?php
			 else :
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-index.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'index' );

			endif;
			?>
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
