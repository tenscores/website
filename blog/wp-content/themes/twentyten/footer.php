<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	</div><!-- #main -->

	<div id="footer" role="contentinfo">
		<div id="colophon">

<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>

			<div id="site-info">
				<a href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php bloginfo( 'name' ); ?>
				</a>
			</div><!-- #site-info -->

			<div id="site-generator">
				<?php do_action( 'twentyten_credits' ); ?>
				<a href="<?php echo esc_url( __('http://wordpress.org/', 'twentyten') ); ?>"
						title="<?php esc_attr_e('Semantic Personal Publishing Platform', 'twentyten'); ?>" rel="generator">
					<?php printf( __('Proudly powered by %s.', 'twentyten'), 'WordPress' ); ?>
				</a>
			</div><!-- #site-generator -->

		</div><!-- #colophon -->
	</div><!-- #footer -->

</div><!-- #wrapper -->
<?php if( is_home() ) : ?>

	<div class="blog-home-footer">
		<div id="wrapper">
			<div id="main">
				<div class="container-slider  active container-advanced">
					<?php

						// Get the the ID of a given category
						$category_id = get_cat_ID( 'Advanced' );

						// Get the URL of this category
						$category_link_advanced = get_category_link( $category_id );

						/**
						 * The WordPress Query class.
						 * @link http://codex.wordpress.org/Function_Reference/WP_Query
						 *
						 */
						$category_args_advanced = array(

							//Category Parameters
							'cat'              => $category_id,

							//Type & Status Parameters
							'post_type'   => 'post',


							'post_status' => 'publish',


							//Pagination Parameters
							'posts_per_page'         => 100,

							//Parameters relating to caching
							'no_found_rows'          => true,
							'cache_results'          => true,
							'update_post_term_cache' => false,
							'update_post_meta_cache' => false,

						);

						$query_advanced = new WP_Query( $category_args_advanced );

						if( $query_advanced->have_posts() ) {
							echo '<button type="button" class="advanced_home_slider_prev home_slider_prev"><i class="icon icon-angle-left"></i></button>';
							echo '<div id="advanced_home_slider" class="blog_home_slider">';
								while( $query_advanced->have_posts() ) {
									$query_advanced->the_post();
									$title = get_the_title();
									 $endOfFirstString = strpos( $title, '<' );
									$shrinked_title = substr( $title, 0, $endOfFirstString );
									echo '<div class="slick-slide"><a href="' . get_permalink() . '">' . tenscores_get_first_image_from_content( get_the_content() ) .  $shrinked_title . '</a></div>';
								}
								wp_reset_postdata();
							echo '</div>';
							echo '<button type="button" class="advanced_home_slider_next home_slider_next"><i class="icon icon-angle-right"></i></button>';
						}
					?>

				</div>

				<div class="container-slider container-beginner">
					<?php

						// Get the the ID of a given category
						$category_id = get_cat_ID( 'Beginners' );

						// Get the URL of this category
						$category_link_beginner = get_category_link( $category_id );

						/**
						 * The WordPress Query class.
						 * @link http://codex.wordpress.org/Function_Reference/WP_Query
						 *
						 */
						$category_args_advanced = array(

							//Category Parameters
							'cat'              => $category_id,

							//Type & Status Parameters
							'post_type'   => 'post',


							'post_status' => 'publish',


							//Pagination Parameters
							'posts_per_page'         => 100,

							//Parameters relating to caching
							'no_found_rows'          => true,
							'cache_results'          => true,
							'update_post_term_cache' => false,
							'update_post_meta_cache' => false,

						);

						$query_advanced = new WP_Query( $category_args_advanced );

						if( $query_advanced->have_posts() ) {
							echo '<button type="button" class="beginner_home_slider_prev home_slider_prev"><i class="icon icon-angle-left"></i></button>';
							echo '<div id="beginner_home_slider" class="blog_home_slider">';
								while( $query_advanced->have_posts() ) {
									$query_advanced->the_post();
									$title = get_the_title();
									 $endOfFirstString = strpos( $title, '<' );
									$shrinked_title = substr( $title, 0, $endOfFirstString );
									echo '<div class="slick-slide"><a href="' . get_permalink() . '">' . tenscores_get_first_image_from_content( get_the_content() ) .  $shrinked_title . '</a></div>';
								}
								wp_reset_postdata();
							echo '</div>';
							echo '<button type="button" class="beginner_home_slider_next home_slider_next"><i class="icon icon-angle-right"></i></button>';
						}
					?>
				</div>
				<br style="clear:both"/>
				<div id="ts_footer_home_cta" class="slider-text">
					<a href="<?php echo home_url( '/subscribe/advanced.php' ); ?>" data-beginner="<?php echo home_url( '/subscribe/beginners.php' );?>" data-advanced="<?php echo home_url( '/subscribe/advanced.php' ); ?>">Click here to be notified when new articles are added.</a>
					<div style="margin-top: 50px;">
					<!-- Google Translate -->
					<div id="google_translate_element"></div>
					<script type="text/javascript">
					  function googleTranslateElementInit() {
					    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
					  }
					</script>
					<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					<!-- end Google Translate -->
				</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>
