(function($){
	$(document).ready(function(){

		$("#advanced_home_slider").slick({
			slidesToShow: 6,
			variableWidth: true,
			infinite: false,
			prevArrow: $(".advanced_home_slider_prev"),
			nextArrow: $(".advanced_home_slider_next"),
			responsive: [
			    
			    {
			      breakpoint: 960,
			      settings: {
			        slidesToShow: 4,
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 2
			      }
			    }
			    // You can unslick at a given breakpoint now by adding:
			    // settings: "unslick"
			    // instead of a settings object
			  ]
		});
		$("#advanced_home_slider").slick( 'slickGoTo', 0 );

		$("#beginner_home_slider").slick({
			slidesToShow: 6,
			variableWidth: true,
			infinite: false,
			prevArrow: $(".beginner_home_slider_prev"),
			nextArrow: $(".beginner_home_slider_next"),
			responsive: [
			    
			    {
			      breakpoint: 960,
			      settings: {
			        slidesToShow: 4,
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 2
			      }
			    }
			    // You can unslick at a given breakpoint now by adding:
			    // settings: "unslick"
			    // instead of a settings object
			  ]
		}).slick( 'slickGoTo', 0 );
		$("#beginner_home_slider").slick( 'slickGoTo', 0 );

		$("[data-tslider]").on('click', function(e){
			e.preventDefault();
			$slider = $(this).attr("data-tslider");

			$('.category-option').removeClass('active');

			if( $(this).hasClass('category-option') ) {
				$(this).addClass('active');
			} else {
				$(this).parent().addClass('active');
			}


			if( $slider == 'beginner' ) {
				var link = $("#ts_footer_home_cta a").attr('data-beginner');
				$("#ts_footer_home_cta a").attr('href', link );
			} else {
				var link = $("#ts_footer_home_cta a").attr('data-advanced');
				$("#ts_footer_home_cta a").attr('href', link );
				 
			}

			$(".blog-home-footer .container-slider").removeClass('active');
			$(".blog-home-footer .container-" + $slider ).addClass('active');
		 
			
		});

	});
})(jQuery);