<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
			<div id="content" role="main">

			<?php if( is_home() ) : ?>
					<h1 class="home-title">Results for all<span style="font-family:Georgia">.</span></h1>
					<p class="home-description">Improving quality score beyond measure. Debunking best practices. Success for PPC beginners and experts. Welcome to our blog. </p>
					<div class="category-options">
						<div class="category-option">
							<div class="category-option_image">
								<a href="http://www.tenscores.com/blog/advanced/">
									<img src="http://www.tenscores.com/blog/wp-content/uploads/2016/08/ppc-expert1.png" width="236" />
								</a>
							</div>
							<?php
    							// Get the the ID of a given category
    							$category_id = get_cat_ID( 'Advanced' );

    							// Get the URL of this category
    							$category_link = get_category_link( $category_id );

    							/**
								 * The WordPress Query class.
								 * @link http://codex.wordpress.org/Function_Reference/WP_Query
								 *
								 */
								$category_args_advanced = array(
		
									//Category Parameters
									'cat'              => $category_id,
									 
									//Type & Status Parameters
									'post_type'   => 'post',
									 
									
									'post_status' => 'publish',
										 
									 
									//Pagination Parameters
									'posts_per_page'         => 100,
									 
									//Parameters relating to caching
									'no_found_rows'          => true,
									'cache_results'          => true,
									'update_post_term_cache' => false,
									'update_post_meta_cache' => false,
									
								);
    							
    							
							?>

							<!-- Print a link to this category -->
							<a href="<?php echo esc_url( $category_link ); ?>" class="category-link" title="Advanced">Advanced</a>
							<div class="category-option_description">
								<?php echo category_description( $category_id ); ?>
							</div>
							<a class="subscribe_button" href="http://www.tenscores.com/blog/subscribe/advanced.php">Subscribe</a>
							<?php 

								$query_advanced = new WP_Query( $category_args_advanced );

    							if( $query_advanced->have_posts() ) {
    								echo '<ul class="blog_home_articles">';
    									while( $query_advanced->have_posts() ) {
    										$query_advanced->the_post();
    										echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
    									}
    									wp_reset_postdata();
    								echo '</ul>';
    							}
    						?>
						</div>
						<div class="category-option">
							<div class="category-option_image">
								<a href="http://www.tenscores.com/blog/beginners/">
									<img src="http://www.tenscores.com/blog/wp-content/uploads/2016/09/adwords-beginner.png" width="130" />
								</a>
							</div>
							<?php
    							// Get the ID of a given category
    							$category_id = get_cat_ID( 'Beginners' );

    							// Get the URL of this category
    							$category_link = get_category_link( $category_id );

								/**
								 * The WordPress Query class.
								 * @link http://codex.wordpress.org/Function_Reference/WP_Query
								 *
								 */
								$category_args_beginner = array(
		
									//Category Parameters
									'cat'              => $category_id,
									 
									//Type & Status Parameters
									'post_type'   => 'post',
									 
									
									'post_status' => 'publish',
										 
									 
									//Pagination Parameters
									'posts_per_page'         => 100,
									 
									//Parameters relating to caching
									'no_found_rows'          => true,
									'cache_results'          => true,
									'update_post_term_cache' => false,
									'update_post_meta_cache' => false,
									
								);
    						
    							
							?>

							<!-- Print a link to this category -->
							<a href="<?php echo esc_url( $category_link ); ?>" class="category-link" title="Beginner">Beginner</a>
							<div class="category-option_description">
								<?php echo category_description( $category_id ); ?>
							</div>
							<a class="subscribe_button" href="http://www.tenscores.com/blog/subscribe/beginners.php">Subscribe</a>
							<?php 

								$query_beginner = new WP_Query( $category_args_beginner );

    							if( $query_beginner->have_posts() ) {
    								echo '<ul class="blog_home_articles">';
    									while( $query_beginner->have_posts() ) {
    										$query_beginner->the_post();
    										echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
    									}
    									wp_reset_postdata();
    								echo '</ul>';
    							}
    						?>
						</div>
					</div>
			<?php
			 else :
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-index.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'index' );

			endif;
			?>
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
