<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/twitter-bootstrap/2.2.1/css/bootstrap-combined.min.css" />
<link media="all" rel="stylesheet" href="http://tenscores.com/files/all.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<script src="https://code.jquery.com/jquery-1.8.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>

<!-- Push/Slide Menu -->
<link rel="stylesheet" type="text/css" href="<?php echo  get_stylesheet_directory_uri(); ?>/custom/css/slidebar.css" />
<script src="http://tenscores.com/blog/wp-content/themes/twentyten/custom/js/modernizr.custom.js"></script>
<!-- Classie - class helper functions by @desandro https://github.com/desandro/classie -->
<script src="http://tenscores.com/blog/wp-content/themes/twentyten/custom/js/classie.js"></script>
<!-- END Push/Slide Menu -->
<script src="https://use.fonticons.com/4098f5bc.js"></script>
<script>
	$(function ()
	{ $( '#showRightPush' ).tooltip();
	});
</script>
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P6TPWD');</script>
<!-- End Google Tag Manager -->

</head>

<body class="cbp-spmenu-push" <?php body_class(); ?>>
	<?php

		$published_articles = get_posts(array(
			'post_status' => 'publish',
			'posts_per_page' => -1, //get all
			'category_name' => 'beginners,advanced'
		));

	?>
	<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
		<a href="http://tenscores.com/blog" style="padding: 18px 10px;font-size: 1.5em;">Blog Home</a>
		<?php
			if( count( $published_articles ) > 0 ) {
				foreach ($published_articles as $article) {
					$category = '';
					if( in_category( 'beginners', $article ) ) {
						$category = '<span class="tenscores_category">' . __( 'Beginner' ) . '</span>';
					}

					if( in_category( 'advanced', $article ) ) {
						$category = '<span class="tenscores_category">' . __( 'Advanced' ) . '</span>';
					}

					echo '<a href="' . get_permalink( $article->ID ) .'">' . $article->post_title . $category . '</a>';
				}
			}
		?>


	</nav>

	<?php include '../files/includes/nav.php'; ?>

<?php if( ! is_single() ) { ?>

<div class="right-menu-control">
	<button id="showRightPush"
		class="btn"
		data-placement="left"
		data-toggle="tooltip"
		title="View all articles">
		<i class="icon icon-angle-left"></i>
	</button>
</div>
	<script>
		var menuRight = document.getElementById( 'cbp-spmenu-s2' ),
			showRightPush = document.getElementById( 'showRightPush' ),
			body = document.body;

		showRightPush.onclick = function() {
			classie.toggle( this, 'active' );
			classie.toggle( body, 'cbp-spmenu-push-toleft' );
			classie.toggle( menuRight, 'cbp-spmenu-open' );
			disableOther( 'showRightPush' );
		};

		function disableOther( button ) {
			if( button !== 'showRightPush' ) {
				classie.toggle( showRightPush, 'disabled' );
			}
		}
	</script>

<?php } else { ?>

	<div class="single-post-header">
		<a href="<?php echo home_url(); ?>">
			<img width="24" src="<?php echo get_template_directory_uri(); ?>/images/x.png" />
		</a>
	</div>
	<div class="right-menu-control">
		<button id="showRightPush"
			class="btn"
			data-placement="left"
			data-toggle="tooltip"
			title="View all articles">
			<i class="icon icon-angle-left"></i>
		</button>
	</div>
	<script>
		var menuRight = document.getElementById( 'cbp-spmenu-s2' ),
			showRightPush = document.getElementById( 'showRightPush' );

		showRightPush.onclick = function() {
			var body = document.body;
			classie.toggle( this, 'active' );
			classie.toggle( body, 'cbp-spmenu-push-toleft' );
			classie.toggle( menuRight, 'cbp-spmenu-open' );
			disableOther( 'showRightPush' );
		};

		function disableOther( button ) {
			if( button !== 'showRightPush' ) {
				classie.toggle( showRightPush, 'disabled' );
			}
		}
	</script>

<?php } ?>

<?php do_action( 'tenscores_after_header' ); ?>
<div id="wrapper" class="hfeed">

	<div id="main">
