��          �      ,      �  :   �     �     �     �     �     �       I   !     k     q     �     �  k   �     �  #        5  �  J  1   �  	     
   $     /     8     V  '   i  Z   �  	   �     �       	     y        �     �     �                 	            
                                                 1 revision has been deleted %s revisions have been deleted Delete Deleted Dismiss Donate to this plugin &#187; More b*web Plugins No revision to delete Note: You can only purge revisions for the posts you're allowed to delete Purge Purge revisions Purged Purging Revisions are deactivated on this site, the plugin "Simple Revisions Delete" has no reason to be installed. Something went wrong There is no revisions for this post You can't do this... Project-Id-Version: Simple Revisions Delete by b*web
Report-Msgid-Bugs-To: 
POT-Creation-Date: Wed Jun 03 2015 21:20:22 GMT+0200 (CEST)
PO-Revision-Date: Wed Jun 03 2015 21:24:10 GMT+0200 (CEST)
Last-Translator: admin <webmaster@friesebewakingsdienst.nl>
Language-Team: 
Language: Dutch
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: nl_NL
X-Generator: Loco - https://localise.biz/ 1 versie is verwijderd %s versies zijn verwijderd Verwijder Verwijderd Afbreken Doneer aan deze plugin &#187; Meer b*web plugins Er zijn geen versies om te verwijderen. Nottitie: U kunt alleen versies verwijderen van berichten die u ook zou mogen verwijderen. Schoon op Schoon versies op Opgeschoond Opschonen Versies staan niet aan op deze site. De plugin "Simple Revisions Delete" heeft dan ook geen nut en kan verwijderd worden. Er ging iets mis Dit bericht heeft geen versies. Dat kan niet..  