<?php

/**
 * Plugin Name: Tenscores Share
 * Description: Custom Sharing Solution, extendable.
 * Author: Igor Benic
 * Version: 1.0
 */

if( !defined('ABSPATH') ) {
	die('No Script Kiddies');
}

require_once 'inc/menu-builder.php';
require_once 'inc/metabox.php';
/**
 * Building the Menu
 * @var TenscoresMenu
 */
$tenscores_menu = new TenscoresMenu( array(
	'slug' => 'tenscores_menu',
	'title' => __( 'Tenscores', 'tenscores'),
	'desc' => 'Settings for Tenscores Upwork Plugin',
	'icon' => 'dashicons-welcome-widgets-menus',
	'position' => 99,
));

$tenscores_menu->add_field(array(
	'name' => 'facebook_app_id',
	'title' => 'Facebook App ID'
	));
$tenscores_menu->add_field(array(
	'name' => 'facebook_app_secret',
	'title' => 'Facebook App Secret' 
	));

/**
 * WordPress Cron Schedule
 * @param  array $schedules Registered Schedules
 * @return array            Customized Schedules
 */
function tenscores_upwork_every_six_hours( $schedules ) {
	 
	$schedules['tenscores_every_six_hours'] = array(
		'interval' => 21600, // 60 (seconds) * 60 (minutes) * 6 (hours)
		'display' => __('Every 6 hours', 'tenscores')
	);
	return $schedules;
}
add_filter( 'cron_schedules', 'tenscores_upwork_every_six_hours' );

register_activation_hook(__FILE__, 'tenscores_upwork_activation');
register_deactivation_hook( __FILE__, 'tenscores_upwork_deactivation' );

function tenscores_upwork_deactivation() { 
	
	$timestamp_scheduled =  wp_next_scheduled ( 'tenscores_upwork_check_share' );

	if( $timestamp_scheduled != false ) {
		wp_unschedule_event( $timestamp_scheduled, 'tenscores_upwork_check_share' );

	} 
}

function tenscores_upwork_activation() {  
    if (! wp_next_scheduled ( 'tenscores_upwork_check_share' )) {
		wp_schedule_event(time(), 'tenscores_every_six_hours', 'tenscores_upwork_check_share');
    }
}


function tenscores_upwork_get_shares_for_post( $post_id, $sharing_all_services ) {
	$permalink = get_permalink( $post_id );
	$share_counts = get_post_meta( $post_id, 'tenscores_share_count', true );
	
	if( ! is_array( $share_counts ) ){
		$share_counts = array();
	}

	if( count( $sharing_all_services ) > 0 ) {

		foreach ( $sharing_all_services as $key => $class ) {

			$previous_share_count = 0;
			if( isset( $share_counts[ $key ] ) ) {
				$previous_share_count = intval( $share_counts[ $key ] );
			}

			$share_count = $class::get_share_count( $permalink );

			if( $share_count ) {
				$share_counts[ $key ] = $share_count;
			} 
			
		}
	}
	
	update_post_meta( $post_id, 'tenscores_share_count', $share_counts );

}

add_action('tenscores_upwork_check_share', 'tenscores_upwork_check_share');

function tenscores_upwork_check_share() {

	$all_posts = get_posts( array( 'post_status' => 'publish', 'post_type' => 'post', 'posts_per_page' => -1 ) );
	
	if( $all_posts ){

		$sharing_all_services = apply_filters( 'tenscores_share_services', array() );
		 
		foreach ($all_posts as $post) {

			tenscores_upwork_get_shares_for_post( $post->ID, $sharing_all_services );
			 
		}
	}
}

/**
 * Boolean function that shows if we can show the shares or not
 * @return bool 
 */
function tenscores_upwork_can_show_share(){

	if( !is_single() && !is_category() && !is_archive() && !is_home() ){
		return false;
	}

	return true;

}

add_action( 'wp_enqueue_scripts', 'tenscores_upwork_scripts' );
function tenscores_upwork_scripts() {

	if( ! tenscores_upwork_can_show_share() ){
		return;
	}

	wp_enqueue_script( 'tenscores-sharrre', plugins_url( 'tenscores_upwork.min.js', __FILE__ ), array('jquery'), false, true );

}



/**
 * Getting Share count from the database
 * @param  integer $id ID of the post
 * @return integer      
 */
function tenscores_share_get_count( $shared_counts ) {
	 
	$share_count = 0;
	if( is_array( $shared_counts ) && count( $shared_counts ) > 0 ) {
		foreach ( $shared_counts as $key => $value ) {
			 $share_count += $value;
		}
	}
	return $share_count;
}

function tenscore_upwork_get_count_for( $key = '', $shared_counts = array() ) {
	if( $key == '' ){
		return 0;
	}

	if( is_array( $shared_counts ) && count( $shared_counts ) > 0 ) {
		if( isset( $shared_counts[ $key ] ) ) {
			return intval( $shared_counts[ $key ] );
		}
	}

	return 0;
}

function tenscores_upwork_share_html( $id = 0 ){
	if( ! tenscores_upwork_can_show_share() ){
		return;
	}

	if( $id == 0 ){
		$id = get_the_id();
	}

	if( $id == 0 ) {
		return;
	}

	$permalink = get_permalink( $id );

	$shared_counts = get_post_meta( $id, 'tenscores_share_count', true );

	$sharing_all_services = apply_filters( 'tenscores_share_services', array() );

	?>
	<div class="tenscores_upwork_share">
		<?php echo tenscores_share_get_count( $shared_counts ); ?>
		Shares
		<ul>
			<?php 
				foreach ($sharing_all_services as $key => $value) {
					echo '<li>';
						echo '<a class="tenscores_share_count" href="' . $permalink . '?tenscores_share=' . $key . '" target="_blank">';
					  		echo tenscore_upwork_get_count_for( $key, $shared_counts );
					  	echo '</a>';
					  echo '<a class="tenscores_share_link ' . $key . '" href="' . $permalink . '?tenscores_share=' . $key . '" target="_blank">';
					  	$value::render();
					  echo '</a>';
					echo '</li>';
				}
			?>
		</ul>
	</div>
	<?php
}

add_action( 'template_redirect', 'tenscores_share_redirect');

/**
 * Function to redirect on share
 * @return void 
 */
function tenscores_share_redirect() {
	if( isset($_GET['tenscores_share']) && $_GET['tenscores_share'] != '' ) {
		$sharing_service = $_GET['tenscores_share'];
		$sharing_all_services = apply_filters( 'tenscores_share_services', array() );

		if( isset( $sharing_all_services[ $sharing_service ] ) ) {
			$the_service = $sharing_all_services[ $sharing_service ];
			$the_service = new $the_service();
			$the_service->share();
		}
	}
}

add_filter( 'tenscores_share_services', 'tenscores_share_services' );
function tenscores_share_services( $services ){
	$services['twitter'] = 'TenScores_Share_Twitter';
	$services['facebook'] = 'TenScores_Share_Facebook';
	$services['linkedin'] = 'TenScores_Share_Linkedin';
	$services['google'] = 'TenScores_Share_Google';
	return $services;
} 

abstract class TenScores_Share {

	/**
	 * Slug to use in query strings
	 * @var string
	 */
	protected $slug = '';

	/**
	 * Post object
	 * @var mixed
	 */
	protected $post = null;

	/**
	 * Share URL
	 * @var string
	 */
	protected $share_url = '';

	/**
	 * Arguments to add
	 * @var array
	 */
	protected $args = array(); 

	/**
	 * Constructor Method
	 * Getting the Post
	 */
	public function __construct(){
		global $post;

		if( $post ) {
			$this->post = $post;
		}
	}

	public function prepare_title( $title ) {
		$new_title = $title;
		if( strpos( $new_title, '<br/>' ) !== false ) {
			$new_title = str_replace( '<br/>', ' - ', $new_title );
		}
		return strip_tags( $new_title );
	}

	/**
	 * Save each share attempt
	 * @return void 
	 */
	public function save_share(){
		$sharing_all_services = apply_filters( 'tenscores_share_services', array() );
		$shared_counts = get_post_meta( $this->post->ID, 'tenscores_share_count', true );
		if( ! is_array( $shared_counts ) ) {
			$shared_counts = array();
		}


		$count_for_current_service = 0;

		if( isset( $shared_counts[ $this->slug ] ) ) {
			$count_for_current_service = intval( $shared_counts[ $this->slug ] );
		}

		$count_for_current_service++;
		$shared_counts[ $this->slug ] = $count_for_current_service;
		
		update_post_meta( $this->post->ID, 'tenscores_share_count', $shared_counts );
	}

	public static function get_share_count( $link ){
		return false;
	}

	/**
	 * Redirect the visitor to the sharing link
	 * @return void 
	 */
	public function share() {

		$link = $this->get_link();

		if( $link ){
			$this->save_share();
			wp_redirect( $link );
			exit;	
		}
		
	}

	/**
	 * Get the link to share with all parameters
	 * @return string 
	 */
	public function get_link() {
		return '';
	}

	public static function render( $text = true ){

	}
}

class TenScores_Share_Twitter extends TenScores_Share {

	/**
	 * Slug to use in query strings, database etc
	 * @var string
	 */
	protected $slug = 'twitter';

	/**
	 * Share URL
	 * @var string
	 */
	protected $share_url = 'https://twitter.com/intent/tweet';

	/**
	 * Arguments to add
	 * @var array
	 */
	protected $args = array(
		'url' => '',
		'text' => '',
		'via' => 'tenscores'
	); 

	public function get_link() {
		$this->args['url'] = urlencode( get_permalink( $this->post ) );
		$this->args['text'] = urlencode( $this->prepare_title( $this->post->post_title ) );
		$url = add_query_arg( $this->args, $this->share_url);
		return $url;
	}

	public static function get_share_count( $url ) {
		$check_url = 'http://public.newsharecounts.com/count.json?url=' . urlencode( $url );
		
		$response = wp_remote_retrieve_body( wp_remote_get( $check_url ) );
		
		$encoded_response = json_decode( $response, true );
		
		$share_count = intval( $encoded_response['count'] ); 
		
		return $share_count;
	}

	public static function render( $text = true ){
		?>
		<svg version="1.1" id="twitter_svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="14px" height="14px" viewBox="0 0 430.117 430.117"
	 xml:space="preserve">
		<g>
			<path id="Twitter__x28_alt_x29_" d="M381.384,198.639c24.157-1.993,40.543-12.975,46.849-27.876
				c-8.714,5.353-35.764,11.189-50.703,5.631c-0.732-3.51-1.55-6.844-2.353-9.854c-11.383-41.798-50.357-75.472-91.194-71.404
				c3.304-1.334,6.655-2.576,9.996-3.691c4.495-1.61,30.868-5.901,26.715-15.21c-3.5-8.188-35.722,6.188-41.789,8.067
				c8.009-3.012,21.254-8.193,22.673-17.396c-12.27,1.683-24.315,7.484-33.622,15.919c3.36-3.617,5.909-8.025,6.45-12.769
				C241.68,90.963,222.563,133.113,207.092,174c-12.148-11.773-22.915-21.044-32.574-26.192
				c-27.097-14.531-59.496-29.692-110.355-48.572c-1.561,16.827,8.322,39.201,36.8,54.08c-6.17-0.826-17.453,1.017-26.477,3.178
				c3.675,19.277,15.677,35.159,48.169,42.839c-14.849,0.98-22.523,4.359-29.478,11.642c6.763,13.407,23.266,29.186,52.953,25.947
				c-33.006,14.226-13.458,40.571,13.399,36.642C113.713,320.887,41.479,317.409,0,277.828
				c108.299,147.572,343.716,87.274,378.799-54.866c26.285,0.224,41.737-9.105,51.318-19.39
				C414.973,206.142,393.023,203.486,381.384,198.639z"/>
		</g>
		</svg>
		<?php if( $text ) {
			echo 'Tweet';
		}
	}
}

class TenScores_Share_Facebook extends TenScores_Share {

 

	/**
	 * Slug to use in query strings, database etc
	 * @var string
	 */
	protected $slug = 'facebook';

	/**
	 * Share URL
	 * @var string
	 */
	protected $share_url = 'https://www.facebook.com/sharer/sharer.php';

	/**
	 * Arguments to add
	 * @var array
	 */
	protected $args = array(
		'u' => '',
		't' => ''
	); 

	public function get_link() {

		$this->args['u'] = urlencode( get_permalink( $this->post ) );
		$this->args['t'] = urlencode( $this->prepare_title( $this->post->post_title ) );
		$url = esc_url( add_query_arg( $this->args, $this->share_url ) );
		return $url;
	}

	public static function get_share_count( $link ) {
		 
		$options = get_option('tenscores_menu');
		
		$access_token = $options['facebook_app_id'] . '|' . $options['facebook_app_secret'];
		$check_url = 'https://graph.facebook.com/v2.7/?id=' . urlencode(  $link ) . '&fields=share,og_object{engagement,likes.limit(0)}&access_token=' . $access_token;

		
		$response = wp_remote_retrieve_body( wp_remote_get( $check_url ) );
		
		$encoded_response = json_decode( $response, true );
		
		$share_count = intval( $encoded_response['share']['share_count'] );
		
		return $share_count;
	}

	public static function render( $text = true ){
		?>
		<svg version="1.1" id="facebook_svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="14px" height="14px" viewBox="0 0 430.113 430.114"
	 xml:space="preserve">
		<g>
			<path id="Facebook" d="M158.081,83.3c0,10.839,0,59.218,0,59.218h-43.385v72.412h43.385v215.183h89.122V214.936h59.805
				c0,0,5.601-34.721,8.316-72.685c-7.784,0-67.784,0-67.784,0s0-42.127,0-49.511c0-7.4,9.717-17.354,19.321-17.354
				c9.586,0,29.818,0,48.557,0c0-9.859,0-43.924,0-75.385c-25.016,0-53.476,0-66.021,0C155.878-0.004,158.081,72.48,158.081,83.3z"/>
		</g>

		</svg>
		<?php if( $text ) {
			echo 'Share';
		}
	}
}

class TenScores_Share_Linkedin extends TenScores_Share {

	/**
	 * Slug to use in query strings, database etc
	 * @var string
	 */
	protected $slug = 'linkedin';

	/**
	 * Share URL
	 * @var string
	 */
	protected $share_url = 'https://www.linkedin.com/shareArticle?mini=true';

	/**
	 * Arguments to add
	 * @var array
	 */
	protected $args = array(
		'url' => '',
		'title' => ''
	); 

	public function get_link() {

		$this->args['url'] = get_permalink( $this->post ); 
		$this->args['title'] = urlencode( $this->prepare_title( $this->post->post_title ) );
		$url = $this->share_url . '&url=' . $this->args['url'] . '&title=' . $this->args['title'];
		return $url;
	}

	public static function get_share_count( $link ) {
		$remote_get = json_decode( file_get_contents('https://www.linkedin.com/countserv/count/share?url=' . urlencode( $link ) . '&format=json'), true);

		$share_count = $remote_get['count'];
		return $share_count;
	}

	public static function render( $text = true ){
		?>
		<svg version="1.1" id="linkedin_svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="14px" height="14px" viewBox="0 0 430.117 430.117" style="enable-background:new 0 0 430.117 430.117;"
	 xml:space="preserve">
		<g>
			<path id="LinkedIn" d="M430.117,261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707
				c-25.473,0-40.632,17.142-47.301,33.724c-2.432,5.928-3.058,14.179-3.058,22.477V420.56h-92.219c0,0,1.242-251.285,0-277.32h92.21
				v39.309c-0.187,0.294-0.43,0.611-0.606,0.896h0.606v-0.896c12.251-18.869,34.13-45.824,83.102-45.824
				C384.633,136.724,430.117,176.361,430.117,261.543z M52.183,9.558C20.635,9.558,0,30.251,0,57.463
				c0,26.619,20.038,47.94,50.959,47.94h0.616c32.159,0,52.159-21.317,52.159-47.94C103.128,30.251,83.734,9.558,52.183,9.558z
				 M5.477,420.56h92.184v-277.32H5.477V420.56z"/>
		</g>

		</svg>
		<?php if( $text ) {
			echo 'Share';
		}
		 
	}
}

class TenScores_Share_Google extends TenScores_Share {

	/**
	 * Slug to use in query strings, database etc
	 * @var string
	 */
	protected $slug = 'google';

	/**
	 * Share URL
	 * @var string
	 */
	protected $share_url = 'https://plus.google.com/share';

	/**
	 * Arguments to add
	 * @var array
	 */
	protected $args = array(
		'url' => ''
	); 

	public function get_link() {

		$this->args['url'] = urlencode( get_permalink( $this->post ) );
		$url = esc_url( add_query_arg( $this->args, $this->share_url ) );
		return $url;
	}

	public static function get_share_count( $url ) {
		
		if( !$url ) {
	    	return 0;
	    }
	    

		if ( !filter_var($url, FILTER_VALIDATE_URL) ){
			return 0;
		}

	    foreach (array('apis', 'plusone') as $host) {
	        $ch = curl_init(sprintf('https://%s.google.com/u/0/_/+1/fastbutton?url=%s',
	                                      $host, urlencode($url)));
	        curl_setopt_array($ch, array(
	            CURLOPT_FOLLOWLOCATION => 1,
	            CURLOPT_RETURNTRANSFER => 1,
	            CURLOPT_SSL_VERIFYPEER => 0,
	            CURLOPT_USERAGENT      => 'Mozilla/5.0 (Windows NT 6.1; WOW64) ' .
	                                      'AppleWebKit/537.36 (KHTML, like Gecko) ' .
	                                      'Chrome/32.0.1700.72 Safari/537.36' ));
	        $response = curl_exec($ch);
	        $curlinfo = curl_getinfo($ch);
	        curl_close($ch);

	        if (200 === $curlinfo['http_code'] && 0 < strlen($response)) { break 1; }
	        $response = 0;
	    }
	    
	    if( !$response ) {
	    		return 0;
	    }

	    preg_match_all('/window\.__SSR\s\=\s\{c:\s(\d+?)\./', $response, $match, PREG_SET_ORDER);
	    return (1 === sizeof($match) && 2 === sizeof($match[0])) ? intval($match[0][1]) : 0;
	 
	}

	public static function render( $text = true  ){
		?>
		<svg version="1.1" id="google" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="14px" height="14px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
		<g>
			<path d="M242.1,275.6l-18.2-13.7l-0.1-0.1c-5.8-4.6-10-8.3-10-14.7c0-7,5-11.8,10.9-17.4l0.5-0.4c20-15.2,44.7-34.3,44.7-74.6
				c0-26.9-11.9-44.7-23.3-57.7h13L320,64H186.5c-25.3,0-62.7,3.2-94.6,28.6l-0.1,0.3C70,110.9,57,137.4,57,163.5
				c0,21.2,8.7,42.2,23.9,57.4c21.4,21.6,48.3,26.1,67.1,26.1c1.5,0,3,0,4.5-0.1c-0.8,3-1.2,6.3-1.2,10.3c0,10.9,3.6,19.3,8.1,26.2
				c-24,1.9-58.1,6.5-84.9,22.3C35.1,328.4,32,361.7,32,371.3c0,38.2,35.7,76.8,115.5,76.8c91.6,0,139.5-49.8,139.5-99
				C287,312,264.2,293.5,242.1,275.6z M116.7,139.9c0-13.4,3-23.5,9.3-30.9c6.5-7.9,18.2-13.1,29-13.1c19.9,0,32.9,15,40.4,27.6
				c9.2,15.5,14.9,36.1,14.9,53.6c0,4.9,0,20-10.2,29.8c-7,6.7-18.7,11.4-28.6,11.4c-20.5,0-33.5-14.7-40.7-27
				C120.4,173.5,116.7,153.1,116.7,139.9z M237.8,368c0,27.4-25.2,44.5-65.8,44.5c-48.1,0-80.3-20.6-80.3-51.3
				c0-26.1,21.5-36.8,37.8-42.5c18.9-6.1,44.3-7.3,50.1-7.3c3.9,0,6.1,0,8.7,0.2C224.9,336.8,237.8,347.7,237.8,368z"/>
			<polygon points="402,142 402,64 368,64 368,142 288,142 288,176 368,176 368,257 402,257 402,176 480,176 480,142 	"/>
		</g>
		</svg>
		<?php if( $text ) {
			echo 'Share';
		}
	}
}