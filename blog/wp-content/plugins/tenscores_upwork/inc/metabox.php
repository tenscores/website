<?php

/**
 * Calls the class on the post edit screen.
 */
function tenscores_upwork_metabox() {
    new Tenscores_Sharing_Metabox();
}
 
if ( is_admin() ) {
    add_action( 'load-post.php',     'tenscores_upwork_metabox' );
    add_action( 'load-post-new.php', 'tenscores_upwork_metabox' );
}

add_action( 'admin_init', 'tenscores_upwork_refresh_share_count', 99  );
function tenscores_upwork_refresh_share_count(){
	if( isset( $_POST['get_tenscores_upwork_share_count'] ) ) {

        $post_id = $_POST['post_ID'];
        if( $post_id == 0 ) {
            return false;
        }
        if( isset( $_GET['action'] ) ) {
            unset( $_GET['action'] );
        }
        if( isset( $_GET['message'] ) ) {
            unset( $_GET['message'] );
        }
        $sharing_all_services = apply_filters( 'tenscores_share_services', array() );
	 	
	 	tenscores_upwork_get_shares_for_post( $post_id, $sharing_all_services );
		
        wp_redirect( admin_url( 'post.php?post=' . $post_id .'&action=edit' ), '301' );
       
        exit();
        
    }
}
/**
 * The Class.
 */
class Tenscores_Sharing_Metabox {
 
    /**
     * Hook into the appropriate actions when the class is constructed.
     */
    public function __construct() {
        add_action( 'add_meta_boxes', array( $this, 'add_meta_box' )  );
    }

 
 
    /**
     * Adds the meta box container.
     */
    public function add_meta_box( $post_type ) {
        // Limit meta box to certain post types.
        $post_types = array( 'post' );
 
        if ( in_array( $post_type, $post_types ) ) {
            add_meta_box(
                'tenscores_sharing_metabox',
                __( 'Sharing', 'tenscores' ),
                array( $this, 'render_meta_box_content' ),
                $post_type,
                'side',
                'high'
            );
        }
    }
 
    /**
     * Render Meta Box content.
     *
     * @param WP_Post $post The post object.
     */
    public function render_meta_box_content( $post ) {

 		$shared_counts = get_post_meta( $post->ID, 'tenscores_share_count', true );
 		
 		$sharing_all_services = apply_filters( 'tenscores_share_services', array() );
       
        ?>
        <style>
        	.tenscores_upwork_sharing li {
        		display:inline-block;
        		width: 25%;
        		text-align: center;
        	}

        	.tenscores_upwork_sharing li svg {
        		display: block;
        		margin: 0 auto 10px auto;

        	}
        </style>
      	<ul class="tenscores_upwork_sharing">
			<?php 
				foreach ($sharing_all_services as $key => $value) {
					echo '<li>';

						$value::render( false );

						echo tenscore_upwork_get_count_for( $key, $shared_counts );
					  	 
					   
					echo '</li>';
				}
			?>
		</ul>
		<button type="submit" class="button button-primary" name="get_tenscores_upwork_share_count" value="1">Refresh Counts</button>
        <?php
    }
}